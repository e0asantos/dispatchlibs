package com.cemex.rms.weborb.listener
{
	import com.cemex.rms.common.ProductionData;
	import com.cemex.rms.common.PushInfo;
	import com.cemex.rms.common.lcds.AbstractReceiverService;
	import com.cemex.rms.common.reflection.ReflectionHelper;
	
	import mx.messaging.ChannelSet;
	import mx.messaging.messages.IMessage;
	
	public class ProductionStatusPI extends AbstractReceiverService
	{
		public function ProductionStatusPI()
		{
			super();
		}
		
		public override function getDestinationName():String{
			return "ProductionStatusPI";
		}
		public override function getChannelSet():ChannelSet{
			return servicesContext.getValue("ChannelSetWEBORB") as ChannelSet;
		}
		protected override function receiveMessage(message:Object):void {
			if (message != null && message.hasOwnProperty("VBELN") ){
				
				//logger.info(ReflectionHelper.object2XML(message,"PlantOperativeModeLCDSEvent"));
				var productionData:ProductionData = DispatchLCDSDynamicObjectHelper.transformProductionData(message);

				logger.info(getDestinationName() + "->\t" 
					+"vbeln:"+ productionData.vbeln + "\t" 
					+"posnr:"+ productionData.posnr + "\t" 
					+"stonr:"+ productionData.stonr);
				//var event:ProductionStatusLCDSEvent =  new ProductionStatusLCDSEvent(productionData);
				//dispatcher.dispatchEvent(event);
				
			}
			
		}
		
		
	}
}