package com.cemex.rms.weborb.listener
{
	import com.cemex.rms.common.OrderLoad;
	import com.cemex.rms.common.PushInfo;
	import com.cemex.rms.common.lcds.AbstractReceiverService;
	import com.cemex.rms.common.reflection.ReflectionHelper;
	
	import mx.collections.ArrayCollection;
	import mx.messaging.ChannelSet;
	import mx.messaging.messages.IMessage;
	
	public class SalesOrderPI extends AbstractReceiverService
	{
		public function SalesOrderPI()
		{
			super();
		}
		
		public override function getDestinationName():String{
			return "SalesOrderPI";
		}
		public override function getChannelSet():ChannelSet{
			return servicesContext.getValue("ChannelSetWEBORB") as ChannelSet;
		}
		
		protected override function receiveMessage(message:Object):void {
			//logger.info(ReflectionHelper.object2XML(message,getDestinationName()));
			if (message != null && message.hasOwnProperty("orderNumber")){
				var datas:ArrayCollection =  DispatchLCDSDynamicObjectHelper.transformASOrders2Plain(message);
				
				for (var i:int = 0 ; i  < datas.length ; i++){
					var data:OrderLoad = datas.getItemAt(i) as OrderLoad;
					logger.info(getDestinationName() + "->\t" 
						+"vbeln:"+ data.orderNumber + "\t" 
						+"posnr:"+ data.itemNumber + "\t"
						+"iteCt:"+ data.itemCategory + "\t"
						+"oStat:"+ data.orderStatus + "\t"
						+"lStat:"+ data.loadStatus + "\t"
						
						+"equip:"+ data.equipNumber );
				}
				
			}
		}
		
	}
}