package com.cemex.rms.weborb.listener
{
	import com.cemex.rms.common.Equipment;
	import com.cemex.rms.common.PushInfo;
	import com.cemex.rms.common.lcds.AbstractReceiverService;
	
	import mx.messaging.ChannelSet;
	import mx.messaging.messages.IMessage;
	
	public class VehiclePI extends AbstractReceiverService
	{
		public function VehiclePI()
		{
			super();
		}
		
		public override function getDestinationName():String{
			return "VehiclePI";
		}
		public override function getChannelSet():ChannelSet{
			return servicesContext.getValue("ChannelSetWEBORB") as ChannelSet;
		}
		protected override function receiveMessage(message:Object):void {
			
			
			if (message != null && message["equiCatgry"] != null ){
				var data:Equipment = DispatchLCDSDynamicObjectHelper.transformVehicle(message);

				logger.info("\n"+getDestinationName() + "->\t" 
					+"equip:"+ data.equipNumber+ "\t" 
					+"statu:"+ data.status + "\t"
					+"equSt:"+ data.equipStatus + "\t" 
					+"eqCat:"+ data.equicatgry + "\t" 
					+"label:"+ data.equipLabel + "\t"
					);

				
			}
		}
	}
}