package com.cemex.rms.weborb.listener
{
	import com.cemex.rms.common.push.AbstractChannelSetService;
	import com.cemex.rms.common.push.PushConfigHelper;
	
	import mx.messaging.ChannelSet;

	public class ListenerChannelSetImpl extends AbstractChannelSetService
	{
		public function ListenerChannelSetImpl()
		{
			super();
			
			var lcds:Object ={
				
				
				LCDS_ENDPOINT:
				{
					
					//URL:"rtmp://localhost:1935/weborb",
					URL:"rtmp://mxoccrmsrid01.noam.cemexnet.com:1953/weborb",
					CHANNEL_NAME:"weborb-rtmp-messaging",
					CHANNEL_TYPE:"weborb"
				}
			};
			PushConfigHelper.initLcdsEndpoint(lcds);
		}
		
		protected override function setChannelSet(channelSet:ChannelSet):void {
			servicesContext.setValue("ChannelSetWEBORB",channelSet);
			
		} 
		
	}
}