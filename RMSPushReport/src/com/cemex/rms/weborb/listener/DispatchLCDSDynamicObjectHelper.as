package com.cemex.rms.weborb.listener
{
	import com.cemex.rms.common.AddProd;
	import com.cemex.rms.common.Equipment;
	import com.cemex.rms.common.OrderLoad;
	import com.cemex.rms.common.ProductionData;
	import com.cemex.rms.common.Text;
	import com.cemex.rms.common.date.FlexDateHelper;
	import com.cemex.rms.common.flashislands.MappingHelper;
	import com.cemex.rms.common.reflection.ReflectionHelper;
	
	import mx.collections.ArrayCollection;

	public class DispatchLCDSDynamicObjectHelper
	{
		public function DispatchLCDSDynamicObjectHelper(){
		}
		
		
		
		public static function transformProductionData(pd:Object):ProductionData{
			var newObject:ProductionData =  new ProductionData();
			ReflectionHelper.copySimpleParametersUpperAsCamel(pd,newObject);
			return newObject;
		}
		
		
		public static function transformVehicle(vehicle:Object):Equipment{
			var newObject:Equipment =  new Equipment();
			
			if (vehicle["turnDate"] && (""+vehicle["turnDate"])!= ""){
				vehicle["turnDate"] = FlexDateHelper.getDateFromString(vehicle["turnDate"]);
			}
			if (vehicle["arrivalDate"] && (""+vehicle["arrivalDate"])!= ""){
				vehicle["arrivalDate"] = FlexDateHelper.getDateFromString(vehicle["arrivalDate"]);
			}
			vehicle.maintplant = vehicle["maintPlant"];
			vehicle.equicatgry = vehicle["equiCatgry"];
			vehicle.statusLight = vehicle["statusLigth"];
			
			
			ReflectionHelper.copySimpleParametersAndCast(vehicle,newObject);
			return newObject;
		}
		public static function getAddProdAS(order:Object):ArrayCollection{
			
			//se va a borrar el orderNumber 
			var result:ArrayCollection = MappingHelper.doMappingSimple(order,"addprods", AddProd);
			return result;
		}
		
		public static function getTextsAS(order:Object):ArrayCollection{
			var result:ArrayCollection = MappingHelper.doMappingSimple(order,"texts", Text);
			return result;
		}
		
		public static function transformASOrders2Plain(order:Object):ArrayCollection {
			
			var datas:ArrayCollection =  new ArrayCollection();
			var jobsite:Object =  order["jobSite"];
			var constructionProduct:Object = order["cProducts"];
			var firstConcrete:Object =  order["firstConcrete"];
			
			var loads:ArrayCollection;
			if (order["loads"] is Array){
				loads = new ArrayCollection(order["loads"] as Array);
			}
			else {
				loads = order["loads"]; 
			}
			var addProd:ArrayCollection  = getAddProdAS(order);
			var texts:ArrayCollection  = getTextsAS(order);
			if (loads) {
				var plainLoad:OrderLoad = null;
				for (var j:int = 0 ; j < loads.length ; j ++ ){
					var load:Object = loads[j];
					if (load["loadingDate"]){
						load["loadingDate"] = FlexDateHelper.getDateFromString(load["loadingDate"]);
					}
					load["itemNumber"] = int(load["itemNumber"]);
					
					plainLoad =  transformAS2OrderLoad(order,load,jobsite,constructionProduct,firstConcrete);
					plainLoad.addProd = addProd;
					plainLoad.texts = texts;
					plainLoad.orderLoad = plainLoad.orderNumber + "-" + plainLoad.loadNumber;
					
					datas.addItem(plainLoad);
				}
			}
			return datas;
		}
		
		
		public static function transformASEquipment(vehicle:Object):Equipment {
			var equipment:Equipment =  new Equipment();
			ReflectionHelper.copySimpleParametersAndCast(vehicle,equipment);
			return equipment;
		}
		
		/// Metodos para obtener los Datos en Modo Plain
		public static function transformAS2OrderLoad(order:Object, load:Object,jobsite:Object,constructionProduct:Object,firstConcrete:Object):OrderLoad{
			
			var result:OrderLoad = new OrderLoad();
			
			ReflectionHelper.copySimpleParametersAndCast(order,result);
			ReflectionHelper.copySimpleParametersAndCast(load,result);
			ReflectionHelper.copySimpleParametersAndCast(jobsite,result);
			ReflectionHelper.copySimpleParametersAndCast(constructionProduct,result,"cp");
			ReflectionHelper.copySimpleParametersAndCast(firstConcrete,result,"first");
			
			return result;
		}
		
	}
}