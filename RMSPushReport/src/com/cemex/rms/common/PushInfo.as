package com.cemex.rms.common
{
	import com.cemex.rms.common.logging.ILogger;
	import com.cemex.rms.common.logging.LoggerFactory;
	import com.cemex.rms.common.reflection.ReflectionHelper;
	
	import flash.utils.ByteArray;
	
	import mx.messaging.messages.IMessage;

	public class PushInfo
	{
		public function PushInfo()
		{
		}
		public static var logger:ILogger = LoggerFactory.getLogger("PushInfo");
		public static function logObjectInfo(type:String,from:String,o:IMessage):void{
			
			var myBA:ByteArray = new ByteArray();
			myBA.writeObject(o);
			
			var myBA2:ByteArray = new ByteArray();
			myBA2.writeObject(o.body);
			logger.info(type+"-receiveIMessage from "+from+"(msg-size: "+myBA.length+",obj-size: "+myBA2.length+")" + "\n"
			+ReflectionHelper.object2XML(o.body,from)
			);
		}
	}
}