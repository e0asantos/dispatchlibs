package com.cemex.rms.common
{
	import com.cemex.rms.common.date.FlexDateHelper;
	import com.cemex.rms.common.reflection.ReflectionHelper;
	
	import mx.collections.ArrayCollection;

	public class OrderLoad 
	{	
		public function OrderLoad() {
		}
		
		// Load Data
		public var itemNumber:String="";
		public var loadNumber:String="";
		public var plant:String="";
		
		private var _loadingDate:Date;
		private var _loadingTime:String="";

		public var materialId:String="";
		public var materialDes:String="";
		
		public var loadVolume:Number;
		public var loadUom:String="";
		public var itemCurrency:String="";
		public var itemCategory:String="";
		public var confirmQty:Number;
		
		public var hlevelItem:Number;
		public var deliveryGroup:Number;
		
		/*
		Se borran los datos de  vehiculos
		//public var license:String="";
		//public var vehicleId:String="";
		//public var driver:String="";
		//public var truckVolume:String="";
		//public var equipLabel:String="";
		//public var vehicleType:String="";
		*/
		
		public var equipNumber:String="";
		public function get equipStatus():String{
			
			if (equipNumber != null && equipNumber != ""){
				return "A";
			}
			else {
				return "U";
			}	
		}
		
		public function set equipStatus(_equipStatus:String):void{}
		
		
		
		public var idHlevelItem:String="";
		private var _timePumpJobs:String="";
		
		public var remainFlag:Boolean;
		public var additionalFlag:Boolean;
		private var _cycleTime:String="";
		
		private var _unloadingTime:String="";
		private var _deliveryTime:String="";
		
		public var valueAdded:String="";
		public var constructionProduct:String="";
		public var mepTotcost:Number;
		public var totCost:Number;
		public var loadStatus:String="";

		public var timeFixIndicator:String="";
		public var loadFrequency:String="";
		public var deliveryAmount:String="";
		public var shipmentStatus:String="";
		public var completedDelayed:Boolean;
		
		
		
		
		
		
		//Jobsite Info
		public var jobSiteId:String="";
		public var jobSiteName:String="";
		public var houseNoStreet:String="";
		public var postalCode:String="";
		public var city:String="";
		
		
		
		// Order Info
		public var orderNumber:String="";
		public var soldToNumber:String="";
		public var soldToName:String="";
		public var contactName1:String="";
		public var contactName2:String="";
		public var contactTel:String="";
		public var orderStatus:String="";
		public var deliveryBlock:String="";
		public var shipConditions:String="";
		public var renegFlag:Boolean;
		
		public var orderUom:String="";
		public var orderVolume:Number;
		
		public var warning:String="";
		public var paytermLabel:String="";
		public var orderReason:String="";
		public var vehicleMaxVol:Number;
		
		
		//EXtras
		public var addProd:ArrayCollection;
		public var texts:ArrayCollection;
		
		
		// FirstConcrete
		public var firstOrderNumber:String="";
		public var firstDescription:String="";
		private var _firstDeliveryTime:String="";
		
		
		
		// ConstructionProduct
		public var cpOrderNumber:String="";
		public var cpDescription:String="";
		public var cpQuantity:Number;
		public var cpUom:String="";
		
		
		
		// customInformation	
		//public var totalVolumeService:String="";
		public var startTime:Date; 
		public var endTime:Date; 
		public var orderLoad:String="";
		private var _loadingTimestamp:Date;
		public var cycleTimeMillis:Number;
		
		public var isDelayed:Boolean;
		
		public function get loadingTimestamp():Date{
			if (_loadingTimestamp == null){
				if (_loadingTime != null && _loadingDate != null){
					_loadingTimestamp = FlexDateHelper.parseTime(_loadingDate,_loadingTime);
				}
			}
			return _loadingTimestamp;
		}
		public function set loadingTimestamp(_loadingTimestamp:Date):void{
			this._loadingTimestamp = _loadingTimestamp;
		}
		
		public function get loadingTime():String{
			return _loadingTime;
		}
		public function get loadingDate():Date{
			return _loadingDate;
		}
		public function set loadingTime(_loadingTime:String):void{
			this._loadingTime = FlexDateHelper.arrangeHour(_loadingTime);
			if (_loadingDate != null){
				_loadingTimestamp = FlexDateHelper.parseTime(_loadingDate,_loadingTime);
			}
		}
		public function set loadingDate(_loadingDate:Date):void{
			this._loadingDate = _loadingDate;
			if (_loadingTime != null){
				_loadingTimestamp = FlexDateHelper.parseTime(_loadingDate,_loadingTime);
			}
		}
		public function set cycleTime(_cycleTime:String):void {
			if (_cycleTime != null){
				this._cycleTime = FlexDateHelper.arrangeHour(_cycleTime);
				var split:Array = this._cycleTime.split(":");
				cycleTimeMillis = Number(split[0])*3600 + Number(split[1])*60 + Number(split[2]);
			}	
		}
		public function get cycleTime():String{
			return _cycleTime;
		}
		
		
		public function set unloadingTime(_unloadingTime:String):void {
			if (_unloadingTime != null){
				this._unloadingTime = FlexDateHelper.arrangeHour(_unloadingTime);
			}	
		}
		public function get unloadingTime():String{
			return _unloadingTime;
		}
		public function set deliveryTime(_deliveryTime:String):void {
			if (_deliveryTime != null){
				this._deliveryTime = FlexDateHelper.arrangeHour(_deliveryTime);
			}	
		}
		public function get deliveryTime():String{
			return _deliveryTime;
		}
		
		
		public function set firstDeliveryTime(_firstDeliveryTime:String):void {
			if (_firstDeliveryTime != null){
				this._firstDeliveryTime = FlexDateHelper.arrangeHour(_firstDeliveryTime);
			}	
		}
		public function get firstDeliveryTime():String{
			return _firstDeliveryTime;
		}
		public function set timePumpJobs(_timePumpJobs:String):void {
			if (_timePumpJobs != null){
				this._timePumpJobs = FlexDateHelper.arrangeHour(_timePumpJobs);
			}	
		}
		public function get timePumpJobs():String{
			return _timePumpJobs;
		}
		
		
		
		
		public function clone():OrderLoad{
			return ReflectionHelper.cloneObject(this) as OrderLoad;
		}
		
		public function equals(load:OrderLoad):Boolean {
			return ReflectionHelper.compareObject(this,load);
		}
	}
}