package com.cemex.rms.common
{
	import com.cemex.rms.common.date.FlexDateHelper;

	public class Equipment
	{
		public function Equipment()
		{
		}
		public var equipment:String;
		public var status:String;
		public var equicatgry:String;
		
		public var maintplant:String;
		public var vehicleType:String;
		public var fleetNum:String;
		
		public var licenseNum:String;
		
		public var driver:String;
		public var loadVol:String;
		public var volUnit:String;
		public var objnr:String;
		private var _turnDate:Date;
		private var _turnTime:String;
		private var _arrivalDate:Date;
		private var _arrivalTime:String;
		
		public var turnTimestamp:Date;
		public var arrivalTimestamp:Date;
		
		public var equipLabel:String;
		public var statusLight:String;
		
		
		
		public function get equipStatus():String{
			
			if (status == "ASSN"){
				return "A";
			}
			else {
				return "D"; 
			}
		}
		public function set equipStatus(_equipStatus:String):void{}
		public function get equipNumber():String{
			return equipment;
		}
		public function get license():String{
			return licenseNum;
		}
		
		public function set turnTime(_turnTime:String):void{
			this._turnTime = _turnTime;
			if (_turnDate != null){
				turnTimestamp = FlexDateHelper.parseTime(_turnDate,_turnTime);
			}
		}
		public function get turnTime():String{
			return _turnTime;
		}
		public function set turnDate(_turnDate:Date):void{
			this._turnDate = _turnDate;
			if (_turnTime != null){
				turnTimestamp = FlexDateHelper.parseTime(_turnDate,_turnTime);
			}
		}
		public function get turnDate():Date{
			return _turnDate;
		}
		
		
		
		public function set arrivalTime(_arrivalTime:String):void{
			this._arrivalTime = _arrivalTime;
			if (_arrivalDate != null){
				arrivalTimestamp = FlexDateHelper.parseTime(_arrivalDate,_arrivalTime);
			}
		}
		
		public function get arrivalTime():String{
			return _arrivalTime;
		}
		public function set arrivalDate(_arrivalDate:Date):void{
			this._arrivalDate = _arrivalDate;
			if (_arrivalTime != null){
				arrivalTimestamp = FlexDateHelper.parseTime(_arrivalDate,_arrivalTime);
			}
		}
		
		public function get arrivalDate():Date{
			return _arrivalDate;
		}
		
		
		
		
		
		
		
		
	}
}