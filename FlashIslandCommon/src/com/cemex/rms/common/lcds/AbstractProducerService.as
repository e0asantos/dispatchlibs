package com.cemex.rms.common.lcds
{
	import com.cemex.rms.common.push.AbstractMessagingService;
	import com.cemex.rms.common.push.PushConfigHelper;
	
	import mx.messaging.ChannelSet;
	import mx.messaging.MessageAgent;
	import mx.messaging.Producer;
	import mx.messaging.events.ChannelEvent;
	import mx.messaging.events.ChannelFaultEvent;
	import mx.messaging.events.MessageAckEvent;
	import mx.messaging.events.MessageEvent;
	import mx.messaging.events.MessageFaultEvent;
	import mx.messaging.messages.AsyncMessage;
	import mx.messaging.messages.IMessage;
	
	import weborb.messaging.WeborbProducer;
	
	/**
	 * Esta clase provee de una serie de metodos que permiten que se haga una implementacion
	 * de algun mensajero de una forma mas fácil, simplemente por el hecho de extender esta clase
	 * 
	 * dado que tiene ya la interaccion de unica interface para el cargado de los servicios.
	 * 
	 * 
	 */
	public class AbstractProducerService extends AbstractMessagingService
	{
		public function AbstractProducerService()
		{
			super();
		}
		
		
		private var producer:MessageAgent;
		
		/**
		 * Este metodo desconecta el servicio y manda a llamar el metodo
		 * startService , adema sde despachar un evento de que esta conectado
		 * 
		 */
		public function reconnect():void {
			trace("RECONNECT:",this);
			if (producer != null){
				producer.disconnect();
				
			}
			startService();
			dispatchServiceConnected();
			
		}
		
		private var type:String;
		//servicesContext.getValue("ChannelSet") as ChannelSet;
		
		/**
		 * Este metodo startService depende de que la configuracion se encuentre setteada en el siguiente valor
		 * PushConfigHelper.getFirstChannelSetConfig().channelType
		 * esto se settea con PushConfigHelper.initLcdsEndpoint 
		 * esto se necesita en caso de tener WebOrb porque el tipo de canal es diferente y la forma de procesar los mensajes
		 * 
		 * 
		 * Se settean listeners de los eventos MessageAckEvent.ACKNOWLEDGE, MessageFaultEvent.FAULT, ChannelEvent.CONNECT, ChannelEvent.DISCONNECT, ChannelFaultEvent.FAULT 
		 */
		public override function startService():void {
			trace("START SERVICE ",this,new Date());
			type = PushConfigHelper.getFirstChannelSetConfig().channelType;
			var channelSet:ChannelSet = getChannelSet();
			
			if (channelSet == null){
				dispatchServiceLoadError("Cannot Start ["+getServiceName()+ "] because the ChannelSet is not loaded in the Conext" );
			}
			
			producer =  new Producer();
			producer.channelSet = channelSet;
			producer.destination = getDestinationName();
			
			
			producer.addEventListener(MessageAckEvent.ACKNOWLEDGE,messageAcknowledge,false,0,true);
			producer.addEventListener(MessageFaultEvent.FAULT, faultHandlerMessage,false,0,true);
			
			
			
			producer.addEventListener(ChannelEvent.CONNECT,channelConnect,false,0,true);
			producer.addEventListener(ChannelEvent.DISCONNECT,channelDisconect,false,0,true);
			producer.addEventListener(ChannelFaultEvent.FAULT,channelFault,false,0,true);
				
		}
		
		/**
		 * Esta implementacion manda los mensajes tanto en producers estandard de Flex o en 
		 * ProducerWebOrb crea un mensaje Asyncrono 
		 * 
		 */
		public function sendMessage(message:Object):void {
			var aMessage:AsyncMessage = new AsyncMessage();
			aMessage.body = message;
			if (type == "weborb"){
				(producer as Producer).send(aMessage);
			}
			else {
				(producer as WeborbProducer).send(aMessage);
			}
			
		}      
		
		/**
		 * Esta funcion se manda a llamar cuando llega un acknowledge
		 * 
		 */
		private function messageAcknowledge(e:MessageAckEvent):void{
			logger.info("messageAcknowledge("+e+")");
		}
		
		/**
		 * Esta funcion se manda a llamar cada vez que llega un evento Fault al canal
		 */
		private function faultHandlerMessage(e:MessageFaultEvent):void{
			logger.info("consumerFaultHandlerMessage("+e+")");
		}
		
		
		
		
	}
}