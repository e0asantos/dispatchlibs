package com.cemex.rms.common.lcds
{
	import com.cemex.rms.common.logging.events.LoggerEvent;
	import com.cemex.rms.common.push.AbstractMessagingService;
	import com.cemex.rms.common.push.PushConfigHelper;
	import com.cemex.rms.common.services.AbstractService;
	
	import mx.collections.ArrayCollection;
	import mx.messaging.ChannelSet;
	import mx.messaging.Consumer;
	import mx.messaging.MessageAgent;
	import mx.messaging.events.ChannelEvent;
	import mx.messaging.events.ChannelFaultEvent;
	import mx.messaging.events.MessageAckEvent;
	import mx.messaging.events.MessageEvent;
	import mx.messaging.events.MessageFaultEvent;
	import mx.messaging.messages.AsyncMessage;
	import mx.messaging.messages.IMessage;
	
	import weborb.messaging.WeborbConsumer;
	import weborb.messaging.WeborbMessagingChannel;

	/**
	 * Esta clase nos provee de metodos que nos permiten crear los agentes de mensjaes de 
	 * LCDS o WebOrb de tal forma que permite que sea configurable estas conexiones
	 */
	public class AbstractReceiverService extends AbstractMessagingService
	{
		public function AbstractReceiverService()
		{
			super();
		}
		
		public static var testModeString:String;
		public static var isTestMode:Boolean=false;
		private var consumer:MessageAgent;
		
		/**
		 * Este metodo desconecta el servicio y manda a llamar el metodo
		 * startService , adema sde despachar un evento de que esta conectado
		 * 
		 */
		public function reconnect():void {
			if (consumer != null){
				consumer.disconnect();
				consumer=null;
				trace("::DESCONNECTING");
			}
			/*if(channelSet!=null){
				channelSet.removeChannel(privateChannel);
				//channelSet=null;
			}	
			privateChannel=null;*/
			startService();
			/*dispatchServiceConnected();*/
		}
		protected var type:String;
		/**
		 * Este metodo startService depende de que la configuracion se encuentre setteada en el siguiente valor
		 * PushConfigHelper.getFirstChannelSetConfig().channelType
		 * esto se settea con PushConfigHelper.initLcdsEndpoint 
		 * esto se necesita en caso de tener WebOrb porque el tipo de canal es diferente y la forma de procesar los mensajes cambia un poco
		 * ademas que se necesita crear objetos diferentes 
		 * 
		 * 
		 * Se settean listeners de los eventos 
		 * MessageAckEvent.ACKNOWLEDGE, MessageFaultEvent.FAULT, MessageEvent.MESSAGE  
		 * ChannelEvent.CONNECT, ChannelEvent.DISCONNECT, ChannelFaultEvent.FAULT,
		 * 
		 */
		public static var SQL_SELECTOR:String=null;
		public static var RTMP_URL:String=null;
		public static var channelSet:ChannelSet=null;
		public static var privateChannel:WeborbMessagingChannel
		public function saveRemove():void{
			if(channelSet!=null){
				//channelSet.removeEventListener(MessageEvent.MESSAGE, weborbMessageHandler);
				//channelSet=null;
				privateChannel=null;
				channelSet.channels=[];
				//channelSet=null;
			}
		}
		public override function startService():void {
			
			//var channelSet:ChannelSet = getChannelSet();
			if(channelSet==null){
				channelSet=new ChannelSet();				
			}
			if(privateChannel==null){
				privateChannel=new WeborbMessagingChannel("rtmp",RTMP_URL);
			}
			privateChannel.disablePolling()
			if(channelSet.channels.length==0){
				channelSet.addChannel(privateChannel);
			}
			if (channelSet == null){
				dispatchServiceLoadError("Cannot Start ["+getServiceName()+ "] because the ChannelSet is not loaded in the Conext" );
			}
			type = PushConfigHelper.getFirstChannelSetConfig().channelType;
			if (type == "weborb"){
				consumer = new WeborbConsumer();
			}
			else {
				consumer = new Consumer();
			}
			consumer.destination = getDestinationName();
			if(getDestinationName().toString().toLowerCase().indexOf("vehicle")!=-1 || getDestinationName().toString().toLowerCase().indexOf("sales")!=-1 || getDestinationName().indexOf("ProductionStatusPI")!=-1  || getDestinationName().indexOf("PlantPI")!=-1  || getDestinationName().indexOf("PoConfirmationPI")!=-1){
				consumer["selector"]=SQL_SELECTOR;
			}
			consumer.channelSet = channelSet;
			
			
			
			if (type == "weborb"){
				//var channel:WeborbMessagingChannel = servicesContext.getValue("WEBORB_CHANNEL") as WeborbMessagingChannel;
				channelSet.addEventListener(MessageEvent.MESSAGE, weborbMessageHandler,false,0,true);
			}
			else {
				consumer.addEventListener(MessageEvent.MESSAGE, messageHandler,false,0,true);
			}
			
			consumer.addEventListener(MessageAckEvent.ACKNOWLEDGE,messageAcknowledge,false,0,true);
			consumer.addEventListener(MessageFaultEvent.FAULT, faultHandlerMessage,false,0,true);
			
			consumer.addEventListener(ChannelEvent.CONNECT,channelConnect,false,0,true);
			consumer.addEventListener(ChannelEvent.DISCONNECT,channelDisconect2,false,0,true);
			consumer.addEventListener(ChannelFaultEvent.FAULT,channelFault,false,0,true);
			
			if (type == "weborb"){
				/*var listaPlantas:ArrayCollection=GanttServiceReference.getPlantsPlain();
				listaPlantas.length;*/
				(consumer as WeborbConsumer).subscribe();
			}
			else {
				(consumer as Consumer).subscribe();
			}
			
				
		}
		public function channelDisconect2(e:ChannelEvent):void {
			
			trace("first connection");
			consumer.removeEventListener(MessageAckEvent.ACKNOWLEDGE,messageAcknowledge);
			consumer.removeEventListener(MessageFaultEvent.FAULT, faultHandlerMessage);
			
			consumer.removeEventListener(ChannelEvent.CONNECT,channelConnect);
			consumer.removeEventListener(ChannelEvent.DISCONNECT,channelDisconect2);
			consumer.removeEventListener(ChannelFaultEvent.FAULT,channelFault);
			//startService();
			this.channelDisconect(e);
		} 
		/**
		 * Este metodo es el que recibe los mensjaes de webOrb y los entrega bajo el mismo esquema que el LCDS
		 * pasando por isMessageNeeded, y receiveIMessage 
		 */
		private function weborbMessageHandler(e:MessageEvent):void {
			
			var msg:AsyncMessage= AsyncMessage(e.message) ;
			
			if (msg.destination == getDestinationName()){
				
				if (isMessageNeeded(e.message)){
					receiveIMessage(e.message);
				}
			}
		} 
		/**
		 * Se regresa el Id del cliente 
		 * 
		 */
		public function getClientId():String{
			return consumer.clientId;
		}
		
		/**
		 * ESta funcion se manda a llamar en caso que el canal envie un mensaje de acknowledge
		 */
		private function messageAcknowledge(e:MessageAckEvent):void{
			trace(type+"-messageAcknowledge("+e+")");
			logger.info(type+"-messageAcknowledge("+e+")");
		}
		/**
		 * ESta funcion se manda a llamar en caso que el canal reciba un evento Fault
		 */
		private function faultHandlerMessage(e:MessageFaultEvent):void{
			trace(type+"-faultHandlerMessage("+e+")");
			logger.info(type+"-faultHandlerMessage("+e+")");
			dispatchServiceFault();
		}
		
		/**
		 * Se valida si isMessageNeeded, y receiveIMessage
		 * 
		 */
		private function messageHandler(e:MessageEvent):void { 
			if (isMessageNeeded(e.message)){
				receiveIMessage(e.message);
			}
		} 
		
		/**
		 * Esta funcion se utiliza para validar si el mensaje se va a utilizar 
		 */
		protected function isMessageNeeded(message:IMessage):Boolean{
			return true;		
		}
		
		/**
		 * This function is implemented in case is needed to manage something about the 
		 * message  
		 */
		private var mensajes:Array=[];
		
		public static var initialDatePush:Date=null;
		public static var endDatePush:Date=null;
		protected function receiveIMessage(message:IMessage):void {
			initialDatePush=new Date();
			//ver si no estamos en modo de prueba
			//trace(message);
			//trace("PUSH ARRIVING______"+(new Date()));
			var evento:LoggerEvent=new LoggerEvent(LoggerEvent.LOGGER_EVENT,message.destination);
			this.dispatcher.dispatchEvent(evento);
			//trace(message.headers);
			if(message.headers.hasOwnProperty("testMode") && AbstractReceiverService.isTestMode){
				if(message.headers["testMode"]!=AbstractReceiverService.testModeString){
					return;
				}
			}
			if(message.headers.hasOwnProperty("JMS_SAP_SystemTxId")){
				if(mensajes.indexOf(message.headers["JMS_SAP_SystemTxId"])!=-1){
					return;
				} else {
					mensajes.push(message.headers["JMS_SAP_SystemTxId"]);
				}
			}
			var obj:Object = message.body;
			
			//Alert.show("obj:"+obj);
			if (message.body is Array){
				//Alert.show("obj-Before:"+obj);
				obj = message.body[0];
				//Alert.show("obj-After:"+obj);
			}
			try{
				obj.SAPStamp=new Object();
				obj.SAPStamp=message.timestamp;
			} catch(e){
				
			}
			receiveMessage(obj);
		}
		
		protected function receiveMessage(message:Object):void {
			throw new Error("the function receiveMessage should be Overriden");
		}
		
		
	}
}