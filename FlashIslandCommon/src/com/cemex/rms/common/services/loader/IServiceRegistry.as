package com.cemex.rms.common.services.loader
{
	import com.cemex.rms.common.services.IService;
	
	import mx.collections.ArrayCollection;
	
	/**
	 * Esta clase tiene como objetivo almacenar todos los servicios, 
	 * es como un hashmap de servicios
	 */
	public interface IServiceRegistry
	{
		
		/**
		 * Regresa todos los nombres de los servicios que estan cargados
		 */
		function serviceNames():ArrayCollection;
		
		/**
		 * Se agrega un servicio
		 */
		function put(service:IService):void;
		
		
		/**
		 * Regresa el servicio que cumpla con el Id que se recibe
		 */
		function get(service:String):IService;
		/**
		 * elimina el servicio que se manda como parametro
		 */
		function remove(service:String):void;
		
		/**
		 * Regresa verdadero si el servicio ya ha sido registrado
		 */
		function hasService(service:String):Boolean;
		
		
		
		
	}
}