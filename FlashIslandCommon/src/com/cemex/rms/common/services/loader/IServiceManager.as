package com.cemex.rms.common.services.loader
{
	import com.cemex.rms.common.logging.ILogger;
	import com.cemex.rms.common.services.IService;
	import com.cemex.rms.common.services.config.IServiceModel;
	import com.cemex.rms.common.services.config.ServiceConfig;
	
	import flash.events.IEventDispatcher;
	
	import mx.messaging.config.ConfigMap;
	
	/**
	 * Esta clase tiene como objetivo mandar a cargar todos los servicios
	 */
	public interface IServiceManager {
		
		
		/**
		 * Este es el modelo de servicios que va a cargar el manager
		 */
		function setServiceModel (serviceModel:IServiceModel):void;
		
		
		/**
		 * Este metodo empiez a cargar todos los servicios
		 */
		function startLoadingServices():void ;
		
		/**
		 * Este es el registro de los servicios
		 */
		function get registry():IServiceRegistry;
		
		/**
		 * Este es el setter de los registros de los servicios
		 */
		function set registry(_registry:IServiceRegistry):void;
		
		/**
		 * Regresa el dispatcher  
		 */
		function get dispatcher():IEventDispatcher;
	}
}