package com.cemex.rms.common.services.loader
{
	import com.cemex.rms.common.logging.ILogger;
	import com.cemex.rms.common.logging.LoggerFactory;
	import com.cemex.rms.common.logging.impl.EventLogger;
	import com.cemex.rms.common.services.AbstractService;
	import com.cemex.rms.common.services.IService;
	import com.cemex.rms.common.services.config.IServiceModel;
	import com.cemex.rms.common.services.config.ServiceConfig;
	import com.cemex.rms.common.services.events.ServiceErrorEvent;
	import com.cemex.rms.common.services.events.ServiceEvent;
	import com.cemex.rms.common.services.events.ServiceManagerEvent;
	import com.cemex.rms.common.utils.Context;
	import com.cemex.rms.common.utils.IContext;
	//import com.cemex.rms.dispatcher.helpers.GanttServiceReference;
	
	import flash.events.IEventDispatcher;
	import flash.profiler.showRedrawRegions;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	
	/**
	 * Esta clase tiene como objetivo mandar a cargar todos los servicios
	 */
	public class ServiceManager implements IServiceManager
	{
		
		private static var logger:ILogger = LoggerFactory.getLogger("ServiceManager");
		
		
		private var _dispatcher:IEventDispatcher;
		
		public function get dispatcher():IEventDispatcher{
			return _dispatcher;
		}
		public function set dispatcher(_dispatcher:IEventDispatcher):void{
			this._dispatcher = _dispatcher;
		}
		private var serviceModel:IServiceModel;
		private var servicesContext:IContext;
		
		
		private var _registry:IServiceRegistry;
		public function get registry():IServiceRegistry{
			return _registry;
		}
		public function set registry(_registry:IServiceRegistry):void{
			this._registry = _registry;
		}
		
		// INITIALIZATION PHASE
		/**
		 * En esta clase se orquestan todos los servicios que se 
		 * tienen en un modelo 
		 * e el constructor se inicializan todos los listener de eventos que 
		 * puedan haber en el despachador
		 * 
		 * 
		 */
		public function ServiceManager(_dispatcher:IEventDispatcher) {
			
			this.dispatcher = _dispatcher;
			EventLogger.setDispatcherIfEventLogger(logger,_dispatcher);
			dispatcher.addEventListener(ServiceEvent.SERVICE_LOAD_READY,serviceLoadReady,false,0,true);
			dispatcher.addEventListener(ServiceErrorEvent.SERVICE_LOAD_ERROR,serviceLoadError,false,0,true);
			
			dispatcher.addEventListener(ServiceEvent.SERVICE_REQUEST_READY,serviceRequestReady,false,0,true);
			dispatcher.addEventListener(ServiceErrorEvent.SERVICE_REQUEST_ERROR,serviceRequestError,false,0,true);
			
			dispatcher.addEventListener(ServiceManagerEvent.ALL_SERVICES_READY,allServicesReady,false,0,true);
			dispatcher.addEventListener(ServiceManagerEvent.SERVICES_LOAD_REQUEST,servicesLoadRequest,false,0,true);
			
		}
		
		// SETUP PHASE
		/**
		 * se Settea el modelo de lose servicios que se van a cargar
		 */
		public function setServiceModel(serviceModel:IServiceModel):void {
			this.serviceModel = serviceModel;
			this.servicesContext = new Context();
		}
		
		
		
		
		
		// LOADING PHASE
		/**
		 * SEComienza a cargar todos los servicio que no tengan dependencia de ningun otro servicio,
		 * No se revisa que existan dependencias que no existan , por lo que si algun servicio
		 * depende de algun tipo inexistente, ese servicio no se cargará
		 */
		public function startLoadingServices():void {
			if (serviceModel == null){
				throw new Error("The Service Model Should be setup before starting loading the services");
			}
			// Se mandan inicializar todos los Servicios que no tengan dependendia en ningun otro servicio.
			loadServicesFromDependency(null);
		}
		
		/**
		 * Este metodo manda a inicializar o crear todos los servicios de cierta dependencia
		 * @param serviceDependency:String es el tipo de dependecia que se debe inicializar
		 */
		protected function loadServicesFromDependency(serviceDependency:String):void {
			
			
			var services:Array = serviceModel.getAllServicesFromDependency(serviceDependency);
			var i:int;
			for ( i = 0 ; i < services.length ; i ++ ) {
				
				var serviceStatus:ServiceLoadStatus = services[i] as ServiceLoadStatus;
				var config:ServiceConfig = serviceStatus.config;
				
				serviceStatus.status = "Instantialing";
				var service:IService = createService(config);
				serviceStatus.status = "Instantiated";
				if (_registry != null) {
					_registry.put(service);
					logger.debug("Service["+service.getServiceName()+"] added to the Registry");
				}
				serviceStatus.status = "Starting...";
				service.startService();
				/*if(serviceDependency=="Channels"){
					var listaPlantas:ArrayCollection=GanttServiceReference.getPlantsPlain();
					listaPlantas.length;
					
				}*/
			
			}
			
		}
		
		/**
		 * En base a la configuracion que se recibe como parametro se inicializa el servicio
		 * y se regresa
		 * se le settean los contextos los dispatchers la config
		 * @return el servicio incializado deacuerdo a la configuracion
		 */
		private function createService(config:ServiceConfig):IService {
			
			var service:IService = null;
			
			if (config.classRef != null){
				
				var serviceClass:Class = config.classRef;
				service = new serviceClass() as IService;
			}
			else if (config.classItem != null){
				service = config.classItem.newInstance() as IService;
			}
			else {
				throw new Error("ServiceManager: cannot instantiate the service["+config.name+"] because there is not [classRef or classItem]");
			}
			service.dispatcher = this.dispatcher;
			service.config = config;
			service.servicesContext = servicesContext;
			service.context =  new Context();
			service.dispatcher = this.dispatcher;
			return service;
		}
		
		
		
		
		
		/** 
		 * Esta funcion revisa si un servicio cuando fue cargado ya puede mandar a cargar a los dema sservicios
		 * en caso afirmativo comienza a cargarlos, en caso que no todos los servicios de la dependencia esten listos entonces ignora 
		 * el evento
		 */
		private function checkServiceLoadProcess(lastLoadedType:String):void {
			
			if (!serviceModel.areReadyAllServices()) {
				// Todos los Servicios de ese tipo ya estas cargados , para empezar a cargar sus dependencias
				if (serviceModel.areReadyAllServicesFromType(lastLoadedType)) {
					// Se cargan las dependencias que existan de este Tipo de servicio 
					loadServicesFromDependency(lastLoadedType);
				}
			}
			else {
				dispatcher.dispatchEvent(new ServiceManagerEvent(ServiceManagerEvent.ALL_SERVICES_READY,registry));
			}
		}
		
		private var lastLoadedService:IService;
		/**
		 * Estemetodo se manda a llamar cuando el servicio se cargo bien
		 * este metodo actualiza el estatus en el servicio y settea lavariable lasLoadedeService
		 * ademas revisa si tiene que seguir cargando con la cadena de dependencias
		 */
		public function serviceLoadReady(e:ServiceEvent):void {
			
			var service:IService = e.service;
			var config:ServiceConfig = service.config;
			setServiceStatus(service,"ready",true);
			this.lastLoadedService = service;
			checkServiceLoadProcess(service.getServiceType());
		}
		/**
		 * Este metodo se manda allamar cuando al momento de cargar un servicio hubo un error
		 * se loggea y se cambia el estatus en el servicio
		 */
		public function serviceLoadError(e:ServiceErrorEvent):void{
			var service:IService = e.service;
			var config:ServiceConfig = service.config;
			logger.error("Error " + e.type + " en " +service.getServiceType()+":"+ service.getServiceName() + "["+e.description+"]");
			setServiceStatus(service,"Load Error("+e.description+")");
		}
		
		/**
		 * Este metodo se manda a llamar cuando todod los servicios estan listos
		 * Nada mas loggea 
		 */
		public function allServicesReady(e:ServiceManagerEvent):void{
			if (e.services != null){
				logger.debug("allServicesReady("+e.services.serviceNames()+")");
			}
			
		}
		
		/**
		 * Comienza a cargar los servicio, a traves de un evento, que solicita comenzar a cargar los servcios
		 */
		public function servicesLoadRequest(e:ServiceManagerEvent):void{
			
			if (e.services != null){
				this.registry = e.services ; 
			}
			else {
				this.registry = new ServiceRegistry();
			}
			startLoadingServices();
			
		}
		
		
		
		
		
		/**
		 * Se recibo algun servicio Ready en el servicio 
		 * este metodo nadamas loggea 
		 */
		public function serviceRequestReady(e:ServiceEvent):void{
			var service:IService = e.service;
			var config:ServiceConfig = service.config;
			logger.debug("serviceRequestReady("+e+")");
		}
		/**
		 * Se recibo algun error en el servicio 
		 * este metodo nadamas loggea 
		 */
		public function serviceRequestError(e:ServiceErrorEvent):void{
			var service:IService = e.service;
			var config:ServiceConfig = service.config;
			logger.error("Error " + e.type + " en " +service.getServiceType()+":"+ service.getServiceName() + "["+e.description+"]");
			
		}
		
		/**
		 * Esta funcion settea el status al servicio que se recibe como parametro
		 */
		public function setServiceStatus(iservice:IService , status:String, loaded:Boolean = false):void {
			
			var serviceStatus:ServiceLoadStatus = serviceModel.getService(iservice.getServiceName(),iservice.getServiceType());
			serviceStatus.status = status; 
			if (loaded){
				serviceStatus.loaded = loaded;
			}
			
		}
		
		
	
	}
}