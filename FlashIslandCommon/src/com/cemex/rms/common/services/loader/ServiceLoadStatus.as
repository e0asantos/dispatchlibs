package com.cemex.rms.common.services.loader
{
	import com.cemex.rms.common.logging.ILogger;
	import com.cemex.rms.common.logging.LoggerFactory;
	import com.cemex.rms.common.logging.impl.EventLogger;
	import com.cemex.rms.common.services.config.ServiceConfig;
	
	import flash.events.IEventDispatcher;

	/**
	 * Esta clase es para mantener el status de lo servicios, 
	 * Tiene implementado un logger que permite ver en que momento se estan cargando los servicios
	 * 
	 */
	public class ServiceLoadStatus
	{
		/**
		 * Es el logger de la clase
		 */
		private static var logger:ILogger = LoggerFactory.getLogger("ServiceLoadStatus");
		
		/**
		 * Metodo constructor que recibe la cpnfiguracion de algun servicio y el dispatcher 
		 * que se utilizara para validar los servicios, y el dispatcher para despachar los
		 * eventos en aso de ser EventLogger
		 * @reference EventLogger.setDispatcherIfEventLogger(
		 */
		public function ServiceLoadStatus(_config:ServiceConfig,_dispatcher:IEventDispatcher)
		{
			this._config =_config;
			this._status = "init";
			this.loaded = false;
			// se valida que 
			EventLogger.setDispatcherIfEventLogger(logger,_dispatcher);
			
		}
		private var _status:String;
		public var loaded:Boolean;
		private var _config:ServiceConfig;
		
		private var _dispatcher:IEventDispatcher;
		
		/**
		 * getter es la configuracion de un servicio
		 */
		public function get config():ServiceConfig {
			return _config;
		}
		
		/**
		 * settea la configuracion de un servicio
		 */
		public function set config(_config:ServiceConfig):void{
			this._config = _config;
		}
		
		/**
		 * regresa el status que se tenga en el servicio
		 */
		public function get status():String {
			return _status;
		}
		
		/**
		 * Se settea el status en el servicio
		 */
		public function set status(_status:String):void{
			this._status = _status;
			logger.info("The service ["+_config.name+"] is ["+_status+"]" );
		}
	}
}