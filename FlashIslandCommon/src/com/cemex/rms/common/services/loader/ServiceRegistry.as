package com.cemex.rms.common.services.loader
{
	import com.cemex.rms.common.services.IService;
	import com.cemex.rms.common.utils.DictionaryMap;
	
	import mx.collections.ArrayCollection;
	/**
	 * Esta clase tiene como objetivo almacenar todos los servicios, 
	 * es como un hashmap de servicios
	 */
	public class ServiceRegistry implements IServiceRegistry {
		
		private var map:DictionaryMap;
		
		
		public function ServiceRegistry() {
			map = new DictionaryMap();
		}
		
		/**
		 * Regresa todos los nombres de los servicios que estan cargados
		 */
		public function serviceNames():ArrayCollection {
			return map.getAvailableKeys();
		}
		
		/**
		 * Se agrega un servicio
		 */
		public function put(service:IService):void {	
			
			map.put(service.getServiceName() , service);
		}
		
		
		/**
		 * Regresa el servicio que cumpla con el Id que se recibe
		 */
		public function get(service:String):IService {
			return map.get(service) as IService;
		}
		/**
		 * elimina el servicio que se manda como parametro
		 */
		public function remove(service:String):void {
			map.remove(service);
		}
		
		/**
		 * Regresa verdadero si el servicio ya ha sido registrado
		 */
		public function hasService(service:String):Boolean {
			return map.hasElement(service);
		}
	}
}