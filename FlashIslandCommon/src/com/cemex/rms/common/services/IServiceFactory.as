package com.cemex.rms.common.services
{
	import com.cemex.rms.common.services.config.IServiceModel;
	import com.cemex.rms.common.services.config.ServiceModel;
	
	import flash.events.IEventDispatcher;
	
	import mx.collections.ArrayCollection;

	/**
	 * Esta interfaz nos permite tener un controlador de los servicios
	 * 
	 * 
	 */
	public interface IServiceFactory
	{
		/**
		 * reinicia loa servicios al estado init
		 */
		function resetModel():void;
		
		/**
		 * comienza cargar los servicios
		 */
		function start():void;
		
		/**
		 * regresa al servicio en base id que se le haya puesto en el modelo
		 */
		function getService(service:String):IService;
		/**
		 * regresa verdadero si el servicio esta definido en el modelo
		 */
		function hasService(service:String):Boolean;
		
		/**
		 * regresa la lista de nombres se los servicios que se tienen en el modelo
		 */
		function getServicesNames():ArrayCollection;
		
		/**
		 * Regresa el modelo de los servicios
		 */
		function get model():IServiceModel;
	}
}