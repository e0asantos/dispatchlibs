package com.cemex.rms.common.services.events
{
	import com.cemex.rms.common.services.loader.IServiceRegistry;
	
	import flash.events.Event;

	/**
	 * Este evento se lanza cuando todo los servicios estan listos para ser llamados
	 * o cuando se quiere mandar a solicitar datos a algun servicio
	 * 
	 */
	public class ServiceManagerEvent extends Event
	{
		
		public static const ALL_SERVICES_READY:String 	= "allServicesReady";
		public static const SERVICES_LOAD_REQUEST:String 	= "servicesLoadRequest";
		
		public var description:String;
		public var services:IServiceRegistry;
		public function ServiceManagerEvent(type:String,services:IServiceRegistry=null,description:String=null,bubbles:Boolean = true, cancelable:Boolean = true) {
			super(type, bubbles, cancelable);
			this.description = description;
			this.services = services;
		}
		
	}
}