package com.cemex.rms.common.services.events
{
	import com.cemex.rms.common.services.IService;
	
	import flash.events.Event;

	/**
	 * Este evento se lanza cuando ubo un error al cargar un servicio
	 * o al momento de inicializar el servicio
	 * 
	 */
	public class ServiceErrorEvent extends Event
	{
		
		public static const SERVICE_LOAD_ERROR:String 	= "serviceLoadError";
		public static const SERVICE_REQUEST_ERROR:String 	= "serviceRequestError";
		
		public var service:IService;
		public var description:String;
		public function ServiceErrorEvent(type:String,service:IService,description:String=null,bubbles:Boolean = true, cancelable:Boolean = true) {
			super(type, bubbles, cancelable);
			this.service = service;
			
			this.description = description;
		}

	}
}
