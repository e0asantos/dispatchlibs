package com.cemex.rms.common.services
{

	
	import com.cemex.rms.common.logging.ILogger;
	import com.cemex.rms.common.logging.LoggerFactory;
	import com.cemex.rms.common.logging.impl.EventLogger;
	import com.cemex.rms.common.services.config.ServiceConfig;
	import com.cemex.rms.common.services.events.ServiceErrorEvent;
	import com.cemex.rms.common.services.events.ServiceEvent;
	import com.cemex.rms.common.services.loader.IServiceRegistry;
	import com.cemex.rms.common.utils.IContext;
	
	import flash.events.Event;
	import flash.events.IEventDispatcher;
	import flash.utils.Dictionary;
	import flash.utils.Timer;
	
	//import mx.controls.Alert;
	
	/**
	 * Esta clase abstrata debe extenderse siempre para implementar un servicios
	 * ya que tiene implementado todo aquello que se necesita e incluso simplifica otros metodos que permiten despachar eventos  de los status  
	 * del servicio
	 * 
	 */
	public class AbstractService implements IService
	{
		public function AbstractService() {
		}
		
		/**
		 * es ellogger
		 */
		protected  var logger:ILogger = LoggerFactory.getLogger("AbstractService");
		
		[Bindable]
		private  var _dispatcher:IEventDispatcher;
		
		
		
		/**
		 * es el dispatcher que se setteo al inicio en el service manager
		 */
		public function get dispatcher():IEventDispatcher{
			return _dispatcher;
		}
		/**
		 * Se settea el dispatcher que se utilizara, auqnue este setteo lo hace elservice manager 
		 */
		public function set dispatcher(_dispatcher:IEventDispatcher):void{
			this._dispatcher = _dispatcher;
			EventLogger.setDispatcherIfEventLogger(logger,_dispatcher);
		}
		
		private var _context:IContext;	
		/**
		 * regresa el contexto de este servicio
		 */
		public function get context():IContext{
			return _context;
		}
		/**
		 * settea el contexto de este servicio
		 */
		public function set context(_context:IContext):void {
			this._context = _context;
		}
		
		private var _servicesContext:IContext;
		/**
		 * regresa al contexto se los servicios
		 */
		public function get servicesContext():IContext{
			return _servicesContext;
		}
		/**
		 * settea al contexto se los servicios
		 */
		public function set servicesContext(_servicesContext:IContext):void {
			this._servicesContext = _servicesContext;
		}
		/**
		 * regresa el nombre del servicio que esta setteado en la configuracion del servicio
		 */
		public function getServiceName():String{
			return config.name;
		}
		/**
		 * regresa el tipo del servicio que se tiene en la configuracion del servicio
		 */
		public function getServiceType():String{
			return config.type;
		}
		private var _config:ServiceConfig;
		/**
		 * regresa la configuracion del servicio que se hizo en el modelo
		 */
		public function get config():ServiceConfig{
			return _config;
		}
		/**
		 * se settea la configuracion del servicio qeu se hizo en elmodelo
		 */
		public function set config(_config:ServiceConfig):void{
			this._config = _config;
		}
		
		/**
		 * Este metodo tiene que ser sobre escrito por los otros servicios que quieran inplementar un servicio nuevo
		 * y al fina cuando este listo el servicio deben mandar a llamar el metodo dispatchServiceLoadReady()
		 * o super.startService()  
		 * 
		 * @reference dispatchServiceLoadReady
		 */
		public function startService():void {
			
			dispatchServiceLoadReady();
			
		}
		protected var serviceReady :Boolean = false;
		/**
		 * regresa verdadero si ya se mando a llamar elmetodo dispatchServiceLoadReady()
		 */
		public  function isReady():Boolean{
			return serviceReady;
		}
		
		/**
		 * Este metodo lanza el evento de que el servicio ya esta listo para poder seguir cargando los demas servicios
		 * settea la variable ser serviceready =true
		 * ServiceEvent.SERVICE_LOAD_READY
		 */
		public function dispatchServiceLoadReady():void{
			
			var e:ServiceEvent =  new ServiceEvent(ServiceEvent.SERVICE_LOAD_READY,this);
			serviceReady = true;
			dispatcher.dispatchEvent(e);
			logger.debug("Service ["+getServiceName()+"] REady");
		}
		/**
		 * Despacha un evento de ServiceErrorEvent.SERVICE_LOAD_ERROR
		 */
		public function dispatchServiceLoadError(description:String):void{
			var e:ServiceErrorEvent =  new ServiceErrorEvent(ServiceErrorEvent.SERVICE_LOAD_ERROR,this,description);
			dispatcher.dispatchEvent(e);
		}
		
		/**
		 * Despacha un evento de ServiceEvent.SERVICE_REQUEST_READY
		 */
		public function dispatchServiceRequestReady():void {
			var e:ServiceEvent =  new ServiceEvent(ServiceEvent.SERVICE_REQUEST_READY,this);
			dispatcher.dispatchEvent(e);
		}
		
		/**
		 * Despacha un evento de ServiceErrorEvent.SERVICE_REQUEST_ERROR
		 */
		public function dispatchServiceRequestError(description:String):void {
			var e:ServiceErrorEvent =  new ServiceErrorEvent(ServiceErrorEvent.SERVICE_REQUEST_ERROR,this,description);
			dispatcher.dispatchEvent(e);
		}
	}
}
