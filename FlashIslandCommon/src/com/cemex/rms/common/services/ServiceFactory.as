package com.cemex.rms.common.services
{
	import com.cemex.rms.common.services.config.IServiceModel;
	import com.cemex.rms.common.services.loader.IServiceManager;
	import com.cemex.rms.common.services.loader.IServiceRegistry;
	import com.cemex.rms.common.services.loader.ServiceManager;
	import com.cemex.rms.common.services.loader.ServiceRegistry;
	
	import flash.events.IEventDispatcher;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;

	/**
	 * El objetivo de esta clase es tener un factory de servicios
	 *  
	 * @author japerezh
	 */
	public class ServiceFactory implements IServiceFactory
	{	
		
		
		private var _manager:IServiceManager;
		private var _model:IServiceModel;
		
		
		public function ServiceFactory() {
		}
		/**
		 * En este setter se agrega el modeloque se va a utilizar con lo servicios
		 * Normalmente se recomienda settear antes el tipo de logger a utilizar en 
		 * el Logger factory, o en el serviceFactory
		 */
		[Inspectable]
		public function set model(model:IServiceModel):void{
			this._model = model;
			resetModel();
			if (_manager != null){
				_manager.setServiceModel(model);
			}
		}
		/**
		 * Este metodo reinicia el modelo, comienza a 
		 */
		public function resetModel():void{
			_model.reset(_manager.dispatcher);
		}
		
		/**
		 * se obtiene elmodelo
		 */
		public function get model():IServiceModel{
			return _model;
		}
		
		/**
		 * Se settea el dispatcher de eventos, quien va a controlar, Normalmente se recomienda
		 * Utilizar solamente una instancia del Service Factory, pero se pueden utilizar tantas como se quiera
		 * siempre y cuando cada una tenga un eventDispatcher Diferente
		 * (Esto puede generar algun tipo de conflictos si se utiliza el EventLogger)
		 * 
		 */
		public function set dispatcher(dispatcher:IEventDispatcher):void{
			this._manager =  new ServiceManager(dispatcher);
			if (_model != null){
				_manager.setServiceModel(_model);
			}
		}
		
		
		/**
		 * Cuando se manda a llamar este metodo 
		 * se incializa el manager, y se empiezan a cargar los servicios
		 */
		public function start():void{
			if (_model != null && _manager != null){
				_manager.setServiceModel(_model);
				_manager.startLoadingServices();
			}
			else {
			throw new Error("The Service Factory should have a model and EventDispatcher");
			}
		}
		
		/**
		 * SE obtiene el servicio en base al nombre que se especifico en elModelo deservicios
		 */
		public function getService(service:String):IService{
			return _manager.registry.get(service);
		}
		
		/**
		 * Se valida si se tiene el servicio, esto simplementa valida que el servicio este en el modelo
		 * 
		 */
		public function hasService(service:String):Boolean {
			return _manager.registry.hasService(service);
		}
		
		/**
		 * Regresa una lista de todos los nombres de los servicios que estan en 
		 * el Modelo ser serviicos
		 * 
		 * 
		 */
		public function getServicesNames():ArrayCollection{
			return _manager.registry.serviceNames();
		}
		
	}
}