package com.cemex.rms.common.services.config
{
	import com.cemex.rms.common.services.loader.ServiceLoadStatus;
	
	import flash.events.IEventDispatcher;
	
	import mx.collections.ArrayCollection;
	
	/**
	 * Esta interfaz es para la definicion delModelo de servicios
	 * Se recomienda que se Utilize el Service Model en forma de mxml,
	 * 
	 * 
	 */
	public interface IServiceModel
	{
		
		/**
		 * Este metodo registra algun servicio en elmodelo
		 */
		function registerService(serviceConfig:ServiceConfig,dispatcher:IEventDispatcher):void;
		
		/**
		 * Se obtiene el servicio que se llame 
		 * @param name es el nombre del servicio  
		 * @param type es el tipo de servicio
		 * @return regresa el status del servicio que se busca
		 */
		function getService(name:String ,type:String=null):ServiceLoadStatus;
		
		/**
		 * regresa los nombres de lose servicios que ha sido configurados 
		 */
		function getServiceNames():ArrayCollection;
		
		/**
		 * Se regresa la lista de todos los servicios
		 */
		function getServiceList():Array;
		
		/**
		 * Se regresa una lista de todos los servicios de un cierto tipo
		 * @param type es el tipo espscificado en la construccion de cada servicio
		 */
		function getAllServices(type:String):Array;
		
		/**
		 * Regresa todos los servicios que dependen de algun tipo de servicio
		 * 
		 * @param typeDependecy
		 */
		function getAllServicesFromDependency(typeDependency:String = null):Array;
		
		/**
		 * Valida que todos los servicios esten activos
		 */
		function areReadyAllServices():Boolean;
		
		
		/**
		 * Valida si todos los servicios de cierto tipo ya estan listos 
		 */
		function areReadyAllServicesFromType(type:String):Boolean;
		
		/**
		 * Se reinicia el modelo, normalmente solamente se utiliza en 
		 */
		function reset(_dispatcher:IEventDispatcher):void;
		
	}
}