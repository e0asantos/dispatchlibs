package com.cemex.rms.common.services.config
{
	import com.cemex.rms.common.services.loader.ServiceLoadStatus;
	import com.cemex.rms.common.utils.DictionaryMap;
	
	import flash.events.IEventDispatcher;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;

	/**
	 * Esta clase es la implementacion de IServiceModel
	 * y tiene, se recomienda utilizar esta clase en su vista MXML
	 * 
	 * Ejemplo
	 * -------------
	 * <config1:ServiceModel xmlns:mx="http://www.adobe.com/2006/mxml" xmlns:config="com.cemex.rms.common.services.config.*" xmlns:config1="com.cemex.rms.common.services.config.*">	
	 *		<config:serviceConfigs>
	 * 			<config1:ServiceConfig name="DispatchIsland" type="Island" classItem="com.cemex.rms.dispatcher.services.flashislands.impl.DispatcherIslandImpl"  />
	 *	 		<config1:ServiceConfig name="DispatchChannelSet" type="Channels" typeDependency="Island" classItem="com.cemex.rms.dispatcher.services.lcds.impl.DispatcherChannelSetImpl"  />
	 * 		</config:serviceConfigs>
	 * </config1:ServiceModel>
	 * -------------
	 * y ya despues se puede
	 * instanciar 
	 * var sm:ServiceModel = new TestServiceModel();
	 * 
	 * 
	 */
	public class ServiceModel implements IServiceModel
	{
		public function ServiceModel()
		{
		}
		
		[Inspectable]
		public  var serviceConfigs:Array;

		
		
		private  var serviceNames:ArrayCollection =  new ArrayCollection();
		
		private  var servicesList:Array;
				
		
		/**
		 * Se regresa los nombres de todos los erevicios que se tienen en el modelo
		 */
		public function getServiceNames():ArrayCollection{
			return serviceNames;
		}
		/**
		 * Regresa la lista deservicios que se tienen en el modelo
		 */
		public function getServiceList():Array{
			return servicesList;
		}
		/**
		 * Este metodo registra un servicio utilizando un dispatcher 
		 * y la configuracion que recibe como parametro
		 */
		public function registerService(serviceConfig:ServiceConfig,_dispatcher:IEventDispatcher):void {
			
			
			if (serviceConfig.name == null){
				throw new Error("ServiceModelConfig: some service does not have name");
			}
			if (serviceConfig.type == null){
				throw new Error("ServiceModelConfig: the ServiceConfig["+serviceConfig.name+"] should declare a Type");
			}
			if (serviceConfig.classItem == null && serviceConfig.classRef == null){
				throw new Error("ServiceModelConfig: the ServiceConfig["+serviceConfig.name+"] should declare [classRef or classItem]");
			}
			servicesList.push(new ServiceLoadStatus(serviceConfig,_dispatcher));
		}
		
		/**
		 * Se reinicia el modelo, normalmente solamente se utiliza en 
		 */
		public function reset(_dispatcher:IEventDispatcher):void {
			
			
			initServicesList(_dispatcher);
			resetServicesList();	
		}
		
		/**
		 * Este metodo inicializa todos los servicios los registra y le settea el 
		 * dispatcher de paramtro
		 * 
		 */
		public function initServicesList(_dispatcher:IEventDispatcher):void{
			var i:int;
			if (servicesList == null && serviceConfigs != null){
				servicesList = new Array();
				for (i = 0 ; i < serviceConfigs.length ;i++){
					var config:ServiceConfig = serviceConfigs[i] as ServiceConfig;
					serviceNames.addItem(config.name);
					registerService(config,_dispatcher);
				}
			}
		}
		
		/**
		 * Este metodo reinicia todos los servicios (cambia el status)
		 * 
		 */
		public function resetServicesList():void{
			var i:int;
			for (i = 0 ; i < servicesList.length ;i++){
				var serv:ServiceLoadStatus = servicesList[i] as ServiceLoadStatus;
				//serv.dispatcher = _dispatcher;
				serv.status="init";
				serv.loaded = false
					
			}
		}
		
		/**
		 * Se obtiene el servicio que se llame 
		 * @param name es el nombre del servicio  
		 * @param type es el tipo de servicio
		 * @return regresa el status del servicio que se busca
		 */
		
		public function getService(name:String ,type:String=null):ServiceLoadStatus {
			var result:Array = new Array();
			var i:int;
			for (i = 0 ; i < servicesList.length ;i++) {
				var serviceLoad:ServiceLoadStatus = servicesList[i] as ServiceLoadStatus;
				if (name == serviceLoad.config.name && type == serviceLoad.config.type) {					
					return serviceLoad;
				}
			}
			return null;	
		}
		
		
		/**
		 * Regresa todos los servicios que dependen de algun tipo de servicio
		 * 
		 * @param typeDependecy
		 */
		public function getAllServicesFromDependency(typeDependency:String=null):Array {
			var result:Array = new Array();
			var i:int;
			for (i = 0 ; i < servicesList.length ;i++) {
				var serviceLoad:ServiceLoadStatus = servicesList[i] as ServiceLoadStatus;
				if (typeDependency == serviceLoad.config.typeDependency) {					
					result.push(serviceLoad);
				}
			}
			return result;	
		}

		
		/**
		 * Se regresa una lista de todos los servicios de un cierto tipo
		 * @param type es el tipo espscificado en la construccion de cada servicio
		 */
		public function getAllServices(type:String):Array {
			var result:Array = new Array();
			var i:int;
			for (i = 0 ; i < servicesList.length ;i++) {
				var serviceLoad:ServiceLoadStatus = servicesList[i] as ServiceLoadStatus;
				if (type == serviceLoad.config.type) {
					result.push(serviceLoad);
				}
			}
			return result;
		}
		
		
		
		/**
		 * Valida que todos los servicios esten activos
		 */
		public function areReadyAllServices():Boolean{
			
			var i:int;
			var services:Array = getServiceList();
			
			for (i = 0 ; i < services.length ;i++){
				var serv:ServiceLoadStatus = services[i] as ServiceLoadStatus;
				if (serv.loaded == false){
					return false;
				}
			}
			return true;
		}
		
		
		/**
		 * Valida si todos los servicios de cierto tipo ya estan listos 
		 */
		public function areReadyAllServicesFromType(type:String):Boolean{
			
			var i:int;
			var services:Array = getAllServices(type);
			
			for (i = 0 ; i < services.length ;i++){
				var serv:ServiceLoadStatus = services[i] as ServiceLoadStatus;
				if (serv.loaded == false){
					return false;
				}
			}
			return true;
		}
		
	}
}