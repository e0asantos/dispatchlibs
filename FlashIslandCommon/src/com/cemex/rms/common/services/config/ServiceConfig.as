package com.cemex.rms.common.services.config
{
	import mx.core.ClassFactory;
	import mx.core.IFactory;

	/**
	 * Esta clase tiene como objetivo definir las configuraciones de un Modelo
	 * (Cuando se utiliza el ServiceModel como MXML) este es el tag hijo
	 * 
	 * o si se hace con codigo se deben usar el metodo register
	 * 
	 */
	public class ServiceConfig {
		
		
		
		public function ServiceConfig(){
		}
		/**
		 * Es la variable de nombre del servicio
		 */
		private var _name:String;
		/**
		 * Es la referencia al objeto que se va a utilizar ete se utiliza cuando se configura la clase
		 * programaticamente 
		 * config.classREf = Clazz;
		 */
		private var _classRef:Class;
		
		/**
		 * Esla misma referencia que classRef pero para cuandose utiliza el MXML 
		 * y poder hacer referencia a la clase desde el mxml,
		 * Se recomienda que el ServiceModel sea un MXML 
		 */
		private var _classItem:IFactory;
		
		/**
		 * Es la variable que define que tipo de servicio es, en caso que 
		 * algu otro servicio requiera cargarse justo despues que este , debe usarse 
		 * este nombre en el typeDependecy
		 */
		private var _type:String;
		
		/**
		 * Este es el valor que indica de quien depende este servicio,
		 * 
		 * La secuencia de cargado, primero carga los servicios que no tienen este parametro delcarado
		 * y en cuanto termina de cargar eso, busca todas sus dependencias
		 * Asi que si existe algun servicio que tenga dependencia sy la dependencia no existe
		 * Nunca se lanzaria elevento indicando que todos lose servicios se terminaron de cargar 
		 */
		private var _typeDependency:String;
		
		
		
		
		
		public function get name():String{
			return _name;
		}
		public function set name(_name:String):void{
			this._name = _name;
		}
		public function get id():String{
			return _name;
		}
		public function set id(_id:String):void{
			this._name = _id;
		}
		
		public function get classRef():Class{
			return _classRef;
		}
		public function set classRef(_classRef:Class):void{
			this._classRef = _classRef;
		}
		public function get classItem():IFactory{
			return _classItem;
			
		}
		public function set classItem(_classItem:IFactory):void{
			this._classItem = _classItem;
			
			
		}
	
		
		
		public function get typeDependency():String{
			return _typeDependency;
		}
		public function set typeDependency(_typeDependency:String):void{
			this._typeDependency = _typeDependency;
		}
		
		public function get type():String{
			return _type;
		}
		public function set type(_type:String):void{
			this._type = _type;
		}
		
		
		
	}
}