package com.cemex.rms.common.services
{
	import com.cemex.rms.common.services.config.ServiceConfig;
	import com.cemex.rms.common.services.loader.IServiceRegistry;
	import com.cemex.rms.common.utils.IContext;
	
	import flash.events.IEventDispatcher;

	/**
	 * Esta interfaz es una unico punto de acceso a cualquier servicio, de tal forma 
	 * que ya se tiene un ciclo de vida de los servicios 
	 * 
	 */
	public interface IService
	{
		
		/**
		 * es el dispatcher que se setteo al inicio en el service manager
		 */
		function get dispatcher():IEventDispatcher;
		
		/**
		 * Se settea el dispatcher que se utilizara, auqnue este setteo lo hace elservice manager 
		 */
		function set dispatcher(_dispatcher:IEventDispatcher):void;
		
		/**
		 * es el nombre del servicio que se setteo en elmodelo
		 */
		function getServiceName():String;
		
		/**
		 * regresa el tipode servicio, este es el tipo especificado en el Modelo
		 */
		function getServiceType():String;
		
		/**
		 * regresa el contexto de este servicio
		 */
		function get context():IContext;
		function set context(_context:IContext):void;
		
		/**
		 * regresa al contexto se los servicios
		 */
		function get servicesContext():IContext;
		function set servicesContext(_servicesContext:IContext):void;
		
		/**
		 * Es la configuracion del servicio
		 */
		function get config():ServiceConfig;
		function set config(_servicesConfig:ServiceConfig):void;
		
		
		
		
		
		/**
		 * Comienza a cargar el servicio
		 */
		function startService():void;
		
		
		/**
		 * regresa true si ya se cargo el servicio
		 */
		function isReady():Boolean;
		
		
		
	}
}