package com.cemex.rms.common.views
	
{
	import com.cemex.rms.common.helpers.ContextMenuHelper;
	import com.cemex.rms.common.helpers.PopupHelper;
	import com.cemex.rms.common.logging.ILogger;
	import com.cemex.rms.common.logging.LoggerFactory;
	import com.cemex.rms.common.logging.events.LoggerEvent;
	import com.cemex.rms.common.push.PushServiceStatusEvent;
	import com.cemex.rms.common.reflection.ReflectionHelper;
	import com.cemex.rms.common.services.config.IServiceModel;
	import com.cemex.rms.common.services.events.ServiceEvent;
	import com.cemex.rms.common.utils.DictionaryMap;
	import com.cemex.rms.dispatcher.constants.Logger;
	
	import flash.events.ContextMenuEvent;
	import flash.events.MouseEvent;
	import flash.events.StatusEvent;
	import flash.system.System;
	import flash.utils.Dictionary;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Image;
	import mx.controls.Menu;
	import mx.core.UIComponent;
	import mx.events.MenuEvent;
	import mx.events.ToolTipEvent;
	import mx.managers.ToolTipManager;
	
	import org.robotlegs.mvcs.Mediator;
	
	/**
	 * Este mediador tiene la funcionalidad de 
	 * 
	 * Se necesita sobre escribir los metodos:
	 * 	viewStatusId()	-> el cual nos dice cual es el nombre de la Imagen donde se va a desplegar la informacion de status
	 * 	getModel() 		-> que normalmente lo unico que ahce es trar el modeo cargado en los servicios, actualmente utilizados
	 * 	getView() 		-> En casi de utilizar la injeccion de dependencias de Robotlegs nada mas va a ser un "return view;"
	 * 
	 * @author japerezh
	 */
	public class GenericMediator extends Mediator
	{
		public function GenericMediator()
		{
			super();
		}
		
		
		
		/**
		 * Constante de uso interno para especificar que un servicio esta Connectado
		 */
		public static const CONNECTED:String = "CONNECTED";
		
		/**
		 * Constante de uso interno para especificar que un servicio esta Desconectado
		 */
		public static const DISCONNECTED:String = "DISCONNECTED";
		
		/**
		 * Constante de uso interno para especificar que un servicio esta en Estatus de Warning
		 */
		public static const FAULT:String = "FAULT";
		
		
		
		
		/**
		 * Este metodo regresa el id en la vista de la imagen donde se van a desplegar los leds de colores
		 * el valor que regresa por defualt es "LedStatus"
		 * 
		 */
		public function viewStatusId():String{
			return "LedStatus"; 
		}
		/**
		 * Este es el modelo de los servicios que se estan utilizando en el desarrollo
		 * de los cuales se va a mostrar el estatus en en led con el click
		 */
		public function getModel():IServiceModel{
			throw new Error("This method should be overriden getModel");
		}
		
		
		/**
		 * Este es el comonente que contiene una imagen para mostrar el ledStatus
		 * 
		 */
		public function getView():UIComponent{
			throw new Error("This method should be overriden getView");
		}
		
		/**
		 * This method should be overriden, to get the image leds desired in the application. 
		 * The values that ill be received in the parameter will be the Constants:
		 * CONNECTED,DISCONNECTED and FAULT of this class
		 * 
		 * 
		 */
		public function getIcon(status:String):Class{
			throw new Error("This method should be overriden ")
		}
		
		
		/**
		 * Esta funcion solamente regresa la lista de nombres de los servicios
		 */
		private function servicesList():ArrayCollection{
			
			var model:IServiceModel = getModel();
			return model.getServiceNames();
		}
		
		/**
		 * Esta variable se utiliza para almacenar e forma de hash los valores de status de todos los servicios
		 */
		private var statusMap:DictionaryMap = new DictionaryMap();
		
		/**
		 * Este metodo nos permite inicializar todos los servicios , a su vez tambien actualiza el led de status
		 * 
		 * Agrega los Eventos que utiliza 
		 * Click derecho para copiar el log, Y los eventos de los servicios para poder tener el status actualizado. 
		 * 
		 */
		public function init():void {
			
			var services:ArrayCollection = servicesList();
			for (var i:int = 0; i < services.length ; i ++ ){
				var serv:String = services.getItemAt(i) as String;
				statusMap.put(serv, DISCONNECTED);
			}
			
			updateStatus();
			var comp:UIComponent = getComponent(viewStatusId());
			
			if (comp != null){
				comp.addEventListener(MouseEvent.CLICK,statusClickHandler,false,0,true);
			}
			eventMap.mapListener(eventDispatcher,ServiceEvent.SERVICE_LOAD_READY,serviceReady,null,false,0,true);
			eventMap.mapListener(eventDispatcher,PushServiceStatusEvent.STATUS_SERVICE_CONNECTED,serviceStatus,null,false,0,true);
			eventMap.mapListener(eventDispatcher,PushServiceStatusEvent.STATUS_SERVICE_DISCONNECTED,serviceStatus,null,false,0,true);
			eventMap.mapListener(eventDispatcher,PushServiceStatusEvent.STATUS_SERVICE_FAULT,serviceStatus,null,false,0,true);
			eventMap.mapListener(eventDispatcher,LoggerEvent.LOGGER_EVENT,log,null,false,0,true);
			//ContextMenuHelper.addContextMenuOption("Activate Log", copyLog);
		}
		
		/**
		 * Esta funcion lo que hace es actualizar el estatus cuando el servicio esta listo  
		 */
		protected function serviceReady(e:ServiceEvent):void{
			setServiceStatus(e.service.config.name,CONNECTED);
		}
		
		
		
		/**
		 * Esta funcion actualiza todos los demas estatus que se reciben de los servicios, 
		 * que pueden ser que se pierde la conexion , que se ejecuta un warning
		 * o que se conecta de nuevo  
		 */
		protected function serviceStatus(e:PushServiceStatusEvent):void{
			if (e.type == PushServiceStatusEvent.STATUS_SERVICE_CONNECTED){
				setServiceStatus(e.serviceName,CONNECTED);
			}
			else if (e.type == PushServiceStatusEvent.STATUS_SERVICE_DISCONNECTED){
				
				setServiceStatus(e.serviceName,DISCONNECTED);
				
				
			}
			else if (e.type == PushServiceStatusEvent.STATUS_SERVICE_FAULT){
				setServiceStatus(e.serviceName,FAULT);
				
			}
			
		}
		
		private var logger:ILogger = LoggerFactory.getLogger("GenericMediator");
		/**
		 * Este metodo lo unico que hace es actualizar el status del led principal y actualizar un servicio en el cache de status
		 * 
		 */
		protected function setServiceStatus(service:String,status:String):void{
			logger.debug("setServiceStatus("+service+"::"+status+")");
			statusMap.put(service,status);
			updateStatus();
		}
		
		/**
		 * Esta funcion es el handler de el click en el led principal de satus de tal forma que simplemente despliega un menu con las imagenes
		 * 
		 */
		protected function statusClickHandler(e:MouseEvent):void{
			
			var statusMenu:Menu = PopupHelper.popupFromXML(e,"@label",getStatusXML(),getIconFromMenuItem );
			statusMenu.addEventListener(MenuEvent.ITEM_CLICK,menuClick,false,0,true);
			//Alert.show(ReflectionHelper.object2XML(statusXML));
		}
		
		/**
		 * Este metodo regresa el XML para desplegar el Popup Menu
		 * con todas las imagenes que se requieren 
		 */
		private function getStatusXML():XML {
			
			var result:XML = PopupHelper.getXML("root");
			var services:ArrayCollection = servicesList();
			for (var i:int = 0; i < services.length ; i ++ ){
				var serv:String = services.getItemAt(i) as String;
				
				var icon:String = statusMap.get(serv);
				
				result.appendChild(PopupHelper.getMenuItem(serv,serv,serv,serv,0,true,null,false,icon));
			}
			
			return result;
		}
		
		
		/**
		 * Esta funcion simplemente revisa que exista el valor de referencia del evento del popup menu
		 * y manda allamar la funcion de clickStatus(service:String) la cual se puede sobre escribir
		 * 
		 * @reference clickStatus
		 */
		private function menuClick(event:MenuEvent):void {
			if (event.item.@ref != null ){
				clickStatus(event.item.@ref);		
			}
		}
		
		/**
		 * Esta funcion se manda a llamar cada vez que se hace click en alguna opcion desplegada
		 * en el popup menu que se despleiga sobre el led principal, si se quiere hacer alguna accion es necesario sobre estribir esta funcion
		 */
		public function clickStatus(service:String):void{
		}
		
		
		/**
		 * Este metodo regresa el valor de la clase para pintar el icono en el menu
		 * pero esta funcion manda a llamar al metodo getIcon el cual debe ser sobre escrita 
		 * 
		 * 
		 * @reference getIcon
		 */
		private function getIconFromMenuItem(menuitem:XML):Class{
			var icon:String = menuitem.@icon;
			return getIcon(icon);
		}
		
		/**
		 * Esta funcion lo unico que ahce es actualizar el color del led principal, en base a todos los status en general
		 * Si todos los servicios estan CONNECTED se pinta de verde, en caso que todos los servicios esten desconectados 
		 * se punta con la imagen de DISCONNECTED en cualquier otro caso se pinta con el icono de FAULT.
		 * Estos iconos son especificados en la funcion getIcon que debe ser sobre escrita por la clase que herede esta clase
		 * 
		 */
		protected function updateStatus():void {
			
			var services:ArrayCollection = servicesList();
			var connected:Number = 0 ;
			for (var i:int = 0; i < services.length ; i ++ ){
				var serv:String = services.getItemAt(i) as String;
				if (statusMap.get(serv) == CONNECTED){
					connected++;
				}
			}
			if (connected == 0){
				setComponentStatus(viewStatusId(),DISCONNECTED);
			}
			else if (connected == services.length) {
				setComponentStatus(viewStatusId(),CONNECTED);
			}
			else {
				setComponentStatus(viewStatusId(),FAULT);
			}
			
		}
		
		/**
		 * This function only gets the Component that has the id which is passed as parameters
		 * if it doesn't exists it returns null.
		 * 
		 */
		protected function getComponent(name:String):UIComponent{
			if (getView() != null && getView().hasOwnProperty(name)){
				return getView()[name] as UIComponent;
			}
			return null;
		}
		
		/**
		 * This function sets the icon on the component that should be an Image, 
		 * if the component is not an image this function wont make anything
		 *
		 * the status could be CONNECTED, DISCONNECTED, FAULT 
		 */
		protected function setComponentStatus(component:String, status:String):void{
			
			var image:Image = getComponent(component) as Image;
			if (image != null){
				image.source = getIcon(status);
			}
		}
		
		
		/**
		 * This variable is the logging, this is accessible in case someone wants to bind it in some datagrid or something  
		 */
		[Bindable]
		public var logging:ArrayCollection;
		
		/**
		 * this is the counter of logs in the application
		 */
		private var logCounter:int = 0 ;
		
		/**
		 * This function copies the log to the Clipboard
		 * @reference logging variable
		 */
		public function copyLog(e:ContextMenuEvent):void{
			System.setClipboard(
				ReflectionHelper.object2XML(logging,"Log")
			);
		}
		
		/**
		 * This function append a log String to the logging variable
		 * 
		 */
		public function log(e:LoggerEvent):void
		{
			if(!Logger.inDebug){
				return;
			}
			if(logging == null)
			{
				logging = new ArrayCollection();
			}
			logCounter++;
			logging.addItem("<count value=\""+logCounter+"\"/>"+e.log);
			processLog(e.log);
		}
		
		public function processLog(log:String):void{
		}
		
		
		
	}
}