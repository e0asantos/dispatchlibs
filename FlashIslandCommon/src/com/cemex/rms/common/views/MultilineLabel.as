package com.cemex.rms.common.views
{
	import mx.controls.Label;
	
	import mx.core.UITextField;  
	import flash.text.TextFieldAutoSize;  
	import mx.controls.Label;  
	import flash.display.DisplayObject;  
	
	/**
	 * Esta clase tiene una funcionalidad basica que permite que se cree una etiqueta 
	 * que haga un despliegue de informacion en diferentes lineas
	 * 
	 */
	public class MultilineLabel extends Label  
	{  
		
		/**
		 * Simplemente se cambia la forma de crear los hijos, y se instancia 
		 * Otro objecto 
		 * 
		 */
		override protected function createChildren() : void  
		{  
			// Create a UITextField to display the label.  
			if (!textField)  
			{  
				textField = new UITextField();  
				textField.styleName = this;  
				addChild(DisplayObject(textField));  
			}  
			super.createChildren();  
			textField.multiline = true;  
			textField.wordWrap = true;  
			textField.autoSize = TextFieldAutoSize.LEFT;  
		}  
	}  
}

