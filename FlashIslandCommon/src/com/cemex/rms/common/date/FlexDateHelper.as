package com.cemex.rms.common.date
{
	import com.adobe.fiber.runtime.lib.DateTimeFunc;
	
	import mx.controls.Alert;
	import mx.formatters.DateFormatter;

	/**
	 * Esta clase tiene una serie de metodos estaticos que ayudan a hacer operaciones con las Fechas 
	 * Tanto parseos de String a Date y viceversa 
	 * 
	 */
	public class FlexDateHelper
	{
		public function FlexDateHelper()
		{
		}
		
		
		
		/**
		 * Regresa de una fecha el dia del mes 
		 * @param recibe la fecha de la cual se quiere saber el dia del mes
		 * @return regresa el dia del mes e string
		 */
		public static function getDayOfMonth(date:Date):String{
			if (date == null){
				return null;
			}
			var formater:DateFormatter =  new DateFormatter();
			formater.formatString = "DD";
			return formater.format(date);
		}
		
		
		/**
		 * Este metodo recibe una fecha y un formato y regresa la fecha parseada en ese formato
		 *  
		 * @param date es la fecha que se quiere parsear
		 * @param format es el formato
		 * @return regresa el string de la fecha parseada
		 */
		public static function getDateString(date:Date,format:String):String{
			if (date == null){
				return "";
			}
			var formater:DateFormatter =  new DateFormatter();
			formater.formatString = format;
			return formater.format(date);
		}
		
		
		/**
		 * Este metodo recibe una fecha en texto  y una hora en formato de texto y 
		 * Regresa el tipo de dato Date ya parseado 
		 *  
		 * @param date recibe la fecha en string
		 * @param hour recibe la hora en String (formato HH:MM:SS)
		 * @return regresa el dia del mes e string
		 * @reference getDateFromString
		 */
		public static function parseStringTime(date:String, hour : String) : Date{
			
			var arrangedHour:String = arrangeHour(hour);
			
			var split:Array = arrangedHour.split(":");
			
			var currentDate:Date = getDateFromString(date);
			currentDate.setHours(Number(split[0]), Number(split[1]), Number(split[2]));
			split=null;
			return currentDate;
		}
		
		
		/**
		 * Este metodo recibe un string que contiene una hora
		 * y lo convierte al formato "HH:MM:SS" 
		 * puede recibir la hora en los siguientes formatos:
		 * HHMM, HHMMSS, HH:MM, HH:MM:SS
		 * si no se cumple ninguno de esos formator regresara "00:00:00"
		 * 
		 * @param input es la hora que se quiere convertir
		 * @return regresa el string de la fecha parseada
		 */
		public static function arrangeHour(input:String):String{
			
			if (input.indexOf(":") == -1){
				// Quiere decir que no tiene :
				if (input.length == 4){
					//HHMM
					return input.substring(0,2) + ":" + input.substring(2,4) + ":00";
				}
				else if (input.length == 6){
					//HHMMSS
					return input.substring(0,2) + ":" + input.substring(2,4)+ ":" + input.substring(4,6);
				}	
			}
			else {
				var split:Array = input.split(":");
				if (split.length == 2){
					//HH:MM
					split=null;
					return input +":00";
				}
				else if (split.length == 3){
					//HH:MM:SS
					split=null;
					return input;
				}
			}
			
			return "00:00:00";
		}
		
		
		/**
		 * Este metodo recibe un string que contiene una fecha
		 * y lo convierte objeto tipo Date
		 * puede recibir la fecha en los siguientes formatos:
		 * YYYYMMDD,YYYY/MM/DD
		 * si no se cumple ninguno de esos formator regresara null
		 * 
		 * @param input es la fecha que se quiere convertir
		 * @return regresa el Date de la fecha parseada
		 */
		public static function getDateFromString(input:String):Date{
			
			if (input.indexOf("/") == -1){
				if (input.length == 8){
					//YYYYMMDD
					return new Date(Number(input.substring(0,4)),Number(input.substring(4,6)) - 1,Number(input.substring(6,8)));
				}
			}
			else {
				var split:Array = input.split("/");
				if (split.length == 3){
					//YYYY/MM/DD
					return new Date(split[0],split[1],split[2]);
				}
			}
			
			return null;
		}
		
		
		/**
		 * Este metodo recibe una fecha y le settea la hora que se recibe como parametro value
		 * y lo convierte objeto tipo Date
		 * 
		 * @param base es la fecha (YYYY/MM/DD)
		 * @param value es la hora que se va a settear a la fecha
		 * @param extraMinutes es en caso que se quieran agregar minutos extras
		 * @return regresa el Date de la fecha parseada
		 * @reference arrangeHour, getDateWithHours, addMinutes
		 */
		public static function parseTime(base:Date, value : String, extraMinutes:Number=0) : Date
		{
			if(value.indexOf(":") == -1){
				//separar la hora
				value=value.substr(0,2)+":"+value.substr(2,2)+":"+value.substr(-2);
			}
			if(base == null 
				|| value == null 
				|| value.indexOf(":") == -1 ){
				return null;
			}
			
			var arrangedHour:String = arrangeHour(value);
			var split:Array = arrangedHour.split(":");
			var currentDate:Date = getDateWithHours(base,Number(split[0]), Number(split[1]), Number(split[2]));
			split=null;
			return addMinutes(currentDate, extraMinutes);
		}
		
		
		/**
		 * Este metodo regresa el string de la fecha en el formato
		 * MM/DD/YYYY HH:NN:SS 
		 * 
		 * @param date es la fecha que se quiere parsear
		 * @return regresa la hora parseada a String
		 */
		public static function parseTimeString(date:Date) : String
		{
			if(date == null ){
				return null;
			}
			var dateFormatter : DateFormatter = new DateFormatter();
			dateFormatter.formatString = "MM/DD/YYYY HH:NN:SS";
			
			return dateFormatter.format(date);
		}
		
		
		/**
		 * Este metodo regresa el string de la fecha en el formato
		 * MM/DD/YYYY HH:NN:SS
		 * 
		 * @param date es la fecha que se quiere parsear
		 * @return regresa la hora parseada a String
		 */
		public static function getEnglishTimestampString(date:Date) : String 
		{
			if (date == null){
				return null;
			}
			return getDateString(date, "MM/DD/YYYY HH:NN:SS");
		}
		
		
		/**
		 * Este metodo regresa el string de la fecha en el formato
		 * YYYY/MM/DD HH:NN:SS
		 * 
		 * @param date es la fecha que se quiere parsear
		 * @return regresa la hora parseada a String
		 */
		public static function getTimestampString(date:Date) : String 
		{
			if (date == null){
				return null;
			}
			return getDateString(date, "YYYY/MM/DD HH:NN:SS");
		}
		
		/**
		 * Este metodo quita, resta o substrae los segundos que se reciban en el segudo parametro
		 * 
		 * @param base es la fecha inicial que se le van a restar segundos
		 * @param seconds son los segundos que se van a restar
		 * @return regresa la nueva fecha
		 */
		public static function subSeconds (base:Date, seconds:int):Date{
			
			if(base == null ){
				return null;
			}
			
			if (seconds > 3600){
				return base;
			}
			
			var currHour:Number = base.getHours() ;
			var currMin:Number = base.getMinutes()  - ((seconds / 60) % 60);
			var currSec:Number = base.getSeconds() - (seconds % 60);
			
			if (currSec < 0){
				currSec = 60 - currSec;
				currMin--;
			}
			if (currMin < 0){
				currMin = 60 - currMin;
				currHour--;
			}
			
			var currentDate:Date = new Date(base.getFullYear(), base.getMonth(), Number(getDayOfMonth(base)), currHour, currMin,currSec);
			return currentDate 	;
		}
		
		
		/**
		 * Este metodo agrega minutos a una fecha 
		 * 
		 * @param base es la fecha inicial que se le van a agregar minutos
		 * @param extraMinutes son los minutos que se quieren sumar
		 * @return regresa la nueva fecha
		 */
		public static function addMinutes (base:Date, extraMinutes:int):Date{
			
			if (base == null){
				return null;
			}
			
			/*var deltaMin:Number = extraMinutes % 60;
			var deltaHour:Number = Math.floor(extraMinutes / 60) % 60;
			var deltaDay:Number = Math.floor(Math.floor(extraMinutes / 60) / 60);
			
			var currentDate:Date = new Date(base.getFullYear(), base.getMonth(), Number(getDayOfMonth(base))+deltaDay, base.getHours() + deltaHour, base.getMinutes()+deltaMin,base.getSeconds());
			return currentDate ;*/
			//return DateTimeFunc.dateAdd("n",extraMinutes,base);
			return new Date(base.getTime()+(extraMinutes*1000*60));
		}
		
		
		/**
		 * Este metodo suma los segundos que se reciban en el segudo parametro
		 * 
		 * @param base es la fecha inicial que se le van a sumar segundos
		 * @param extraSeconds son los segundos que se van a sumar
		 * @return regresa la nueva fecha
		 */
		public static function addSeconds (base:Date, extraSeconds:Number):Date{
			/*
			var deltaSec:Number = extraSeconds % 60;
			var deltaMin:Number = Math.floor(extraSeconds / 60) % 60;
			var deltaHour:Number = Math.floor(extraSeconds / 3600) % 3600;
			var deltaDay:Number = Math.floor(Math.floor(extraSeconds / 3600) / 3600);
			
			var currentDate:Date = new Date(base.getFullYear(), base.getMonth(), Number(getDayOfMonth(base))+deltaDay, base.getHours() + deltaHour, base.getMinutes()+deltaMin,base.getSeconds());
			return currentDate ;
		*/
			if (base == null){
				return null;
			}
			/*return DateTimeFunc.dateAdd("s",extraSeconds,base);*/
			return new Date(base.getTime()+(extraSeconds*1000));
		}
		
		/**
		 * Copia una fecha de tal forma que se tiene una nueva referencia
		 * 
		 * @param la fecha a copiar
		 * @return la nueva fecha
		 */
		public static function copyDate(base:Date):Date {
			if (base == null){
				return null;
			}
			
			var currentDate:Date = new Date(base.getFullYear(), base.getMonth(), Number(getDayOfMonth(base)), base.getHours() , base.getMinutes(),base.getSeconds());
			return currentDate 	;
		}
		
		
		/**
		 * Este metodo crea unnuevo objeto Date en base al objeto que se recibe como parametro, y le settea las horas minutos y segundos que se reciben como parametros
		 * 
		 * @param la fecha a copiar
		 * @return la nueva fecha
		 */
		public static function getDateWithHours(base:Date, hours:int, minutes:int, seconds:int):Date{
			if (base == null){
				return null;
			}
			return new Date(base.getFullYear(),base.getMonth(),Number(getDayOfMonth(base)),hours,minutes,seconds);
		}
		/**
		 * Este metodo setea una hora correcta para la hora local de la zona horaria
		 */
		
	}
}