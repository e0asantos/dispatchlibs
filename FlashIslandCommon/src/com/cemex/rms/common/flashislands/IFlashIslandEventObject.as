package com.cemex.rms.common.flashislands
{
	/**
	 * Esta interface es la que nos permite poder definir objetos fijos que sean los eventos que
	 * se mandan al flash island.
	 * 
	 */
	public interface IFlashIslandEventObject
	{
		 function getEventName():String;
	}
}