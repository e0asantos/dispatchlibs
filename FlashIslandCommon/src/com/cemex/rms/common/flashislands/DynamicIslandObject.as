package com.cemex.rms.common.flashislands
{
	import com.cemex.rms.common.reflection.ReflectionHelper;
	
	import flash.utils.Proxy;
	import flash.utils.flash_proxy;
	
	import mx.collections.ArrayCollection;
	/**
	 * Este es un objeto dinamico que permite validar en el momento en que se settearon todos los datasources 
	 * y tener un lugar donde ver todas las variablesque settea el WD a Flash
	 * 
	 */
	public dynamic class DynamicIslandObject extends Proxy
	{
		
		private var listenFields:ArrayCollection;
		private var loadedDataListener:IFlashIslandDataListener;
		/**
		 * 
		 * @param loadedDataListener Es el listener que va 
		 * @param listenFields es la lista de todos los valores que va a validar
		 * @param cleanAfterDataReady esta variable borra el buffer data cuando se lanza el evento
		 */
		public function DynamicIslandObject(loadedDataListener:IFlashIslandDataListener,listenFields:ArrayCollection,cleanAfterDataReady:Boolean = false) {
			this.listenFields = listenFields;
			this.loadedDataListener = loadedDataListener;
			this.cleanAfterDataReady = cleanAfterDataReady;
		}
		/**
		 * Donde se almacenan las variables que fueron configuradas desde un inicio
		 */
		public var data:Object =  new Object();
		
		/**
		 * Donde se almacenan las variables extras que no fueron configuradas per que el flashisland
		 * de manda
		 */
		public var extraData:Object =  new Object();
		private var cleanAfterDataReady:Boolean = false;
		
		/**
		 * 
		 * Overrides the behavior of an object property that can be called as a function. Proxy
		 */	
		
		 override flash_proxy function callProperty(methodName:*, ... args):* {
	       /* var res:*;
	        switch (methodName.toString()) {
	            case 'clear':
	                _item = new Array();
	                break;
	            case 'sum':
	                var sum:Number = 0;
	                for each (var i:* in _item) {
	                    // ignore non-numeric values
	                    if (!isNaN(i)) {
	                        sum += i;
	                    }
	                }
	                res = sum;
	                break;
	            default:
	                res = _item[methodName].apply(_item, args);
	                break;
	        }
	        return res;*/
	        return null;
	    }

		
		 /**
		 * 
		 * Overrides any request for a property's value. Proxy
		 */ 
		override flash_proxy function getProperty(name:*):*
		{
			return data[name];
		}
		/**
		 * revisa si esta en la lista
		 */
		private function hasName(name:String):Boolean{
			for (var i:int  = 0 ; i < listenFields.length ; i ++){
				var temp:String = listenFields.getItemAt(i) as String;
				if (temp == name){
					return true;
				}
			}
			return false;
		}
		
		private var loadedFields:int = 0;
		
		
		 /**
		 * 
		 * Overrides a call to change a property's value.
		 * listenFields.contains(name) 
		 */
		//public static var dictionaryGlobal:Array=[];
		override flash_proxy function setProperty(name:*, value:*):void
		{
			if (hasName("" + name)){
				
				loadedFields++;
				//
				data[name] = value;
				if(!(value is String)){
					//dictionaryGlobal.push(value);
				}
			}
			else {
				extraData[name] = value;
				if(!(value is String)){
					//dictionaryGlobal.push(value);
				}
				//Alert.show(ReflectionHelper.object2XML(value,name));
			}
			
			if (loadedFields == listenFields.length){
				loadedDataListener.flashIslandDataReady();
				if(cleanAfterDataReady){
					cleanAll();
				}
				loadedFields = 0;
				//Alert.show(ReflectionHelper.object2XML(data,"data"));
			}
		}
		/**
		 * Borra todas las variables que estan en la lista, 
		 * 
		 */
		public function cleanAll():void{
			for (var i:int  = 0 ; i < listenFields.length ; i ++){
				var temp:String = listenFields.getItemAt(i) as String;
				data[temp] = null;
				extraData = new Object();
				
			}
		}
		
		 /**
		 * 
		 * Overrides a request to check whether an object has a particular property by name. Proxy
		 *
		*/  
		override flash_proxy function hasProperty(name:*):Boolean {
			
			tempString +="\n" + name;
			return true;
		}
		
		private var tempString:String = "";
		
		
		/**
		 * 
		 * Overrides the request to delete a property. Proxy
		  
		override flash_proxy function deleteProperty(name:*):Boolean{
			return super.deleteProperty(name);
		}
		*/
		
		 /**
		 * 
		 * Overrides the use of the descendant operator. Proxy
		 
		override flash_proxy function getDescendants(name:*):*{
			Alert.show("getDescendants("+name+")");
			return null;
			//return super.getDescendants(name);
		}
		
		*/ 
		
		 /**
		 * 
		 * Checks whether a supplied QName is also marked as an attribute. Proxy
		 * 
		*
		override flash_proxy function isAttribute(name:*):Boolean{
			Alert.show("isAttribute("+name+")");
			
			return true;
			//return super.isAttribute(name);
		}
		
		*/ 
		 /**
		 * 
		 * Allows enumeration of the proxied object's properties by index number to retrieve property names. Proxy
		 * 
		override flash_proxy function nextName(index:int):String{
			return super.nextName(name);
		}
		*/ 
		 /**
		 * 
		 * Allows enumeration of the proxied object's properties by index number. Proxy
		 *  
		override flash_proxy function nextNameIndex(index:int):int{
			return super.nextNameIndex(name);
		}
		*/ 
		 /**
		 * 
		 * Allows enumeration of the proxied object's properties by index number to retrieve property values. Proxy
		 * 
		override flash_proxy function nextValue(index:int):*{
			return super.nextValue(name);
		}
		*/ 
		 
		

		
		
	}
}