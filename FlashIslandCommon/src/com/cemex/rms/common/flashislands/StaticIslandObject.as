package com.cemex.rms.common.flashislands
{
	import com.cemex.rms.common.reflection.ReflectionHelper;
	
	import flash.utils.Proxy;
	import flash.utils.flash_proxy;
	
	import mx.collections.ArrayCollection;
	/**
	 * Este es un objeto dinamico que permite validar en el momento en que se settearon todos los datasources 
	 * y tener un lugar donde ver todas las variablesque settea el WD a Flash
	 * 
	 */
	public dynamic class StaticIslandObject extends Object
	{
		
		private var listenFields:ArrayCollection;
		private var loadedDataListener:IFlashIslandDataListener;
		/**
		 * 
		 * @param loadedDataListener Es el listener que va 
		 * @param listenFields es la lista de todos los valores que va a validar
		 * @param cleanAfterDataReady esta variable borra el buffer data cuando se lanza el evento
		 */
		public function StaticIslandObject(loadedDataListener:IFlashIslandDataListener,listenFields:ArrayCollection,cleanAfterDataReady:Boolean = false) {
			this.listenFields = listenFields;
			this.loadedDataListener = loadedDataListener;
			this.cleanAfterDataReady = cleanAfterDataReady;
		}
		/**
		 * Donde se almacenan las variables que fueron configuradas desde un inicio
		 */
		public var data:Object =  new Object();
		
		/**
		 * Donde se almacenan las variables extras que no fueron configuradas per que el flashisland
		 * de manda
		 */
		public var extraData:Object =  new Object();
		private var cleanAfterDataReady:Boolean = false;
		
		/**
		 * 
		 * Overrides the behavior of an object property that can be called as a function. Proxy
		 */	
		
		 

		
		
		
		private var loadedFields:int = 0;
		
		private var _ds_sorder:Object;
		public function set DS_SORDER(value:Object):void{
			_ds_sorder=value;
			loadedFields++;
			if (loadedFields == listenFields.length){
				loadedDataListener.flashIslandDataReady();
				loadedFields = 0;
			}
		}
		public function get DS_SORDER():Object{
			return _ds_sorder;
		}
		
		private var _data:Object;
		public function set DATA(value:Object):void{
			_data=value;
			loadedFields++;
			if (loadedFields == listenFields.length){
				loadedDataListener.flashIslandDataReady();
				loadedFields = 0;
			}
		}
		public function get DATA():Object{
			return _data;
		}
		
		
		private var _import_disso;
		public function set IMPORT_DISSO(value:Object):void{
			_import_disso=value;
			loadedFields++;
			if (loadedFields == listenFields.length){
				loadedDataListener.flashIslandDataReady();
				loadedFields = 0;
			}
		}
		public function get IMPORT_DISSO():Object{
			return _import_disso;
		}
		
		
		private var _initialConfig:Object;
		public function set INITIALCONFIG(value:Object):void{
			_initialConfig=value;
			loadedFields++;
			if (loadedFields == listenFields.length){
				loadedDataListener.flashIslandDataReady();
				loadedFields = 0;
			}
		}
		public function get INITIALCONFIG():Object{
			return _initialConfig;
		}
		
		
		private var _zparams:Object;
		public function set ZPARAMS(value:Object):void{
			_zparams=value;
			loadedFields++;
			if (loadedFields == listenFields.length){
				loadedDataListener.flashIslandDataReady();
				loadedFields = 0;
			}
		}
		public function get ZPARAMS():Object{
			return _zparams;
		}
		
		
		
		
		private var _zfield_atributes:Object;
		public function set ZFIELD_ATTRIBUTES(value:Object):void{
			_zfield_atributes=value;
			loadedFields++;
			if (loadedFields == listenFields.length){
				loadedDataListener.flashIslandDataReady();
				loadedFields = 0;
			}
		}
		public function get ZFIELD_ATTRIBUTES():Object{
			return _zfield_atributes;
		}
		
		
		
		
		/**
		 * Borra todas las variables que estan en la lista, 
		 * 
		 */
		public function cleanAll():void{
			for (var i:int  = 0 ; i < listenFields.length ; i ++){
				var temp:String = listenFields.getItemAt(i) as String;
				data[temp] = null;
				extraData = new Object();
				
			}
		}
		
		
		
		

		
		
	}
}