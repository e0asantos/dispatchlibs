package com.cemex.rms.common.flashislands
{
	import com.cemex.rms.common.logging.ILogger;
	import com.cemex.rms.common.logging.LoggerFactory;
	import com.cemex.rms.common.reflection.ReflectionHelper;
	
	import mx.collections.ArrayCollection;
	
	/**
	 * ESte helper nos ayuda a hacer mappeos, 
	 * 
	 */
	public class MappingHelper
	{
		public function MappingHelper()
		{
		}
		/**
		 * Este metodo nos ayuda a obtener de otro un atributo
		 * 
		 */
		public static function getObject(object:Object,field:String):Object{
			
			if (object != null && object[field] != null ){
				return object[field];
			}
			throw Error("Error[DPC-FIH-0012.02] No se encontro el campo["+field+"] en el contexto en " + ReflectionHelper.object2XML(object,"object"));
			return null;
		}
		/**
		 * ESte metodo nos ayuda a settear un valor en algun campo de un objeto  
		 */
		public static function setObject(object:Object,field:String,value:Object):void {
			if (object != null && object[field] != null ){
				object[field] = value;
			}
		}
		
		/**
		 * Metodo que nos ayuda a hacer un mappeo de un ArrayCollection de datos que sus parametros tienen SNAKE_UPPER_CASE  
		 * y los asigna al objeto de la clase que se recibe como parametro , pero los settea con lowerCamelCase 
		 * 
		 * Es el parametro extraFunctions es un Array que se le pasa directo a la funcion copySimpleParametersAndCast
		 * y regresa un ArrayCollection de Objectos del tipo del parametro clazz
		 * 
		 * @param dataSource es el objeto de donde se sacara el dato
		 * @field es el nombre del parametro que se quiere obtener
		 * @clazz es el tipo de clase donde se van a copiar los datos que tiene los campos de la forma camelCase
		 * @extraFunctions es un array de funciones que ayudan a settear los hijos que son objeto complejos la funcion sera llamada
		 * 					siempre que exista un parametro complejo, de tal forma que la funcion debe validar que esta procesando el 
		 * 					parametro que debe procesar
		 */
		public static function doMapping(dataSource:Object,field:String,clazz:Class, extraFunctions:Array=null):ArrayCollection {
			if (dataSource == null || dataSource[field] == null){
				logger.warning("Error[DPC-FIH-0012.02] No se encontro el campo "+field+" en el contexto");
				return new ArrayCollection();
			}
			
			var dataSources:ArrayCollection =  dataSource[field] as ArrayCollection;
			if (dataSources == null){
				logger.warning("Error[DPC-FIH-0012.02] El campo "+field+" no es ArrayCollection");
				return new ArrayCollection();
			}
			var result:ArrayCollection = new ArrayCollection();
			
			for (var i:int = 0 ; i < dataSources.length ; i ++ ) {
				var data:Object = dataSources.getItemAt(i);
				var newObject:* = new clazz();
				
				ReflectionHelper.copySimpleParametersUpperAsCamel(data,newObject,"",extraFunctions);
				
				
				result.addItem(newObject);	
			}
			return result;
		}
		
		public static var logger:ILogger = LoggerFactory.getLogger("Mapping Helper");
		
		
		/**
		 * Metodo que nos ayuda a hacer un mappeo de un ArrayCollection de datos que sus parametros tienen parametros del tipo camel case
		 * pero que son muy parecidos a los objetos del tipo de datos que se recibe como parametro clazz
		 * 
		 * Es el parametro extraFunctions es un Array que se le pasa directo a la funcion copySimpleParametersAndCast
		 * y regresa un ArrayCollection de Objectos del tipo del parametro clazz
		 * 
		 * @param dataSource es el objeto de donde se sacara el dato
		 * @field es el nombre del parametro que se quiere obtener
		 * @clazz es el tipo de clase donde se van a copiar los datos que tiene los campos de la forma camelCase
		 * @extraFunctions es un array de funciones que ayudan a settear los hijos que son objeto complejos la funcion sera llamada
		 * 					siempre que exista un parametro complejo, de tal forma que la funcion debe validar que esta procesando el 
		 * 					parametro que debe procesar
		 * @reference copySimpleParametersAndCast, makeFirstCamel
		 */
		public static function doMappingSimple(dataSource:Object,field:String,clazz:Class, extraFunctions:Array=null):ArrayCollection {
			if (dataSource == null || dataSource[field] == null){
				logger.warning("Error[DPC-FIH-0012.02] No se encontro el campo "+field+" en el contexto");
				return new ArrayCollection();
			}
			var dataSources:ArrayCollection;
			if (dataSource[field] is Array){
				dataSources = new ArrayCollection( dataSource[field] as Array);
			}
			else {
				dataSources = dataSource[field] as ArrayCollection; 
			}
			 
			if (dataSources == null){
				logger.warning("Error[DPC-FIH-0012.02] El campo "+field+" no es ArrayCollection");
			}
			var result:ArrayCollection = new ArrayCollection();
			
			for (var i:int = 0 ; i < dataSources.length ; i ++ ) {
				var data:Object = dataSources.getItemAt(i);
				var newObject:* = new clazz();
				                 
				ReflectionHelper.copySimpleParametersAndCast(data,newObject,"",extraFunctions);
				
				result.addItem(newObject);	
			}
			return result;
		}
		
	}
}