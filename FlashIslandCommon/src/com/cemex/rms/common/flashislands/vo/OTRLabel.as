package com.cemex.rms.common.flashislands.vo
{
	/**
	 * Este objeto es el que se recibe/recibira con la funcion de FlashIslandUtils e ABAP
	 * 
	 * Este objeto trae las etiquetas para sustituir los textos
	 */
	public class OTRLabel
	{
		public function OTRLabel()
		{
		}
		public var alias:String;
		public var name:String;
		public var value:String;
		public var matchedPrefix:Boolean;
	}
}