package com.cemex.rms.common.flashislands.vo
{
	/**
	 * Este objeto es el que se recibe/recibira con la funcion de FlashIslandUtils e ABAP
	 * 
	 * Este objeto trae los datos necesarios para obtener configuraciones de la TVARVC
	 */
	public class TVARVC
	{
		public function TVARVC()
		{
		}
		
		public var	numb:String;
		public var 	opti:String;
		public var 	name:String;
		public var 	high:String;
		public var 	type:String;
		public var 	low:String;
		public var 	clieIndep:String;
		public var 	sign:String;
	}
}