package com.cemex.rms.common.flashislands
{
	import com.cemex.rms.common.helpers.ContextMenuHelper;
	import com.cemex.rms.common.helpers.FullScreenHelper;
	import com.cemex.rms.common.reflection.ReflectionHelper;
	import com.cemex.rms.common.services.AbstractService;
	import com.cemex.rms.dispatcher.services.flashislands.impl.DispatcherIslandImpl;
	
	import flash.display.StageDisplayState;
	import flash.events.ContextMenuEvent;
	import flash.events.Event;
	import flash.external.ExternalInterface;
	import flash.ui.ContextMenuItem;
	
	import mx.collections.ArrayCollection;
	import mx.core.Application;
	
	import sap.FlashIsland;
	import sap.FlashIslandConst;
	import sap.FlashIslandConstPhases;
	import sap.FlashIslandLoader;

	/**
	 * Este es el servicio basico de un FlashIsland donde se controla  el status del flashisland 
	 * Esta clase trata de abstraer las llamadas a los metodos estaticos del 
	 * com.sap.FlashIsland
	 * Y dar una interface dentro del Service Manager
	 * 
	 * 
	 */
	public class AbstractFlashIslandService extends AbstractService implements IFlashIslandDataListener
	{
		
		public function AbstractFlashIslandService(cleanDataAfterReady:Boolean=false)
		{
			super();
			//mexico buffer
			if(DispatcherIslandImpl.COUNTRY!="EUR"){
				buffer = new DynamicIslandObject(this,getIslandFields(),cleanDataAfterReady);
			} else {
			//europe buffer
				buffer=new StaticIslandObject(this,getIslandFields(),cleanDataAfterReady);
			}
		}
		public function getIslandFields():ArrayCollection{
			return null;
		}
		/**
		 * Este objeto nos ayuda a asegurar que la informacion llega de los flashIslands
		 * 
		 */
		protected var buffer:Object;
		
		/**
		 * Este es el metodo que se manda a llamar por el factory de servicios 
		 */
		public override function startService():void {
			
			var par:*=Application.application.parameters;
			/*if(!Application.application.parameters.hasOwnProperty("IslandLibImplURL")){
				Application.application.parameters["IslandLibImplURL"]="/sap/public/bc/ur/nw7/FlashIslands/FlashIslandLibImpl.swf?LAST_MODIFIED=20110414123428";
				Application.application.parameters["id"]="WD31";
				Application.application.parameters["jsPrefix"]="UCF_AcfAdapter.";
				Application.application.parameters["LAST_MODIFIED"]="20150612180214.";
				Application.application.parameters["themeBase"]="/sap/public/bc/ur/nw5/themes/sap_tradeshow_plus?201312012333";
				Application.application.parameters["traceLevel"]="3";
			}*/
			FlashIsland.register(buffer);
			trace("FlashIsland.register    "+(new Date()))
			FlashIsland.addEventListener(sap.FlashIslandConstPhases.EVENT_END_UPDATE_DATASOURCES, dataUpdatedEnd,false,0,true);
			FlashIsland.addEventListener(FlashIslandConstPhases.EVENT_BEGIN_FREEZE,freezedHandler,false,0,true);
			FlashIsland.addEventListener(FlashIslandConstPhases.EVENT_END_FREEZE,unfreezedHandler,false,0,true);
			
		}
		
		
		/**
		 * Este metodo se manda a llamar para manejar cuando llegaron los datos del FlashIsland 
		 */
		public function dataUpdatedEnd(e:Event):void {
			trace("FlashIsland dataUpdatedEnd    "+(new Date()))
			flashIslandDataReady();
		}	
		
		public static var disableAppWhileEvent:Boolean = false;
		
		/**
		 * Este metodo se manda a llamar cuando el Flash island manda la señal
		 * de congelado 
		 */
		private function freezedHandler(e:Event):void {
			trace("FlashIsland freezedHandler    "+(new Date()))
			frozen = true;
			freezed();
		}	
		
		/**
		 * Este metodo se manda a llamar cuando el flashisland se descongela 
		 */
		private function unfreezedHandler(e:Event):void {
			trace("FlashIsland unfreezedHandler    "+(new Date()))
			//Application.application.enabled = true;
			frozen = false;
			unfreezed();
		}	
		
		/**
		 * Cuado se descongela esta avisando que sela regresa el control al flash
		 * de tal forma que se pueden enviar mas eventos en caso de haber uno encolados 
		 */
		public function unfreezed():void {
			dispatchFirstEvent();
		}
		/**
		 * esta funcion es para que se sobre escriba en caso qeu se quier ahacer algo cuando se descongela
		 */
		public function freezed():void {
		}
		private var frozen:Boolean = false;
			
		/**
		 * Este metodo tiene que ser sobre escrito cuando se quiere hacer un flashisland
		 * 
		 */
		public function flashIslandDataReady():void{
		}
		
		
		
		/**
		 * ESta es la cola de eventos de flashisland, es para asegurar que no se envien dos eventos al m
		 * mismo tiempo 
		 */
		protected var queue:ArrayCollection = new ArrayCollection();
		
		
		/**
		 * Este metodo es el que envia a la cola de flashisland  
		 */
		public function dispatchWDEvent(fio:IFlashIslandEventObject):void {
			var request:WDEventRequest= new WDEventRequest();
			
			request.fio = fio;
			queue.addItem(request);
			if (!frozen) {
				dispatchFirstEvent();
			}	
		}
		
		/**
		 * Este metodo lo que hace es despachar el sguiente evento que este encolado   
		 */
		private function dispatchFirstEvent():void{
			
			if (!frozen){
				if (queue.length > 0){
					var request:WDEventRequest = queue.removeItemAt(0) as WDEventRequest;
					//logger.info("dispatchFirstEvent"+ReflectionHelper.object2XML(request,"request"));
					dispatchWDEventToIsland(request);
				}
			}
		}
		
		/**
		 * Este metodo lanza el evento a los flash islands y es el que convierte a objetos dinamicos, 
		 * dado que e flashisland solamente receibe datos de ese tipo. 
		 * 
		 */
		public function dispatchWDEventToIsland(request:WDEventRequest):void{
			
			logger.info("dispatchWDEventToIsland:" + request.fio.getEventName());
			trace("dispatchWDEventToIsland:" + request.fio.getEventName());
			if (request.fio is SimpleFlashIslandEventObject){
				FlashIsland.fireEvent(this, request.fio.getEventName());
			}
			else {
				var obj:Object=ReflectionHelper.cloneAsDynamicObject(request.fio);
				FlashIsland.fireEvent(this, request.fio.getEventName(),ReflectionHelper.cloneAsDynamicObject(request.fio));
			}
		}
		
		public function dispatchWDRawBytes(request:IFlashIslandEventObject):void{
			
			logger.info("dispatchWDEventToIsland:" + request.getEventName());
			trace("dispatchWDEventToIsland:" + request.getEventName());
			var sendob:Object={fileBytes:request["fileBytes"]};
			FlashIsland.fireEvent(this, request.getEventName(),sendob);
		}
		
		
	}
}