package com.cemex.rms.common.flashislands
{
	/**
	 * Esta interface lo unico que debe hacer es registrar un listener 
	 * de que los datos estan listos
	 * 
	 */
	public interface IFlashIslandDataListener
	{
		function flashIslandDataReady():void;
	}
}