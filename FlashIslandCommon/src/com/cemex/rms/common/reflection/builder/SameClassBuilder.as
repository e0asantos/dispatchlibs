package com.cemex.rms.common.reflection.builder
{
	/**
	 * ESta clase crea una instancia de un el mismo tipo que el objeto que se recibe
	 */
	public class SameClassBuilder implements IObjectBuilder
	{
		public function SameClassBuilder()
		{
		}
		 	
		public function builderFromClass(source:Object):Object {
			var Clazz:Class =  source.constructor;
			return new Clazz();	
		}
	}
}