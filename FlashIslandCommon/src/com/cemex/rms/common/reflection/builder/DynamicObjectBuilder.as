package com.cemex.rms.common.reflection.builder
{
	/**
	 * ESta clase crea una instancia de un objeto generico
	 * 
	 */
	public class DynamicObjectBuilder implements IObjectBuilder
	{
		public function DynamicObjectBuilder()
		{
		}
		public function builderFromClass(source:Object):Object
		{
			return new Object();
		}
	}
}