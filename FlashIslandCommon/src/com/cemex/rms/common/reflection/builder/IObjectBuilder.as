package com.cemex.rms.common.reflection.builder
{
	/**
	 * Esta interface especifica la forma en que se clona un objeto
	 */
	public interface IObjectBuilder
	{
		function builderFromClass(source:Object):Object;
	}
}