package com.cemex.rms.common.reflection
{
	import com.cemex.rms.common.date.FlexDateHelper;
	import com.cemex.rms.common.reflection.builder.DynamicObjectBuilder;
	import com.cemex.rms.common.reflection.builder.IObjectBuilder;
	import com.cemex.rms.common.reflection.builder.SameClassBuilder;
	
	import flash.display.DisplayObject;
	import flash.system.System;
	import flash.utils.ByteArray;
	import flash.utils.describeType;
	import flash.utils.getQualifiedClassName;
	
	import mx.collections.ArrayCollection;
	import mx.containers.HBox;
	import mx.controls.Alert;
	import mx.utils.ObjectUtil;

	
	/**
	 * ESta clase ayuda a hacer operaciones que requieran Reflection 
	 * PAra flex
	 */
	public class ReflectionHelper
	{
		public function ReflectionHelper()
		{
		}
		
		/**
		 * Esta funcion convierte el string que se recibe en el primer parametro en a notacion camello
		 * El string lo transforma de la forma 
		 * 
		 * STRING_DE_PRUEBA -> stringDePrueba 
		 */
		
		private  static function changeUpperCase2Camel(upperCase:String, firstLowerCase:Boolean):String{
			var split:Array =  upperCase.split("_");
			
			var result:String = "";
			var tmp:String="";
			for (var i:int  = 0 ; i < split.length ; i ++){
				 tmp=  split[i] as String;
				if(firstLowerCase && i == 0  ){
					result += tmp.substring(0,tmp.length).toLocaleLowerCase();
				}
				else {
					result += tmp.substring(0,1).toLocaleUpperCase() + tmp.substring(1,tmp.length).toLocaleLowerCase();
				}
				
				
			}
			split=null;
			return result;
		}
		
		/**
		 * ESta funcion convierte la primera letra en Mayuscula o minuscula
		 * si es 
		 * @param name es el string que se va 
		 * @param firstLowerCase es la variable que nos indica si la primera va a ser minuscula o mayuscula
		 */
		private  static function makeFirstCamel(name:String, firstLowerCase:Boolean):String{
			if(firstLowerCase ){
				return  name.substring(0,1).toLocaleLowerCase()+ name.substring(1,name.length);
			}
			else {
				return name.substring(0,1).toLocaleUpperCase() + name.substring(1,name.length);
			}	
		}
		
		/**
		 * Este metodo cambia de notacion camello a  notacion 
		 * pruebaVariable=> PRUEBA_VARIABLE
		 * 
		 */
		private  static function changeCamel2UpperCase(camel:String):String{
			var result:String ="";
			for (var i:int  = 0 ; i < camel.length ; i ++){
				
				var someChar:String = camel.charAt(i);
				var upperChar:String =  someChar.toUpperCase();
				
				if (someChar === upperChar){
					result+= "_";
				}
				result += upperChar;
				
			}
			return result;
		}
		
		private static var dynamicObjectBuilder:IObjectBuilder = new DynamicObjectBuilder();
		private static var sameObjectBuilder:IObjectBuilder = new SameClassBuilder();
		
	
		/**
		 * Este metodo clona un objeto utilizando el mismo tipo de Objeto que se tiene
		 * incluso los objetos anidados
		 */
		public static function cloneObject(source:Object,justSimples:Boolean = false):Object {
			if(source is DisplayObject){
				return null;
			}
			return abstractCloneObject(source,sameObjectBuilder,justSimples);
		}
		
		/**
		 * Este metodo clona un objeto , incluyendo los objetos anidados, pero 
		 * utilizando la clase Object por cada objeto nuevo 
		 */
		public static function cloneAsDynamicObject(source:Object,justSimples:Boolean = false):Object {
			return abstractCloneObject(source,dynamicObjectBuilder,justSimples);
		}
		
		
		/**
		 * Este metodo borra los parametros mx_internal_uid o mas bien lo
		 */
		public static function cleanParameters(source:Object):void{
			/*var result:String = "";
			var objDescriptor:XML=describeType(source);
			var property:XML;
			
			for each(property in objDescriptor.elements("variable")){
			if (property.@name == "mx_internal_uid") {
			source[property.@name] = null;
			}
			}
			for each(property in objDescriptor.elements("accessor")){
			if (property.@name == "mx_internal_uid"){
			source[property.@name] = null;
			}
			}
			for (var p:* in source) {
			if (p == "mx_internal_uid"){
			source[p] = null;
			break;
			}
			}*/
			
			if (source != null && source["mx_internal_uid"] != null){
				source["mx_internal_uid"] = null;
			}
		}
		
		
		
		/**
		 * Este metodo borra los parametros mx_internal_uid o mas bien lo dentro de todos los objetos , arreglos y demas
		 */
		public static function cleanObjectFromUI(source:Object):void 
		{
			var result:Object = null;
			if (source != null){
				var i:int;
				if (source is ArrayCollection){
					
					var arrc:ArrayCollection  = source as ArrayCollection;
					for (i = 0 ; i < arrc.length ; i++ ){
						cleanObjectFromUI(arrc.getItemAt(i));
					}
				}
				else if (source is Array){
					var arr:Array  = source as Array;
					for (i = 0 ; i < arr.length ; i++ ){
						cleanObjectFromUI(arr[i]);
					}
				}
				else if (source is XML){
					//result = new XML((source as XML).toXMLString());
				}
				else if(!ObjectUtil.isSimple(source)) {  
					cleanParameters(source);
				}
				else {
					//Alert.show("cleanSource:"+source);
				}
			}
		}
		
		
		/**
		 * Este metodo regresa de una clase todos nombres de las constantes 'const' que tiene  
		 * la clase que recibe de parametro de entrada
		 */
		public static function getConstNames(clazz:Class,startWith:String=""):ArrayCollection{

			var result:ArrayCollection =  new ArrayCollection();
			
			var objDescriptor:XML=describeType(clazz);
			var property:XML;
			
			for each(property in objDescriptor.elements("constant")){
				var name:String = property.@name;
				
				if (name.indexOf(startWith) == 0){
					result.addItem(property.@name);	
				}
			}
			objDescriptor=null;
			property=null;
			return result;
			
		}
		
		/**
		 * Este metodo clona un objeto pero recibe como parametor el tipo de buiilder que utilizara cuando haga el clone
		 * ademas puede copuar los valores simples nada mas
		 */
		private static function abstractCloneObject(source:Object, builder:IObjectBuilder, justSimples:Boolean):Object 
		{
			var result:Object = null;
			var bufferObject:ByteArray=new ByteArray();
			if (source != null){
				var i:int;
				if (source is ArrayCollection && !justSimples){
					
					/*var arrc:ArrayCollection  = source as ArrayCollection;
					var newArrc:ArrayCollection =  new ArrayCollection();
					for (i = 0 ; i < arrc.length ; i++ ){
						newArrc.addItemAt(abstractCloneObject(arrc.getItemAt(i),builder,justSimples),i);
					}*/
					bufferObject.writeObject(source);
					bufferObject.position=0;
					result = bufferObject.readObject();
					//result = newArrc;
				}
				else if (source is Array && !justSimples){
					var arr:Array  = source as Array;
					//var newArr:Array =  new Array();
					
					/*for (i = 0 ; i < arr.length ; i++ ){
						newArr.push(abstractCloneObject(arr[i],builder,justSimples));
					}*/
					bufferObject.writeObject(source);
					bufferObject.position=0;
					result = bufferObject.readObject();
				}
				else if (source is Date){
					//result = FlexDateHelper.copyDate(source as Date);
					bufferObject.writeObject(source);
					bufferObject.position=0;
					result = bufferObject.readObject();
				}
				else if (source is XML){
					result = new XML((source as XML).toXMLString());
				}
				else if(!ObjectUtil.isSimple(source) && !justSimples) {  
					result = builder.builderFromClass(source);
					copyParameters(source,result,builder);
				}
				else {	
					if(ObjectUtil.isSimple(source)) {
						result = source;
					}
					else {
						
						//Alert.show("source:"+source);
					}
				}
			}
			
			return result;
		}
		/**
		 * Esta funcion tiene como objetivo copiar los valores de un objeto a otro eliminando el mx_internal_uid de tal forma que 
		 * pueda sustituirse los objetos y no tengan un mismo id cuando se grafican  
		 * 
		 * 
		 */
		public static function copyParameters(source:Object, target:Object,builder:IObjectBuilder=null):void{
			var result:String = "";
			var objDescriptor:XML=describeType(source);
			var property:XML;
			var buffer:ByteArray = new ByteArray();
			var initialObject:Object=null;
			if (builder == null){
				builder = sameObjectBuilder;
			}
			for each(property in objDescriptor.elements("variable")){
				initialObject=cloneObject(source[property.@name]);
				if (property.@name != "mx_internal_uid" && initialObject!=null && property.@name!="graphicReference") {
					target[property.@name] = initialObject;
				}
			}
			for each(property in objDescriptor.elements("accessor")){
				initialObject=cloneObject(source[property.@name]);
				if (property.@name != "mx_internal_uid" && initialObject!=null){
					target[property.@name] = initialObject;
				}
			}
			for (var p:* in source) {
				initialObject=cloneObject(source[p]);
				if (p != "mx_internal_uid" && initialObject!=null){
					target[p] = initialObject;
				}
			}
		}
		
		/**
		 * Esta funcion tiene como objetivo copiar los valores de un objeto a otro eliminando el mx_internal_uid de tal forma que 
		 * pueda sustituirse los objetos y no tengan un mismo id cuando se grafican  
		 * 
		 */
		public static function copySimpleParameters(source:Object, target:Object,builder:IObjectBuilder=null):void{
			var result:String = "";
			var objDescriptor:XML=describeType(source);
			var property:XML;
			if (builder == null){
				builder = sameObjectBuilder;
			}
			for each(property in objDescriptor.elements("variable")){
				if (property.@name != "mx_internal_uid") {
					target[property.@name] = cloneObject(source[property.@name],true);
				}
			}
			for each(property in objDescriptor.elements("accessor")){
				
				if (property.@name != "mx_internal_uid"){
					target[property.@name] = cloneObject(source[property.@name],true);
				}
			}
			for (var p:* in source) {
				if (p != "mx_internal_uid"){
					target[p] = cloneObject(source[p],true);
				}
			}
			objDescriptor=null;
			property=null;
			
		}
		
		/**
		 * Este metodo copia los parametros simples y convierte los nombres en notacion camello
		 * 
		 * @reference makeFirstCamel
		 */
		public static function copySimpleParametersAndCast(source:Object, target:Object, targetPrefix:String="",extraFunctions:Array=null):void{
			
			//var objDescriptor:XML=describeType(source);
			//var property:XML;
			var buffer:ByteArray = new ByteArray();
			for (var p:* in source) {
				
				var param:String = ""+ p;
			
				var sourceValue:*= source[param];
				var hasPreffix:Boolean = targetPrefix != "";
				var newParamName:String = targetPrefix + makeFirstCamel(param,!hasPreffix);
				if (target.hasOwnProperty(newParamName)){
					
					if(ObjectUtil.isSimple(sourceValue)) {
						if (sourceValue != null){
							target[newParamName] = sourceValue;
						}
					}
					else if (sourceValue is Date) {
						target[newParamName] = lowClone(sourceValue);//FlexDateHelper.copyDate(sourceValue as Date);
					}
					else {
						if (extraFunctions != null){
							
							var obj:Object = null;
							for (var j:int = 0 ; j < extraFunctions.length ; j++ ) {
								var extraObject:Function = extraFunctions[j] as Function;
								obj = extraObject(source,param);
								if (obj != null){
									break;
								}
							}
							if (obj != null){
								target[newParamName] = obj;
							}
						}
					}
				}
			}
		}
		
		public static function lowClone(source:Object):Object{
			var buffer:ByteArray=new ByteArray();
			buffer.writeObject(source);
			buffer.position=0;
			return buffer.readObject();
		}
		
		/**
		 * Este metodo copia los parametros simples y convierte los nombres en notacion camello
		 * 
		 * @reference changeUpperCase2Camel
		 */
		public static function copySimpleParametersUpperAsCamel(source:Object, target:Object, targetPrefix:String="",extraFunctions:Array=null):void{
			
			//var objDescriptor:XML=describeType(source);
			//var property:XML;
			for (var p:* in source) {
				
				var param:String = ""+ p;
				var upperParam:String = param.toUpperCase();
				var sourceValue:*= source[param];
				
				if (param === upperParam) {
					 
					var hasPreffix:Boolean = targetPrefix != "";
					var newParamName:String = targetPrefix + ReflectionHelper.changeUpperCase2Camel(param,!hasPreffix);
					if (target.hasOwnProperty(newParamName)){
						
						if(ObjectUtil.isSimple(sourceValue)) {
							
							target[newParamName] = sourceValue;
						}
						else if (sourceValue is Date) {
							target[newParamName] = FlexDateHelper.copyDate(sourceValue as Date);
						}
						else {
							if (extraFunctions != null){
								
								var obj:Object = null;
								for (var j:int = 0 ; j < extraFunctions.length ; j++ ) {
									var extraObject:Function = extraFunctions[j] as Function;
									obj = extraObject(source,param);
									if (obj != null){
										break;
									}
								}
								if (obj != null){
									target[newParamName] = obj;
								}
							}
						}
					}
				}
				
			}
		}
		
		/**
		 * Este metodo copia los parametros que se reciben en una lista
		 * 
		 */
		public static function copyParameterFromList(source:Object, target:Object,fields:ArrayCollection, max:int=-1):void{

			if (max == -1){
				max = fields.length;
			}
			for (var i:int  = 0 ;  i < max  ; i++) {
				var field:String = fields.getItemAt(i) as String;
				if (field != "mx_internal_uid") {
					target[field] = cloneObject(source[field]);
				}
			}
		}
		
		/**
		 * Este metodo copia los atributos de un objeto a otro
		 * 
		 */
		public static function compareObject(source:Object, target:Object):Object 
		{
			
			if (getQualifiedClassName(source) != getQualifiedClassName(target)){
				return false;
			}
			
			var result:Object = null;
			if (source != null){
				var i:int;
				if (source is ArrayCollection){
					var arrcS:ArrayCollection  = source as ArrayCollection;
					var arrcT:ArrayCollection  = target as ArrayCollection;
					if (arrcS.length != arrcT.length){
						return false;
					}
					for (i = 0 ; i < arrcS.length ; i++ ){
						if(!compareObject(arrcS.getItemAt(i),arrcT.getItemAt(i))){
							return false;
						}
					}
				}
				else if (source is Array){
					
					var arrS:Array  = source as Array;
					var arrT:Array  = target as Array;
					if (arrS.length != arrT.length){
						return false;
					}
					for (i = 0 ; i < arrcS.length ; i++ ){
						if(!compareObject(arrS[i],arrT[i])){
							return false;
						}
					}
				}
				else if (source is XML){
					return (target as XML).toXMLString() == (source as XML).toXMLString();
				}
				else if(!ObjectUtil.isSimple(source)) {  
					
					return compareObjectProperties(source,target);
				}
				else {
					return target == source;
				}
			}
			
			return result;
		}
		
		
		/**
		 * Este metodo compara todas las variables, getters y setters , y las variablesdinamicas de un 
		 * objeto a otro
		 * 
		 * @reference compareObject
		 */
		private static function compareObjectProperties(source:Object, target:Object):Boolean{
			
			
			
			var result:String = "";
			var objDescriptor:XML=describeType(source);
			var property:XML;
			
			for each(property in objDescriptor.elements("variable")){
				if (property.@name != "mx_internal_uid") {
					if (!compareObject(source[property.@name],target[property.@name])){
						return false;
					}
				}
			}
			for each(property in objDescriptor.elements("accessor")){
				
				if (property.@name != "mx_internal_uid"){
					if (!compareObject(source[property.@name],target[property.@name])){
						return false;
					}
				}
			}
			for (var p:* in source) {
				if (p != "mx_internal_uid"){
					if (!compareObject(source[p],target[p])){
						return false;
					}
				}
			}
			return true;
		}
		
		
		/**
		 * Este metodo convierte un objeto a un string de XML
		 * 
		 */
		public static function object2XML(obj:Object,name:String="node",indent:String=""):String 
		{
			
			
			var result:String="";
			
			var clazz:String = "";
			
			//result += p + ":" + obj[p] +"("+getQualifiedClassName(obj[p])+")"+ "\t";
			clazz =  getQualifiedClassName(obj);
			
			
			if (obj is ArrayCollection){
				result += transformArrayCollectionToXML(obj,name,indent +"  ");
			}
			else if (obj is Array){
				result += transformArrayToXML(obj,name,indent +"  ");
			}
			else if (obj is XML){
				result += (obj as XML).toXMLString();
			}
			else if (clazz == "sap.core.wd.context::WDContextNode"){
				result += transformObjectToXML(obj,name,indent +"  ");
			} 
			else if(!ObjectUtil.isSimple(obj)) {  
				result += transformFlexClassToXML(obj,name,indent +"  ");
				
			}
			else {
				// es nativo
				if (obj != null ){
					// 
					result += indent+"<"+name+" class=\""+clazz+"\">";
					result += ""+obj;
					result += "</"+name+">\n";
				}
			}
			return result;
		}
		
		/**
		 * este metodo crea un reflector del objeto creando un objeto nuevo en forma de string
		 * para hacer copy/paste y no escribirlo
		 * */
		
		public static function object2ObjectString(obj:Object,name:String="node",indent:String=""):String 
		{
			
			
			var result:String="";
			
			var clazz:String = "";
			
			//result += p + ":" + obj[p] +"("+getQualifiedClassName(obj[p])+")"+ "\t";
			clazz =  getQualifiedClassName(obj);
			clazz=clazz.substr(clazz.indexOf("::")+2);
			
			
			if (obj is ArrayCollection){
				result += transformArrayCollectionToXML(obj,name,indent +"  ");
			}
			else if (obj is Array){
				result += transformArrayToXML(obj,name,indent +"  ");
			}
			else if (obj is XML){
				result += (obj as XML).toXMLString();
			}
			else if (clazz == "WDContextNode"){
				result += transformObjectToXML(obj,name,indent +"  ");
			} 
			else if(!ObjectUtil.isSimple(obj)) {
				clazz="var var1:"+clazz+"=new "+clazz+"();\n";
				var objDescriptor:XML=describeType(obj);
				var property:XML;
				var propertyValue:Object;
				for each(property in objDescriptor.elements("variable")){
					
					propertyValue=obj[property.@name];
					if (propertyValue!=null){
						
						clazz+="var1."+property.@name+"="+propertyValue+";\n";
					}                           
				}
			}
			
			return clazz;
		}
		
		/**
		 * Este metodo convierte un ArrayCollection a un string de XML
		 * 
		 */
		private static function transformArrayCollectionToXML(source:Object,name:String="node",indent:String=""):String{
			var result:String = "";
			var p:String = null;
			
			// 
			result += indent+"<"+name+" class=\""+getQualifiedClassName(source)+"\">";	
			// Se deja un espacio para los siguientes nodos
			var hasAttrs:Boolean = false;
			var arr:ArrayCollection = source as ArrayCollection;
			var tempResult:String = "";
			for (var i:int = 0 ; i < arr.length ; i++) {
				hasAttrs = true;
				try{
					var preResult=object2XML(arr[i],name, indent +"  ");
					
				} catch(e){
					var errorFound:String="error";
					continue;
				}
				tempResult += preResult;//object2XML(arr[i],name, indent +"  ");
			}
			
			if (hasAttrs){
				result += "\n" + tempResult + indent ; 
			}
			result += "</"+name+">\n";
			return result ;
		}
		/**
		 * Este metodo convierte un Array a un string de XML
		 * 
		 */
		private static function transformArrayToXML(source:Object,name:String="node",indent:String=""):String{
			var result:String = "";
			var p:String = null;
			
			
			result += indent+"<"+name+" class=\""+getQualifiedClassName(source)+"\">";	
			// Se deja un espacio para los siguientes nodos
			var hasAttrs:Boolean = false;
			var arr:Array = source as Array;
			var tempResult:String = "";
			for (var i:int = 0 ; i < arr.length ; i++) {
				hasAttrs = true;
				tempResult += object2XML(arr[i],name, indent +"  ");
			}
			
			if (hasAttrs){
				result += "\n" + tempResult + indent ; 
			}
			result += "</"+name+">\n";
			
			return result ;
		}
		
		
		/**
		 * Este metodo convierte un Object a un string de XML
		 * 
		 */
		private static function transformObjectToXML(source:Object,name:String="node",indent:String=""):String{
			var result:String = "";
			var p:String = null;
			result += indent+"<"+name+" class=\""+getQualifiedClassName(source)+"\">";
			var hasAttrs:Boolean = false;
			var tempResult:String = "";
			var ps:Array =  new Array();
			
			for (p in source) {
				ps.push(p);
			}
			ps = ps.sort();
			
			for (var i:int = 0 ; i < ps.length ; i ++) {
				p = ps[i];
				hasAttrs = true;
				tempResult += object2XML(source[p],p, indent +"  ");
			}
			
			if (hasAttrs){
				result += "\n" + tempResult + indent ; 
			}
			result += "</"+name+">\n";
			return result ;
		}
		
		/**
		 * Este metodo convierte un classe de Flex a un string de XML
		 * 
		 */
		private static function transformFlexClassToXML(source:Object,name:String="node",indent:String=""):String 
		{
			var result:String = "";
			var objDescriptor:XML=describeType(source);
			var property:XML;
			var propertyValue:Object;
			
			//class=\""+getQualifiedClassName(source)+"\"
			result += indent+"<"+name+" class=\""+getQualifiedClassName(source)+"\">";	
			
			
			var hasAttrs:Boolean = false;
			var tempResult:String = "";
			var ps:Array =  new Array();
			
			for each(property in objDescriptor.elements("variable")){
				
				propertyValue=source[property.@name];
				if (propertyValue!=null){
					
					ps.push(property.@name);
					//tempResult += object2XML(propertyValue,property.@name, indent +"  ");             
				}                           
				
			}
			for each(property in objDescriptor.elements("accessor")){
				if (property.@access == "readwrite" || property.@access == "readonly"){
					propertyValue=source[property.@name];
					if (propertyValue!=null){
						ps.push(property.@name);
						//tempResult += object2XML(propertyValue,property.@name, indent +"  ");  
						
					}   
				}
			}
			var p:*;
			for (p in source) {
				ps.push(p);
				//tempResult += object2XML(source[p],p, indent +"  ");
			}
			
			ps = ps.sort();
			
			var chale:Array = new Array();
			for (var j:int = 0 ;j < ps.length ; j ++) {
				p = ps[j];
				hasAttrs = true;
				try {
					
					//Alert.show( source + "::p::" + p);
				tempResult += object2XML(source[p],p, indent +"  ");
					chale.push(source + "::p::" + p);
				}
				catch (e:*){
					Alert.show(chale.toString());
					Alert.show( source + "::p::" + p + "::e::" + e);
					
				}
			}
			
			if (hasAttrs){
				result += "\n" + tempResult + indent ; 
			}
			result += "</"+name+">\n";
			return result;
		}
		
		
		
		
		/**
		 * Este metodo convierte un objeto en un string de AS para poder replicar el objeto.
		 * 
		 */
		public static function object2AS(obj:Object,indent:String=""):String 
		{
			
			trace("object2AS()   init");
			var result:String="";
			
			var clazz:String = "";
			
			//result += p + ":" + obj[p] +"("+getQualifiedClassName(obj[p])+")"+ "\t";
			clazz =  getQualifiedClassName(obj);
			trace("getQualifiedClassName()   init");
			
			if (obj is ArrayCollection){
				result += transformArrayCollectionToAS(obj,indent +"  ");
			}
			else if (obj is Array){
				result +=  transformArrayToAS(obj,indent +"  ");
			}
			else if (obj is XML){
				result += "new XML('"(obj as XML).toXMLString()+"')";
			}
			else if (clazz == "sap.core.wd.context::WDContextNode"){
				result += transformObjectToAS(obj,indent +"  ");
			} 
			else if(!ObjectUtil.isSimple(obj)) {  
				result += transformFlexClassToAS(obj,indent +"  ");
				
			}
			else {
				// es nativo
				if (obj != null ){
					// class=\""+clazz+"\"
					result += "/*"+clazz+"*/ "+ transformSimpleObjectToString(obj) + "\n";
				}
			}
			trace("object2AS()   end");
			return result;
		}
		
		/**
		 * Este metodo convierte un Simple objeto en un string de AS para poder replicar el objeto.
		 * 
		 */
		private static function transformSimpleObjectToString(object:Object):String{
			
			if (object == null){
				return "null";
			}
			else if (object is String){
				return '"'+object+'"';
			}
			else if (object is Date){
				var base:Date = object as Date;
				return "new Date("+base.getFullYear()+","+ base.getMonth()+","+ FlexDateHelper.getDayOfMonth(base)+","+ base.getHours() +","+ base.getMinutes()+","+base.getSeconds()+")";
			}
			return "" + object;
		}
		
		/**
		 * Este metodo convierte un Simple ArrayCollection en un string de AS para poder replicar el objeto.
		 * 
		 */
		private static function transformArrayCollectionToAS(source:Object,indent:String=""):String{
			var result:String = "";
			var p:String = null;
			
			// class=\""+getQualifiedClassName(source)+"\"
			result += "new ArrayCollection([";	
			// Se deja un espacio para los siguientes nodos
			var hasAttrs:Boolean = false;
			var arr:ArrayCollection = source as ArrayCollection;
			var tempResult:String = "";
			for (var i:int = 0 ; i < arr.length ; i++) {
				hasAttrs = true;
				tempResult+=indent+"  ";
				if (i!=0){
					tempResult+=",";
				}
				tempResult += object2AS(arr[i],indent +"  ");
			}
			
			if (hasAttrs){
				result += "\n" + tempResult + indent ; 
			}
			result += "])\n";
			return result ;
		}
		
		/**
		 * Este metodo convierte un Array en un string de AS para poder replicar el objeto.
		 * 
		 */
		private static function transformArrayToAS(source:Object,indent:String=""):String{
			var result:String = "";
			var p:String = null;
			
			
			result += "[";	
			// Se deja un espacio para los siguientes nodos
			var hasAttrs:Boolean = false;
			var arr:Array = source as Array;
			var tempResult:String = "";
			for (var i:int = 0 ; i < arr.length ; i++) {
				hasAttrs = true;
				tempResult+=indent+"  ";
				if (i!=0){
					tempResult+=",";
				}
				tempResult += object2AS(arr[i], indent +"  ");
			}
			
			if (hasAttrs){
				result += "\n" + tempResult + indent ; 
			}
			result += "]\n";
			return result ;
		}
		
		
		
		/**
		 * Este metodo convierte un Object en un string de AS para poder replicar el objeto.
		 * 
		 */
		private static function transformObjectToAS(source:Object,indent:String=""):String{
			var result:String = "";
			var p:String = null;
			result += "/*"+getQualifiedClassName(source)+"*/{";
			var hasAttrs:Boolean = false;
			var tempResult:String = "";
		
			
			var ps:Array =  new Array();
			
			for (p in source) {
				ps.push(p);
			}
			ps = ps.sort();
			for (var i:int = 0 ; i < ps.length ; i ++) {
				p = ps[i];
				hasAttrs = true;
				tempResult += indent;
				if (i!=0){
					tempResult+=",";
				}
				tempResult +=  transformSimpleObjectToString(p) + ":" + object2AS(source[p],indent +"  ");
			}
			
			if (hasAttrs){
				result += "\n" + tempResult + indent ; 
			}
			result += "}\n";
			return result ;
		}
		
		
		/**
		 * Este metodo convierte un Flex Class en un string de AS para poder replicar el objeto.
		 * 
		 */
		private static function transformFlexClassToAS(source:Object,indent:String=""):String 
		{
			var result:String = "";
			var objDescriptor:XML=describeType(source);
			var property:XML;
			var propertyValue:Object;
			
			//class=\""+getQualifiedClassName(source)+"\"
			result += "/*"+getQualifiedClassName(source)+"*/{";	
			var i:int = 0;
			
			var hasAttrs:Boolean = false;
			var tempResult:String = "";
			
			var ps:Array =  new Array();
			
			for each(property in objDescriptor.elements("variable")){
				
				propertyValue=source[property.@name];
				if (propertyValue!=null){
					ps.push(property.@name);
					             
				}                           
				
			}
			for each(property in objDescriptor.elements("accessor")){
				if (property.@access == "readwrite" || property.@access == "readonly"){
					propertyValue=source[property.@name];
					if (propertyValue!=null){
						ps.push(property.@name);
					}   
				}
			}
			var p:*;
			for (p in source) {
				ps.push(p);
				//tempResult += transformSimpleObjectToString(p) + ":" + object2AS(source[p], indent +"  ");
			}
			ps = ps.sort();
			
			for (var j:int = 0 ;j < ps.length ; j ++) {
				p = ps[j];
				hasAttrs = true;
				
				tempResult += indent;
				if (j != 0){
					tempResult+=",";
				}
				tempResult += transformSimpleObjectToString(p) + ":" + object2AS(source[p], indent +"  ");
			}
			
			
			if (hasAttrs){
				result += "\n" + tempResult + indent ; 
			}
			objDescriptor=null;
			property= null;
			result += "}\n";
			return result;
		}
		
	}
	
}


