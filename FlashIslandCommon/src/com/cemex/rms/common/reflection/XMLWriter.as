package com.cemex.rms.common.reflection
{
	/**
	 * Esta clase tiene como objetivo ayudar a crear objetos del tipo XML
	 */
	public class XMLWriter
	{
		public var xml:XML;
		
		public function XMLWriter()
		{
			xml=<obj/>;
		}       
		
		/**
		 * Esta clase regresa un objetoXML con el nombre que se especifica en el parametro 
		 * propertyName y se le agrega el valor del segundo parametro propertyValue
		 * @param propertyName el nombre del tag
		 * @param propertyValue el valor que se tendra
		 * @return regresa el objeto XML
		 */
		public function addProperty(propertyName:String, propertyValue:String):XML {
			
			var xmlProperty:XML=<new/>
			xmlProperty.setName(propertyName);
			xmlProperty.appendChild(propertyValue);
			xml.appendChild(xmlProperty);
			return xmlProperty;
		}  
	}
}