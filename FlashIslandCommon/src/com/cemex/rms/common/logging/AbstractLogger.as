package com.cemex.rms.common.logging
{
	import flash.system.System;
	
	/**
	 * Esta clase nos da una implementacion abstracta de
	 * 
	 */
	public class AbstractLogger implements ILogger
	{
		public function AbstractLogger(name:String) {
			this.name = name;
		}
		
		private var name:String;
		public function setLoggerOff():void{
			loggerOff = true;
		}
		/**
		 * Este es un abstract method, que nos define donde se va a implementar la forma de loggear
		 * 
		 */ 
		public function logImpl(value:String):void{
			throw new Error("La clase AbstractLogger debe sobre escribirse el metodo logImpl");		
		}
		/**
		 * Se loggean mensajes informativos
		 */
		public function info(value:String):void{
			//trace("info",value)
			appendLine("info",value);
		}
		/**
		 * Se loggean mensajes de Debug
		 */
		public function debug(value:String):void{
			//trace("debug",value);
			appendLine("debug",value);
		}
		/**
		 * Se loggean warnings
		 */
		public function warning(value:String):void{
			//trace("warning",value);
			appendLine("warning",value);
		}
		/**
		 * Se loggean errores
		 */
		public function error(value:String):void{
			//trace("error",value);
			appendLine("error",value);
		}
		public var loggerOff:Boolean = false;
		/**
		 * Este metodo es el que arma el mensaje para que se loggee
		 * Le adjunta el tiempo, la referenca, el nombre y el valor
		 */
		public function appendLine(reference:String,value:String):void{
			var now:Date = new Date();
			//trace("PUSH INCOMING");
			if (!loggerOff){
				//trace(value.substr(value.indexOf("orderNumber"),40));
				//logImpl("<time>" + now + "</time>" + "<" + reference + "/>" + "<" + name + "/>" + "<payload>" + value + "</payload>");
				trace("<time>" + now + "</time>" + "<" + reference + "/>" + "<" + name + "/>" + "<payload>" + value + "</payload>");
			}
		}

		
		
	}
}