package com.cemex.rms.common.logging
{
	
	/**
	 * Esta interface define un elemento de loggeo
	 * para poder switchear entre cualquier tipo de logger
	 * que se requiera
	 */
	public interface ILogger
	{
		/**
		 * este metodo es utilizado para loggear informacion de la aplicacion
		 */
		function info(value:String):void;
		
		/**
		 * eeste metodo es utilizado para loggear informacion que pueda servir para debuggear
		 */
		function debug(value:String):void;
		
		/**
		 * este metodo es utilizdo para loggear mensajes de warning
		 */
		function warning(value:String):void;
		
		/**
		 * este metodo es utilizado para loggear errores
		 */
		function error(value:String):void;
		
		function setLoggerOff():void;
	}
}