package com.cemex.rms.common.logging.impl
{
	import com.cemex.rms.common.logging.AbstractLogger;
	/**
	 * ESte logger guarda localmente en un string todos los eventos del log que se generen 
	 * 
	 * @deprecated
	 */
	public class StringLogger extends AbstractLogger {
		public function StringLogger(name:String)
		{
			super(name);
		}
		
		
		public var log:String = "";
		public override function logImpl(value:String):void{
			log += value + "\n";
		}
	}
}