package com.cemex.rms.common.logging.impl
{
	import mx.controls.Alert;
	import com.cemex.rms.common.logging.AbstractLogger;

	/**
	 * Esta clase es la implementacion de un logger utilizando Alert
	 * Este no es recomendado usarlo para quese despliegue la inforamcion temporalmente 
	 * 
	 */
	public class AlertLogger extends AbstractLogger {
		public function AlertLogger(name:String)
		{
			super(name);
		}
		
		
		public var log:String = "";
		/**
		 * Esta implementacion lanza un Alert.show
		 */
		public override function logImpl(value:String):void{
			Alert.show(value);
		}
	}
}