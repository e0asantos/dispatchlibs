package com.cemex.rms.common.logging.impl
{
	import com.cemex.rms.common.logging.AbstractLogger;

	/**
	 * esta clase es la implementacion de un logger del tipo trace, el cual nos da la opcion de ver los 
	 * logs cuando se debuggean
	 * 
	 */
	public class TraceLogger extends AbstractLogger
	{
		public function TraceLogger(name:String)
		{
			super(name);
		}
		/**
		 * Esta implementacion lanza un trace
		 */
		public override function logImpl(value:String):void{
			trace(value);
		}
	}
}