package com.cemex.rms.common.logging.impl
{
	import com.cemex.rms.common.logging.AbstractLogger;
	import com.cemex.rms.common.logging.ILogger;
	import com.cemex.rms.common.logging.events.LoggerEvent;
	import com.cemex.rms.common.utils.DictionaryMap;
	
	import flash.events.IEventDispatcher;
	
	import mx.controls.Alert;
	/**
	 * Este es un logger que nos permite ver todos los datos loggeos en forma de evento, de tal forma que pueden ser capturados l
	 * en cualquier padre del dispatcher que se este utilizando
	 * de tal forma que se puede llegara ver un mensaje de loggeo en algun componente
	 * que vaya a desplegar la informacion
	 * 
	 * 
	 * 
	 */
	public class EventLogger extends AbstractLogger
	{
		public static var staticDispatcher:IEventDispatcher;
		
		private var id:String;
		
		public var dispatcher:IEventDispatcher;
		
		
		public function EventLogger(name:String)
		{
			super(name);
		}
		
		/**
		 * esta es la implementacion que va a utilizar el dispatcher local, en caso de no tener
		 * un dispatcher local utilizara el  dispatcher staticDispatcher de esta misma clase
		 * si el staticDispatcher es null, no hace nada
		 * 
		 */
		public override function logImpl(value:String):void{
			
			
			if (dispatcher != null){
				dispatcher.dispatchEvent(new LoggerEvent(LoggerEvent.LOGGER_EVENT,value));
			}	
			else {
				if (staticDispatcher != null){
					staticDispatcher.dispatchEvent(new LoggerEvent(LoggerEvent.LOGGER_EVENT,value));
				}	
			}
		}
		
		/**
		 * Este metodo nos ayuda a definir si se va a settear el dispatcher en el logger, 
		 * en caso de que el dispatcher sea diferente de null
		 * 
		 */
		public static function setDispatcherIfEventLogger(ilogger:ILogger , dispatcher:IEventDispatcher):void{
			var logger:EventLogger = ilogger as EventLogger;
			if (logger != null && dispatcher != null){
				logger.dispatcher = dispatcher;
			}
		}
	}
}