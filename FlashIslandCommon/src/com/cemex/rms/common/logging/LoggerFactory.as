package com.cemex.rms.common.logging
{
	import com.cemex.rms.common.logging.impl.AlertLogger;
	import com.cemex.rms.common.logging.impl.EventLogger;
	import com.cemex.rms.common.logging.impl.StringLogger;
	import com.cemex.rms.common.logging.impl.TraceLogger;

	/**
	 * Esta clase es un patron tipo Factory que nos permite switchear del tipo de logger que se va a autilizar
	 * 
	 * Por default se utiliza el Trace pero, en general es recomendable usar el EventLogger para
	 * que se pueda desplegar la informacion en cualquier componente visual
	 * 
	 * 
	 */
	public class LoggerFactory
	{
		public function LoggerFactory()
		{
		}
		public static const LOGGER_TYPE_ALERT:String = "ALERT_TYPE";
		public static const LOGGER_TYPE_TRACE:String = "TRACE_TYPE";
		public static const LOGGER_TYPE_STRING:String = "STRING_TYPE";
		public static const LOGGER_TYPE_EVENT:String = "EVENT_TYPE";
		
		public static var defaultLogger:String=LOGGER_TYPE_TRACE;
		
		/**
		 * Este metodo crea una instancia del logger deacuerdo al typo que se reciba como parametro
		 * 
		 * a menos que el default cambie, o que el parametro sea null el logger que se va a 
		 * crear es del tipo LOGGER_TYPE_TRACE 
		 * 
		 */
		public static function getLogger(name:String, type:String=null):ILogger {
			if (type==null){
				type=defaultLogger;
			}
			
			if (type == LOGGER_TYPE_ALERT){
				return new AlertLogger(name) as ILogger;
			}
			else if (type == LOGGER_TYPE_STRING){
				return new StringLogger(name) as ILogger;
			}
			else if(type == LOGGER_TYPE_EVENT){
				
				return new EventLogger(name) as ILogger;
			}
			else {
				return new TraceLogger(name) as ILogger;
			}
		}
	}
}