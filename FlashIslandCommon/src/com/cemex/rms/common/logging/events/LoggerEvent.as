package com.cemex.rms.common.logging.events
{
	import com.cemex.rms.common.logging.impl.EventLogger;
	import com.cemex.rms.common.services.IService;
	import com.cemex.rms.common.services.loader.ServiceLoadStatus;
	
	import flash.events.Event;
	/**
	 * Este evento es el evento de log que se utilza para hacer llegar la informacion a otras 
	 * capas 
	 */
	public class LoggerEvent extends Event
	{
		public static const LOGGER_EVENT:String 	= "loggerEvent";
		public static const LOGGER_EVENT_INIT:String 	= "loggerEventInit";
		
		
		public var log:String;
		public var logger:EventLogger;
		
		public function LoggerEvent(type:String, log:String, logger:EventLogger=null, bubbles:Boolean = true, cancelable:Boolean = true) {
			super(type, bubbles, cancelable);
			this.log = log;
			this.logger = logger;
		}

	}
}
