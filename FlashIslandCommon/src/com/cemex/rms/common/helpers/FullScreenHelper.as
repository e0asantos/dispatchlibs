package com.cemex.rms.common.helpers
{
	import flash.display.StageDisplayState;
	import flash.external.ExternalInterface;
	
	import mx.core.Application;
	/**
	 * Esta clase tiene como objetivo injectar un poco de codigo
	 * 
	 * 
	 */
	public class FullScreenHelper
	{
		public function FullScreenHelper()
		{
		}
		/**
		 * Este metodo lo que hace es injectar la variable allowFullScreen al swf container, 
		 * en una arquitectura de FlashIslands 
		 * 
		 */
		public static function prepareFlashIslandFullScreen():void{
			var paramObj:Object = Application.application.parameters;
			if (paramObj != null && paramObj["Id"] != null){
				ExternalInterface.call("eval","var o = document.getElementById('"+paramObj["Id"]+"');if (o != null){o['allowFullScreen'] = true;}");
			}
		}
		
		/**
		 * Esta funcion nos ayuda a cambiar la pantalla de fullscreeen y de regreso, 
		 * Esto funciona en Flex 3.3, talvez en otras versiones tambien
		 */
		public static function toggleFullScreen():void {
			try {
				switch (Application.application.systemManager.stage.displayState) {
					case StageDisplayState.FULL_SCREEN:
						Application.application.systemManager.stage.displayState = StageDisplayState.NORMAL; 
						break;
					default:
						Application.application.systemManager.stage.displayState = StageDisplayState.FULL_SCREEN;
						break;
				}
			} catch (err:SecurityError) {
				
			}
		}

	}
}