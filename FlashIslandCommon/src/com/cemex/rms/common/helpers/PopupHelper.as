package com.cemex.rms.common.helpers
{
	import com.cemex.rms.common.utils.DictionaryMap;
	
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Menu;
	import mx.core.Application;
	
	/**
	 * 
	 * 
	 * 
	 */
	public class PopupHelper 
	{
		public function PopupHelper()
		{
		}
		
		public static const MENU_ACTION_EVENT:int=1;
		public static const MENU_ACTION_FUNCTION:int=2;
	
		/**
		 * Esta funcion crea un xml usando como tagname el parametro que se recibe como parametro
		 */
		public static  function getXML(name:String):XML{
			var xml:XML = <new />;
			xml.setName(name);
			return xml;
		}
		
		public static  function getMenuItemSepataror():XML{
			return getMenuItem(null,null,null,null,4,true,"separator");
		}
		public static  function getMenuItem(label:String,data:*,extra:String,ref:String,callType:int,enabled:Boolean=true, type:String=null,checked:Boolean=true,icon:String=null):XML{
			var result:XML = getXML("menuitem");
			result.@label =  label;
			result.@ref =  ref;
			result.@callType =  callType;
			result.@data = data;
			result.@extra = extra;
			result.@enabled = enabled;
			result.@icon = icon;
			
			if (type != null){
				result.@type=type;
				result.@toggled = ""+checked;
			}
			return result;
		}
		
		/***
		 * Esta funcion nos ayuda a crear un Menu a partir de algun objeto
		 * Esta funcion lo que hace es desplegar el Menu sin que se salga de la parte visible de la pantalla
		 * 
		 * @param event es la referencia para saber en que punto de la pantalla se hizo click
		 * @param labelField este es el campo del XML que se utilizara para desplegar las etiquetas (normalmente es @label)
		 * @param data es el XML que se utilizará para mostrar el menu
		 * @param iconFunction es la funcion que recibirá el XML y dirá que icono se mostrara en el popup menu
		 */
		public static  function popupFromXML(event:MouseEvent,labelField:String,data:Object,iconFunction:Function = null):Menu{
			
			var appW:int = Application.application.width;
			var appH:int = Application.application.height;
			
			var pt:Point = new Point(event.localX, event.localY);
			pt = event.target.localToGlobal(pt);
			
			
			var menu:Menu = Menu.createMenu(null, data, false);
			menu.labelField = labelField
			/*
			var list:ArrayCollection = iconsMap.getAvailableKeys();
			for (var i:int = 0 ; i < list.length ; i ++){
				var name:String = list.getItemAt(i) as String;
				menu.setStyle(name, iconsMap.get(name));
			}
				*/
			if (iconFunction != null){
				menu.iconFunction = iconFunction;
			}
			
			
			//menu.iconField = iconField;
			
			menu.show(pt.x, pt.y);
			
			//Alert.show("popw:"+myMenu.width +  ", poph:"+myMenu.height);
			var menuW:int = menu.width;
			var menuH:int = menu.height;
			// el popup no se va a ver completo
			
			var menuPos:Point = new Point(); 
			if (pt.x + menuW < appW){
				menuPos.x = pt.x-11;	
			}
			else {
				// el popup se puede ver si se recorre a la izquierda
				menuPos.x = appW - menuW;
			}
			
			if (pt.y + menuH < appH){
				menuPos.y = pt.y-11;
			}
			else {
				// el popup se puede ver si se recorre a la izquierda
				menuPos.y = appH - menuH;
			}
			
			menu.x = menuPos.x;
			menu.y = menuPos.y;
			return menu;
		}
		
	}
}