package com.cemex.rms.common.helpers
{
	import mx.controls.Alert;
	
	/**
	 * Esta funcion trata de injectar Codigo javascript
	 * 
	 */
	public class ScriptInjector
	{
		public function ScriptInjector()
		{
		}
		import flash.external.ExternalInterface;
		
		public static function test():void{
			
			var vbscript:XML = <vbscript>
				<![CDATA[			
				alert(' CreateObject');
				
				]]>
				</vbscript>;
			VariableScript(vbscript,"vbscript","window.execScript");
			
		}
		public static function InjectVB(vbXML:XML):* {
			
			return InjectAlertScript(vbXML,"vbscript");
		}
		public static function InjectJS(vbXML:XML):* {
			return InjectEvalScript(vbXML,"javascript");
		}
		public static function InjectExecScript(vbXML:XML,type:String):* {
			return InjectScript(vbXML,type,"window.execScript");
		}
		public static function InjectEvalScript(vbXML:XML,type:String):* {
			return InjectScript(vbXML,type,"eval");
		}
		public static function InjectAlertScript(vbXML:XML,type:String):* {
			return InjectScript(vbXML,type,"alert");
		}
		
		
		public static function InjectScript(vbXML:XML,type:String,evalFunction:String):* {
			var CRLF:String = String.fromCharCode(13)+String.fromCharCode(10);
			var vb:String = vbXML.toString();
			var vb_arr:Array = vb.split(CRLF);
			var jsvb:String="function(){" + CRLF;
			jsvb += " var temp='';" + CRLF;
			for (var i:int = 0; i <vb_arr.length; i++) {
				var vbTemp:* = vb_arr[i];
				jsvb+=" temp+=('" + vbTemp + "' + String.fromCharCode(13));" + CRLF;
			}
			jsvb+= " "+evalFunction+"(temp, '"+type+"');" + CRLF;
			jsvb+= "}";
			Alert.show(jsvb);
			
			return ExternalInterface.call(jsvb);
		}
		
		public static function WritelnScript(vbXML:XML,type:String,evalFunction:String):* {
			var CRLF:String = String.fromCharCode(13)+String.fromCharCode(10);
			var vb:String = vbXML.toString();
			var vb_arr:Array = vb.split(CRLF);
			
			
			
			var jsvb:String="function(){" + CRLF;
			
			jsvb += "document.write('<script language=\"vbscript\">' + String.fromCharCode(13));"+ CRLF;
			for (var i:int = 0; i <vb_arr.length; i++) {
				var vbTemp:* = vb_arr[i];
				jsvb+=" document.write(\"" + vbTemp + "\" + String.fromCharCode(13));" + CRLF;
			}
			jsvb += 'document.write("</script>");';
			jsvb+= "}";
			Alert.show(jsvb);
			
			return ExternalInterface.call(jsvb);
		}
		
		
		public static function VariableScript(vbXML:XML,type:String,evalFunction:String):* {
			var CRLF:String = String.fromCharCode(13)+String.fromCharCode(10);
			var vb:String = vbXML.toString();
			var vb_arr:Array = vb.split(CRLF);
			
			
			
			var jsvb:String="";//function(){" + CRLF;
			
			//jsvb += "var temp = '<script language=\"vbscript\">' + String.fromCharCode(13);"+ CRLF;
			for (var i:int = 0; i <vb_arr.length; i++) {
				var vbTemp:* = vb_arr[i];
				jsvb+= vbTemp + CRLF;
			}
			//jsvb += 'temp+="</script>";' + CRLF;
			//jsvb += 'eval(temp);';
			
			jsvb+= "}";
			Alert.show(jsvb);
			
			return ExternalInterface.call("eval",jsvb);
		}
		
	}
}