package com.cemex.rms.common.helpers
{
	import flash.events.ContextMenuEvent;
	import flash.events.Event;
	import flash.ui.ContextMenuItem;
	
	import mx.core.Application;

	/**
	 * Este helper nos ayuda a crear menus contextuales
	 * con una serie de metodos
	 * 
	 */
	public class ContextMenuHelper
	{
		public function ContextMenuHelper()
		{
		}
		
		/**
		 * Este metodo Crea un ContextMenuItem  lo agrega al menu contextual y le settea el listener que se recibe como parametro
		 * 
		 * @param text es el texto que aparece en el menu contextual
		 * @param handler es la funcion listener que se manda a llamar cuando le dan click al menu contextual
		 * @param separator para ver si va a haber un separador en el menu 
		 * @param enabled Para ver si va a estar habilitado 
		 * @param visible para definir si esta visible esta opcion
		 */
		public static function addContextMenuOption(text:String, handler:Function, separator:Boolean=false,enabled:Boolean=true,visible:Boolean = true):void{
			var menu:ContextMenuItem = new ContextMenuItem(text,separator,enabled,visible);  
			Application.application.contextMenu.customItems.push(menu);
			menu.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, handler,false,0,true);
		}
		public static function cleanMenu():void{
			Application.application.contextMenu.customItems=[];
		}
	}
}