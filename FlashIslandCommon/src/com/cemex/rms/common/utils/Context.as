package com.cemex.rms.common.utils
{
	import mx.collections.ArrayCollection;

	/**
	 * Esta clase tiene como objetivo crear un context, que realmente tiene cmo objetivo 
	 * definir el tipo de dato Object para almacenar, y regresar un valor
	 * 
	 * 
	 */	
	public class Context extends DictionaryMap implements IContext
	{
		public function Context() {
			super();
		}
		
		/**
		 * Regresa todas las llaves disponibles en el contexto  
		 */
		public function getKeys():ArrayCollection {
			return super.getAvailableKeys();
		}
		/**
		 * regresa el objeto segun la llave que se re
		 */
		public function getValue(key:String):Object{
			return super.get(key);
		}
		
		/**
		 * Este metodo almacena una variable en el contexto
		 */
		public function setValue(key:String,value:Object):void{
			super.put(key,value);
		}
		
		
	}
}