package com.cemex.rms.common.utils
{
	import mx.collections.ArrayCollection;

	/**
	 * Este Componente tiene como objetivo almacenar datos de tal forma 
	 * que se genere un contexto de servicios en la aplicacion
	 * 
	 */
	public interface IContext
	{
		
		/**
		 * Este metodo tiene como objetivo listar todas las llaves que se tienen actualmente
		 * 
		 */
		function getKeys():ArrayCollection;
		
		/**
		 * Este metodo tiene como objetivo devolver el valor que se almacenó anteriormente
		 * y en caso de que no se haya guardado valor regresará null
		 */
		function getValue(key:String):Object;
		
		/**
		 * Este metodo tiene como objetivo settear un valor en el contexto 
		 * 
		 */
		function setValue(key:String,value:Object):void;
		
		/**
		 * este metodo remueve un valor, que tenga la llave que se esta pasando como parametro
		 * 
		 */
		function remove(key:String):void;
	}
}