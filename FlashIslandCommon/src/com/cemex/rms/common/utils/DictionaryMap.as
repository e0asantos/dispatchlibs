package com.cemex.rms.common.utils
{
	import flash.utils.Dictionary;
	
	import mx.collections.ArrayCollection;
	
	/**
	 * Esta clase sirve como un elemento parecido al HashMap de java
	 * Tiene metodos put,remove y para almacenar al estilo un properties 
	 * 
	 */
	public class DictionaryMap 
	{
		/**
		 * este es el mapa interno
		 */
		private var map:Dictionary;
		
		/**
		 * Esta es la referencia de todas las llaves que se guardan
		 */
		private var keys:ArrayCollection;
		
		/**
		 * esta es una forma de obtener el mapa interno
		 */
		public function get internalMap():Dictionary {
			return map;
		}
		
		/**
		 * Este constructor inicializa todas las variables internas
		 */
		public function DictionaryMap() {
			
			map =  new Dictionary(true);
			keys =  new ArrayCollection();
		}
		
		
	
		/**
		 * Este metodo agrega una variable al mapa
		 * la variable puede ser cualquier objeto 
		 */
		public function put(key:String,value:*):void {	
			
			if (!hasElement(key)) {
				//keys.push(key);
				keys.addItem(key);
			}
			map[key] = value;
		}
		
		
		/**
		 * Este metodo obtiene el valor almacenado con cierta llave
		 * en caso de no existir regresa null 
		 * 
		 */
		public function get(key:String):* {
			if (hasElement(key)){
				return map[key];
			}
			else {
				return null;
			}
		}
		
		
		/**
		 * Esta funcion revisa que exista un atributo
		 * 
		 */
		public function hasElement(key:String):Boolean{
			return map.hasOwnProperty(key) && map[key] != null; 
		}
		
		/**
		 * Esta funcion remueve un elemento de la estructura
		 * 
		 */
		public function remove(key:String):void {

			var index:int= keys.getItemIndex(key);
			if(index!=-1){
				keys.removeItemAt(index);
				map[key] = null;
			}
		}
		
		/**
		 * Esta funcion regresa la lista de keys que se tienen en el mapa
		 * 
		 */
		public function getAvailableKeys():ArrayCollection {
			return keys;
		}
		
		
		
	}
}