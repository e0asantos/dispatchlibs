package com.cemex.rms.common.push
{
	/**
	 * Esta funcion nos define los nombres de los destinos para hacer la conexion del Pus 
	 * 
	 */
	public class PushDestiny
	{
		public function PushDestiny()
		{
		}
		public var destinyId:String;
		public var destinyName:String;
	}
}