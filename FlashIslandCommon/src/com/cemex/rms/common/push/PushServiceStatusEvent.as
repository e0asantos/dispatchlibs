package com.cemex.rms.common.push
{
	import flash.events.Event;
	
	/**
	 * Este evento se lanza cuando se estan utilizando servicios de Push de LCDS o WebOrb
	 * 
	 */
	public class PushServiceStatusEvent extends Event
	{
		
		public static const STATUS_SERVICE_CONNECTED:String ="DSServiceConnected";
		public static const STATUS_SERVICE_DISCONNECTED:String ="DSServiceDisconnected";
		public static const STATUS_SERVICE_FAULT:String ="DSServiceFault";
		
		
		public var serviceName:String;
		
		public function PushServiceStatusEvent(type:String,serviceName:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			
			this.serviceName = serviceName;
		}
	}
}