package com.cemex.rms.common.push
{
	/**
	 * Esta funcion nos define el canal que se va a utilizar para conectarse 
	 * al Servidor de Push
	 */
	public class PushEndpoint
	{
		public function PushEndpoint()
		{
		}
		
		public var channelType:String;
		public var channelName:String;
		public var url:String;
		public var interval:int;
	}
}