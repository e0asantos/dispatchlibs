package com.cemex.rms.common.push
{
	import com.cemex.rms.common.services.AbstractService;
	
	import mx.controls.Alert;
	import mx.messaging.ChannelSet;
	import mx.messaging.Consumer;
	import mx.messaging.MessageAgent;
	import mx.messaging.events.ChannelEvent;
	import mx.messaging.events.ChannelFaultEvent;
	import mx.messaging.events.MessageAckEvent;
	import mx.messaging.events.MessageEvent;
	import mx.messaging.events.MessageFaultEvent;
	import mx.messaging.messages.IMessage;

	/**
	 * Esta clase para facilitar la implementacion de algun servicio de mensajeria
	 * 
	 */
	public class AbstractMessagingService extends AbstractBasicChannelSetService
	{
		public function AbstractMessagingService()
		{
			super();
		}
		
		/**
		 * Abstract Method
		 * para regresar el nombre del destino que se va a conectar
		 * Nota debe ser sobre escrito
		 */
		public function getDestinationName():String{
			throw new Error("getDestinationName Should be Overriden");
		}
		
		
		/**
		 * Abstract Method
		 * 
		 * para regresar el canal al que se va a conectar , en caos de estar utilizando el abstract channel Service
		 * se puede obtener del serviceContext
		 * Nota debe ser sobre escrito
		 */
		public function getChannelSet():ChannelSet{
			throw new Error("The method getChannelSet should be overriden");
		}
		
	
		
		
		
		
		
	}
}