package com.cemex.rms.common.push
{
	import com.cemex.rms.common.services.AbstractService;
	
	import mx.collections.ArrayCollection;
	import mx.messaging.ChannelSet;
	import mx.messaging.channels.AMFChannel;
	import mx.messaging.channels.RTMPChannel;
	import mx.messaging.events.ChannelEvent;
	import mx.messaging.events.ChannelFaultEvent;
	
	import weborb.messaging.WeborbMessagingChannel;
	
	/**
	 * Esta clase abstracta Se puede utilizar de la siguiente forma 
	 * En realidad lo que hace es conectarse 
	 * De preferencia se deberia usar solamente un canal en la configuracion
	 *  
	 * public class DispatcherChannelSetImpl extends AbstractChannelSetService
	 * {
	 * 	public function DispatcherChannelSetImpl()
	 * 	{
	 * 		super();
	 * 	}
	 * 	protected override function setChannelSet(channelSet:ChannelSet):void{	
	 * 		servicesContext.setValue("ChannelSet",channelSet);
	 * 	}
	 * }
	 * el metodo setChannelSet tiene que ser sobre escrito en caso que se quiera cambiar el nombre de la variable que se quiere 
	 * por default la variable del contexto para recuperar el channelSet es "ChannelSet"
	 * 
	 * e
	 */
	public class AbstractChannelSetService extends AbstractBasicChannelSetService
	{
		public function AbstractChannelSetService()
		{
			super();
		}
		
		/**
		 * Este metodo debe ser sobrescrito por los hijo y deben definir el nombre de la variable con la que van a acceder al 
		 * contexto de los servicios, ejemplo:
		 * servicesContext.setValue("ChannelSet",channelSet); 
		 * 
		 */
		protected function setChannelSet(channelSet:ChannelSet):void{
			servicesContext.setValue("ChannelSet",channelSet);
		} 
		
		/**
		 * Este metodo es el que se sobreescribe del startService
		 * este metodo requiere que la configuracion ya se haya hecho en el PushConfigHelper.initLcdsEndpoint
		 * 
		 * @reference PushConfigHelper
		 */
		public override function startService():void {
			
			
			
				
			var c:Object = PushConfigHelper.getChannelSetConfig();
			var channelSet:ChannelSet = new ChannelSet();

			if (c is ArrayCollection) {
				setChannelsFromArrayCollection( c as ArrayCollection, channelSet);		
			}
				
			else if (c is XML) {
				setChannelsFromXML(c as XMLList,channelSet);
			}
			_channelSet = channelSet;
			setChannelSet(channelSet);
			
			
			
			channelSet.addEventListener(ChannelEvent.CONNECT,channelConnect,false,0,true);
			channelSet.addEventListener(ChannelEvent.DISCONNECT,channelDisconect,false,0,true);
			channelSet.addEventListener(ChannelFaultEvent.FAULT,channelFault,false,0,true);
			
			dispatchServiceLoadReady();
		}
		
		
		private var _channelSet:ChannelSet;
		
		/**
		 * Este metodo recibe un arrayCollection como parametro para configurar los canales y los agrega al channel set 
		 * Se espera que los objetos dentro de este arrayCollection sean del tipo PushEndpoint
		 */
		private function setChannelsFromArrayCollection(arr:ArrayCollection, channelSet:ChannelSet):void{
			
			for (var i:int  = 0; i < arr.length ; i++ ){
				var conf:PushEndpoint = arr.getItemAt(i) as PushEndpoint;
				if (conf != null){
					addChannel(channelSet, conf.channelType,conf.channelName,conf.url,conf.interval);
				}
			}
		}
		
		
		
		/**
		 * Este metodo recibe un xml como parametro para configurar los canales y los agrega al channel set 
		 * <channel>
		 * 	<type>string</type>
		 * 	<name>string</name>
		 * 	<endpoint>string</endpoint>
		 * 	<interval>integer</interval>
		 * </channel>
		 */
		private function setChannelsFromXML(channels:XMLList, channelSet:ChannelSet):void{
			
			for each(var channel:XML in channels){
			
			
				var type:String = channel.type ;
				var name:String = channel.name ;
				var endpoint:String = channel.endpoint ;
				var interval:int = 0;
				if (channel.interval != null){
					interval = int(channel.interval);
				}
				
				addChannel(channelSet, type,name,endpoint,interval);
			}
		}
		/**
		 * Es la constante que se utiliza en el metodo addChannel como tipo de canal RTMP
		 */
		public static const CHANNEL_RTMP:String ="rtmp";
		
		/**
		 * Es la constante que se utiliza en el metodo addChannel como tipo de canal AMF Polling
		 */
		public static const CHANNEL_AMF_POLLING:String ="amf-polling";
		
		/**
		 * Es la constante que se utiliza en el metodo addChannel como tipo de canal AMF
		 */
		public static const CHANNEL_AMF:String ="amf";
		
		/**
		 * Es la constante que se utiliza en el metodo addChannel como tipo de canal WebOrb
		 */
		public static const CHANNEL_WEBORB:String ="weborb";  
		
		/**
		 * Crea el Canal segun el Tipo que se reciba como parametro
		 * 
		 * @param channelSet es el channel set al que se va a agregar el canal
		 * @type es el tipo de canal puede tener valores  
		 */
		protected function addChannel(channelSet:ChannelSet,type:String, name:String, endpoint:String,interval:int=1):void{
			if (type == CHANNEL_RTMP) {
				
				var _rtmpChannel:RTMPChannel = getRTMPChannel(name,endpoint);
				
				_rtmpChannel.addEventListener(ChannelEvent.CONNECT,channelConnect,false,0,true);
				_rtmpChannel.addEventListener(ChannelEvent.DISCONNECT,channelDisconect,false,0,true);
				_rtmpChannel.addEventListener(ChannelFaultEvent.FAULT,channelFault,false,0,true);
				channelSet.addChannel(_rtmpChannel);
			}
			else if (type == CHANNEL_AMF_POLLING) {
				
				var _amfPollingChannel:AMFChannel = getAMFChannel(name,endpoint);
				_amfPollingChannel.pollingEnabled = true;
				_amfPollingChannel.pollingInterval = interval;
				
				_amfPollingChannel.addEventListener(ChannelEvent.CONNECT,channelConnect,false,0,true);
				_amfPollingChannel.addEventListener(ChannelEvent.DISCONNECT,channelDisconect,false,0,true);
				_amfPollingChannel.addEventListener(ChannelFaultEvent.FAULT,channelFault,false,0,true);
				channelSet.addChannel(_amfPollingChannel);
			}
			else if(type == CHANNEL_AMF) {
				var _amfChannel:AMFChannel = getAMFChannel(name,endpoint);
				
				_amfChannel.addEventListener(ChannelEvent.CONNECT,channelConnect,false,0,true);
				_amfChannel.addEventListener(ChannelEvent.DISCONNECT,channelDisconect,false,0,true);
				_amfChannel.addEventListener(ChannelFaultEvent.FAULT,channelFault,false,0,true);
				channelSet.addChannel(_amfChannel);
			}	
			else if (type == CHANNEL_WEBORB) {
				var _weborbChannel:WeborbMessagingChannel = getWebOrbChannel(name,endpoint);
				
				_weborbChannel.addEventListener(ChannelEvent.CONNECT,channelConnect,false,0,true);
				_weborbChannel.addEventListener(ChannelEvent.DISCONNECT,channelDisconect,false,0,true);
				_weborbChannel.addEventListener(ChannelFaultEvent.FAULT,channelFault,false,0,true);
				//_weborbChannel.addEventListener(MessageEvent.MESSAGE,message);
				//xservicesContext.setValue("WEBORB_CHANNEL",_weborbChannel);
				channelSet.addChannel(_weborbChannel);
			}		
		}
		
	
	
	
		
		/**
		 * Crea el Canal de Tipo WebOrb
		 * 
		 */
		private function getWebOrbChannel(name:String, endpoint:String):WeborbMessagingChannel{
			var amfChannelRTMP:WeborbMessagingChannel = new WeborbMessagingChannel(name, endpoint);
			return amfChannelRTMP;
		}
	
	/**
	 * Crea el Canal de Tipo AMF
	 * 
	 */
		private function getAMFChannel(name:String, endpoint:String):AMFChannel{
			var amfChannel:AMFChannel = new AMFChannel(name, endpoint);
			return amfChannel;
		}
		
		/**
		 * Crea el Canal de Tipo RTMP
		 * 
		 */
		private function getRTMPChannel(name:String, endpoint:String):RTMPChannel{
			var amfChannelRTMP:RTMPChannel = new RTMPChannel(name, endpoint);
			return amfChannelRTMP;
		}
		
	}
}