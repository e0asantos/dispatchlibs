package com.cemex.rms.common.push
{
	import com.cemex.rms.common.services.AbstractService;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.messaging.ChannelSet;
	import mx.messaging.channels.AMFChannel;
	import mx.messaging.channels.RTMPChannel;
	import mx.messaging.events.ChannelEvent;
	import mx.messaging.events.ChannelFaultEvent;
	
	/**
	 * ESta clase da las operaciones basicas para hacer una coexion con un servicio remoto, en si da todos los metodos que
	 * para enviar eventos de ChannelSet
	 * incluso los listener que se va a requerir en el channel set
	 * 
	 */
	public class AbstractBasicChannelSetService extends AbstractService
	{
		public function AbstractBasicChannelSetService()
		{
			super();
		}
		
		private var _connected:Boolean = false;
		
		/**
		 * regresa true si el servicio ya se conecto
		 */
		public function isConnected():Boolean{
			return _connected;
		}
		
		/**
		 * Este se manda llamar cuando elcanal ya se conectó 
		 * se despachan los eventos: service load read , y service connected
		 */
		protected function channelConnect(e:ChannelEvent):void { 
			if (!isReady()){
				dispatchServiceLoadReady();
			}
			dispatchServiceConnected();
			
			logger.warning("channelConnect:"+getServiceName()+"("+e+")");
			trace("channelConnect:"+getServiceName()+"("+e+")");
			_connected = true;
		}
		
		/**
		 * Este metodo es llamado cuando se desconecto el canal 
		 * despacha elevento service disconnected
		 */
		protected function channelDisconect(e:ChannelEvent=null):void {
			_connected = false;
			dispatchServiceDisconnected();
			logger.warning("channelDisconect:"+getServiceName()+"("+e+")");
			trace("channelDisconect:"+getServiceName()+"("+e+")");
		} 
		/**
		 * estemetodo se manda a llamar cuando hubo un evento ChannelFaultEvent
		 * y dispara un evento del tipo Service Fault
		 */
		protected function channelFault(e:ChannelFaultEvent):void {
			
			dispatchServiceFault();
			logger.warning("channelFault:"+getServiceName()+"("+e+")");
			trace("channelFault:"+getServiceName()+"("+e+")");
			
		} 
		
		/**
		 * ESte metodo dispara un evento del tipo serviceConnected
		 */
		public function dispatchServiceConnected():void{
			trace("dispatchServiceConnected()",this,new Date());
			var event:PushServiceStatusEvent = new PushServiceStatusEvent(PushServiceStatusEvent.STATUS_SERVICE_CONNECTED, getServiceName());
		
			dispatcher.dispatchEvent(event);
		}
		
		/**
		 * ESte metodo dispara un evento del tipo serviceDisconected
		 */
		public function dispatchServiceDisconnected():void{
			trace("dispatchServiceDisconnected()",this,new Date());
			var event:PushServiceStatusEvent = new PushServiceStatusEvent(PushServiceStatusEvent.STATUS_SERVICE_DISCONNECTED, getServiceName());
			dispatcher.dispatchEvent(event);
		}
		
		/**
		 * ESte metodo dispara un evento del tipo serviceFault
		 */
		public function dispatchServiceFault():void{
			trace("dispatchServiceFault()",this,new Date());
			var event:PushServiceStatusEvent = new PushServiceStatusEvent(PushServiceStatusEvent.STATUS_SERVICE_FAULT, getServiceName());
			dispatcher.dispatchEvent(event);
		}
		
	}
}