package com.cemex.rms.common.push
{
	import com.cemex.rms.common.flashislands.MappingHelper;
	import com.cemex.rms.common.lcds.AbstractReceiverService;
	import com.cemex.rms.common.reflection.ReflectionHelper;
	import com.cemex.rms.common.utils.DictionaryMap;
	
	import mx.collections.ArrayCollection;
	
	/**
	 * Esta clase es una serie de metodos que ayudan a procesar y configurar los datos del Push con
	 * LCDS o WebOrb 
	 * 
	 */
	public class PushConfigHelper
	{
		public function PushConfigHelper()
		{
		}
		
		public static const FLASH_ISLAND_OBJECT:String = "FLASH_ISLAND_OBJECT";
		public static const XML_OBJECT:String = "XML";
		public static const configType:String = FLASH_ISLAND_OBJECT;
		
		/**
		 * Este metodo nos ayuda a settear la configuracion , 
		 * Esta pensado para que la configuracion sea de un Arreglo de objetos Generidoc (new Object)
		 *  y si el parametro type es == FLASH_ISLAND_OBJECT
		 * se hace un parseo de los objetos del Array Collection a objetos PushEndpoint tambien se esta considerando que 
		 * los objetos tienen los parametros con mayusculas
		 * Es decir esta pensado para acoplarse con los Flashislands
		 *  junto con la libreria e ABAP FlashUtils
		 * 
		 * @reference PushEndpoint
		 */
		public static function initLcdsEndpoint(lcds:Object,type:String="FLASH_ISLAND_OBJECT"):void{
			if(lcds!=null){
				AbstractReceiverService.RTMP_URL=lcds["LCDS_ENDPOINT"]["URL"]
			}
			if (type  == FLASH_ISLAND_OBJECT){
				
				var newEndpoint:PushEndpoint =  new PushEndpoint();
				
				ReflectionHelper.copySimpleParametersUpperAsCamel(lcds["LCDS_ENDPOINT"],newEndpoint);
				
				var array:ArrayCollection = new ArrayCollection();
				array.addItem(newEndpoint);
				
				channelSetConfig = array;
				
			}
			else {
				channelSetConfig = lcds;
			}
			
		}
		
		private static var channelSetConfig:Object;
		private static var destiniesMap:DictionaryMap = new DictionaryMap();
		
		/**
		 * Al igual que el metodo este metodo esta principalmente pensado para uqe se 
		 * integre con el FlashIsland util en Abap, el cual 
		 * define los nodos del contexto
		 * con los nombres necesarios para que se llene el objeto, 
		 * con los valores PushDestiny
		 * 
		 * @reference PushDestiny
		 */
		public static function initLcdsDestinies(lcds:Object):void{
			
			var destinies:ArrayCollection = MappingHelper.doMapping(lcds,"LCDS_DESTINY",PushDestiny);
			
			
			for (var i:int = 0 ; i < destinies.length ; i ++){
				var d:PushDestiny = destinies.getItemAt(i) as PushDestiny;
				/*if(d.destinyId.toLowerCase().indexOf("confirmation")==-1 && d.destinyName.toLowerCase().indexOf("confirmation")==-1){*/
					destiniesMap.put(d.destinyId,d);
				/*}*/
				
				
			}
		}
		
		/**
		 * Se obtiene el primer chanal de configuracion que es el que nos define cual tipo de mensajero se va a
		 * utilizar (WebOrb,RTMP,etc.)
		 * Es por esta razon que se recomienda que solamente se utilize un solo canal 
		 * 
		 */
		public static function getFirstChannelSetConfig():PushEndpoint{
			var config:ArrayCollection = channelSetConfig as ArrayCollection;
			if (config != null && config.length > 0){
				return config.getItemAt(0) as PushEndpoint;
			}
			return null;
		}
		/**
		 * Regresa la coniguracion que se tenia 
		 * 
		 */
		public static function getChannelSetConfig():Object{
			return channelSetConfig;
		}
		
		/**
		 * Hace  se obtiene el nombre verdadero del destino( esto es para que puedan variar en base a la configuracion) 
		 * de las tablas
		 */
		public static function getDestinyName(destinyId:String):String{
			
			var destiny:PushDestiny = destiniesMap.get(destinyId) as PushDestiny;
			if (destiny == null){
				return "";
			}
			return 	destiny.destinyName;
		}
	}
}