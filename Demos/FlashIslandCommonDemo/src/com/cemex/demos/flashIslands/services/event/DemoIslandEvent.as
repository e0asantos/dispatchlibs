package com.cemex.demos.flashIslands.services.event
{
	import flash.events.Event;
	
	public class DemoIslandEvent extends Event
	{
		public static const DATA_RECEIVED:String = "DATA_RECEIVED";
		
		public function DemoIslandEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
	}
}