package com.cemex.demos.flashIslands.services.wda
{
	import com.cemex.rms.common.services.IService;
	import com.cemex.rms.common.utils.DictionaryMap;
	
	
	import mx.collections.ArrayCollection;

	public interface IDemoIsland extends IService
	{
		
		
		function getDataProvider():ArrayCollection;
		
		
		
		function getMonthLbl():String;
		function getOtd():String;
		function getRty():String;
		
		
		
		
		
		
	}
}