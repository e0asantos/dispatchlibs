package com.cemex.demos.flashIslands.services.wda
{
	import com.cemex.demos.flashIslands.services.event.DemoIslandEvent;
	import com.cemex.rms.common.flashislands.AbstractFlashIslandService;
	import com.cemex.rms.common.reflection.ReflectionHelper;
	import com.cemex.rms.common.utils.DictionaryMap;
	
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	
	
	
	/**
	 * 
	 * 
	 * 
	 * 
	 */
	public class DemoIslandImpl extends AbstractFlashIslandService implements IDemoIsland
	{
		public function DemoIslandImpl() {
			super();
		}
		
		
		
		
		//****************************
		// WebDynpro Varibles Declarations 
		// Para asegurar que estos campos estan setteados 
		// y lanzar el evento flashIslandDataReady
		public override function getIslandFields():ArrayCollection {
			return new ArrayCollection(["dataSource","MonthLbl","otd","rty"]);
		}
		
		public function getDataProvider():ArrayCollection{
			return buffer["dataSource"] as ArrayCollection;
		}
		
		public function getMonthLbl():String{
			return buffer["MonthLbl"] as String;
		}
		public function getOtd():String{
			return buffer["otd"] as String;
		}
		public function getRty():String{
			return buffer["rty"] as String;
		}
	
		
		private var loaded:Boolean = false;
		
		/**
		 * Esta funcion valida que todos los datos esten disponibles para comenzar a activar todos los 
		 * lugares donde se requiera hacer algo con los datos
		 */
		public override function flashIslandDataReady():void {
			
			if (getDataProvider() == null){
				return;
			}
			if (!loaded){
				this.dispatchServiceLoadReady();
				loaded= true;
			}
			
			var event:DemoIslandEvent = new DemoIslandEvent(DemoIslandEvent.DATA_RECEIVED);
			// Se necesita agragar un poco mas de logica en WDA para que se pueda tener un mejor control en los evento que son lanzados
			// Dado que por lo general solamente se va a necesitar esta informacion en la carga inicial, y despues las actualizaciones lleguen con LCDS
			if (event != null){
				
				
				dispatcher.dispatchEvent(event);
				//deleteContextServerData();
			}
			
		}
		
		
	}
}