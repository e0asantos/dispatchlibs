package com.cemex.demos.flashIslands.services.wda
{
	import com.cemex.demos.flashIslands.services.event.DemoIslandEvent;
	import com.cemex.rms.common.flashislands.AbstractFlashIslandService;
	import com.cemex.rms.common.reflection.ReflectionHelper;
	import com.cemex.rms.common.services.AbstractService;
	import com.cemex.rms.common.utils.DictionaryMap;
	
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	
	
	
	/**
	 * 
	 * 
	 * 
	 * 
	 */
	public class DemoIslandMock extends AbstractService implements IDemoIsland
	{
		public function DemoIslandMock() {
			super();
			
		}
		
		
		
		
		
		public function getDataProvider():ArrayCollection{
			
			var result:ArrayCollection =  new ArrayCollection();
			result.addItem({"Month":"07/2008", "OTD":90, "RTY":85});
			result.addItem({"Month":"08/2008", "OTD":50, "RTY":65});
			result.addItem({"Month":"09/2008", "OTD":70, "RTY":75});
			return result;
		}
		
		public function getMonthLbl():String{
			return "Month";
		}
		public function getOtd():String{
			return "OTD";
		}
		public function getRty():String{
			return "RTY";
		}
	
		
		private var loaded:Boolean = false;
		
		
		public override function startService():void {
			dispatchServiceLoadReady();		
		}		
		
	}
}