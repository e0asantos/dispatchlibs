package com.cemex.demos.flashIslands.services
{
	import com.cemex.demos.flashIslands.services.wda.IDemoIsland;
	import com.cemex.rms.common.services.IServiceFactory;
	
	import mx.collections.ArrayCollection;

	
	public class DemoServiceFactory implements IDemoServiceFactory
	{
		public function DemoServiceFactory()
		{
		}
		
		//[Inject]
		public var serviceFactory:IServiceFactory;
		

		
		public function getFlashIslandService():IDemoIsland {
			
			var result:IDemoIsland = serviceFactory.getService("DemoIsland") as IDemoIsland;
			if (!result.isReady()){
				result = serviceFactory.getService("DemoIslandMock") as IDemoIsland;
			}
			return result;
		}
		
	}
}