package com.cemex.demos.flashIslands.views.mediator
{
	import com.cemex.demos.flashIslands.services.IDemoServiceFactory;
	import com.cemex.demos.flashIslands.services.event.DemoIslandEvent;
	import com.cemex.demos.flashIslands.views.BarChartView;
	import com.cemex.rms.common.logging.ILogger;
	import com.cemex.rms.common.logging.LoggerFactory;
	import com.cemex.rms.common.logging.events.LoggerEvent;
	import com.cemex.rms.common.services.events.ServiceEvent;
	
	import flash.events.MouseEvent;
	
	import mx.binding.utils.BindingUtils;
	import mx.collections.ArrayCollection;
	import mx.controls.Image;
	import mx.graphics.IFill;
	import mx.graphics.SolidColor;
	
	import org.robotlegs.mvcs.Mediator;
	
	/**
	 * 
	 * DispatcherViewMediator es la clase mediador que le da funcionalidad a la vista, tambien
	 *  se encarga de hacer que los botones y la funcionalidad externa funcionen
	 * a demas que esta clase es la que se encarga de recibir los datos externos
	 * Es decir esta clase se encarga de afectar los datos y la vista del gantt
	 * 
	 * 
	 */
	public class BarChartViewMediator extends Mediator {
		
		public function BarChartViewMediator() {
			super();
		}
		
		
		/**
		 * La vista 
		 */
		[Inject]
		public var view : BarChartView;
		[Inject]
		public var services : IDemoServiceFactory;
		
		public function dataReceived(e:DemoIslandEvent):void{
			dataSource = services.getFlashIslandService().getDataProvider();
		}
		
		public function refresh(e:MouseEvent):void{
			view.axis.categoryField = services.getFlashIslandService().getMonthLbl();
			view.otd.xField = services.getFlashIslandService().getOtd();
			view.rty.xField = services.getFlashIslandService().getRty();
			dataSource = services.getFlashIslandService().getDataProvider();
		}
		
		[Bindable]
		public var dataSource:ArrayCollection = new ArrayCollection();
		
		
		
		[Bindable]
		public var otdFill:IFill = new SolidColor(0xbcd0df, 1.0);
		[Bindable]
		public var rtyFill:IFill = new SolidColor(0xFFD965, 1.0);
		
		override public function onRegister():void {
			
			
			eventMap.mapListener(eventDispatcher,DemoIslandEvent.DATA_RECEIVED				,dataReceived);
			
			
			BindingUtils.bindProperty(view.axis, 	"dataProvider",this,["dataSource"]);
			BindingUtils.bindProperty(view.rty, 	"dataProvider",this,["dataSource"]);
			BindingUtils.bindProperty(view.otd, 	"dataProvider",this,["dataSource"]);
			BindingUtils.bindProperty(view.legend,	"dataProvider",this,["dataSource"]);
			
			
			view.otd.setStyle("fill",otdFill);
			view.rty.setStyle("fill",rtyFill);

			view.refresh.addEventListener(MouseEvent.CLICK,refresh);
		
			eventMap.mapListener(eventDispatcher,ServiceEvent.SERVICE_LOAD_READY,serviceReady);
			
			
			view.DemoIsland.source = red;
			view.DemoIslandMock.source = red;
		}
		
		
		[Embed(source="/assets/s_s_ledg.gif")]            
		[Bindable]             
		private var green:Class;
		
		[Embed(source="/assets/s_s_ledr.gif")]            
		[Bindable]             
		private var red:Class;
		
		[Embed(source="/assets/s_s_ledy.gif")]            
		[Bindable]             
		private var yellow:Class;
		
		public function serviceReady(e:ServiceEvent):void{
			setServiceStatus(e.service.config.name,"CONNECTED");
		}
		public function setServiceStatus(service:String, status:String):void{
			
			if (view.hasOwnProperty(service)){
				var hbox:Image= (view[service] as Image);
				if (status == "CONNECTED"){
					hbox.source = green;
				}
				else if (status == "DISCONNECTED"){
					hbox.source = red;
				}
				else if (status == "FAULT"){
					hbox.source = yellow;
				}
			}
			else {
				logger.info("service as no LEd indicator" + service);			
			}
			
			
		}
		
		private var logger:ILogger = LoggerFactory.getLogger("BarChartViewMediator");
		
		private var logging:ArrayCollection;
		private var logCounter:int = 0 ;
		public function log(e:LoggerEvent):void{
			
			if (logging == null){
				logging = new ArrayCollection();
		
			}
			++logCounter;
			logging.addItem("("+logCounter+")"+e.log);
		}
	}
	
}