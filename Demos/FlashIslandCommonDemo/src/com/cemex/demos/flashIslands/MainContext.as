package com.cemex.demos.flashIslands
{
	import com.cemex.demos.flashIslands.controller.StartupCommand;
	import com.cemex.demos.flashIslands.services.DemoServiceFactory;
	import com.cemex.demos.flashIslands.services.DemoServicesModel;
	import com.cemex.demos.flashIslands.services.IDemoServiceFactory;
	import com.cemex.demos.flashIslands.views.BarChartView;
	import com.cemex.demos.flashIslands.views.DemoView;
	import com.cemex.demos.flashIslands.views.mediator.BarChartViewMediator;
	import com.cemex.rms.common.logging.LoggerFactory;
	import com.cemex.rms.common.services.IServiceFactory;
	import com.cemex.rms.common.services.ServiceFactory;
	import com.cemex.rms.common.services.events.ServiceManagerEvent;
	
	import flash.display.DisplayObjectContainer;
	
	import org.robotlegs.base.ContextEvent;
	import org.robotlegs.mvcs.Context;
	
	public class MainContext extends Context
	{
		
		
		public function MainContext(contextView:DisplayObjectContainer=null, autoStartup:Boolean=true)
		{
			super(contextView, autoStartup);			
		}
		
		
		
		override public function startup():void
		{
			
			
			LoggerFactory.defaultLogger = LoggerFactory.LOGGER_TYPE_EVENT;
			// SE inicializa el factory de servicios
			var factory:ServiceFactory = new ServiceFactory();
			
			
			// Se le settea el dispatcher del contexto a los servicios
			// para poder utilizar los listeners de los servicios con la arquitectura de 
			// Robotlegs
			factory.dispatcher = eventDispatcher;
			factory.model = new DemoServicesModel();
			// Se settea el Factory de Servicios
			injector.mapValue(IServiceFactory, factory);
			
			
			var demoService:DemoServiceFactory = new DemoServiceFactory();
			//injector.injectInto(ganttService);
			demoService.serviceFactory = factory;
			injector.mapValue(IDemoServiceFactory,demoService);
			
			// Se settea el mapa de Vistas mediador

			
			
			mediatorMap.mapView(BarChartView,BarChartViewMediator );
			
			// se crea el mediador de la vista principal el FlashIsland Settea datos
			// en cuanto se manda a cargar el servicio de FlashIslands
			// Los demas mediadores no se tienen que generar asi
			mediatorMap.createMediator((contextView as MainModule).view );
			
			
			// 
			//mediatorMap.mapView(TaskBranchRenderer, TaskBranchRendererMediator);
			
			
			commandMap.mapEvent(ContextEvent.STARTUP, StartupCommand, ContextEvent, true);
			// Se mandan a inicializar las cosas
			dispatchEvent( new ContextEvent( ContextEvent.STARTUP ) );
			
			
			
		}
	}
}