package com.cemex.common.gantt.overlap
{
	import com.cemex.common.gantt.GanttTask;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;

	public class GanttOverlapTask extends GanttTask
	{
		public function GanttOverlapTask()
		{
			super();
		}
		
		
		public function get colorsConfiguration():Array {
			var overlap:OverLapDescription = getOverLaps();
			if (overlap != null && overlap.overlapType != OverLapDescription.OVERLAP_TYPE_OFF){
				return [overlap.points.length];
			}
			return [2];
			
		}
		public function get fillColors():Array {			
			var overlap:OverLapDescription = getOverLaps();
			if (overlap != null && overlap.overlapType != OverLapDescription.OVERLAP_TYPE_OFF){
				return getFieldArray(overlap.points, "fillColors");
			}
			return [0xFF0000,0xFF0000];
			
		}
		public function get fillAlphas():Array {
			
			var overlap:OverLapDescription = getOverLaps();
			if (overlap != null && overlap.overlapType != OverLapDescription.OVERLAP_TYPE_OFF){
				return getFieldArray(overlap.points, "fillAlphas");
			}
			return [0.00,0.00];
		}
		public function get gradientRatio():Array {
			var overlap:OverLapDescription = getOverLaps();
			if (overlap != null && overlap.overlapType != OverLapDescription.OVERLAP_TYPE_OFF){
				return getFieldArray(overlap.points, "gradientRatio");
			}
			return [0, 255];
		}
		
	
		
		public function getFieldArray(points:Array,fieldName:String):Array{
			var result:Array =  new Array();
			for (var i:int = 0  ; i < points.length ; i ++){
				var temp:OverlapPoint = points[i] as OverlapPoint;
				result.push(temp[fieldName]);
			}
			return result;
		}
	
	
	
		public function getOverLaps():OverLapDescription {
			
			var result:OverLapDescription = new OverLapDescription();
			result.overlapType = OverLapDescription.OVERLAP_TYPE_MULTI;
			
			var container:ArrayCollection = getContainer();
			
			var points:Array =  new Array();
			
			for (var i:int  = 0 ; i < container.length ; i ++ ) {
				
				var temp:GanttTask = container.getItemAt(i) as GanttTask;
				var overlap:OverLapDescription = getOverlap(this,temp);
				if (overlap.overlapType != OverLapDescription.OVERLAP_TYPE_OFF) {
					//
					
					for (var j:int = 0 ; j <  overlap.points.length ; j ++ ){
						var point:OverlapPoint = overlap.points[j] as OverlapPoint;
						points.push(point);
					}
				}
			}
			
			if (points.length > 0){
				try {
				sortPoints(points);
				}
				catch (e:*){
					Alert.show(""+e);
				}
				
				
				result.overlapType = OverLapDescription.OVERLAP_TYPE_MULTI;
				rawPoints = points;
				result.points = removeNoisePoints(points);
				return result;
			}
			else {
				return null
			}
			
		}
		
		public var rawPoints:Array;
		
		private var overlapColor:Number = 0xFF0000;
	
	
		public function removeNoisePoints(points:Array):Array{
			var result:Array =  new Array();
			var isRight:Boolean = false;
			for (var i:int  = 0 ; i < points.length/2; i ++ ) {
				// Se obtienen los dos puntos porque desde un inicio estan en orden
				var p1:OverlapPoint = points[( i * 2 ) + 0] as OverlapPoint;
				var p2:OverlapPoint = points[( i * 2 ) + 1] as OverlapPoint;
				
				if (p1.overlapType == OverLapDescription.OVERLAP_TYPE_ALL){
					result =  new Array();
					result.push(p1);
					result.push(p2);
					
					return result;
				}
				else if (p1.overlapType == OverLapDescription.OVERLAP_TYPE_RIGHT){
					if (!isRight){
						result.push(p1);
						result.push(p2);
					}
					 isRight = true;
				}
				else if (p1.overlapType == OverLapDescription.OVERLAP_TYPE_LEFT){
					
					if (isRight){
							
							
							p1 = getOverLapPoint(0, overlapColor,0.8,OverLapDescription.OVERLAP_TYPE_ALL);
							p2 = getOverLapPoint(255, overlapColor,0.8,OverLapDescription.OVERLAP_TYPE_ALL);
							

							result =  new Array();
							result.push(p1);
							result.push(p2);
							
							return result;
					}
					else {
						result =  new Array();
						result.push(p1);
						result.push(p2);
					}
				}
				else if (p1.overlapType == OverLapDescription.OVERLAP_TYPE_CENTER){
					if(!isRight){
						result.push(p1);
						result.push(p2);
					}
				}
			}
					
				
			return result;
		}
		public function sortPoints(points:Array):void{
			
			for (var pass:int  = 1 ; pass < points.length/2 ; pass ++ ) {
				for (var j:int  = 0 ; j < points.length/2 - pass ; j ++ ) {
					
					var p1:OverlapPoint = points[(2*j)] as OverlapPoint;
					var p2:OverlapPoint = points[(2*j)+1] as OverlapPoint;

					var q1:OverlapPoint = points[(j + 1) * 2] as OverlapPoint;
					var q2:OverlapPoint = points[(j + 1) * 2 + 1] as OverlapPoint;
					
					if (p1.gradientRatio > q1.gradientRatio){
						var temp1:OverlapPoint = p1;
						var temp2:OverlapPoint = p2;
						points[ j*2 ] = q1;
						points[ (j*2)+1 ] = q2;
						points[ (j + 1)*2 ] = temp1;
						points[ (j + 1)*2 +1] = temp2;
						
					}
				}
			}
		}
		
		public function getFirstOverLap():OverLapDescription {
			
			var result:Array = new Array();
			var container:ArrayCollection = getContainer();
			for (var i:int  = 0 ; i < container.length ; i ++ ) {
				
				var temp:GanttTask = container.getItemAt(i) as GanttTask;
				var overlap:OverLapDescription = getOverlap(this,temp);
				if (overlap.overlapType != OverLapDescription.OVERLAP_TYPE_OFF) {
					return overlap;
				}
			}
			return null;
		}
		
		
		private function getOverlap(task:GanttTask, other:GanttTask):OverLapDescription{
			var overlap:OverLapDescription = new OverLapDescription();
			var p:OverlapPoint;
			if (task != other){
				
				var validation:Boolean = (task.startTime.time < other.endTime.time)
            						  && (task.endTime.time   > other.startTime.time);
				if (validation){
					// Ahora necesitamos saber que tipo de overlap existe
					var taskTime:Number = task.endTime.time - task.startTime.time;
					var temp:int;
					if (task.startTime.time  >= other.startTime.time) { 
						
						if (task.endTime.time  > other.endTime.time){
							// LEFT
							var overTimeL:Number = other.endTime.time - task.startTime.time;
							overlap.overlapType = OverLapDescription.OVERLAP_TYPE_LEFT;
							//logger.info("LEFT(o.s:"+other.startTime.time+", o.e:"+other.endTime.time+" )"+
							//			",t.s:"+task.startTime.time+", t.e:"+task.endTime.time+" )");
							temp = (overTimeL / taskTime) * 255;
							
							
							p = getOverLapPoint(temp - 1, overlapColor,0.8,OverLapDescription.OVERLAP_TYPE_LEFT);
							/* p =  new OverlapPoint();
							p.gradientRatio = temp - 1;
							p.fillColors = overlapColor; 
							p.fillAlphas = 0.8;
							p.overlapType = OverLapDescription.OVERLAP_TYPE_LEFT; */
							overlap.points.push(p);
							
							p = getOverLapPoint(temp, 0,0.00,OverLapDescription.OVERLAP_TYPE_LEFT);
							/* p =  new OverlapPoint();
							p.gradientRatio = temp;
							p.fillColors = 0; 
							p.fillAlphas = 0.00;
							p.overlapType = OverLapDescription.OVERLAP_TYPE_LEFT; */
							overlap.points.push(p);	
							
						}
						else {
							//ALL
							overlap.overlapType = OverLapDescription.OVERLAP_TYPE_ALL;
							
							p = getOverLapPoint(0, overlapColor,0.8,OverLapDescription.OVERLAP_TYPE_ALL);
							/* p =  new OverlapPoint();
							p.gradientRatio = 0; 
							p.fillColors = overlapColor; 
							p.fillAlphas = 0.8;
							p.overlapType = OverLapDescription.OVERLAP_TYPE_ALL; */
							overlap.points.push(p);
							
							
							
							p = getOverLapPoint(255, overlapColor,0.8,OverLapDescription.OVERLAP_TYPE_ALL);
							/* p =  new OverlapPoint();
							p.gradientRatio = 255;
							p.fillColors = overlapColor; 
							p.fillAlphas = 0.8;
							p.overlapType = OverLapDescription.OVERLAP_TYPE_ALL; */
							overlap.points.push(p);
							
							
							
						}
					}
					else {
						if (task.endTime.time  > other.endTime.time){
							// CENTER
							var overStart:Number = other.startTime.time - task.startTime.time;
							var overEnd:Number   = other.endTime.time - task.startTime.time;
							
							overlap.overlapType = OverLapDescription.OVERLAP_TYPE_CENTER;
							
							temp = (overStart / taskTime) * 255;
							
							
							p = getOverLapPoint(temp - 1, 0,0.00,OverLapDescription.OVERLAP_TYPE_CENTER);
							/* p =  new OverlapPoint();
							p.gradientRatio = temp - 1;
							p.fillColors = 0; 
							p.fillAlphas = 0.00;
							p.overlapType = OverLapDescription.OVERLAP_TYPE_CENTER; */
							overlap.points.push(p);
							
							
							p = getOverLapPoint(temp, overlapColor,0.8,OverLapDescription.OVERLAP_TYPE_CENTER);
							/* p =  new OverlapPoint();
							p.gradientRatio = temp;
							p.fillColors = overlapColor; 
							p.fillAlphas = 0.8;
							p.overlapType = OverLapDescription.OVERLAP_TYPE_CENTER; */
							overlap.points.push(p);	
							
							temp = (overEnd / taskTime) * 255;
							
							p = getOverLapPoint(temp - 1, overlapColor,0.8,OverLapDescription.OVERLAP_TYPE_CENTER);
							/* p =  new OverlapPoint();
							p.gradientRatio = temp - 1;
							p.fillColors = overlapColor; 
							p.fillAlphas = 0.8;
							p.overlapType = OverLapDescription.OVERLAP_TYPE_CENTER; */
							overlap.points.push(p);
							
							p = getOverLapPoint(temp, 0,0.00,OverLapDescription.OVERLAP_TYPE_CENTER);
							/* p =  new OverlapPoint();
							p.gradientRatio = temp;
							p.fillColors = 0; 
							p.fillAlphas = 0.00;
							p.overlapType = OverLapDescription.OVERLAP_TYPE_CENTER; */
							overlap.points.push(p);	
							
							
						}
						else {
							// RIGHT
							var overTimeR:Number = task.endTime.time - other.startTime.time;
							overlap.overlapType = OverLapDescription.OVERLAP_TYPE_RIGHT;
							temp = 255 - (overTimeR / taskTime) * 254;
							
							p = getOverLapPoint(temp - 6, 0, 0.00,OverLapDescription.OVERLAP_TYPE_RIGHT);
						/* 	p =  new OverlapPoint();
							p.gradientRatio = temp  - 6;
							p.fillColors = 0; 
							p.fillAlphas = 0.00;
							p.overlapType = OverLapDescription.OVERLAP_TYPE_RIGHT; */
							overlap.points.push(p);
							
							
							p = getOverLapPoint(temp - 5, overlapColor,0.8,OverLapDescription.OVERLAP_TYPE_RIGHT);
							/* p =  new OverlapPoint();
							p.gradientRatio = temp - 5;
							p.fillColors = overlapColor; 
							p.fillAlphas = 0.8;
							p.overlapType = OverLapDescription.OVERLAP_TYPE_RIGHT; */
							overlap.points.push(p);
						}
					}
				}			
				else {
					overlap.overlapType = OverLapDescription.OVERLAP_TYPE_OFF;
				}
			}
			else {
				overlap.overlapType = OverLapDescription.OVERLAP_TYPE_OFF;
			}
			
			//logger.info(overlap.overlapType + "(p:"+overlap.p +", q:" + overlap.q+")");
			return overlap;
		}
		
		public function getOverLapPoint(ratio:int, colors:Number,alpha:Number,desc:String ):OverlapPoint{
			var p:OverlapPoint =  new OverlapPoint();
			p.gradientRatio = ratio;
			p.fillColors = colors; 
			p.fillAlphas = alpha;
			p.overlapType = desc;
			return p;
		}
		
		
		
		
		
		
		
		
		
	
	}
}