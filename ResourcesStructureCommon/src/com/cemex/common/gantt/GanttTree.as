package com.cemex.common.gantt
{
	import com.cemex.common.tree.AbstractTree;
	import com.cemex.rms.common.reflection.ReflectionHelper;
	
	import mx.collections.ArrayCollection;

	public class GanttTree extends AbstractTree
	{
		public function GanttTree(fields:ArrayCollection)
		{
			super(fields);
		}
	
		
		protected function hasSameValues(target:Object,temp:Object):Boolean {
			return true;
		}
		
		protected override function compareFunctions(target:Object,temp:Object):Boolean {
			var result:Boolean = false;
			if ((target is GanttTask) && (temp is GanttTask)){
				var targetTask:GanttTask = target as GanttTask;
				var tempTask:GanttTask = temp as GanttTask;
				
				
				// Estamos hablando de la misma carga 
				if (hasSameValues(target,temp)){
					
					//result = true;
					if (targetTask.step == GanttTask.STEP_INITIAL || targetTask.step == GanttTask.STEP_MOVED){
						
						result = 
							tempTask.step != GanttTask.STEP_GHOSTX || 
							tempTask.step != GanttTask.STEP_INITIAL || 
							tempTask.step != GanttTask.STEP_MOVED ;
					}
					else if (targetTask.step == GanttTask.STEP_GHOSTX){
						
						result = tempTask.step != GanttTask.STEP_MOVED;
						
					}
				}
			}
			else {
				result = ReflectionHelper.compareObject(target,temp);
			}
			return result;
		}
	}
}