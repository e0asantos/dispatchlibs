package com.cemex.common.tree
{
	import com.cemex.common.filters.FilterHelper;
	import com.cemex.rms.common.logging.ILogger;
	import com.cemex.rms.common.logging.LoggerFactory;
	import com.cemex.rms.common.reflection.ReflectionHelper;
	import com.cemex.rms.common.utils.DictionaryMap;
	
	import flash.utils.ByteArray;
	
	import mx.collections.ArrayCollection;
	
	
	public class AbstractTree {
		public function AbstractTree(fields:ArrayCollection) {
			
			reset();
			
			this.fields = fields;
		
			//this.dataFields=dataFields;
		}
		
		private var logger:ILogger = LoggerFactory.getLogger("AbstractTree",LoggerFactory.LOGGER_TYPE_TRACE);
		protected function newNode():NodeResource{
			throw new Error("This method should be overriden because is an abstract Class");
		}
		protected function newTask():NodeTask {
			throw new Error("This method should be overriden because is an abstract Class");
		}
		
		
		public function setFilters(filters:ArrayCollection):void{
			this.filters = filters;
		}
		public function timeChanged(time:Date):void {
		
			if (time == null){
				time = new Date();
			}
			for(var i:int = 0 ; i < allTasks.length ; i ++){
				var task:NodeTask = allTasks.getItemAt(i) as NodeTask;
				if (task != null){
					timeChangedTask(time,task);
				}
			}
		}
		
		
		public function timeChangedTask(time:Date,nodeTask:NodeTask):void{
		}
		private var root:NodeResource;
		private var fields:ArrayCollection;
		//private var dataFields:ArrayCollection;
		private var ctx:DictionaryMap;
		private var rawDatas:ArrayCollection;
		private var allTasks:ArrayCollection;
		private var filters:ArrayCollection;
		
		public function getTasksTree():ArrayCollection {
			return allTasks;
		}
		
		public function getResourcesTree():ArrayCollection {
			return root.getChildren();
		}
		
		public function getRoot():NodeResource{
			return root;
		}
		public function reset():void {
			
			this.ctx = new DictionaryMap();
			if (this.rawDatas == null)
				this.rawDatas = new ArrayCollection();
			
			if (this.allTasks == null)
				this.allTasks = new ArrayCollection();
			
			this.allTasks.removeAll();
			this.rawDatas.removeAll();
			
			root = newNode();
			root.id = "";
			ctx.put("",root);
		}
		
		public function regenerate():void {
			var currentTasks:ArrayCollection = allTasks;
			//reset();
			for (var  i:int  = 0 ; i < currentTasks.length ; i ++){
				var temp:NodeTask =  currentTasks.getItemAt(i) as NodeTask;
				currentTasks.itemUpdated(temp);
				//addData(temp.data, temp.step);
			}
		}
		
		public function refresh():void {
			
		}
		//private static var logger:ILogger = LoggerFactory.getLogger("AbstractTree");
		
		/* 	
		public function addDataNodeInLine(node:NodeTask):void{
		
		var arr:ArrayCollection =  node.getContainer();
		for (var i:int = 0 ; i < arr.length ; i ++) {
		var n:NodeTask = arr.getItemAt(i) as NodeTask;
		addData(n.data,n.step);
		}
		
		} */
		
		
		public function addData(data:Object,step:String,date:Date,isHeader:Boolean):void {
			if (data != null){
				
				rawDatas.addItem(data);
				
				var baseId:String = "";
				// Se iteran los campos que se tienen configurados 
				for (var i:int = 0 ; i < fields.length ; i ++){
					var field:String = fields.getItemAt(i) as String; 
					
					if (data.hasOwnProperty(field) && data[field] != null){
						var value:String =  data[field];
						
						var nodeId:String = baseId + ( i != 0 ? ":" : "" )+ value;
						var task:NodeTask = addNode( baseId, nodeId, i, data, field, value,step,date,isHeader);	
						baseId = nodeId;
						
						if (task != null && task.canRenderField(field)){
							break;
						}
					}
				}
			}
		}
		
		public function initRoots(fields:ArrayCollection,field:String,datas:ArrayCollection):void{
			for (var i:int = 0 ; i < datas.length ; i ++){
				var value:String = datas.getItemAt(i) as String;
				var data:Object = new Object();
				data[field] = value;
				addDataToRoot(field,value,data,fields );
			}
		}
		private var myBA:ByteArray = new ByteArray(); 
		public function addDataToRoot(field:String,nodeId:String,data:Object,fields:ArrayCollection):void{
			var parent:NodeResource = ctx.get("") as NodeResource;
			var node:NodeResource = ctx.get(nodeId) as NodeResource;
			
			// si el nodo no existe e crea y se pone en el mapa
			if (node == null) {
				
				node =  newNode();
				node.id = nodeId;
				
				//ReflectionHelper.copyParameters(data,node.data);
				//ReflectionHelper.copyParameterFromList(data,node     , fields, 1);
				//node.label = value;
				
				myBA.writeObject( data ); 
				myBA.position = 0; 
				node.data=myBA.readObject();
				myBA=null;
				node.setLabelField(field);
				ctx.put(nodeId,node);
				removeAll(node,parent.getChildren(),"id");
				insert   (node,parent.getChildren());
			}
		}
		
		
		public function shouldRenderTask(target:Object):Boolean {
			return FilterHelper.matchFilters(target,filters);
		}
		
		
		/**
		 * Este metodo nos ayuda agregar un nodo, ya sea GanttResource o un ArrayCollection
		 * 
		 * 
		 */
		public function addNode( baseId:String, nodeId:String, index:int,data:Object, field:String,value:String, step:String,date:Date,isHeader:Boolean):NodeTask {
			
			var parent:NodeResource;
			var shouldRender:Boolean = shouldRenderTask(data);
			if (value != null){
				
				// Se obtiene el padre para agregarle los nodos hijos
				// este nunca puede ser null
				parent = ctx.get(baseId) as NodeResource;
				
				var node:NodeResource = ctx.get(nodeId) as NodeResource;
				
				// si el nodo no existe e crea y se pone en el mapa
				if (node == null) {
					
					node =  newNode();
					node.isHeader = isHeader;
					node.id = nodeId;
					
					ReflectionHelper.copyParameters(data,node.data);
					ReflectionHelper.copyParameterFromList(data,node     , fields, index + 1);
					//node.label = value;
					node.setLabelField(field);
					//if (shouldRender || isHeader){
					if(shouldRender|| isHeader){
						
						ctx.put(nodeId,node);
						insert   (node,parent.getChildren());
					}
					//}
				}
				
				
				// si no es el ultimo se va a crear el nodo
				var task:NodeTask = newTask();
				if ((index == fields.length - 1 || (task.canRenderField(field))) && !isHeader){
					// si es el ultimo quiere decir que aqui se van a agregar como ArrayCollection
					
					task.data = data;
					
					node.hasVisibleTasks = true;
					task.setIdFields(fields);
					//ReflectionHelper.copyParameters(data,task);
					task.resourceId = nodeId;
					task.setContainer(node.getChildren());
					task.setAllTasks(allTasks);
					task.step = step;
					
					// Es una
					//if (task.step != GanttTask.STEP_GHOSTX) {
					removeFromAllResources(task,root,parent);
					if (shouldRender){
						insert   (task,node.getChildren());
					}
					//node.getChildren().refresh();
					//}
					
					//logger.info("addNode:ghost("+step+")"+getQualifiedClassName(this)+"\n" +  
					//	ReflectionHelper.object2XML(task,"task")
					//);
					removeAll(task,allTasks);
					if (shouldRender){
						if (date != null){
							timeChangedTask(date,task);
						}
						insert   (task,allTasks);
						
					}
					refreshAllTaskFrom(node.getChildren(),allTasks);
					
					allTasks.refresh();
					//logger.info("insert allTasks("+ReflectionHelper.object2XML(task)+")");
					return task;
				}
				else {
					node.hasVisibleTasks = false;
				}
			}
			return null;
		}
		protected function refreshAllTaskFrom(tasks:ArrayCollection,allTasks:ArrayCollection):void{
			for(var i:int = 0 ; i < tasks.length ; i ++){
				var task:* = tasks.getItemAt(i);
				allTasks.itemUpdated(task);
			}
		}
		
		
		protected function compareFunctions(target:Object,temp:Object):Boolean{
			return ReflectionHelper.compareObject(target,temp);
		}
		
		protected function compareFunctionJustField(target:Object,temp:Object, field:String):Boolean{
			return ReflectionHelper.compareObject(target[field],temp[field]);
		}
		
		private function removeFromAllResources(target:Object, node:NodeResource,parent:NodeResource):void{
			var arr:ArrayCollection = node.getChildren();
			
			var childIsTask:Boolean =  false;
			for ( var i:int = arr.length - 1 ; i >= 0   ; i--) {
				var temp:Object = arr.getItemAt(i);
				if (temp is NodeResource){
					var nodeTemp:NodeResource = temp  as NodeResource;
					removeFromAllResources(target, nodeTemp,node);
				}
				else {
					childIsTask = true;
					break;
				}
			}
			if (childIsTask){
				removeAll(target, arr);
				/*
				if (arr.length == 0){
					removeAll(node,parent.getChildren(),"id");
				}*/
			}
		}
		
	
		
		
		private function insert(target:Object, values:ArrayCollection):void{
			values.addItem(target);
			
		}
		
		public function removeAll(target:Object, values:ArrayCollection, compareField:String=null):void{
			
			if (values != null ){
				for ( var i:int = values.length - 1 ; i >= 0   ; i--) {
					if (values.length > 0){
						var temp:Object = values.getItemAt(i);
						try {
							if (compareField == null){
								if (compareFunctions(target,temp)){
									
									values.removeItemAt(i);
									//break;
								}
							}
							else {
								if (compareFunctionJustField(target,temp,compareField)){
									values.removeItemAt(i);
								}					
							}
						}
						catch (e:*){
							logger.error(e);
						}
					}
				}
				
			}
		}
	}
}