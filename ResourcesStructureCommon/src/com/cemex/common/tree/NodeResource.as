package com.cemex.common.tree
{
	import mx.collections.ArrayCollection;
	import mx.core.UIComponent;
	import mx.data.errors.NoDataAvailableError;

	//import flash.events.EventDispatcher;
	public class NodeResource 
	{
		public function NodeResource() {
			super();
			_children = new ArrayCollection();
			_data = new Object();
		}
		
		
		private var _id:String;
		private var _hasVisibleTasks:Boolean;
		
		private var _children:ArrayCollection;
		
		
		
		public var isHeader:Boolean = false;
		
		/*
		*/
		
		
		public function getChildren():ArrayCollection{
			return _children;
		}
		
		public function get children():ArrayCollection{
			if (!hasVisibleTasks){
				return _children;
			}
			else {
				return null;
			}
		}
		public function set children(_children:ArrayCollection):void{
			this._children = _children;
		}
		
		// este es un objeto que sirve para guardar datos que son
		// los que se utilizan para compara este nodo con algun otro 
		private var _data:Object; 
		
		public function get data():Object{
			return _data;
		}
		public function set data(_data:Object):void{
			this._data = _data;
		}
		
		
		public function get hasVisibleTasks():Boolean{
			return _hasVisibleTasks;
		}
		public function set hasVisibleTasks(_hasVisibleTasks:Boolean):void{
			this._hasVisibleTasks = _hasVisibleTasks;
		}
		
		public function get id():String{
			return this._id;
		}
		public function set id(_id:String):void{
			this._id = _id;
		}
		
		
		
		
		private var _labelField:String;
		
		public function setLabelField(_labelField:String):void{
			this._labelField = _labelField;
		}
		public function getLabelField():String{
			return this._labelField;
		}
		
		public function set labelField(_labelField:String):void{
		}
		public function get labelField():String{
			return this._labelField;
		}
		
		public function get childrenCount():int{
			return _children.length;
		}
		
		public function get totalChildrenCount():int {
			return countBranch(this);
		}
		
		protected static function countBranch(object:Object):int{
			
			if (object is NodeResource){
				// Es un nodo
				var node:NodeResource = object as NodeResource;
				var count:int = 0 ;
				for (var i:int = 0 ; i < node.getChildren().length ; i ++){
					count += countBranch(node.getChildren().getItemAt(i));
				}
				return count;
			}
			else {
				return 1;
			}
		}
		public function getMinStartDate():Date{
			var minDate:Date = null ;
			if ( getChildren() != null){
				var min:Number = -1 ;
				for(var i:int = 0 ; i < getChildren().length ; i ++){
					var child:Object  = getChildren().getItemAt(i);
					if (child is NodeTask){
						
						var ctask:NodeTask = child as NodeTask;
						if (ctask != null && ctask.startTime != null){
							//Alert.show(this.label +  ":c:"  + child.startTime);
							if (min == -1){
								min = ctask.startTime.getTime();
								minDate = ctask.startTime;
							}
							if (min > ctask.startTime.getTime()){
								min = ctask.startTime.getTime();
								minDate = ctask.startTime;
							}
						}
					}
					else if(child is NodeResource){
						var cresource:NodeResource  = child as NodeResource;
						var tempTime:Date = cresource.getMinStartDate();
							
						
						if (tempTime != null){
							if (min == -1){
								min = tempTime.getTime();
								minDate = tempTime;
							}
							if (min > tempTime.getTime()){
								min = tempTime.getTime();
								minDate = tempTime;								
							}
						}
					}
				}
			}
			return minDate;
		}
		
		
		/**
		 * Esta funcion sirve para saber si cierto nodo tiene un path simple
		 * 
		 */
		public function isOnePathBranch():Boolean {
			
			return countBranch(this) == 1;
		}
		
		/*public function cleanUID():void {
			ReflectionHelper.cleanParameters(this);
			var arr:ArrayCollection =  getChildren();
			for (var i:int = 0 ; i < arr.length ; i ++){
				var obj:Object =  arr.getItemAt(i);
				if (obj is NodeResource){
					var node:NodeResource = obj  as NodeResource;
					node.cleanUID();
				}
			}
		}*/
		
		
	}
}