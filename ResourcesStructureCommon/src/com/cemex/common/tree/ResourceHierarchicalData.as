package com.cemex.common.tree
{
	import mx.collections.ArrayCollection;
	import mx.collections.HierarchicalData;
	
	public class ResourceHierarchicalData extends HierarchicalData
	{
		public function ResourceHierarchicalData(value:Object=null)
		{
			super(value);
			
		}
		
		
		public override function getChildren(node:Object):Object
		{
			if (node == null)
				return null;
			var children:*;
			if (node is NodeResource)
			{
				var nodo:NodeResource = node as NodeResource;
				return nodo.getChildren();
			}
			//no children exist for this node
			if(children == undefined)
				return null;
			return children;
		}
		
		
		
		public override function canHaveChildren(node:Object):Boolean {
			/*try {
				if (node is NodeResource) {
					
					var nodo:NodeResource = node as NodeResource;
					
					//return !nodo.hasVisibleTasks;
					if (nodo.children != null && nodo.children.length > 0)  {
						//return !nodo.hasVisibleTasks;	
						return nodo.children.getItemAt(0) is NodeResource;
					}
				}
			}
			catch (e:Error) {
				trace("[Descriptor] exception checking for isBranch");
			}*/
			return false;
		}
		
		public override function hasChildren(node:Object):Boolean {
			/*try {
				if (node is NodeResource) {
					
					var nodo:NodeResource = node as NodeResource;
					if (nodo != null && nodo.getChildren() != null && nodo.getChildren().length > 0)  {
						//return !nodo.hasVisibleTasks;
						return nodo.children.getItemAt(0) is NodeResource;
						//return true;
					}
				}
			}
			catch (e:Error) {
				trace("[Descriptor] exception checking for isBranch");
			}*/
			return false;
		}


		

	}
}