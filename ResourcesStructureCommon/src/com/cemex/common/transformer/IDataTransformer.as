package com.cemex.common.transformer
{
	import com.cemex.common.tree.NodeTask;
	
	import ilog.gantt.GanttDataGrid;
	import ilog.gantt.GanttSheet;
	
	import mx.collections.ArrayCollection;
	import mx.collections.Sort;

	public interface IDataTransformer {
		
		
		function addData(data:Object,step:String,date:Date):void;
		//function addDataNodeInLine(node:NodeTask):void;
		
		function addDatas(datas:ArrayCollection,step:String,date:Date):void;
		
		function initRoots(datas:ArrayCollection):void;
		function initHeaders(datas:ArrayCollection):void;
		
		
		function regenerate():void;
		
		function timeChanged(time:Date):void;
		
		function getViewID():String;
		
		//function updateAll():void;
		//function checkTimedValues():void;
		//function refresh():void;
		
		function getResourcesTree():ArrayCollection;
		function getTasksTree():ArrayCollection;
		function setFilters(filters:ArrayCollection):void;
		
		function setColumns(dataGrid:GanttDataGrid,ganttSheet:GanttSheet):void;
		function getSorter():Sort;
		function getSorterFunction(field:String):Function;
		
/* 		function getStartTime(payload:Object):Date;
		function getEndTime(startTime:Date,payload:Object):Date;
		
		function getColor(payload:Object):Number;
		function getBorder(payload:Object):Number;
		function getThickness(payload:Object):Number;
		
	 */
		
	}
}