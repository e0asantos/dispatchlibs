package com.cemex.common.transformer
{
	import com.cemex.rms.common.gantt.GanttResource;
	import com.cemex.rms.common.gantt.GanttTree;
	
	import ilog.gantt.GanttDataGrid;
	import ilog.gantt.GanttSheet;
	
	import mx.collections.ArrayCollection;
	import mx.collections.Sort;
	import mx.collections.SortField;
	import mx.controls.AdvancedDataGrid;
	import mx.controls.Alert;
	import mx.controls.advancedDataGridClasses.AdvancedDataGridColumn;
	import mx.core.IFactory;
	import mx.events.AdvancedDataGridEvent;
	import mx.events.DataGridEvent;
	import mx.utils.ObjectUtil;

	public class GenericDataTransformer  implements IDataTransformer
	{
		protected var tree:GanttTree;
		
		protected var fields:ArrayCollection;
		protected var fieldColumns:ArrayCollection;
		
		
		public function GenericDataTransformer() {
			
			super();
		
		}
		public function getViewID():String{
			throw new Error("The viewID should be Overriden");
		}
		public function timeChanged(time:Date):void {
			tree.timeChanged(time);
		}
		
		
		
		protected function getColumn(field:FieldColumn):AdvancedDataGridColumn{
			
			var column:AdvancedDataGridColumn =  new AdvancedDataGridColumn();
			column.width = field.width;
			column.dataField = field.name;
			
			column.headerText = field.text;
			//column.sortCompareFunction = getSorterFunction(field.name);	
			
			if (field.renderer != null){
				column.itemRenderer = field.renderer;
			}
			if (field.headerRenderer != null){
				column.headerRenderer = field.headerRenderer;
				column.sortable = false;
			}
			return column;
		}
		
		
		//public function getSortFunction(field:String):Function {
		//	//return dob_sort;
		//	return null;
		//}
		
		/*public function dob_sort(itemA:Object, itemB:Object):int {
			//var dateA:Date = itemA.dob ? new Date(itemA.dob) : null;
			//var dateB:Date = itemB.dob ? new Date(itemB.dob) : null;
			//return ObjectUtil.dateCompare(dateA, dateB);
			return ObjectUtil.stringCompare(itemA["label"],itemB["label"]);             
		} 
		*/
		
		
		
		public function setColumns(dataGrid:GanttDataGrid,ganttSheet:GanttSheet):void{
			var arr:Array =  new Array();
			for (var i:int=0 ; i < fieldColumns.length ; i ++  ){
				arr.push(getColumn(fieldColumns.getItemAt(i) as FieldColumn));
			}
			dataGrid.columns = arr;
			ganttSheet.taskItemRenderer =  getTaskRenderer();
			setColumnsDataGridCustomization(dataGrid);
			setTreePlusMinus(dataGrid);
		}
		public  function setTreePlusMinus(dataGrid:GanttDataGrid):void{
			
		
		}
		
		
		
		
		public  function setColumnsDataGridCustomization(dataGrid:GanttDataGrid):void{
			
			dataGrid.styleFunction=null; 
			dataGrid.iconFunction=null;
			
			
		}
	
		public function getTaskRenderer():IFactory{
			throw new Error("The method getTaskRendere should be implemented");
		}
		public function setFieldColumns(fieldColumns:ArrayCollection):void{
			this.fieldColumns =  fieldColumns;
			var ad:AdvancedDataGrid;
			
		}
		
		public function setFields(fields:ArrayCollection):void{
			this.fields =  fields;
			tree = new GanttTree(fields);
		}
		public function setFilters(filters:ArrayCollection):void{
			tree.setFilters(filters);
		}
		
		public function getSorter():Sort {
			
			var sort:Sort = new Sort();
			
				
				
					var temp:FieldColumn = fieldColumns.getItemAt(0) as FieldColumn;
					var sorter:SortField = new SortField(temp.name, false,false);
					//var sorter:SortField = new SortField(null, false,false);
					sorter.compareFunction = getSorterFunction(temp.name);
					
					//sorter = new SortField(temp.name, false,false);
					//sorter.compareFunction = sortByMinDate;
					//sortFields.push(sorter);
				
				sort.fields = [sorter];
				
				
				//var fooSort:Sort = new Sort();
				//var sortField:SortField = new SortField(null, true);
				//fooSort.compareFunction = compareFooAssets;
				//fooAssetsADG.dataProvider.sort = fooSort;
				//fooSort.fields = [sortField];
				//fooAssetsADG.dataProvider.refresh();

				
			
			//sort.compareFunction = getSorterFunction(temp.name);
			
			return sort;
		}
		
		public function getSorterFunction(field:String):Function{
			return sortByMinDate;
		}
		
		public function sortByMinDate(a:Object=null, b:Object=null, c:Object=null):int {
			//Alert.show("compare(a:"+a+",b:"+b+")");
			var x:GanttResource = a as GanttResource;
			var y:GanttResource = b as GanttResource;
			if (x != null && y != null){
				//Alert.show("stringCompare(a:"+x.label+":"+FlexDateHelper.getTimestampString(x.getMinStartDate())+" | b:"+y.label+":"+FlexDateHelper.getTimestampString(y.getMinStartDate())+") | c:" + ObjectUtil.dateCompare(x.getMinStartDate(),y.getMinStartDate()));
				return ObjectUtil.dateCompare(x.getMinStartDate(),y.getMinStartDate());
				
				/*if (x.hasVisibleTasks && y.hasVisibleTasks){
					

					return ObjectUtil.dateCompare(x.getMinStartDate(),y.getMinStartDate());
				}	
				else {
					//Alert.show("stringCompare(a:"+x.label+":"+FlexDateHelper.getTimestampString(x.getMinStartDate())+" - b:"+y.label+":"+FlexDateHelper.getTimestampString(y.getMinStartDate())+")\nc:" + c);
					return ObjectUtil.dateCompare(x.getMinStartDate(),y.getMinStartDate());
					
					if (x.getChildren().length != 0 && y.getChildren().length != 0){
						
						//return ObjectUtil.compare(x.getChildren().length,y.getChildren().length);
					}
					else {
						if (x.getChildren().length == 0 || y.getChildren().length == 0){
							Alert.show("stringCompare(a:"+x.label+":"+FlexDateHelper.getTimestampString(x.getMinStartDate())+"\nb:"+y.label+":"+FlexDateHelper.getTimestampString(y.getMinStartDate())+")");
							return ObjectUtil.compare(x.getChildren().length,y.getChildren().length);
						}
						else {
							//Alert.show("label");
							return ObjectUtil.stringCompare(x.label,y.label);
						}
					}
					if (x.hasVisibleTasks){
						Alert.show("x.has");
						return 1;
					}
					else if (y.hasVisibleTasks){
						Alert.show("y.has");
						return -1;
					}
					else {
						//
						//Alert.show("yObject");
						
						
						
						
					}	
				}
				*/
			}
			else {
				Alert.show("compare(a:"+a+",b:"+b+")");
				return ObjectUtil.compare(a,b);
			}
		}
		
		
		
		
		//ESte metodo no se va a usar
		private function advancedDataGridDefaultSort(dataGrid:GanttDataGrid, dgColumn:int):void{
			dataGrid.dispatchEvent(new AdvancedDataGridEvent(AdvancedDataGridEvent.SORT, false,true,dgColumn));
		}
		// Este metodo no sale
		private function dataGridDefaultSort(dgName:Object, dgColumn:int):void{ 
			dgName.dispatchEvent(new DataGridEvent(DataGridEvent.HEADER_RELEASE,   false,true,dgColumn,null,0,null,null,0));
		}
		
		
		public function addData(data:Object,step:String,date:Date):void {
			tree.addData(data,step,date,false);
		}
		
		
		public function initRoots(datas:ArrayCollection):void{
			tree.reset();
			tree.initRoots(fields,fields.getItemAt(0) as String,datas);
		}

		/* 
		public function addDataNodeInLine(node:NodeTask):void {
			tree.addDataNodeInLine(node);
		} */
		
		
		public function addDatas(datas:ArrayCollection,step:String,date:Date):void{
			
			for (var i:int = 0 ; i < datas.length ; i ++) {
				var temp:Object =  datas[i];
				addData(temp,step,date);
			}
		}
		
		
		public function initHeaders(datas:ArrayCollection):void{
			tree.reset();
			for (var i:int = 0 ; i < datas.length ; i ++) {
				var temp:Object =  datas[i];
				addHeader(temp);
			}
		}
		public function addHeader(data:Object):void {
			tree.addData(data,null,null,true);
		}
		public function regenerate():void {
			tree.regenerate();
		}
		
		
		public function getResourcesTree():ArrayCollection{
			
			var result:ArrayCollection =  this.tree.getResourcesTree();
			return result;
		}
		public function getTasksTree():ArrayCollection{
			return this.tree.getTasksTree();
		}
		
		
	
	}
}