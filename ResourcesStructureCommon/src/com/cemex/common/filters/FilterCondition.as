package com.cemex.common.filters
{
	import mx.collections.ArrayCollection;

	public class FilterCondition
	{
		public function FilterCondition()
		{
			this.values =  new ArrayCollection();
		}
		public static const EQUALS:String = "EQ";
		public static const GREATER_THAN:String = "GT";
		public static const LOWER_THAN:String = "LT";
		
		
		public var values:ArrayCollection;
		public var field:String;
		public var action:String ;
		
	}
}