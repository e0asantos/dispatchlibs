package stress.listeners
{
	import com.cemex.rms.common.PushInfo;
	import com.cemex.rms.common.lcds.AbstractReceiverService;
	import com.cemex.rms.common.reflection.ReflectionHelper;
	
	import flash.utils.ByteArray;
	
	import mx.messaging.ChannelSet;
	import mx.messaging.messages.IMessage;
	
	import stress.StressEvent;
	
	public class TestPI extends AbstractReceiverService
	{
		public function TestPI()
		{
			super();
		}
		
		public override function getDestinationName():String{
			return "TestPI";
		}
		public override function getChannelSet():ChannelSet{
			
			return servicesContext.getValue("ChannelSetWEBORB") as ChannelSet;
		}
		
		public var counter:Number = 0;
		override protected function receiveMessage(message:Object):void{		
			var e:StressEvent =  PushInfo.logObjectInfo(getDestinationName(),++counter,message,logger);
			dispatcher.dispatchEvent(e);
		}
	}
}