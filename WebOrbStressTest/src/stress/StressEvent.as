package stress
{
	import flash.events.Event;
	
	public class StressEvent extends Event
	{
		public static const DATA_ARRIVED:String = "dataArrivedEvent";
		public var text:String;
		public function StressEvent(text:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(DATA_ARRIVED, bubbles, cancelable);
			this.text = text;
		}
	}
}