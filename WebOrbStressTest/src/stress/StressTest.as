package stress
{
	import com.cemex.rms.common.logging.ILogger;
	import com.cemex.rms.common.logging.LoggerFactory;
	import com.cemex.rms.common.logging.events.LoggerEvent;
	import com.cemex.rms.common.push.PushServiceStatusEvent;
	import com.cemex.rms.common.services.ServiceFactory;
	import com.cemex.rms.common.services.events.ServiceManagerEvent;
	
	import mx.controls.Alert;
	import mx.core.UIComponent;

	public class StressTest
	{
		public function StressTest(id:int,parent:UIComponent)
		{
			logger = LoggerFactory.getLogger("Stress["+id+"]");
			
			this.ui = new UIComponent();
		
				this.parent = parent;
		
			this.id = id;
			factory = new ServiceFactory();
			factory.dispatcher = ui;
			factory.model = new TestServicesModel();
			
			
			ui.addEventListener(LoggerEvent.LOGGER_EVENT,logHandler);
			ui.addEventListener(PushServiceStatusEvent.STATUS_SERVICE_CONNECTED,serviceStatusHandler);
			ui.addEventListener(PushServiceStatusEvent.STATUS_SERVICE_DISCONNECTED,serviceStatusHandler);
			ui.addEventListener(PushServiceStatusEvent.STATUS_SERVICE_FAULT,serviceStatusHandler);
			ui.dispatchEvent(new ServiceManagerEvent(ServiceManagerEvent.SERVICES_LOAD_REQUEST)) ;
			ui.addEventListener(StressEvent.DATA_ARRIVED,dataArrived);
				
		}
		private var id:int;
		private var ui:UIComponent;
		private var parent:UIComponent;
		private var factory:ServiceFactory;
		private var logger:ILogger;
		
		public function dataArrived(e:StressEvent):void{
			var text :String = "Stress["+id+"]\t"+e.text;
			var event:StressEvent = new StressEvent(text);
			logger.warning(text);
			parent.dispatchEvent(event);
		}
		public function serviceStatusHandler(e:PushServiceStatusEvent):void{
			logger.debug(e.serviceName + "->" + e.type);	
		}
		
		public function logHandler(e:LoggerEvent):void{ 
			//logger.debug(e.log);	
			//Alert.show(""+e+"");
		}
		
	}
}