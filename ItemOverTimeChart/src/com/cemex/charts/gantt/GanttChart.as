package com.cemex.charts.gantt
{
	import com.adobe.fiber.runtime.lib.DateTimeFunc;
	import com.cemex.charts.gantt.common.FlexDateHelper;
	import com.cemex.charts.gantt.common.TimeConstants;
	import com.cemex.charts.gantt.views.GanttChartComponent;
	import com.cemex.charts.gantt.views.TimeLineTasks;
	import com.cemex.charts.gantt.views.mediators.DraggingEvent;
	import com.cemex.charts.gantt.views.time.TimeController;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.binding.utils.BindingUtils;
	import mx.containers.Canvas;
	import mx.controls.Alert;
	import mx.core.DragSource;
	import mx.core.IFactory;
	import mx.core.UIComponent;
	import mx.events.DragEvent;
	import mx.events.FlexEvent;
	import mx.managers.DragManager;

	public class GanttChart extends Canvas
	{
		public function GanttChart()
		{
			super();
			addEventListener(DraggingEvent.DRAG_TRIGGER,dragTrigger);
			addEventListener(DraggingEvent.DRAG_OVER_TARGET,dragOverTarget);
			addEventListener(DraggingEvent.DRAG_DROP,dropInTarget);
		}
		
		public var context:GanttContext;
		[Inspectable]
		public var view:GanttChartComponent;
		
		
		public function set resourcesDataProvider(dataProvider:Object):void{
			var binding:Object = new Object();
			binding.dataProvider = dataProvider; 
			BindingUtils.bindProperty(view.resources,"dataProvider",binding,"dataProvider");
			//view.resources.dataProvider = dataProvider;	
		}
		public function get resourcesDataProvider():Object{
			return view.resources.dataProvider;	
		}
		
		
		
		public function set zoom(zoomDelta:int):void {
			
			//Alert.show("zoomDelta:"+zoomDelta);
			
			time.setZoomDelta(zoomDelta);
			//context.getTimeController().setZoomDelta(zoomDelta );
		}
		public function get zoom():int {
			return 0;	
		}
		
		
		public function set tasksDataProvider(dataProvider:Object):void{
			var binding:Object = new Object();
			binding.dataProvider = dataProvider; 
			BindingUtils.bindProperty(view.tasks,"dataProvider",binding,"dataProvider");	
		}
		public function get tasksDataProvider():Object{
			return view.tasks.dataProvider;	
		}
		
		
		
		private var _taskRenderer:IFactory;
		[Inspectable]
		public function set taskRenderer(renderer:IFactory):void{
			_taskRenderer = renderer;
		}
		public function get taskRenderer():IFactory{
			return _taskRenderer;	
		}
		
		
		
		protected override function createChildren():void
		{
			context  =  new GanttContext(this);
			super.createChildren();
			
			view = new GanttChartComponent();
			view.percentHeight = 100;
			view.percentWidth = 100;
			addChild(view);
			
			
		
		}	
		
		
		public function isMovementPermitted(dragInitiator:*,dropTarget:*):Boolean{
			return true;
		}
		
		
		public function dragTrigger(e:DraggingEvent):void {
			
			var sourceEvent:Event = e.sourceEvent;
			//Alert.show("dragTrigger:"+sourceEvent);
			if (sourceEvent is MouseEvent) {
				var mouseEvent:MouseEvent = sourceEvent as MouseEvent;
				// the drag initiator is the object being dragged (target of the mouse event)
				var dragInitiator:UIComponent = mouseEvent.currentTarget as UIComponent;
				// the drag source contains data about what's being dragged
				var dragSource:DragSource = new DragSource();
				dragSource.addData(mouseEvent,"InitialMouseEvent");
				// ask the DragManger to begin the drag
				
				DragManager.doDrag( dragInitiator, dragSource, mouseEvent);
				
				mouseEvent.stopImmediatePropagation();
				
			}
			else {
				throw new Error("BuilderAssemblerManager.mouseDownPaletteEvent el evento sourceEvent("+sourceEvent+") no es MouseEvent");
			}
		}
		public function get time():TimeController {
			return view.time;
		}
		
		public function dragOverTarget(e:DraggingEvent):void {
			
			var sourceEvent:Event = e.sourceEvent;
			//Alert.show("dragOverTarget:"+sourceEvent);
			if(sourceEvent is DragEvent) {
				
				var dragEvent:DragEvent = sourceEvent as DragEvent;
				
				var dropTarget:TimeLineTasks = dragEvent.currentTarget as TimeLineTasks;
				var dragInitiator:UIComponent = dragEvent.dragInitiator as UIComponent;
				
				//dragInitiator.hasOwnProperty("data")
				//&& dropTarget.hasOwnProperty("data")
				
				if (dropTarget != null){
					if (!isMovementPermitted(dragInitiator,dropTarget)){
						
						DragManager.acceptDragDrop( dropTarget );
						DragManager.showFeedback(DragManager.NONE);
						//clearAll();
					}
					else {
						
						DragManager.acceptDragDrop( dropTarget );
						DragManager.showFeedback(DragManager.COPY);						
					}
				}
				
				
			} 
		}
		
		
		public function dropInTarget(e:DraggingEvent):void {
			
			var sourceEvent:Event = e.sourceEvent;
			//Alert.show("dropInTarget:"+sourceEvent);
			if(sourceEvent is DragEvent) {
				
				var dragEvent:DragEvent = sourceEvent as DragEvent;
				
				var dragInitiator:UIComponent = dragEvent.dragInitiator as UIComponent;
				var dropTarget:TimeLineTasks = dragEvent.currentTarget as TimeLineTasks;
				var ime:MouseEvent = dragEvent.dragSource.dataForFormat("InitialMouseEvent") as MouseEvent;
				if (dropTarget != null){
					var equis:Number = (dragEvent.localX - ime.localX);
					Alert.show("local:"+equis + "\n" +
					"::" + FlexDateHelper.getTimestampString(dropTarget.mediator.time.getPositionTime(equis)));
					
				
					//Alert.show(FlexDateHelper.getTimestampString(now) +"\n"+ 
				//		FlexDateHelper.getTimestampString(now2));
					dropTarget.mediator.temp.x = equis;
					dropTarget.mediator.temp.texto.text = FlexDateHelper.getTimestampString(dropTarget.mediator.time.getPositionTime(equis));
				}
				
				
				//addElement(dragInitiator,dropTarget);
				 
				
			}
		}
		
		
	}
}