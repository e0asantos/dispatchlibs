package com.cemex.charts.gantt.views.components
{
	import mx.collections.ArrayCollection;

	public class TimeBarDescriptor
	{
		public function TimeBarDescriptor()
		{
		}
		
		public var mainText:String;
		public var columns:ArrayCollection;
		
		public function addColumn(text:String, width:int):void{
			if (columns == null){
				columns = new ArrayCollection();
			}
			var column:TimeBarColumnDescriptor = new TimeBarColumnDescriptor();
			column.text = text;
			column.width = width;
			columns.addItem(column);
		}
		
	}
}