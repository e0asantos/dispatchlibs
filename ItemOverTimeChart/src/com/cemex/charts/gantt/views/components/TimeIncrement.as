package com.cemex.charts.gantt.views.components
{
	import com.adobe.fiber.runtime.lib.DateTimeFunc;

	public class TimeIncrement
	{
		public function TimeIncrement()
		{
		}
		
		public var incremetDelta:Number;
		public var incremetUnits:String;
		public var times:Number;
		public var formatter:String;
		
		public var minWidth:Number;
		
		private var _millisecondsIncrement:Number = -1;
		public function get millisecondsIncrement():Number {
			if (_millisecondsIncrement == -1){
				
				
				var now:Date = new Date();
				var another:Date = DateTimeFunc.dateAdd(incremetUnits,incremetDelta,now);
				_millisecondsIncrement = another.getTime() - now.getTime();
			}
			return _millisecondsIncrement;
		}
		
		
		
	}
}