package com.cemex.charts.gantt.views
{
	import com.cemex.charts.gantt.views.mediators.TimeLineTasksMediator;
	import com.cemex.charts.gantt.views.time.TimeController;
	
	import flash.display.Graphics;
	
	import mx.binding.utils.BindingUtils;
	import mx.containers.Canvas;
	import mx.controls.Alert;
	import mx.core.UIComponent;
	import mx.events.ResizeEvent;
	
	public class TimeLineTasks extends Canvas
	{
		public function TimeLineTasks() {
			super();
			setStyle("backgroundColor",0xFFFFFF);
			setStyle("backgroundAlpha",0.0001);
			
			verticalScrollPolicy="off";
			horizontalScrollPolicy="off";
			
		}
		
		
		public var mediator:TimeLineTasksMediator;
	/*	
		private var _time:TimeController;
		public function set time(_time:TimeController):void{
			this._time = _time;
		}
		
		public function get time():TimeController{
			return _time;
		}
		
		*/
		public override function get data():Object{
			return super.data;
		}
		
		
		
		public override function set data(_data:Object):void{
			super.data = _data;
			if (_data["height"] != null){
				BindingUtils.bindProperty(this,"height",data,["height"]);
			}
		}
		
		
		
		override public function get height():Number{
			if (this["data"] != null && this["data"]["height"] != null){
				return this["data"]["height"];
			}
			else {
				return super.height;
			}
		}
		
		
		override protected function updateDisplayList (w : Number, h : Number) :void {
			super.updateDisplayList (w, h);
			var g : Graphics = graphics;
			
		}
		
		
		
	}
}