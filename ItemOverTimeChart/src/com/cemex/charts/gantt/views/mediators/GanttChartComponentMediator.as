package com.cemex.charts.gantt.views.mediators
{
	import com.cemex.charts.gantt.GanttChart;
	import com.cemex.charts.gantt.common.FlexDateHelper;
	import com.cemex.charts.gantt.common.TimeControllerEvent;
	import com.cemex.charts.gantt.views.GanttChartComponent;
	import com.cemex.charts.gantt.views.time.TimeController;
	
	import flash.events.MouseEvent;
	import flash.profiler.showRedrawRegions;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ICollectionView;
	import mx.collections.IViewCursor;
	import mx.controls.Alert;
	import mx.events.AdvancedDataGridEvent;
	import mx.events.ListEvent;
	import mx.events.ScrollEvent;
	
	import org.robotlegs.mvcs.Mediator;
	
	public class GanttChartComponentMediator extends Mediator
	{
		public function GanttChartComponentMediator()
		{
			super();
		}
		
		[Inject]
		public var view:GanttChartComponent;
		
		
		override public function onRegister():void {
			//view = (contextView as GanttChart).view;
			
			
			view.resources.addEventListener(AdvancedDataGridEvent.ITEM_CLOSE,itemCloseHandler);
			view.resources.addEventListener(AdvancedDataGridEvent.ITEM_OPEN,itemOpenHandler);
			view.resources.addEventListener(ScrollEvent.SCROLL,scrollHandler);
			view.resources.addEventListener(ListEvent.CHANGE,changeSelectionHandler);
			
			
			//view.tasks.addEventListener(ScrollEvent.SCROLL,scrollHandler);
			view.tasks.addEventListener(ListEvent.CHANGE,changeSelectionHandler);
			//view.time.dataProvider = new ArrayCollection();
			var percent:Number = time.getOnViewPercent() * view.timeScroller.maxScrollPosition;
			view.timeScroller.scrollPosition = percent ;
			//Alert.show("percent:"+percent);
			/*
			
			view.timeScroller.minScrollPosition = 0;
			view.timeScroller.maxScrollPosition = interval;
			
			scrollPosition="50" 
			maxScrollPosition="100" 
			minScrollPosition="0"
			pageSize="10"  
			repeatDelay="1000" 
			repeatInterval="500"
			*/			
			
			
			
			view.timeScroller.addEventListener(ScrollEvent.SCROLL,timeScrollerScrollHandler);
			
			view.timeScroller.addEventListener(MouseEvent.MOUSE_WHEEL,timeScrollerWheelHandler);
			//view.timeScroller.addEventListener(MouseEvent.CLICK,timeScrollerClickHandler);
			
			// SEccion de eventos del mouse
			//view.reconnect.addEventListener(MouseEvent.CLICK,reconnect);
			
			//eventMap.mapListener(eventDispatcher,TogglePlaintViewEvent.TOGGLE_PLAIN_VIEW,togglePlainView);
			eventMap.mapListener(eventDispatcher,TimeControllerEvent.VISIBLE_TIME_CHANGE,changeVisibleTimeBar);
			
			//Alert.show("timeController:"+timeController);
		}
		
		public function changeVisibleTimeBar(e:TimeControllerEvent):void{
			
			//var columns:Col = timeController.getColumns();
		}
		
		// ****************
		// time Scroller 
		// ****************
		public function timeScrollerWheelHandler(e:MouseEvent):void {
			
			setScrollPosition(view.timeScroller.scrollPosition - e.delta);
			
		}
		
		public function setScrollPosition(pos:Number):void{
		
			if (pos > view.timeScroller.maxScrollPosition){
				pos = view.timeScroller.maxScrollPosition;
			}
			else if (pos < view.timeScroller.minScrollPosition){
				pos = view.timeScroller.minScrollPosition;
			}
			view.timeScroller.scrollPosition = pos;
			
			
			var percent:Number = (pos) / view.timeScroller.maxScrollPosition;
			var date:Date = time.getPercentDate(percent);
 			time.setVisibleScreenStart(date);
			
		}
		
		public function timeScrollerClickHandler(e:MouseEvent):void {
			var x:Number = e.localX;
			
			//var y:int = e.localY;
			
			//Alert.show("e.x:"+x);
		}
		public function timeScrollerScrollHandler(e:ScrollEvent):void{
			
			//Alert.show("delta:"+e.delta);
			
			setScrollPosition(view.timeScroller.scrollPosition );
			
			//var percent:Number = (view.timeScroller.scrollPosition + e.delta) / view.timeScroller.maxScrollPosition;
			
			//Alert.show(FlexDateHelper.getTimestampString(time.getPercentDate(percent)));
			//var percent:Number = time.getOnViewPercent() * ;
			
		}
		
		public function get time():TimeController{
			return view.time;
		}
		
		
		// ****************
		// Visible Resources
		// ****************
		
		
		public function itemOpenHandler(e:AdvancedDataGridEvent):void{
			reasignTasksData();
		}
		public function itemCloseHandler(e:AdvancedDataGridEvent):void{
			reasignTasksData();
		}
		public function reasignTasksData():void {
			var topIndex:int = view.resources.verticalScrollPosition;
			var numItemsInWindow:int = ICollectionView(view.resources.dataProvider).length - view.resources.maxVerticalScrollPosition;
			
			var dp:Object = view.resources.dataProvider;
			var cursor:IViewCursor = dp.createCursor();
			var newData:ArrayCollection =  new ArrayCollection();
			while( !cursor.afterLast )
			{
				newData.addItem(cursor.current);
				// Access each column field like: cursor.current.MyFieldName
				//trace(cursor.current["Actual"] +"::"+ getQualifiedClassName(cursor.current));
				// Obviously don't forget to move to next row:
				cursor.moveNext();
			} 
			view.tasks.dataProvider = newData;
		}
		
		
		
		// ****************
		// Scroll and selection
		// ****************
		public function scrollHandler(e:ScrollEvent):void{
			//var position:int = e.position;
			view.tasks.verticalScrollPosition = view.resources.verticalScrollPosition;
		}
		public function changeSelectionHandler(e:ListEvent):void{
			//var index:int = e.rowIndex;
			view.tasks.selectedIndex= view.resources.selectedIndex;
			
		}
	}
}