package com.cemex.charts.gantt.views.mediators
{
	
	import flash.events.Event;
	
	public class DraggingEvent extends Event
	{
		public static const DRAG_TRIGGER:String 		= "triggerDraggingEvent";
		public static const DRAG_OVER_TARGET:String 	= "overTargetDraggingEvent";
		public static const DRAG_DROP:String 			= "dropDraggingEvent";
		
		[Bindable]
		public var sourceEvent:Event;
		
		public function DraggingEvent(type:String,sourceEvent:Event=null, bubbles:Boolean = true, cancelable:Boolean = true) {
			
			super(type, bubbles, cancelable);
			
   			this.sourceEvent = sourceEvent;
		}

	}
}