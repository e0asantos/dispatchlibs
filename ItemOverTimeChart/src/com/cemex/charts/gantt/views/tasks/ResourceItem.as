package com.cemex.charts.gantt.views.tasks
{
	public class ResourceItem
	{
		public function ResourceItem()
		{
		}
		
		private var _data:Object;
		private var _startTime:Date;
		private var _endTime:Date;
		private var _resourceId:String;
		
		public function get data():Object{
			return _data;
		}
		public function get startTime():Date{
			return _startTime;
		}
		public function get endTime():Date{
			return _endTime;
		}
		public function get resourceId():String{
			return _resourceId;
		}
		
		
		public function set data(_data:Object):void{
			this._data = _data;
		}
		public function set startTime(_startTime:Date):void{
			this._startTime = _startTime;
		}
		public function set endTime(_endTime:Date):void{
			this._endTime = _endTime;
		}
		public function set resourceId(_resourceId:String):void{
			this._resourceId = _resourceId;
		}
	}
}