package com.cemex.charts.gantt
{
	import com.cemex.charts.gantt.views.GanttChartComponent;
	import com.cemex.charts.gantt.views.TimeLineTasks;
	import com.cemex.charts.gantt.views.mediators.GanttChartComponentMediator;
	import com.cemex.charts.gantt.views.mediators.TimeLineTasksMediator;
	import com.cemex.charts.gantt.views.time.ITimeController;
	import com.cemex.charts.gantt.views.time.TimeController;
	import com.cemex.charts.gantt.views.time.TimeControllerMediator;
	
	import flash.display.DisplayObjectContainer;
	
	import mx.controls.Alert;
	
	import org.robotlegs.core.IMediator;
	import org.robotlegs.mvcs.Context;
	
	public class GanttContext extends Context
	{
		public function GanttContext(contextView:DisplayObjectContainer=null, autoStartup:Boolean=true)
		{
			super(contextView, autoStartup);
			//var mainMediator:GanttChartComponentMediator =  new GanttChartComponentMediator();
			//mediatorMap.registerMediator(contextView,mainMediator);
			
		}
		override public function startup():void
		{
			// Se settea el mapa de Vistas mediador
			//mediatorMap.mapView(LoadPerOrderOrderResourceRenderer, LoadPerOrderOrderResourceRendererMediator);
			
			//injector.mapSingletonOf(ITimeController,TimeController);
			
			injector.mapSingleton(TimeControllerMediator);
			
			mediatorMap.mapView(GanttChartComponent,GanttChartComponentMediator);
			mediatorMap.mapView(TimeLineTasks,TimeLineTasksMediator);
			mediatorMap.mapView(TimeController,TimeControllerMediator);
			
			
			//var mediator:IMediator = mediatorMap.createMediator(contextView);
			//mediator.setViewComponent(contextView);
			
			
			//Alert.show("startup2");
			
			
			// Se mappean los eventos con comandos
			//commandMap.mapEvent(ContextEvent.STARTUP, StartupCommand, ContextEvent, true);
						
		}
	}
}