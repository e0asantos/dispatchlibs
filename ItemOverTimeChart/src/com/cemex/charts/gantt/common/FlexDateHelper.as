package com.cemex.charts.gantt.common
{
	import mx.formatters.DateFormatter;

	public class FlexDateHelper
	{
		public function FlexDateHelper()
		{
		}
		
		public static function getDayOfMonth(date:Date):String{
			var formater:DateFormatter =  new DateFormatter();
			formater.formatString = "DD";
			return formater.format(date);
		}
		
		public static function getDateString(date:Date,format:String):String{
			var formater:DateFormatter =  new DateFormatter();
			
			formater.formatString = format;
			return formater.format(date);
		}
		
		public static function parseTime(base:Date, value : String, extraMinutes:Number=0) : Date
		{
			var split:Array = value.split(":");
			var currentDate:Date = FlexDateHelper.getDateWithHours(base,Number(split[0]), Number(split[1]), Number(split[2]));
			
			return FlexDateHelper.addMinutes(currentDate, extraMinutes);
		}
		
		public static function parseTimeString(date:Date) : String
		{
			var dateFormatter : DateFormatter = new DateFormatter();
			dateFormatter.formatString = "MM/DD/YYYY JJ:NN:SS";
			return dateFormatter.format(date);
		}
		
		public static function getEnglishTimestampString(date:Date) : String 
		{
			return getDateString(date, "MM/DD/YYYY JJ:NN:SS");
		}
		public static function getTimestampString(date:Date) : String 
		{
			return getDateString(date, "YYYY/MM/DD JJ:NN:SS");
		}
		
		
		public static function subSeconds (base:Date, seconds:int):Date{
			
			if (seconds > 3600){
				return base;
			}
			
			var currHour:Number = base.getHours() ;
			var currMin:Number = base.getMinutes()  - ((seconds / 60) % 60);
			var currSec:Number = base.getSeconds() - (seconds % 60);
			
			if (currSec < 0){
				currSec = 60 - currSec;
				currMin--;
			}
			if (currMin < 0){
				currMin = 60 - currMin;
				currHour--;
			}
			
			var currentDate:Date = new Date(base.getFullYear(), base.getMonth(), Number(getDayOfMonth(base)), currHour, currMin,currSec);
			return currentDate 	;
		}
		
		
		
		public static function addMinutes (base:Date, extraMinutes:int):Date{
			
			var deltaMin:Number = extraMinutes % 60;
			var deltaHour:Number = Math.floor(extraMinutes / 60) % 60;
			var deltaDay:Number = Math.floor(Math.floor(extraMinutes / 60) / 60);
			
			var currentDate:Date = new Date(base.getFullYear(), base.getMonth(), Number(getDayOfMonth(base))+deltaDay, base.getHours() + deltaHour, base.getMinutes()+deltaMin,base.getSeconds());
			return currentDate ;
		}
		
		/*
		public static function addSeconds (base:Date, extraSeconds:int):Date{
			
			var deltaMin:Number = extraMinutes % 60;
			var deltaHour:Number = Math.floor(extraMinutes / 60) % 60;
			var deltaDay:Number = Math.floor(Math.floor(extraMinutes / 60) / 60);
			
			var currentDate:Date = new Date(base.getFullYear(), base.getMonth(), Number(getDayOfMonth(base))+deltaDay, base.getHours() + deltaHour, base.getMinutes()+deltaMin,base.getSeconds());
			return currentDate ;
		}
		*/
		public static function copyDate(base:Date):Date {
			var currentDate:Date = new Date(base.getFullYear(), base.getMonth(), Number(getDayOfMonth(base)), base.getHours() , base.getMinutes(),base.getSeconds());
			return currentDate 	;
		}
		
		
		public static function getDateWithHours(base:Date, hours:int, minutes:int, seconds:int):Date{
			return new Date(base.getFullYear(),base.getMonth(),Number(FlexDateHelper.getDayOfMonth(base)),hours,minutes,seconds);
		}
	}
}