package com.cemex.charts.gantt.common
{
	public class TimeConstants
	{
		public function TimeConstants()
		{
		}
		public static const RESOLUTION_MILLISECONDS:Number=1;
		public static const RESOLUTION_SECONDS:Number=1000;
		public static const RESOLUTION_MINUTES:Number=60*1000;
		public static const RESOLUTION_MINUTES_5:Number=5*60*1000;
		public static const RESOLUTION_MINUTES_15:Number=15*60*1000;
		public static const RESOLUTION_MINUTES_30:Number=30*60*1000;
		public static const RESOLUTION_HOURS:Number=60*60*1000;
		public static const RESOLUTION_HOURS_2:Number=2*60*60*1000;
		public static const RESOLUTION_HOURS_4:Number=4*60*60*1000;
		public static const RESOLUTION_HOURS_12:Number=4*60*60*1000;
		public static const RESOLUTION_DAY:Number=24*60*60*1000;
		public static const RESOLUTION_WEEK:Number=24*60*60*1000;
		public static const RESOLUTION_MONTH:Number=30*24*60*60*1000;
		public static const RESOLUTION_QUARTIL:Number=4*30*24*60*60*1000;
		public static const RESOLUTION_YEAR:Number=365*24*60*60*1000;
		public static const RESOLUTION_DECADE:Number=10*365.25*24*60*60*1000;
		public static const RESOLUTION_CENTURY:Number=100*365.25*24*60*60*1000;
		
		
		
		public static const IRESOLUTION_MILLISECONDS:Number=1;
		public static const IRESOLUTION_SECONDS:Number=2;
		public static const IRESOLUTION_MINUTES:Number=3;
		public static const IRESOLUTION_MINUTES_5:Number=4;
		public static const IRESOLUTION_MINUTES_15:Number=5;
		public static const IRESOLUTION_MINUTES_30:Number=6;
		public static const IRESOLUTION_HOURS:Number=7;
		public static const IRESOLUTION_HOURS_QUARTILE:Number=8;
		public static const IRESOLUTION_HOURS_SEXTILE:Number=9;
		public static const IRESOLUTION_DAY:Number=10;
		public static const IRESOLUTION_WEEK:Number=11;
		public static const IRESOLUTION_MONTH:Number=12;
		public static const IRESOLUTION_QUARTIL:Number=13;
		public static const IRESOLUTION_YEAR:Number=14;
		public static const IRESOLUTION_DECADE:Number=15;
		public static const IRESOLUTION_CENTURY:Number=16
		
		
		
		
		
		public static const MIN_ZOOM_LEVEL:Number=1;
		public static const MAX_ZOOM_LEVEL:Number=7;
		public static const LEVEL_MILLISECONDS:Number=1;
		public static const LEVEL_SECONDS:Number=2;
		public static const LEVEL_MINUTES:Number=3;
		public static const LEVEL_HOURS:Number=4;
		public static const LEVEL_DAY:Number=5;
		public static const LEVEL_WEEK:Number=6;
		public static const LEVEL_MONTH:Number=7;
		public static const LEVEL_QUARTIL:Number=8;
		public static const LEVEL_YEAR:Number=9;
		public static const LEVEL_DECADE:Number=10;
		public static const LEVEL_CENTURY:Number=11;
		
		
		
		
		public static const REFERENCE_YEAR:String="yyyy";
		public static const REFERENCE_QUARTAL:String="q";
		public static const REFERENCE_MONTH:String="m";
		public static const REFERENCE_DAYOFYEAR:String="y";
		public static const REFERENCE_DAY:String="d";
		public static const REFERENCE_DAYOFWEEK:String="w";
		public static const REFERENCE_WEEK:String="ww";
		public static const REFERENCE_HOUR:String="h";
		public static const REFERENCE_MINUTE:String="n";
		public static const REFERENCE_SECONDS:String="s";
		public static const REFERENCE_MILLISECONDS:String="l";
		
		
		
	}
}