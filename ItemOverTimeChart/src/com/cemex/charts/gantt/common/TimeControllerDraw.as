package com.cemex.charts.gantt.common
{
	import com.cemex.charts.gantt.views.components.TimeBarIncrement;
	import com.cemex.charts.gantt.views.components.TimeIncrement;
	
	import flash.display.Graphics;
	import flash.events.MouseEvent;
	
	import mx.containers.Canvas;
	import mx.core.UIComponent;
	import mx.events.FlexEvent;


	/**
	 <mx:AdvancedDataGrid id ="time" width="100%" height="45" sortExpertMode="true" 
						 sortableColumns="false" draggableColumns="false"
						 resizableColumns="false"
						 >
		
	</mx:AdvancedDataGrid>*/
	
	public class TimeControllerDraw extends UIComponent
	{
		public function TimeControllerDraw(){
			addEventListener(FlexEvent.CREATION_COMPLETE,init);
			addEventListener(MouseEvent.MOUSE_MOVE,mouseMove);
		}
		
		public function init(e:FlexEvent):void{
			
		}
		public function mouseMove(e:MouseEvent):void{
			
		}
		
		
		
		override protected function updateDisplayList (w : Number, h : Number) :void {
			super.updateDisplayList (w, h);
			var g : Graphics = graphics;
			g.clear ();
			drawLine(g,height*5,0x990000);
			drawLine(g,200,0xFF0000);
		}
		
		public function drawLine(g:Graphics,posicion:int,color:Number,thinkness:int=2):void{
			
			g.lineStyle(thinkness,color , 1);
			g.moveTo(posicion, -1);
			g.lineTo(posicion, height+1);
			
		}
		
		private var currentTimeBarIncrement:TimeBarIncrement = null;
		/*
		public function getTimeBarIncrement():TimeBarIncrement{
			
			var increment:TimeBarIncrement =  new TimeBarIncrement();
			increment.start = _visibleScreenStart;
			increment.end = _visibleScreenEnd;
			
			increment.lowerPast = FlexDateHelper.copyDate(increment.start);
			increment.higherPast = FlexDateHelper.copyDate(increment.start);
			
			var screenInterval:Number = increment.getScreenInterval();
			
			//var times:Number = 1;
			//var uwidth:Number = 1;
			//var uinc:Number = 5;
			if (screenInterval <= TimeConstants.RESOLUTION_MINUTES_5){
				//Se quieren ver  5 minutos
				//Alert.show("RESOLUTION_MINUTES_5");
				increment.lower  = getIncrement(30	,"NN:SS", 1	,TimeConstants.REFERENCE_MINUTE);
				increment.higher = getIncrement(150	,"YYYY/MM/DD HH", 5	,TimeConstants.REFERENCE_MINUTE,5);
				increment.lowerPast.setSeconds (0);
				increment.higherPast.setMinutes(floorTo(increment.start.getMinutes(), 5	) , 0);
			} 
			else if (screenInterval <= TimeConstants.RESOLUTION_MINUTES_15) {
				//Alert.show("RESOLUTION_MINUTES_15");
				increment.lower  = getIncrement(30	 ,"NN:SS", 1		,TimeConstants.REFERENCE_MINUTE);
				increment.higher = getIncrement(30*15,"YYYY/MM/DD HH", 15		,TimeConstants.REFERENCE_MINUTE ,15);
				increment.lowerPast.setSeconds (0);
				increment.higherPast.setMinutes(floorTo(increment.start.getMinutes(), 15), 0);
			}
			else if (screenInterval <= TimeConstants.RESOLUTION_MINUTES_30){				
				// Esta viendo menos de 30 minutos
				//Alert.show("RESOLUTION_MINUTES_30");
				increment.lower  = getIncrement(30	,"NN:SS", 5			,TimeConstants.REFERENCE_MINUTE);
				increment.higher = getIncrement(180	,"YYYY/MM/DD HH", 30		,TimeConstants.REFERENCE_MINUTE ,6);
				increment.lowerPast.setMinutes (floorTo(increment.start.getMinutes(),  5), 0);
				increment.higherPast.setMinutes(floorTo(increment.start.getMinutes(), 30), 0);
			}
			else if (screenInterval <= TimeConstants.RESOLUTION_HOURS) {
				// ESta viendo una hora	
				//Alert.show("RESOLUTION_HOURS");
				increment.lower  = getIncrement(30	,"NN:SS", 15		,TimeConstants.REFERENCE_MINUTE);
				increment.higher = getIncrement(120	,"YYYY/MM/DD HH",  1		,TimeConstants.REFERENCE_HOUR ,4);
				increment.lowerPast.setMinutes (floorTo(increment.start.getMinutes(), 15), 0);
				increment.higherPast.setMinutes(0, 0);
			}
			else if (screenInterval <= TimeConstants.RESOLUTION_HOURS_4){
				//Alert.show("RESOLUTION_HOURS_4");	
				increment.lower  = getIncrement(30	,"NN:SS", 15		,TimeConstants.REFERENCE_MINUTE);
				increment.higher = getIncrement(120	,"YYYY/MM/DD HH",  1		,TimeConstants.REFERENCE_HOUR ,4);
				increment.lowerPast.setMinutes (floorTo(increment.start.getMinutes(), 15), 0);
				increment.higherPast.setHours(floorTo(increment.start.getHours(), 4),0, 0);
			} 
			else if (screenInterval <= TimeConstants.RESOLUTION_HOURS_12){
				
				increment.lower  = getIncrement(30		,"HH",  1		,TimeConstants.REFERENCE_HOUR);
				increment.higher = getIncrement(30*12	,"YYYY/MM/DD HH",  12		,TimeConstants.REFERENCE_HOUR ,12);
				increment.lowerPast.setMinutes(0 , 0);
				increment.higherPast.setHours(floorTo(increment.start.getHours(), 12), 0, 0);
			} 
			else if (screenInterval <= TimeConstants.RESOLUTION_DAY){
				// ESta viendo un poco menos que el Dia
				increment.lower  = getIncrement(30		,"HH",  1		,TimeConstants.REFERENCE_HOUR);
				increment.higher = getIncrement(30*24	,"YYYY/MM/DD HH",  1		,TimeConstants.REFERENCE_DAY ,24);
				increment.lowerPast.setMinutes(0 , 0);
				increment.higherPast.setHours(0, 0, 0);
			} 
			else {
				// ESta viendo el dia Completo
			}
			currentTimeBarIncrement = increment;
			return currentTimeBarIncrement;
		}
		*/
		public function getIncrement(minWidth:Number ,formatter:String ,incrementDelta:Number, incrementUnits:String,times:Number =1):TimeIncrement {
			
			var result:TimeIncrement = new TimeIncrement();
			result.minWidth = minWidth;
			result.incremetDelta = incrementDelta;
			result.incremetUnits = incrementUnits;
			result.times = times;
			result.formatter = formatter;
			
			return result;
		}
		public function floorTo(value:Number, to:Number):Number{
			return Math.floor(value / to) * to;
		}
		
		private var _visibleScreenStart:Date = new Date();
		private var _visibleScreenEnd:Date = FlexDateHelper.addMinutes(new Date(),30);
		private var _visibleRangeStart:Date= FlexDateHelper.getDateWithHours(new Date(),0,0,0);;
		private var _visibleRangeEnd:Date =FlexDateHelper.getDateWithHours(new Date(),23,59,59);
		
		/*
		[Inject]
		public var view:GanttChartComponent;
		*/
		
		public function setVisibleScreenStart(date:Date):void{
			_visibleScreenStart = date;
		}
		public function setVisibleScreenEnd(date:Date):void{
			_visibleScreenEnd = date;
		}
		public function setVisibleRangeStartDate(date:Date):void{
			_visibleRangeStart = date;
		}
		public function setVisibleRangeEndDate(date:Date):void{
			_visibleRangeEnd = date;
		}
		public function getTimeBarWidth():Number {
			return width;
		}
		
	}
}