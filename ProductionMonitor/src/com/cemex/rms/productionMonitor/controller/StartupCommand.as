/*

Copyright (c) 2010 Neoris, All Rights Reserved 

@author   Ivan Alvarez Frias
@contact  e-ialvarez@neoris.com
@project  RMSDispatcher

@internal 

*/
package com.cemex.rms.productionMonitor.controller
{
	import com.cemex.rms.common.logging.impl.EventLogger;
	import com.cemex.rms.common.services.events.ServiceManagerEvent;
	
	import flash.utils.describeType;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.logging.ILogger;
	import mx.logging.Log;
	
	import org.robotlegs.base.ContextEvent;
	import org.robotlegs.mvcs.Command;
	
	/**
	 *
	 * Defines the associated <code>Command</code> implementation for 
	 * the user gesture.
	 *
	 * <p>
	 * Commands are stateless, short-lived objects used to perform a single unit of work within an application. 
	 * Commands are appropriate for communication between application tiers and are able to send 
	 * system events that will either launch other Commands or be received by a Mediator to perform 
	 * work on a View Component in response to the event. Commands are an excellent place to encapsulate 
	 * the business logic of your application.
	 * </p>
	 *
	 * @see org.robotlegs.mvcs.Command
	 *
	 */
	public final class StartupCommand extends Command 
	{
		/**
		 * Create variable to point to this class. 
		 * 
		 * @private
		 *
		 */     	
		private var logger:ILogger = Log.getLogger(describeType(this).@name.split("::").join("."));
		
		
		/**
		 *
		 * Method handle the logic for <code>StartupCommand</code>
		 *
		 */        
		override public function execute():void    
		{
			
			EventLogger.staticDispatcher = this.eventDispatcher; 
			
			// Se  solicita comenzar a cargar los servicios
			dispatch(new ServiceManagerEvent(ServiceManagerEvent.SERVICES_LOAD_REQUEST) );
			dispatch(new ContextEvent( ContextEvent.STARTUP_COMPLETE ) );
		}
	}
}