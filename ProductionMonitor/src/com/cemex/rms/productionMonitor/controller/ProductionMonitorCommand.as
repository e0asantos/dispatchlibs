/*

Copyright (c) 2010 Neoris, All Rights Reserved 

@author   Ivan Alvarez Frias
@contact  e-ialvarez@neoris.com
@project  RMSDispatcher

@internal 

*/
package com.cemex.rms.productionMonitor.controller
{
	import com.cemex.rms.common.logging.ILogger;
	import com.cemex.rms.common.logging.LoggerFactory;
	import com.cemex.rms.productionMonitor.common.PushQueues;
	import com.cemex.rms.productionMonitor.services.IProductionMonitorServiceFactory;
	import com.cemex.rms.productionMonitor.services.ServicesContants;
	import com.cemex.rms.productionMonitor.services.event.ProductionMonitorEvent;
	import mx.controls.Alert;
	import flash.external.ExternalInterface;
	import flash.utils.describeType;
	
	import org.robotlegs.mvcs.Command;
	
	/**
	 *
	 * Defines the associated <code>Command</code> implementation for 
	 * the user gesture.
	 *
	 * <p>
	 * Commands are stateless, short-lived objects used to perform a single unit of work within an application. 
	 * Commands are appropriate for communication between application tiers and are able to send 
	 * system events that will either launch other Commands or be received by a Mediator to perform 
	 * work on a View Component in response to the event. Commands are an excellent place to encapsulate 
	 * the business logic of your application.
	 * </p>
	 *
	 * @see org.robotlegs.mvcs.Command
	 *
	 */
	public final class ProductionMonitorCommand extends Command 
	{
		/**
		 * Create variable to point to this class. 
		 * 
		 * @private
		 *
		 */     	
		private var logger:ILogger = LoggerFactory.getLogger(describeType(this).@name.split("::").join("."));
		
		
		[Inject] public var service:IProductionMonitorServiceFactory;
		
		[Inject] public var event:ProductionMonitorEvent;
		
		[Inject] public var queues:PushQueues;
		
		//private logger:com.cemex.rms.common.logging.ILogger = LoggerFactory.getLogger("ProductionMonitorCommand");
		/**
		 *
		 * Method handle the logic for <code>StartupCommand</code>
		 *
		 */        
		override public function execute():void {
			
			var status:String = service.getFlashIslandService().getCurrentStatus();
			
			
			var eventType:String = event.type;
			var value:Object = event.value;
			var plant:String = service.getFlashIslandService().getPlant();
			
			var currPlant:String = null;
			var stop:Object = null;
			
			logger.debug("Executed::" + eventType + 
				" wtih status("+status+") " + 
				" hasAbnormalQueued("+queues.hasAbnormalQueued()+") " +
				" hasStops("+queues.hasStops()+")" );
			
			if (eventType == ProductionMonitorEvent.ISLAND_DATA_RECEIVED){
				// En caso que el webdynpro avise que tiene algo que ahcer
				if (status == ServicesContants.WAITING_PUSH){
					// El webdynpro esta avisando que esta esperando
					if (queues.hasAbnormalQueued()){
						triggerAbnormal();
					}
					else if (queues.hasStops()){
						triggerStop();
					}
					else if(queues.hasPo()){
						//no se hace nada
						triggerPO();
					}
				}
				else {
					// Cualquier otro status que no sea waiting push se debe ignorar
				}
			}
			else if (eventType == ProductionMonitorEvent.PUSH_ABNORMAL_ARRIVED){
				
				// Revisar que vengan de la misma planta 
				//para agregarlo a los queues
				if (value != null && value["WERKS"] != null ) {
					currPlant = value["WERKS"] as String ;
					
					logger.debug("currPlant("+currPlant+")  " + " plant("+plant+")");
					
					//ExternalInterface.call("alert","currPlant("+currPlant+")  " + " plant("+plant+")");
					
					if(plant == currPlant) {
						queues.addAbnormal(value);
						if (status == ServicesContants.WAITING_PUSH){
							triggerAbnormal();
						}
					}
				}
			}
			else if (eventType == ProductionMonitorEvent.PUSH_STOP_ARRIVED){
				
				if (value != null && value["WERKS"] != null ) {
					logger.debug("currPlant("+currPlant+")  " + " plant("+plant+")");
					currPlant = value["WERKS"] as String ;
					if(plant == currPlant) {
						queues.addStop(value);
						triggerStop();	
						if (status == ServicesContants.WAITING_PUSH) {
						}
					}
				}	
			}
			else if (eventType == ProductionMonitorEvent.PUSH_PO_ARRIVED){
				// Estado invalido
				//Alert.show("Filtering PUSH");
				if (value != null && value["plant"] != null ) {
					logger.debug("currPlant("+currPlant+")  " + " plant("+plant+")");
					currPlant = value["plant"] as String ;
					if(plant == currPlant) {
						queues.addPo(value);
						if (status == ServicesContants.WAITING_PUSH) {
							//Alert.show("Filtering PUSH 2");
							triggerPO();	
						}
					}
				}	
			}
		}
		
		public function isValidWerk(value:Object):Boolean {
			
			var plant:String = service.getFlashIslandService().getPlant();
			if (value != null && value["werks"] != null ) {
				var currPlant:String = value["werks"] as String ;
				if(plant == currPlant) {
					return true;					
				}
			}
			
			return false;
		}
		
		public function triggerAbnormal():void{
			queues.attendedAbnormal();
			service.getFlashIslandService().sendAbnormal();
		}
		public function triggerStop():void{
			var stop:Object = queues.nextStop();
			
			service.getFlashIslandService().sendStop(stop);
		}
		public function triggerPO():void{
			//Alert.show("triggerPO");
			var stop:Object = queues.nextPo();
			service.getFlashIslandService().sendPO(stop);
		}
	}
}