package com.cemex.rms.productionMonitor.common
{
	import mx.collections.ArrayCollection;

	/**
	 * Esta clase es un singleton 
	 * 
	 * 
	 * 
	 */
	public class PushQueues
	{
		public function PushQueues()
		{
		}
		
		
		
		private var abnormalQueue:ArrayCollection =  new ArrayCollection();
		public function hasAbnormalQueued():Boolean{
			return abnormalQueue.length > 0;
		}
		public function addAbnormal(abnormal:Object):void{
			abnormalQueue.addItem(abnormal);
		}
		public function attendedAbnormal():void{
			abnormalQueue.removeAll();
		}
		
		
		private var stopQueue:ArrayCollection =  new ArrayCollection();
		public function hasStops():Boolean{
			return stopsSize() > 0 ;
		}
		public function stopsSize():Number{
			return stopQueue.length;
		}
		public function addStop(stop:Object):void{
			stopQueue.addItem(stop);
		}
		public function nextStop():Object{
			if(hasStops()){
				return stopQueue.removeItemAt(0);
			}
			return null;
		}
		
		private var poQueue:ArrayCollection =  new ArrayCollection();
		public function hasPo():Boolean{
			return poSize() > 0 ;
		}
		public function poSize():Number{
			return poQueue.length;
		}
		public function addPo(stop:Object):void{
			poQueue.addItem(stop);
		}
		public function nextPo():Object{
			if(hasPo()){
				return poQueue.removeItemAt(0);
			}
			return null;
		}
		
	}
}