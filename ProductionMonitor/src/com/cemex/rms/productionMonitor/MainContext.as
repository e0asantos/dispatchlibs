package com.cemex.rms.productionMonitor
{
	import com.cemex.rms.common.logging.LoggerFactory;
	import com.cemex.rms.common.services.IServiceFactory;
	import com.cemex.rms.common.services.ServiceFactory;
	import com.cemex.rms.productionMonitor.common.PushQueues;
	import com.cemex.rms.productionMonitor.controller.ProductionMonitorCommand;
	import com.cemex.rms.productionMonitor.controller.StartupCommand;
	import com.cemex.rms.productionMonitor.services.IProductionMonitorServiceFactory;
	import com.cemex.rms.productionMonitor.services.ProductionMonitorServiceFactory;
	import com.cemex.rms.productionMonitor.services.ProductionMonitorServiceModel;
	import com.cemex.rms.productionMonitor.services.event.ProductionMonitorEvent;
	import com.cemex.rms.productionMonitor.views.ProductionMonitorMediator;
	import com.cemex.rms.productionMonitor.views.ProductionMonitorView;
	
	import flash.display.DisplayObjectContainer;
	
	import org.robotlegs.base.ContextEvent;
	import org.robotlegs.mvcs.Context;
	
	public class MainContext extends Context
	{
		
		
		public function MainContext(contextView:DisplayObjectContainer=null, autoStartup:Boolean=true)
		{
			super(contextView, autoStartup);			
		}
		
		
		
		override public function startup():void
		{
			
			
			LoggerFactory.defaultLogger = LoggerFactory.LOGGER_TYPE_EVENT;
			// SE inicializa el factory de servicios
			var factory:ServiceFactory = new ServiceFactory();
			
			
			// Se le settea el dispatcher del contexto a los servicios
			// para poder utilizar los listeners de los servicios con la arquitectura de 
			// Robotlegs
			factory.dispatcher = eventDispatcher;
			factory.model = new ProductionMonitorServiceModel();
			// Se settea el Factory de Servicios
			injector.mapValue(IServiceFactory, factory);
			
		
			var demoService:ProductionMonitorServiceFactory = new ProductionMonitorServiceFactory();
			//injector.injectInto(ganttService);
			demoService.serviceFactory = factory;
			injector.mapValue(IProductionMonitorServiceFactory,demoService);

			
			mediatorMap.mapView(ProductionMonitorView, ProductionMonitorMediator);
			// se crea el mediador de la vista principal el FlashIsland Settea datos
			// en cuanto se manda a cargar el servicio de FlashIslands
			mediatorMap.createMediator((contextView as MainModule).view );
			
			
			injector.mapSingleton(PushQueues);
			
			commandMap.mapEvent(ProductionMonitorEvent.ISLAND_DATA_RECEIVED, ProductionMonitorCommand, ProductionMonitorEvent,false);
			commandMap.mapEvent(ProductionMonitorEvent.PUSH_ABNORMAL_ARRIVED, ProductionMonitorCommand, ProductionMonitorEvent,false);
			commandMap.mapEvent(ProductionMonitorEvent.PUSH_STOP_ARRIVED, ProductionMonitorCommand, ProductionMonitorEvent,false);
			commandMap.mapEvent(ProductionMonitorEvent.PUSH_PO_ARRIVED, ProductionMonitorCommand, ProductionMonitorEvent,false);
			
			commandMap.mapEvent(ContextEvent.STARTUP, StartupCommand, ContextEvent, true);
			// Se mandan a inicializar las cosas
			dispatchEvent( new ContextEvent( ContextEvent.STARTUP ) );
			
			
			
		}
	}
}