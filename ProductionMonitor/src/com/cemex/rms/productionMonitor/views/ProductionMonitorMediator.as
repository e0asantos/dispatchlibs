package com.cemex.rms.productionMonitor.views

{
	import com.cemex.rms.common.helpers.ContextMenuHelper;
	import com.cemex.rms.common.logging.ILogger;
	import com.cemex.rms.common.logging.LoggerFactory;
	import com.cemex.rms.common.reflection.ReflectionHelper;
	import com.cemex.rms.common.services.config.IServiceModel;
	import com.cemex.rms.common.views.GenericMediator;
	import com.cemex.rms.productionMonitor.services.IProductionMonitorServiceFactory;
	import com.cemex.rms.productionMonitor.services.ServicesContants;
	import com.cemex.rms.productionMonitor.services.event.ProductionMonitorEvent;
	
	import flash.events.ContextMenuEvent;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.system.System;
	
	import mx.controls.Alert;
	import mx.core.UIComponent;
	import mx.events.ItemClickEvent;
	import mx.messaging.events.ChannelEvent;
	
	public class ProductionMonitorMediator extends GenericMediator
	{
		public function ProductionMonitorMediator()
		{
			super();
		}
		
		public override function getView():UIComponent{
			return view;
		}
		[Inject]
		public var view: ProductionMonitorView;
	
		[Inject]
		public var services: IProductionMonitorServiceFactory;
		
		
		override public function onRegister():void {
			
			init();
			
			//view.stop.addEventListener(MouseEvent.CLICK,simulateStop);
			//view.abnormal.addEventListener(MouseEvent.CLICK,simulateAbnormal);
			ContextMenuHelper.addContextMenuOption("Clean2 Log", cleanLog);
			ContextMenuHelper.addContextMenuOption("Copy2 log", copiarLog);
			
		}
		public function cleanLog(e:ContextMenuEvent):void{

			logging.removeAll();
		}
		public function copiarLog(evt:ContextMenuEvent):void{
			System.setClipboard(
				ReflectionHelper.object2XML(logging,"Log")
			);
		}
		
		override protected function setServiceStatus(service:String,status:String):void{
			super.setServiceStatus(service,status);
			//ExternalInterface.call("alert",service+":"+status);	
		}
		
		[Bindable] [Embed(source="/assets/s_s_ledg.gif")] private var green:Class;             
		[Bindable] [Embed(source="/assets/s_s_ledr.gif")] private var red:Class;
		[Bindable] [Embed(source="/assets/s_s_ledy.gif")] private var yellow:Class;            
		
		override public function getIcon(status:String):Class {
			if (status == CONNECTED){
				return green;
			}
			else if (status == FAULT){
				return yellow;
			}
			else {
				return red;
			}
		}
		
		override public function getModel():IServiceModel{
			return services.getModel();
		}
		override public function clickStatus(ref:String):void{
			Alert.show("statusClickes:"+ref);
		}
		
		/*
		public function simulateAbnormal(e:MouseEvent):void{
			var value:Object ={
				werks:services.getFlashIslandService().getPlant(),
					country:"MX"
			};
			logger.debug("MEd::simulateAbnormal");
			dispatch(new ProductionMonitorEvent(ProductionMonitorEvent.PUSH_ABNORMAL_ARRIVED,value));
			
		}
		public function simulateStop(e:MouseEvent):void{
			var message:Object = {
				werks:services.getFlashIslandService().getPlant(),
				arbpl:"DOSIFI_1",
				aufnr:"100000000078",
				fabNo:"123456",
				cycle:"1"
			};
			logger.debug("MEd::simulateStop");
			
			dispatch(new ProductionMonitorEvent(ProductionMonitorEvent.PUSH_STOP_ARRIVED,message));
		}
		*/
		private var logger:ILogger = LoggerFactory.getLogger("ProductionMonitorMediator");
		
		
		
		override public function processLog(log:String):void{
			//Alert.show(log);
		}
		
		
		
		
	}
}