package com.cemex.rms.productionMonitor.services
{
	import com.cemex.rms.common.services.IServiceFactory;
	import com.cemex.rms.common.services.config.IServiceModel;
	import com.cemex.rms.productionMonitor.services.wda.IProductionMonitorIsland;
	
	import mx.collections.ArrayCollection;

	
	public class ProductionMonitorServiceFactory implements IProductionMonitorServiceFactory
	{
		public function ProductionMonitorServiceFactory()
		{
		}

		public var serviceFactory:IServiceFactory;
		public function getModel():IServiceModel{
			return serviceFactory.model;
		}
		public function getFlashIslandService():IProductionMonitorIsland {
			
			var result:IProductionMonitorIsland = serviceFactory.getService("ProductionMonitorIsland") as IProductionMonitorIsland;
			if (!result.isReady()){
				result = serviceFactory.getService("ProductionMonitorIslandMock") as IProductionMonitorIsland;
			}
			return result;
		}
		
	}
}