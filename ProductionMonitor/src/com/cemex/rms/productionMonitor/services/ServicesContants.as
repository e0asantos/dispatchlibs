package com.cemex.rms.productionMonitor.services
{
	public class ServicesContants
	{
		public function ServicesContants()
		{
		}
		public static const WAITING_PUSH:String ="WAITING_PUSH";
		public static const WEB_DYNPRO_BUSY:String ="WEB_DYNPRO_BUSY";
		public static const FLASH_TRIGGERED_EVENT:String ="FLASH_TRIGGERED_EVENT";
	}
}