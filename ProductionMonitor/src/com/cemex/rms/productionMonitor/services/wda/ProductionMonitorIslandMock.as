package com.cemex.rms.productionMonitor.services.wda
{
	import com.cemex.rms.common.flashislands.IFlashIslandEventObject;
	import com.cemex.rms.common.push.PushConfigHelper;
	import com.cemex.rms.common.reflection.ReflectionHelper;
	import com.cemex.rms.common.services.AbstractService;
	import com.cemex.rms.productionMonitor.services.ServicesContants;
	import com.cemex.rms.productionMonitor.services.event.ProductionMonitorEvent;
	import com.cemex.rms.productionMonitor.services.requests.AbnormalConsumption;
	import com.cemex.rms.productionMonitor.services.requests.PushStop;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	
	
	
	/**
	 * 
	 * 
	 * 
	 * 
	 */
	public class ProductionMonitorIslandMock extends AbstractService implements IProductionMonitorIsland
	{
		public function ProductionMonitorIslandMock() {
			super();
			
			var lcds:Object ={
				LCDS_DESTINY:new ArrayCollection([
					{DESTINY_NAME:"AbnormalConsumptionPI", DESTINY_ID:"ABNORMAL_CONSUMPTION"},
					{DESTINY_NAME:"ProductionStopPI", DESTINY_ID:"PRODUCTION_STOP"}
				]),
				LCDS_ENDPOINT:
				{
					URL:"rtmp://mxoccrmsrid01.noam.cemexnet.com:2038",
					CHANNEL_NAME:"RMS_RTMP_CHANNEL",
					CHANNEL_TYPE:"rtmp"
				}
				
			};
			PushConfigHelper.initLcdsDestinies(lcds);
			PushConfigHelper.initLcdsEndpoint(lcds);
			
			
		}
		
		
		public var status:String = ServicesContants.WAITING_PUSH;
		public function getCurrentStatus():String{
			return status;
		}
		public function setCurrentStatus(value:String):void{
			status = value;
			logger.debug("Status:" + status);
		
		}
		
		public function dispatchWDEvent (o:IFlashIslandEventObject):void{
			logger.debug("\n" + ReflectionHelper.object2XML(o,o.getEventName()));
			logger.debug("Status:" + status);
			setCurrentStatus(ServicesContants.FLASH_TRIGGERED_EVENT);
			Alert.show("status:"+ status + "\n"+ReflectionHelper.object2XML(o,o.getEventName()));
		}
		
		public function sendStop(stop:Object):void{
			var length:Number = 0;
			
			var request:PushStop =  new PushStop();
			ReflectionHelper.copySimpleParameters(stop,request);
			dispatchWDEvent(request);
		}
		public function sendAbnormal():void{
			var request:AbnormalConsumption =  new AbnormalConsumption();
			dispatchWDEvent(request);
		}
		
		public function getPlant():String{
			return "X001";
		}
		public function setStopValues(object:Object):void{
			
		}
	}
}