package com.cemex.rms.productionMonitor.services.wda
{
	import com.cemex.rms.common.flashislands.AbstractFlashIslandService;
	import com.cemex.rms.common.push.PushConfigHelper;
	import com.cemex.rms.common.reflection.ReflectionHelper;
	import com.cemex.rms.productionMonitor.services.ServicesContants;
	import com.cemex.rms.productionMonitor.services.event.ProductionMonitorEvent;
	import com.cemex.rms.productionMonitor.services.requests.AbnormalConsumption;
	import com.cemex.rms.productionMonitor.services.requests.POConfirmationReq;
	import com.cemex.rms.productionMonitor.services.requests.PushStop;
	import mx.controls.Alert;
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	
	
	
	/**
	 * 
	 * 
	 * 
	 * 
	 */
	public class ProductionMonitorIslandImpl extends AbstractFlashIslandService implements IProductionMonitorIsland
	{
		public function ProductionMonitorIslandImpl() {
			super();
		}
		
		
		
		
		//****************************
		// WebDynpro Varibles Declarations 
		// Para asegurar que estos campos estan setteados 
		// y lanzar el evento flashIslandDataReady
		public override function getIslandFields():ArrayCollection {
			return new ArrayCollection(["DYNAMIC","CONFIG"]);
		}
		
		
		public function sendStop(stop:Object):void{
			
			
			var request:PushStop =  new PushStop();
			ReflectionHelper.copySimpleParametersUpperAsCamel(stop,request);
			setCurrentStatus(ServicesContants.FLASH_TRIGGERED_EVENT);
			dispatchWDEvent(request);
		}
		
		public function sendAbnormal():void {
			var request:AbnormalConsumption =  new AbnormalConsumption();
			request.werks = getPlant();
			setCurrentStatus(ServicesContants.FLASH_TRIGGERED_EVENT);
			logger.debug("Sending Event:" + request.getEventName());
			dispatchWDEvent(request);
			
		}
		
		public function sendPO(stop:Object):void{
			var request:POConfirmationReq =  new POConfirmationReq();
			ReflectionHelper.copySimpleParametersUpperAsCamel(stop,request);
			request.werks=getPlant();
			setCurrentStatus(ServicesContants.FLASH_TRIGGERED_EVENT);
			dispatchWDEvent(request);
		}
		
		/**
		 * Esta es una variable dinamica que nos permite tener ua platica con WD
		 * 
		 */
		public function getDynamicNode():Object {
			
			return buffer.data["DYNAMIC"]	
		}
		public function getCurrentStatus():String {
			if (getDynamicNode() == null){
				return "";
			}
			return getDynamicNode()["ISLAND_COMMAND"];	
		}
		
		public function setCurrentStatus(value:String):void{
			if (getDynamicNode() != null){
				getDynamicNode()["ISLAND_COMMAND"]  = value;
			}
		}
		
		public function getPlant():String{
			if (getDynamicNode() == null){
				return null;
			}
			return getDynamicNode()["WERKS"];	
		}
		
		private var loaded:Boolean = false;
		
		/**
		 * Esta funcion valida que todos los datos esten disponibles para comenzar a activar todos los 
		 * lugares donde se requiera hacer algo con los datos
		 */
		public override function flashIslandDataReady():void {
			
			if (buffer.data["DYNAMIC"] == null){
				return;
			}
			
			// Esta es la validacion basica de la primera carga
			if (!loaded){
				var lcds:Object = buffer.data["CONFIG"]["LCDS"];
				
				PushConfigHelper.initLcdsDestinies(lcds);
				PushConfigHelper.initLcdsEndpoint(lcds);
				
				this.dispatchServiceLoadReady();
				
				loaded = true;
			}
			logger.debug(ReflectionHelper.object2XML(buffer.data,"FlashIsland-Data"));
			logger.debug(ReflectionHelper.object2XML(buffer.extraData,"FlashIsland-ExtraData"));
			//Alert.show(new Date() + ReflectionHelper.object2XML(buffer.data,"FlashIsland-Data"));
			//dispatchEvent(new ProductionMonitorEvent(ProductionMonitorEvent.ISLAND_DATA_RECEIVED,getCurrentStatus()));
			//dispatcher.dispatchEvent(new ProductionMonitorEvent(ProductionMonitorEvent.ISLAND_DATA_RECEIVED,getCurrentStatus()));
		}
		
	}
}