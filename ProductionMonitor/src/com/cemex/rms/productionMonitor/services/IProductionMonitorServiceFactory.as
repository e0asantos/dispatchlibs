package com.cemex.rms.productionMonitor.services
{
	import com.cemex.rms.common.services.IServiceFactory;
	import com.cemex.rms.common.services.config.IServiceModel;
	import com.cemex.rms.productionMonitor.services.wda.IProductionMonitorIsland;
	
	import mx.collections.ArrayCollection;

	public interface IProductionMonitorServiceFactory
	{
		
		function getModel():IServiceModel;
		
		function getFlashIslandService():IProductionMonitorIsland;
		/*
		
		
		function getChartPIReceiver():IChartReceiver ;
		function getChartInternalReceiver():IChartReceiver ;
		function getChartInternalProducer():IChartProducer ;
		*/
	}
}