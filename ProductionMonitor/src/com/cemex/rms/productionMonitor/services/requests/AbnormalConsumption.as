 package com.cemex.rms.productionMonitor.services.requests
{
	import com.cemex.rms.common.flashislands.IFlashIslandEventObject;
	
	import sap.FlashIsland;

	public class AbnormalConsumption implements IFlashIslandEventObject
	{
		public function AbnormalConsumption()
		{
			//FlashIsland.fireEvent(this,"abnormalConsumption",{"werks":param1});
		}
		
		public var werks:String;
		public function getEventName():String{
			return "abnormalConsumption";
		}
		
		
	}
}