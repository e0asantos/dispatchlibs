package com.cemex.rms.productionMonitor.services.requests
{
	import com.cemex.rms.common.flashislands.IFlashIslandEventObject;
	
	public class POConfirmationReq implements IFlashIslandEventObject
	{
		public function POConfirmationReq()
		{
		}
		
		public var werks:String;
		public function getEventName():String{
			return "poConfirmation";
		}
	}
}