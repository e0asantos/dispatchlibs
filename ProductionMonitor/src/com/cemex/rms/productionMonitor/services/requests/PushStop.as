 	package com.cemex.rms.productionMonitor.services.requests
{
	import com.cemex.rms.common.flashislands.IFlashIslandEventObject;

	public class PushStop implements IFlashIslandEventObject
	{
		public function PushStop()
		{
		}
		public var werks:String;
		public var arbpl:String;
		public var aufnr:String;
		public var fabno:String;
		public var cycle:String;
		public function getEventName():String{
			return "pushStop";
		}
	}
}