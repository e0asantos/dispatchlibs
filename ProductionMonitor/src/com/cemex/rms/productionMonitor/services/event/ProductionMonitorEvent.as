package com.cemex.rms.productionMonitor.services.event
{
	import flash.events.Event;
	
	public class ProductionMonitorEvent extends Event
	{
		public static const PUSH_STOP_ARRIVED:String = "PUSH_STOP_ARRIVED";
		
		public static const PUSH_PO_ARRIVED:String = "PUSH_PO_ARRIVED";
		
		public static const PUSH_ABNORMAL_ARRIVED:String = "PUSH_ABNORMAL_ARRIVED";
		
		public static const ISLAND_DATA_RECEIVED:String = "ISLAND_DATA_RECEIVED";
		
		
		
		
		public var value:Object;
		public function ProductionMonitorEvent(type:String,value:Object, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			this.value = value;
		}
	}
}