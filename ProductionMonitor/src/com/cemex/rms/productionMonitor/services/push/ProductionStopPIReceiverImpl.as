package com.cemex.rms.productionMonitor.services.push
{
	import com.cemex.rms.common.lcds.AbstractReceiverService;
	import com.cemex.rms.common.push.PushConfigHelper;
	import com.cemex.rms.common.reflection.ReflectionHelper;
	import com.cemex.rms.productionMonitor.services.event.ProductionMonitorEvent;
	
	import mx.messaging.ChannelSet;
	import mx.utils.ObjectUtil;
	
	public class ProductionStopPIReceiverImpl extends AbstractReceiverService implements IProductionMonitorReceiver
	{
		public function ProductionStopPIReceiverImpl()
		{
			super();
		}
		
		public override function getDestinationName():String{
			return PushConfigHelper.getDestinyName("PRODUCTION_STOP");
		}
		
		public override function getChannelSet():ChannelSet{
			return servicesContext.getValue("ChannelSet") as ChannelSet;
		}
		
		protected override function receiveMessage(message:Object):void {
			
			//Alert.show(ReflectionHelper.object2XML(message,getDestinationName()));
			var isSimple:Boolean = ObjectUtil.isSimple(message);
			logger.info("isSimple("+isSimple+")" +ReflectionHelper.object2XML(message,getDestinationName()));
			
			if (!isSimple){
				logger.info("Lanzando");
				
				dispatcher.dispatchEvent(new ProductionMonitorEvent(ProductionMonitorEvent.PUSH_STOP_ARRIVED,message));
			}
			
		}
		
	
		
		
	}
}