package com.cemex.rms.productionMonitor.services.push
{
	import com.cemex.rms.common.push.AbstractChannelSetService;
	
	import mx.collections.ArrayCollection;
	import mx.messaging.ChannelSet;

	public class ProductionMonitorChannelSetImpl extends AbstractChannelSetService
	{
		public function ProductionMonitorChannelSetImpl()
		{
			super();
		}
		
		protected override function setChannelSet(channelSet:ChannelSet):void{
			servicesContext.setValue("ChannelSet",channelSet);
		} 
		
	}
}