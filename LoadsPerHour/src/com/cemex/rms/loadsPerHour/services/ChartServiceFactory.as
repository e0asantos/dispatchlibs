package com.cemex.rms.loadsPerHour.services
{
	import com.cemex.rms.common.services.IServiceFactory;
	import com.cemex.rms.loadsPerHour.services.flashIslands.IChartIsland;
	import com.cemex.rms.loadsPerHour.services.lcds.IChartProducer;
	import com.cemex.rms.loadsPerHour.services.lcds.IChartReceiver;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	
	public class ChartServiceFactory implements IChartServiceFactory
	{
		public function ChartServiceFactory()
		{
		}
		
		//[Inject]
		public var serviceFactory:IServiceFactory;
		

		
		public function getFlashIslandService():IChartIsland {
			var result:IChartIsland = serviceFactory.getService("ChartIsland") as IChartIsland;
			if (!result.isReady()){
				result = serviceFactory.getService("ChartIslandMock") as IChartIsland;
				
			}
			return result;
		}
		
		public function getChartPIReceiver():IChartReceiver {
			var receiver:IChartReceiver = serviceFactory.getService("ChartPIReceiver") as IChartReceiver;
			return receiver;
		}
		public function getChartInternalReceiver():IChartReceiver {
			
			
			var receiver:IChartReceiver = serviceFactory.getService("ChartInternalReceiver") as IChartReceiver;
			return receiver;
		}
		public function getChartInternalProducer():IChartProducer {
			
			var producer:IChartProducer = serviceFactory.getService("ChartInternalProducer") as IChartProducer;
			return producer;
		}
	}
}