package com.cemex.rms.loadsPerHour.services.flashIslands.requests
{
	import com.cemex.rms.common.flashislands.IFlashIslandEventObject;

	public class ShowMEPWindowsRequest implements IFlashIslandEventObject
	{
		public function getEventName():String{
			return "showMepWindows";
		}
		public function ShowMEPWindowsRequest()
		{
		}
	}
}