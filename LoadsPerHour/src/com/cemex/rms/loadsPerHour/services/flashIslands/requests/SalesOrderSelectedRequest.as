package com.cemex.rms.loadsPerHour.services.flashIslands.requests
{
	import com.cemex.rms.common.flashislands.IFlashIslandEventObject;

	public class SalesOrderSelectedRequest implements IFlashIslandEventObject
	{
		public function getEventName():String{
			return "salesOrderSelected";
		}
		public function SalesOrderSelectedRequest()
		{
		}
	}
}