package com.cemex.rms.loadsPerHour.services.flashIslands
{
	import com.cemex.rms.common.services.IService;
	import com.cemex.rms.common.utils.DictionaryMap;
	
	
	import mx.collections.ArrayCollection;

	public interface IChartIsland extends IService
	{
		
		
		function getUser():String;
		function getData():String;
		
		
		
		
		function getDataSourceInputMEP():Object;
		function getDataSourceOutputMEP():Object;
		function getDataSourceSimParams():ArrayCollection;
		function getDataSourceItemsChanged():ArrayCollection;
		function getDataSourceOrdersDetail():ArrayCollection;
		function getDataSourceAnOrderDetail():ArrayCollection;
		function getDataSourceLinesDetail():ArrayCollection;
		function getDataSource():ArrayCollection;
		function getDataSourceGlobalData():Object;
		function getDataSourceMaxPlant():ArrayCollection;
		
		
		
		
		
		
		
		
		
		
		
		function getRMSDetail():void;
		function plantSelected():void;
		function requestMovOrd():void;
		
		function resetActionExecuted():void;
		function salesOrderSelected():void;
		function saveData():void;
		function setItemsChanged():void;
		function showMEPWindows():void;
		function updateData():void;
		function validaMEP():void;
		//function orderOnHold(order:OrderOnHoldRequest,payload:OrderLoad):void;
		
	}
}