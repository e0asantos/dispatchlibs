package com.cemex.rms.loadsPerHour.services.flashIslands.events
{
	import flash.events.Event;
	
	public class ChartIslandEvent extends Event
	{
		public static const SHOW_GRAPH:String = "SHOW_GRAPH";
		public static const REFRESH_FILTERS:String = "REFRESH_FILTERS";
		public static const SHOW_MEP:String = "SHOW_MEP";
		public static const SHOW_DETAIL:String = "SHOW_DETAIL";
		public static const PLANT_SIM_PARAM:String = "PLANT_SIM_PARAM";
		public static const CONFIRM_CHANGES:String = "CONFIRM_CHANGES";
		public static const SAVE_DATA:String = "SAVE_DATA";
		public static const PLANT_SELECTED:String = "PLANT_SELECTED";
		public static const MOVE_FULL_ORDER:String = "MOVE_FULL_ORDER";
		public static const MEP_SELECTED:String = "MEP_SELECTED";
		public static const SHOW_SUCCESS_MSG_CHG_PLT:String = "SHOW_SUCCESS_MSG_CHG_PLT";
		public static const SHOW_ERR_MSG_CHG_PLT:String = "SHOW_ERR_MSG_CHG_PLT";
		
		
		
		public function ChartIslandEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
	}
}