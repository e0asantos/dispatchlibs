package com.cemex.rms.loadsPerHour.services.flashIslands.impl
{
	import com.cemex.rms.common.flashislands.AbstractFlashIslandService;
	import com.cemex.rms.common.reflection.ReflectionHelper;
	import com.cemex.rms.common.utils.DictionaryMap;
	import com.cemex.rms.loadsPerHour.services.flashIslands.IChartIsland;
	import com.cemex.rms.loadsPerHour.services.flashIslands.events.ChartIslandEvent;
	import com.cemex.rms.loadsPerHour.services.flashIslands.requests.GetRMSDetailRequest;
	import com.cemex.rms.loadsPerHour.services.flashIslands.requests.PlantSelectedRequest;
	import com.cemex.rms.loadsPerHour.services.flashIslands.requests.RequestMovOrdRequest;
	import com.cemex.rms.loadsPerHour.services.flashIslands.requests.ResetActionExecutedRequest;
	import com.cemex.rms.loadsPerHour.services.flashIslands.requests.SalesOrderSelectedRequest;
	import com.cemex.rms.loadsPerHour.services.flashIslands.requests.SaveDataRequest;
	import com.cemex.rms.loadsPerHour.services.flashIslands.requests.SetItemsChangedRequest;
	import com.cemex.rms.loadsPerHour.services.flashIslands.requests.ShowMEPWindowsRequest;
	import com.cemex.rms.loadsPerHour.services.flashIslands.requests.UpdateDataRequest;
	import com.cemex.rms.loadsPerHour.services.flashIslands.requests.ValidaMEPRequest;
	
	import mx.collections.ArrayCollection;
	
	
	
	/**
	 * 
	 * 
	 * 
	 * 
	 */
	public class ChartIslandImpl extends AbstractFlashIslandService implements IChartIsland
	{
		public function ChartIslandImpl() {
			super();
			
		}
		
		public function getData():String{
				
			return ReflectionHelper.object2XML(buffer.data,"IslandData","  ") ;
		}
		
		//****************************
		// WebDynpro Varibles Declarations 
		// Para asegurar que estos campos estan setteados 
		public override function getIslandFields():ArrayCollection {
			return new ArrayCollection([
											//"dataSourceIniLoadsPerHour",
											//"dataSourceFinLoadsPerHour",
											"dataSourceInputMEP",
											"dataSourceOutputMEP",
											"dataSourceSimParams",
											"dataSourceItemsChanged",
											"dataSourceOrdersDetail",
											"dataSourceAnOrderDetail",
											"dataSourceLinesDetail",
											"dataSource",
											"dataSourceGlobalData",
											"dataSourceMaxPlant"])
		}
			
		
		public function getUser():String{
			return "User";
		}
		
		
		
		private var loaded:Boolean = false;
		/**
		 * Esta funcion valida que todos los datos esten disponibles para comenzar a activar todos los 
		 * lugares donde se requiera hacer algo con los datos
		 */
		public override function flashIslandDataReady():void {
			
			if (getDataSourceGlobalData() == null){
				return;
			}
			if (!loaded){
				this.dispatchServiceLoadReady();
				loaded= true;
			}
			//Alert.show(ReflectionHelper.object2XML(buffer.data,"IslandData","  ") );
			var lv_event:String = getDataSourceGlobalData()["ACTION_EXECUTED"].toString();
			//txt_planta.text = dataSourceGlobalData["ACTION_EXECUTED"].toString();
			//Alert.show(dataSourceGlobalData["ACTION_EXECUTED"].toString());
			
			
			dispatcher.dispatchEvent(new ChartIslandEvent(lv_event));
			
			getDataSourceGlobalData()["ACTION_EXECUTED"]= "IGNORE_THIS_TIME";
			/*
			switch(lv_event){
				case "SHOW_GRAPH":
					dispatcher.dispatchEvent(new FillChartEven( getDataSource()));
					break;
				case "REFRESH_FILTERS":
					this.limpiar();
					break;
				case "SHOW_MEP":
					
					break;
				case "SHOW_DETAIL":
					this.abrir();
					break;
				case "PLANT_SIM_PARAM":
					this.reFillGraphPlant(gv_plant_selected);						
					//Alert.show(ReflectionHelper.object2XML(dataSourceSimParams.getItemAt(0)));
					FlashIsland.fireEvent(dataSourceGlobalData, "resetActionExecuted");
					break;
				case "CONFIRM_CHANGES":
					//Obtiene todos los valores en memoria de los valores modificados
					dataSourceGlobalData["NUM_ITEMS_CHANGED"] = gv_num_items_changed;
					//FlashIsland.fireEvent(dataSourceGlobalData, "resetActionExecuted");
					FlashIsland.fireEvent(dataSourceGlobalData, "setItemsChanged");	
					break;
				case "SAVE_DATA":
					//Obtiene los valores a guardar
					FlashIsland.fireEvent(dataSourceGlobalData, "saveData");
					break;
				case "PLANT_SELECTED":
					break;
				case "MOVE_FULL_ORDER":
					//this.changeSalesOrder(gv_sales_order_selected, gv_origen_graph, gv_current_graph); 
					//debe identificar el pedido que selecciono y moverlo todo
					//limpiarlo de la gráfica origen
					//agregarlo a la otra grafica
					//g1.crearColumna(
				case "MEP_SELECTED":
					//debe identificar si agregó una planta que no existía para agregarla en la lista
					//si existe, entonces hay que ir por el pedido seleccionado y llevarlo a la planta
					break;
				case "SHOW_SUCCESS_MSG_CHG_PLT":
					//despliega mensaje de exito o error 
					break;
				case "SHOW_ERR_MSG_CHG_PLT":
					//Despliega mensaje de error en el cambio de planta
					break;
				
			}
		
			*/
			// aqui va el codigo para convertir el o para avisar que los datos ya estan listos
		}
		
		
		public function getDataSourceInputMEP():Object{
			return buffer["dataSourceInputMEP"];
		}
		
		public function getDataSourceOutputMEP():Object{
			return buffer["dataSourceOutputMEP"];
		}			
		
		public function getDataSourceSimParams():ArrayCollection{
			return buffer["dataSourceSimParams"];
		}
		
		public function getDataSourceItemsChanged():ArrayCollection{
			return buffer["dataSourceItemsChanged"];
		}
		
		public function getDataSourceOrdersDetail():ArrayCollection{
			return buffer["dataSourceOrdersDetail"];
		}
		
		public function getDataSourceAnOrderDetail():ArrayCollection{
			return buffer["dataSourceAnOrderDetail"];
		}
		
		public function getDataSourceLinesDetail():ArrayCollection{
			return buffer["dataSourceLinesDetail"];
		}
		
		public function getDataSource():ArrayCollection{
			return buffer["dataSource"];
		}
		
		public function getDataSourceGlobalData():Object{
			return buffer["dataSourceGlobalData"];
		}
		public function getDataSourceMaxPlant():ArrayCollection{
			return buffer["dataSourceMaxPlant"];
		}
		
		
		
		public function getRMSDetail():void{
			dispatchWDEvent(new GetRMSDetailRequest());
		}
		public function plantSelected():void{
			dispatchWDEvent(new PlantSelectedRequest());
		}
		public function requestMovOrd():void{
			dispatchWDEvent(new RequestMovOrdRequest());
		}
		public function resetActionExecuted():void{
			dispatchWDEvent(new ResetActionExecutedRequest());
		}
		public function salesOrderSelected():void{
			dispatchWDEvent(new SalesOrderSelectedRequest());
		}
		public function saveData():void{
			dispatchWDEvent(new SaveDataRequest());
		}
		public function setItemsChanged():void{
			dispatchWDEvent(new SetItemsChangedRequest());
		}
		public function showMEPWindows():void{
			dispatchWDEvent(new ShowMEPWindowsRequest());
		}
		public function updateData():void{
			dispatchWDEvent(new UpdateDataRequest());
		}
		public function validaMEP():void{
			dispatchWDEvent(new ValidaMEPRequest());
		}
		
		
		public function sendMessage(s:String):void{
			logger.info(s);
			//Alert.show(s);
		}
		
		
		private function addResoursesToRawDataXML(datas:ArrayCollection):String{
			var rawDataXML:String =  ReflectionHelper.object2XML(buffer.data,"IslandData","  ") ;
			return rawDataXML;
		}
		
	}
}