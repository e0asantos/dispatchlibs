package com.cemex.rms.loadsPerHour.services.flashIslands.requests
{
	import com.cemex.rms.common.flashislands.IFlashIslandEventObject;

	public class ResetActionExecutedRequest implements IFlashIslandEventObject
	{
		public function getEventName():String{
			return "resetActionExecuted";
		}
		public function ResetActionExecutedRequest()
		{
		}
	}
}