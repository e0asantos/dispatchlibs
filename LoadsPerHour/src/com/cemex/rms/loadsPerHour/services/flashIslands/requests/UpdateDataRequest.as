package com.cemex.rms.loadsPerHour.services.flashIslands.requests
{
	import com.cemex.rms.common.flashislands.IFlashIslandEventObject;

	public class UpdateDataRequest implements IFlashIslandEventObject
	{
		public function getEventName():String{
			return "updateData";
		}
		public function UpdateDataRequest()
		{
		}
	}
}