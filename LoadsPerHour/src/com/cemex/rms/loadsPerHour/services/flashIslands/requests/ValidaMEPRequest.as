package com.cemex.rms.loadsPerHour.services.flashIslands.requests
{
	import com.cemex.rms.common.flashislands.IFlashIslandEventObject;

	public class ValidaMEPRequest implements IFlashIslandEventObject
	{
		public function getEventName():String{
			return "validaMEP";
		}
		public function ValidaMEPRequest()
		{
		}
	}
}