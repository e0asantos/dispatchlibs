package com.cemex.rms.loadsPerHour.services.flashIslands.requests
{
	import com.cemex.rms.common.flashislands.IFlashIslandEventObject;

	public class PlantSelectedRequest implements IFlashIslandEventObject
	{
		public function getEventName():String{
			return "PlantSelected";
		}
		public function PlantSelectedRequest()
		{
		}
	}
}