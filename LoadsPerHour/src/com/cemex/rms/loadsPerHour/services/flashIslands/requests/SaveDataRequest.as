package com.cemex.rms.loadsPerHour.services.flashIslands.requests
{
	import com.cemex.rms.common.flashislands.IFlashIslandEventObject;

	public class SaveDataRequest implements IFlashIslandEventObject
	{
		public function getEventName():String{
			return "saveData";
		}
		public function SaveDataRequest()
		{
		}
	}
}