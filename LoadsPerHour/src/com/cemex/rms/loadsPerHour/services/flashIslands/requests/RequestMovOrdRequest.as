package com.cemex.rms.loadsPerHour.services.flashIslands.requests
{
	import com.cemex.rms.common.flashislands.IFlashIslandEventObject;

	public class RequestMovOrdRequest implements IFlashIslandEventObject
	{
		public function getEventName():String{
			return "requestMovOrd";
		}
		public function RequestMovOrdRequest()
		{
		}
	}
}