package com.cemex.rms.loadsPerHour.services.flashIslands.requests
{
	import com.cemex.rms.common.flashislands.IFlashIslandEventObject;

	public class GetRMSDetailRequest implements IFlashIslandEventObject
	{
		public function getEventName():String{
			return "GetRMSDetail";
		}
		public function GetRMSDetailRequest()
		{
		}
	}
}