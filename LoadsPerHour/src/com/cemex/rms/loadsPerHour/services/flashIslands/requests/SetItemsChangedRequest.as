package com.cemex.rms.loadsPerHour.services.flashIslands.requests
{
	import com.cemex.rms.common.flashislands.IFlashIslandEventObject;

	public class SetItemsChangedRequest implements IFlashIslandEventObject
	{
		public function getEventName():String{
			return "setItemsChanged";
		}
		public function SetItemsChangedRequest()
		{
		}
	}
}