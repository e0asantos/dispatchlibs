package com.cemex.rms.loadsPerHour.services.lcds
{
	public interface IChartProducer
	{
		function sendString(mensaje:String):void;
		function sendObject(mensaje:Object):void;
		function reconnect():void;
	}
}