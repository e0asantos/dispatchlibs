package com.cemex.rms.loadsPerHour.services.lcds.impl
{
	import com.cemex.rms.common.lcds.AbstractProducerService;
	import com.cemex.rms.common.services.AbstractService;
	import com.cemex.rms.loadsPerHour.services.lcds.IChartProducer;
	
	import mx.controls.Alert;
	import mx.messaging.ChannelSet;
	import mx.messaging.messages.IMessage;
	
	public class ChartInternalProducerImpl extends AbstractProducerService implements IChartProducer{
		
		
		public function ChartInternalProducerImpl()
		{
			super();
		}
		
		public override function getDestinationName():String{
			return "ASDestination";
		}
		
		public override function getChannelSet():ChannelSet{
			return servicesContext.getValue("ChannelSet") as ChannelSet;
		}
		
		public function sendString(mensaje:String):void{
			sendMessage(mensaje);
		}
		public function sendObject(mensaje:Object):void{
			sendMessage(mensaje);
		}
		
	}
}