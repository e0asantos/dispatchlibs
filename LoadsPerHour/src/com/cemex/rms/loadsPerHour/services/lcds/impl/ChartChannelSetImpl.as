package com.cemex.rms.loadsPerHour.services.lcds.impl
{
	import com.cemex.rms.common.lcds.AbstractChannelSetService;
	
	import mx.collections.ArrayCollection;
	import mx.messaging.ChannelSet;

	public class ChartChannelSetImpl extends AbstractChannelSetService
	{
		public function ChartChannelSetImpl()
		{
			super();
		}
		
		protected override function setChannelSet(channelSet:ChannelSet):void{
			
			servicesContext.setValue("ChannelSet",channelSet);
		} 
		
		protected override function getChannelSetConfig():Object{
			var result:ArrayCollection= new ArrayCollection();
			result.addItem(
			{	"type":"rtmp",
			"name":"amf-rtmp-channel",
			"endpoint":"rtmp://mxoccrmsrpd01.noam.cemexnet.com:2038"});
			
			return result;
		
		}
	}
}