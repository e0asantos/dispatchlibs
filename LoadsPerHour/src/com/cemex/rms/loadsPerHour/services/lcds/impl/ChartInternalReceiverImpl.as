package com.cemex.rms.loadsPerHour.services.lcds.impl
{
	import com.cemex.rms.common.lcds.AbstractReceiverService;
	import com.cemex.rms.common.reflection.ReflectionHelper;

	import com.cemex.rms.loadsPerHour.services.lcds.IChartReceiver;
	
	import flash.utils.getQualifiedClassName;
	
	import mx.collections.ArrayCollection;
	import mx.messaging.ChannelSet;
	
	public class ChartInternalReceiverImpl extends AbstractReceiverService implements IChartReceiver {
		
		public function ChartInternalReceiverImpl()
		{
			super();
		}
		
		public override function getDestinationName():String{
			return "ASDestination";
		}
		
		protected override function receiveMessage(message:Object):void {
			
			
			
			
			
			
		}
		public override function getChannelSet():ChannelSet{
			return servicesContext.getValue("ChannelSet") as ChannelSet;
		}
	}
}