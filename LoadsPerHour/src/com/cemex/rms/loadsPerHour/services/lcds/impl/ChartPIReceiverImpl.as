package com.cemex.rms.loadsPerHour.services.lcds.impl
{
	import com.cemex.rms.common.lcds.AbstractReceiverService;
	import com.cemex.rms.common.services.AbstractService;
	
	import com.cemex.rms.loadsPerHour.services.lcds.IChartReceiver;
	
	import mx.controls.Alert;
	import mx.messaging.ChannelSet;
	import mx.messaging.messages.IMessage;
	
	public class ChartPIReceiverImpl extends AbstractReceiverService implements IChartReceiver
	{
		public function ChartPIReceiverImpl()
		{
			super();
		}
		
		public override function getDestinationName():String{
			return "JMSDestination";
		}
		
		protected override function receiveMessage(message:Object):void {
			logger.info("receiveMessage("+message+")");
		}
		public override function getChannelSet():ChannelSet{
			return servicesContext.getValue("ChannelSet") as ChannelSet;
		}
		
		
	}
}