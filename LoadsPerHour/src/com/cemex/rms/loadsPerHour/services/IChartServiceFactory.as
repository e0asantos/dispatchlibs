package com.cemex.rms.loadsPerHour.services
{
	import com.cemex.rms.common.services.IServiceFactory;
	import com.cemex.rms.loadsPerHour.services.flashIslands.IChartIsland;
	import com.cemex.rms.loadsPerHour.services.lcds.IChartProducer;
	import com.cemex.rms.loadsPerHour.services.lcds.IChartReceiver;
	
	import mx.collections.ArrayCollection;

	public interface IChartServiceFactory
	{
		
		function getFlashIslandService():IChartIsland;
		
		function getChartPIReceiver():IChartReceiver ;
		function getChartInternalReceiver():IChartReceiver ;
		function getChartInternalProducer():IChartProducer ;
		
	}
}