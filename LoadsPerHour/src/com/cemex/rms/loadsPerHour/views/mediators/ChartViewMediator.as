package com.cemex.rms.loadsPerHour.views.mediators
{
	import com.cemex.rms.common.date.FlexDateHelper;
	import com.cemex.rms.common.lcds.DSServiceStatusEvent;
	import com.cemex.rms.common.logging.ILogger;
	import com.cemex.rms.common.logging.LoggerFactory;
	import com.cemex.rms.common.logging.events.LoggerEvent;
	import com.cemex.rms.common.reflection.ReflectionHelper;
	import com.cemex.rms.common.services.events.ServiceEvent;
	import com.cemex.rms.common.services.events.ServiceManagerEvent;
	import com.cemex.rms.loadsPerHour.services.IChartServiceFactory;
	import com.cemex.rms.loadsPerHour.services.flashIslands.events.ChartIslandEvent;
	import com.cemex.rms.loadsPerHour.services.flashIslands.impl.FillChartEven;
	import com.cemex.rms.loadsPerHour.views.ChartView;
	import com.cemex.rms.loadsPerHour.views.OrderFullDetail;
	import com.neoris.chart.ILoadsPerHourChart;
	import com.neoris.events.ChartIdentifierEvent;
	import com.neoris.events.LoadsPerHourChartEvent;
	
	import flash.events.MouseEvent;
	import flash.system.System;
	
	import mx.binding.utils.BindingUtils;
	import mx.collections.ArrayCollection;
	import mx.collections.HierarchicalData;
	import mx.collections.Sort;
	import mx.containers.Canvas;
	import mx.containers.HBox;
	import mx.containers.ViewStack;
	import mx.controls.Alert;
	import mx.controls.Label;
	import mx.controls.advancedDataGridClasses.AdvancedDataGridItemRenderer;
	import mx.core.Application;
	import mx.core.UIComponent;
	import mx.core.UITextField;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	
	import org.robotlegs.mvcs.Mediator;
	
	/**
	 * 
	 * DispatcherViewMediator es la clase mediador que le da funcionalidad a la vista, tambien
	 *  se encarga de hacer que los botones y la funcionalidad externa funcionen
	 * a demas que esta clase es la que se encarga de recibir los datos externos
	 * Es decir esta clase se encarga de afectar los datos y la vista del gantt
	 * 
	 * 
	 */
	public class ChartViewMediator extends Mediator {
		
		public function ChartViewMediator() {
			super();
		}
		
		
		/**
		 * La vista 
		 */
		[Inject]
		public var view : ChartView;
		[Inject]
		public var services : IChartServiceFactory;
		
		
		
		override public function onRegister():void {
			
			// SEccion de eventos del mouse
			/*
			view.reconnect.addEventListener(MouseEvent.CLICK,reconnect);
			
			
			view.zoomin.addEventListener(MouseEvent.CLICK,zoomin);
			view.zoomout.addEventListener(MouseEvent.CLICK,zoomout);
			view.showAll.addEventListener(MouseEvent.CLICK,showAll);
			//view.role.addEventListener(Event.CHANGE, changeRole);
			view.plantType.addEventListener(Event.CHANGE, changePlantType);
			
			
			view.refresh.addEventListener(MouseEvent.CLICK,refresh);
			view.currentTime.addEventListener(MouseEvent.CLICK,gotoCurrentTime);
			
			// SEccion de eventos Varios
				
			view.gantt.ganttSheet.addEventListener(GanttSheetEvent.ITEM_EDIT_END,itemEditEnd);
			
			view.viewStack.addEventListener(Event.CHANGE,changeView);
			
			
			
			
			/// SEccion de eventos externos
			
			
			//eventMap.mapListener(eventDispatcher,DeltaLoadEvent.ADD_DELTA_LOAD,addDeltaLoad);
			
			eventMap.mapListener(eventDispatcher,FlashIslandsDataEvent.FLASHISLAND_DATA_READY,islandDataEventHandler);
			eventMap.mapListener(eventDispatcher,FlashIslandsDataEvent.FLASHISLAND_DATA_CONFIRM,islandDataConfirmEventHandler);
			eventMap.mapListener(eventDispatcher,LCDSDataEvent.LCDS_DATA_RECEIVED,lcdsDataConfirmEventHandler);
			
			eventMap.mapListener(eventDispatcher,ServiceEvent.SERVICE_LOAD_READY,serviceReady);
			eventMap.mapListener(eventDispatcher,DSServiceStatusEvent.STATUS_SERVICE_CONNECTED,serviceStatus);
			eventMap.mapListener(eventDispatcher,DSServiceStatusEvent.STATUS_SERVICE_DISCONNECTED,serviceStatus);
			eventMap.mapListener(eventDispatcher,DSServiceStatusEvent.STATUS_SERVICE_FAULT,serviceStatus);
			
			content = view.gantt.ganttSheet.getChildByName("content")as UIComponent;
			timeIndicators = content.getChildByName("timeIndicators")as UIComponent;
			timeController = TimeControllerGetter.getTimeController(view.gantt.ganttSheet);
			
			
			timeIndicatorTimer = new Timer(0);
			timeIndicatorTimer.addEventListener(TimerEvent.TIMER,updateUITimerEvent)
			timeIndicatorTimer.start();
			*/
			
			eventMap.mapListener(eventDispatcher,LoggerEvent.LOGGER_EVENT,log);
			
			//eventMap.mapListener(eventDispatcher,FillChartEven.FILL_CHART,fillChartHandler);
			eventMap.mapListener(eventDispatcher,ChartIslandEvent.SHOW_GRAPH				,fillChartHandler);
			eventMap.mapListener(eventDispatcher,ChartIslandEvent.REFRESH_FILTERS			,limpiar);
			//eventMap.mapListener(eventDispatcher,ChartIslandEvent.SHOW_MEP					,handler);
			eventMap.mapListener(eventDispatcher,ChartIslandEvent.SHOW_DETAIL				,showDetailHandler);
			eventMap.mapListener(eventDispatcher,ChartIslandEvent.PLANT_SIM_PARAM			,plantSimParamHandler);
			eventMap.mapListener(eventDispatcher,ChartIslandEvent.CONFIRM_CHANGES			,confirmChangesHandler);
			eventMap.mapListener(eventDispatcher,ChartIslandEvent.SAVE_DATA					,saveDataHandler);
			//eventMap.mapListener(eventDispatcher,ChartIslandEvent.PLANT_SELECTED			,handler);
			//eventMap.mapListener(eventDispatcher,ChartIslandEvent.MOVE_FULL_ORDER			,handler);
			//eventMap.mapListener(eventDispatcher,ChartIslandEvent.MEP_SELECTED				,handler);
			//eventMap.mapListener(eventDispatcher,ChartIslandEvent.SHOW_SUCCESS_MSG_CHG_PLT	,handler);
			//eventMap.mapListener(eventDispatcher,ChartIslandEvent.SHOW_ERR_MSG_CHG_PLT		,handler);
			
			
			var lv_str1:String = null;
			var lv_str2:String = null;
			var lv_str3:String = null;
			/* lv_str1 = "Current Descriptcion of full detail:";
			lv_str2 = "01/01/01";
			lv_str3 = lv_str1 + "\t"+ lv_str2;
			lv_str3 = lv_str3 + "\n" + lv_str3;
			Alert.show(lv_str3); */
			view.addEventListener(FillChartEven.FILL_CHART,fillChartHandler);
			
			
			view.btn_abrir.addEventListener(MouseEvent.CLICK,abrir);
			
			//dragComplete="dragCompleteHandler(event);"
			view.list_plants.addEventListener(DragEvent.DRAG_COMPLETE,dragCompleteHandler);
			
			view.canvasPrincipal.addEventListener(DragEvent.DRAG_ENTER,dragEnterHandler);
			view.canvasPrincipal.addEventListener(DragEvent.DRAG_OVER,dragOverHandler);
			view.canvasPrincipal.addEventListener(DragEvent.DRAG_DROP,dragDropHandler);
			//dragEnter="dragEnterHandler(event);" 
			//dragOver="dragOverHandler(event);"
			//dragDrop="dragDropHandler(event);"
			
			//id="list_plants" dataProvider="{gv_plantsList}"
			gv_plantsList
			BindingUtils.bindProperty(view.list_plants,"dataProvider",this,["gv_plantsList"]);
			
			gv_plants
			
			
			//BindingUtils.bindProperty(view.txt_planta,"text",this,["gv_plants[1]"]);
			//BindingUtils.bindProperty(view.txt_planta2,"text",this,["gv_plants[2]"]);
			//BindingUtils.bindProperty(view.txt_planta3,"text",this,["gv_plants[3]"]);
			
			gv_resume_plant1
			
			BindingUtils.bindProperty(view.lbl_resume, "text",this,["gv_resume_plant1"]);
			BindingUtils.bindProperty(view.lbl_resume2,"text",this,["gv_resume_plant2"]);
			BindingUtils.bindProperty(view.lbl_resume3,"text",this,["gv_resume_plant3"]);
			
			
			
			view.copyLog.addEventListener(MouseEvent.CLICK,copyLog);
			view.logList.addEventListener(ListEvent.ITEM_CLICK,logClick);
			
			view.clipboard.addEventListener(MouseEvent.CLICK,copyToClipboard);
			
			eventMap.mapListener(eventDispatcher,ServiceManagerEvent.ALL_SERVICES_READY,allServiceReady);
			eventMap.mapListener(eventDispatcher,ServiceEvent.SERVICE_LOAD_READY,serviceReady);
			
			//view.grafica.addEventListener(FlexEvent.CREATION_COMPLETE,creationCompleteHandler);
			
				
		}
		
		
		public function allServiceReady(e:ServiceManagerEvent):void{
			logger.info("allServicesReady :" + e);
			if (g1 == null){
				registerChartEvents();
			}
		}
		
		public function serviceReady(e:ServiceEvent):void{
			logger.info("serviceReady :" + e);
			if (g1 == null && e.service.getServiceName() == "ChartIsland"){
				registerChartEvents();
			}
		}
		public function copyToClipboard(e:MouseEvent):void {
			
				System.setClipboard(
					services.getFlashIslandService().getData()
				);
			
		}

		
		
		public function logClick(e:ListEvent):void {
			
			view.logText.text = ""+e.itemRenderer.data;
			view.logText.verticalScrollPosition = 0;
		}
		public function copyLog(e:MouseEvent):void{
			System.setClipboard(
				ReflectionHelper.object2XML(logging,"Log")
			);
		}
		public function showDetailHandler(e:ChartIslandEvent):void{
			abrir();
		}
		public function abrirHandler(e:MouseEvent):void{
			abrir();
		}
		
		[Bindable]
		public var logging:ArrayCollection;
		private var logCounter:int;
		/// SEccion de eventos externos 
		public function log(e:LoggerEvent):void{
			
			if (logging == null) {
				logging = new ArrayCollection();
				logCounter = 0;
				BindingUtils.bindProperty(view.logList,"dataProvider",this,"logging");
			}
			++logCounter;
			logging.addItem("("+logCounter+")"+e.log);
			
			view.logText.text  =  "("+logCounter+")"+e.log+"\r\n";
			
			view.logList.verticalScrollPosition = view.logList.maxVerticalScrollPosition + 1;
			view.logText.verticalScrollPosition = 0;
		}
		
		//public function changeRole(e:Event):void{
		//	services.getFlashIslandService().setUserRole(view.role.selectedLabel);
		//}
		
		
		
		
	
		
		
	
	
	import com.cemex.rms.common.reflection.ReflectionHelper;

	
	import mx.collections.ArrayCollection;
	import mx.collections.Sort;
	import mx.collections.SortField;
	import mx.controls.Alert;
	import mx.core.IFlexDisplayObject;
	import mx.events.DragEvent;
	import mx.events.FlexEvent;
	import mx.managers.DragManager;
	import mx.managers.PopUpManager;
	
	/*
	import sap.FlashIsland;
	
	
	
	[Bindable]
	public var _dataSource:ArrayCollection;					//Current loadPerHours
	public var dataSourceIniLoadsPerHour:ArrayCollection;
	public var dataSourceFinLoadsPerHour:ArrayCollection;
	
	[Bindable]
	public var _dataSource_ini:ArrayCollection;				//load Per hours Initial
	
	[Bindable]
	public var _dataSourceMaxPlant:ArrayCollection;			//Cap Max
	
	[Bindable]
	public var _dataSourceGlobalData:Object;				//Global data
	
	[Bindable]
	public var _dataSourceOrdersDetail:ArrayCollection;		//Orders detail
	
	[Bindable]
	public var _dataSourceAnOrderDetail:ArrayCollection;	//An Order Detail
	
	[Bindable]
	public var _dataSourceLinesDetail:ArrayCollection;		//Detail Lines
	
	[Bindable]
	public var dataSourceStatus:ArrayCollection;			//Status
	
	[Bindable]
	public var _dataSourceSimParams:ArrayCollection;		
	
	[Bindable]
	public var _dataSourceItemsChanged:ArrayCollection;
	
	[Bindable]
	public  var _dataSourceInputMEP:Object;
	
	[Bindable]
	public  var _dataSourceOutputMEP:ArrayCollection;
	*/
	
	
	private var detailPopUP:IFlexDisplayObject;
	private var gv_plant_selected:String = "";
	private var gv_pos_selected:int = 0;
	private var gv_num_items_changed:int = 0;
	
	public var variablePublica:String = null;
	
	//variables de la planta
	private var gv_capMax:int = 0;					//CapacidadMax Planta
	private var gv_plantName:String = "";			//NombrePlanta
	private var gv_plantName2:String = "";
	private var gv_plantName3:String = "";
	
	[Bindable]
	public  var gv_resume_plant1:String = "Completed: __ m3 / In Progress: __ m3 / Pending: __ m3";
	[Bindable]
	public  var gv_resume_plant2:String = "Completed: __ m3 / In Progress: __ m3 / Pending: __ m3";
	[Bindable]
	public  var gv_resume_plant3:String = "Completed: __ m3 / In Progress: __ m3 / Pending: __ m3";
	private var gv_vbeln_selected:String =  "";
	private var gv_posnr_selected:String = "" ;
	//private var gv_plantas:Array = new Array();
	
	private var gv_myText:String = "";
	private const _MAX_LIST_PLANT:int = 10;		//Valor maximo arreglo de list plants
	private var gv_is_first_time:int = 0;
	//variables gráficos
	private var loadsPerHourChart:*;
	private var g1:ILoadsPerHourChart;
	private var g2:ILoadsPerHourChart;
	private var g3:ILoadsPerHourChart;
	private var gv_current_graph:int 	= 1;
	private var gv_origen_graph:int     = 1;
	private var gv_sales_order_selected:int = 0;
	
	[Bindable]
	public var gv_plants:Array = new Array([]);
	[Bindable]
	public var gv_plantsList:Array = new Array([""], [""], [""]);
	//private var gv_current_plant:String;
	
	public var popDetail:OrderFullDetail = new OrderFullDetail();
	
	/****************************************************************************************
	 *  abrir - Abre el popup y registra un nuevo evento de CREATION_COMPLETE               *
	 * **************************************************************************************/
	public function abrir():void{
		popDetail.percentHeight=100;
		popDetail.percentWidth=100;
		popDetail.addEventListener(FlexEvent.CREATION_COMPLETE, agregar);
		
		PopUpManager.addPopUp(popDetail,view,true);
		PopUpManager.centerPopUp(popDetail);
		services.getFlashIslandService().resetActionExecuted();
		//FlashIsland.fireEvent(dataSourceGlobalData, "resetActionExecuted");
	}
	
	/****************************************************************************************
	 *  agregar - Configura los string (labels,values, obs ) del popUp OrderFullDetail      *
	 * **************************************************************************************/
	public function agregar(e:FlexEvent):void{				
		popDetail.setLabelsDetail(this.getLabelsOrderDetail());
		popDetail.setValueDetail(this.getValuesOrderDetail());
		popDetail.setObsDetail(this.getObsOrderDetail());
		//popDetail.setObsDetail("Observaciones");
	}
	
	
	/****************************************************************************************
	 *  -------------------------METODOS GET ----------------------------------------------- *
	 * **************************************************************************************
	public function get dataSourceInputMEP():Object{
		return this._dataSourceInputMEP;
	}
	
	public function get dataSourceOutputMEP():ArrayCollection{
		return this._dataSourceOutputMEP;
	}			
	
	public function get dataSourceSimParams():ArrayCollection{
		return this._dataSourceSimParams;
	}
	
	public function get dataSourceItemsChanged():ArrayCollection{
		return this._dataSourceItemsChanged;
	}
	
	public function get dataSourceOrdersDetail():ArrayCollection{
		return this._dataSourceOrdersDetail;
	}
	
	public function get dataSourceAnOrderDetail():ArrayCollection{
		return this._dataSourceAnOrderDetail;
	}
	
	public function get dataSourceLinesDetail():ArrayCollection{
		return this._dataSourceLinesDetail;
	}
	
	public function get dataSource():ArrayCollection{
		return this._dataSource;
	}
	
	public function get dataSourceGlobalData():Object{
		return this._dataSourceGlobalData;
	}
	
	public function get dataSourceMaxPlant():ArrayCollection{
		return this._dataSourceMaxPlant;
	}
	
	****************************************************************************************
	 *  -------------------------METODOS SET----------------------------------------------- *
	 * **************************************************************************************
	public function set dataSourceInputMEP(_dataSourceInputMEP:Object):void{
		this._dataSourceInputMEP = _dataSourceInputMEP;
		loadedCount ++;
		flashIslandDataReady();
	}
	
	public function set dataSourceOutputMEP(_dataSourceOutputMEP:ArrayCollection):void{
		this._dataSourceOutputMEP = _dataSourceOutputMEP;
		loadedCount ++;
		flashIslandDataReady();
	}
	
	
	public function set dataSourceItemsChanged(_dataSourceItemsChanged:ArrayCollection):void{
		this._dataSourceItemsChanged = _dataSourceItemsChanged;
		loadedCount ++;
		flashIslandDataReady();
	}
	
	public function set dataSourceSimParams(_dataSourceSimParams:ArrayCollection):void{
		this._dataSourceSimParams = _dataSourceSimParams;
		loadedCount ++;
		flashIslandDataReady();
	}
	
	public function set dataSourceOrdersDetail(_dataSourceOrdersDetail:ArrayCollection):void{
		this._dataSourceOrdersDetail = _dataSourceOrdersDetail;
		loadedCount ++;
		flashIslandDataReady();
	}
	
	public function set dataSourceAnOrderDetail(_dataSourceAnOrderDetail:ArrayCollection):void{
		this._dataSourceAnOrderDetail = _dataSourceAnOrderDetail;
		loadedCount ++;
		flashIslandDataReady();
	} 
	
	public function set dataSourceLinesDetail(_dataSourceLinesDetail:ArrayCollection):void{
		this._dataSourceLinesDetail = _dataSourceLinesDetail;
		loadedCount ++;
		flashIslandDataReady();
	} 
	
	public function set dataSourceGlobalData(_dataSourceGlobalData:Object):void{
		this._dataSourceGlobalData = _dataSourceGlobalData;
		loadedCount ++;
		flashIslandDataReady();
	}
	
	public function set dataSourceMaxPlant(_dataSourceMaxPlant:ArrayCollection):void{
		this._dataSourceMaxPlant = _dataSourceMaxPlant;
		//fill_test();
		loadedCount ++;
		flashIslandDataReady();
	}
	
	public function set dataSource(_dataSource:ArrayCollection):void{
		this._dataSource = _dataSource;
		var status:String = null;
		var ob_globalData:Object = null;
		loadedCount ++;
		flashIslandDataReady();
		
	}
	
	*/
	public function reFillGraphPlant(p_plant:String):void{
		var ob_simParam:Object 			= null;
		var ob_loadph:Object			= null;
		var lv_frecuency:int 			= 0;
		var lv_loadTime:int 			= 0;
		var lv_volume:int 				= 0;
		var lv_capMax:int 				= 0;
		var lv_hora:int 				= 0;
		var lv_min:int					= 0;
		var lv_nroPedido:int			= 0;
		var lv_posnr:int				= 0;
		var lv_stePedido:int			= 0;
		var lv_color:uint				= 0;
		var lv_desc_tooltip:String		= "";
		var lv_num_reg_col:int			= 0;	
		var lv_iter_reg:int 			= 0;
		//Alert.show("A Repintar Grafica ");
		// Lee los parametros de simulación
		
		var dataSourceSimParams:ArrayCollection = services.getFlashIslandService().getDataSourceSimParams();
		var dataSource:ArrayCollection = services.getFlashIslandService().getDataSource();
		
		
		dataSource
		var i:Number;
		for(i =0; i < dataSourceSimParams.length; i++){
			ob_simParam = dataSourceSimParams.getItemAt(i);
			if(ob_simParam["PLANT"].toString()==p_plant){
				lv_frecuency = parseInt(ob_simParam["FRECUENCY"].toString());
				lv_loadTime  = parseInt(ob_simParam["LOADING_TIME"].toString());
				lv_volume	 = parseInt(ob_simParam["VOLUME"].toString());
				
				break;
			}
		}
		
		//Obtiene la capacidad Máxima de la planta
		lv_capMax = this.getPlantCapMax(p_plant);
		
		//Limpia la gráfica seleccionada
		//g1.limpiar();
		
		// calcula el número de registros por columna
		if(lv_capMax>0){
			lv_num_reg_col = lv_volume / lv_capMax;
		}else{
			lv_num_reg_col = lv_volume;
		}
		
		//Recorre loadsPerHour con los gráficos
		lv_hora = lv_loadTime;
		for(i =0; i<dataSource.length; i++){
			ob_loadph = dataSource.getItemAt(i);
			//Selecciona los de una planta especifica
			if(ob_loadph["WERKS"].toString()==p_plant){							
				lv_hora		 = parseInt(ob_loadph["UALBG"].toString().substring(0,2));
				lv_min		 = parseInt(ob_loadph["UALBG"].toString().substring(2,4));
				lv_nroPedido = parseInt(ob_loadph["VBELN"].toString());
				lv_posnr	 = parseInt(ob_loadph["POSNR"].toString());
				lv_stePedido = this.getStatePedido(ob_loadph["OBJNR"].toString());
				lv_color     = ob_loadph["COLOR"];		
				lv_desc_tooltip = this.getItemPartialDetail(ob_loadph);
				switch (gv_current_graph){
					case 1:
						g1.crearColumna(1,lv_hora,lv_stePedido,lv_nroPedido,lv_min,lv_posnr,lv_color,lv_desc_tooltip);
						
						//g1.crearColumna(1,lv_hora,lv_stePedido,lv_nroPedido,lv_color,lv_desc_tooltip);
						break;
					case 2:
						g2.crearColumna(1,lv_hora,lv_stePedido,lv_nroPedido,lv_min,lv_posnr,lv_color,lv_desc_tooltip);
						
						//g2.crearColumna(1,lv_hora,lv_stePedido,lv_nroPedido,lv_color,lv_desc_tooltip);
						break;
					case 3:
						g3.crearColumna(1,lv_hora,lv_stePedido,lv_nroPedido,lv_min,lv_posnr,lv_color,lv_desc_tooltip);
						
						//g3.crearColumna(1,lv_hora,lv_stePedido,lv_nroPedido,lv_color,lv_desc_tooltip);
						break;
				}
				//g1.crearColumna(1,lv_hora,lv_stePedido,lv_nroPedido,lv_color,lv_desc_tooltip);
				
				
			}
		}
	}
	
	
	
	
	public function plantSimParamHandler(e:ChartIslandEvent):void{
		this.reFillGraphPlant(gv_plant_selected);						
		//Alert.show(ReflectionHelper.object2XML(dataSourceSimParams.getItemAt(0)));
		//FlashIsland.fireEvent(dataSourceGlobalData, "resetActionExecuted");
		services.getFlashIslandService().resetActionExecuted();
	}
	public function confirmChangesHandler(e:ChartIslandEvent):void{
		//Obtiene todos los valores en memoria de los valores modificados
		var dataSourceGlobalData:Object = services.getFlashIslandService().getDataSourceGlobalData();
		dataSourceGlobalData["NUM_ITEMS_CHANGED"] = gv_num_items_changed;
		//FlashIsland.fireEvent(dataSourceGlobalData, "resetActionExecuted");
		//FlashIsland.fireEvent(dataSourceGlobalData, "setItemsChanged");	
		services.getFlashIslandService().setItemsChanged();
		
	}
	public function saveDataHandler(e:ChartIslandEvent):void{
		//Obtiene los valores a guardar
		//FlashIsland.fireEvent(dataSourceGlobalData, "saveData");
		services.getFlashIslandService().saveData();
		
	}
	
	
	/****************************************************************************************
	 *  limpiear : Refresca las gráficas											        *
	 * **************************************************************************************/
	public function limpiar(e:ChartIslandEvent):void{
		//Alert.show("Limpiando");
		g1.limpiar();
		g1.capacidadMaximaAnim([0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
		g1.maximumPlantCapacity(0);
		g2.limpiar();
		g2.capacidadMaximaAnim([0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
		g2.maximumPlantCapacity(0);
		g3.limpiar();
		g3.capacidadMaximaAnim([0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]);
		g3.maximumPlantCapacity(0); 
		var dataSourceGlobalData:Object = services.getFlashIslandService().getDataSourceGlobalData();
		
		services.getFlashIslandService().resetActionExecuted();
		//FlashIsland.fireEvent(dataSourceGlobalData, "resetActionExecuted");
	}
	
	public function fireEventResetActionExecuted():void{
		
		
		services.getFlashIslandService().resetActionExecuted();
		//FlashIsland.fireEvent(dataSourceGlobalData, "resetActionExecuted");
	}
	
	public function sortArray(p_array:ArrayCollection, p_field:String):void{
		var sortField:SortField = new SortField();
		sortField.name = p_field;
		sortField.numeric = false;
		var sort:Sort = new Sort();
		sort.fields = [sortField];
		sort.reverse();
		//Alert.show("Intenta Order");
		//p_array["sort"] = sort;
		p_array.sort = sort;
		p_array.refresh();
		//p_array["refresh()"]; 
		//	Alert.show("Intenta Order");
	}
	
	public function confirm():void{
		//Obtiene la información en las 3 gráficas
		//g1.getColumna(
		
	}
	public var dataSourceIniLoadsPerHour:ArrayCollection;
	public var dataSourceFinLoadsPerHour:ArrayCollection;
	private var logger:ILogger = LoggerFactory.getLogger("ChartViewMediator");
	private var counter:int = 0 ; 
	public function fillChartHandler(e:ChartIslandEvent):void{
		
		//Alert.show("Paso ("+(++counter)+")");
		var ob_loadph:Object = null;			// Carga Por Horas
		var hora:int      = 0;
		var min:int		  = 0;
		var nroPedido:int = 0;
		var posnr:int     = 0;
		var stePedido:int = 0;
		var lv_delta:int = 0;
		var lv_capMax:int = 0;
		var lv_highLimit:int = 0;
		
		var color:uint;
		var ob_globalData:Object = null;
		var desc_tooltip:String = null;
		var werks:String = null;
		var werks_old:String = null;
		var currentTime:Date = new Date();		// Hora Actual
		
		var lv_items_completed:Array 	= new Array(3, 0);		//# items completed per graph
		var lv_items_inProgress:Array 	= new Array(3, 0);		//# items in progress per graph
		var lv_items_pending:Array	 	= new Array(3, 0);		//# items pending per graph
		
		var lv_items_completed1:int		= 0;		//# items completed per graph
		var lv_items_inProgress1:int 	= 0;		//# items in progress per graph
		var lv_items_pending1:int	 	= 0;		//# items pending per graph
		var lv_items_completed2:int 	= 0;		//# items completed per graph
		var lv_items_inProgress2:int 	= 0;		//# items in progress per graph
		var lv_items_pending2:int	 	= 0;		//# items pending per graph
		var lv_items_completed3:int 	= 0;		//# items completed per graph
		var lv_items_inProgress3:int 	= 0;		//# items in progress per graph
		var lv_items_pending3:int	 	= 0;		//# items pending per graph
		
		var str_current_time:String = currentTime.getHours() + ":00:00";
		//Organiza el data source por plantas
		//this.sortArray(e.chartData,"WERKS");
		
		//Copia los valores iniciales del dataSource
		//logger.info("dataSource 1");
		var dataSource:ArrayCollection = services.getFlashIslandService().getDataSource();
		
		//logger.info("dataSource 2("+dataSource+")");
		dataSourceIniLoadsPerHour = dataSource;
		dataSourceFinLoadsPerHour = dataSource;
		
		if (g1 == null){
			registerChartEvents();
		}
		//logger.info("dataSource 2.1("+gv_plants+")");
		//logger.info("dataSource 2.2("+gv_plantsList+")");
		//Alert.show("Entro a fillchart");
		//Configura los parametors de la planta
		g1.sapHoraLocal(str_current_time);			
		//logger.info("dataSource 2("+dataSource+")");	
		g2.sapHoraLocal(str_current_time);			
		//logger.info("dataSource 2("+dataSource+")");	
		g3.sapHoraLocal(str_current_time);
		//logger.info("dataSource 3("+gv_plants+")");
		//logger.info("dataSource 3("+gv_plantsList+")");
		
		
		if(gv_is_first_time==1){
			limpiar(null);
		}
		
		gv_is_first_time = 1;
		
		
		this.setPlantParameters();
		
		ob_loadph 	= dataSource.getItemAt(0);
		if(ob_loadph["WERKS"]!=null){
			werks_old = ob_loadph["WERKS"].toString();
		}
		//logger.info("dataSource 4");
		var lv_contador_plants:int = 1;
		var lv_contador_listPlants:int = 0;
		//logger.info("dataSource 5");
		
		for(var i:Number=0; i<dataSource.length; i++){
			ob_loadph 	= dataSource.getItemAt(i);
			werks = ob_loadph["WERKS"].toString();
			// Determina si hay una nueva planta
			if(werks_old!=werks){
				//Incrementa el contador de plantas  
				lv_contador_plants = lv_contador_plants + 1;
				//Incrementa el contador de la lista de plantas que se despliega a la izquierda
				if(lv_contador_plants >3){
					
					lv_contador_listPlants = lv_contador_listPlants + 1;	
					gv_plantsList[lv_contador_listPlants] = werks;
				}
			}
			//logger.info("dataSource 6.1");
			werks_old = werks;
			//Alert.show(ob_loadph.toString());
			//loadsPerHourChart.crearColumna(1,i,1,9);
			
			hora 	  = parseInt(ob_loadph["UALBG"].toString().substring(0,2));
			min       = parseInt(ob_loadph["UALBG"].toString().substring(2,4));
			nroPedido = parseInt(ob_loadph["VBELN"].toString());
			posnr     = parseInt(ob_loadph["POSNR"].toString());
			stePedido = this.getStatePedido(ob_loadph["OBJNR"].toString());
			color     = ob_loadph["COLOR"];		
			desc_tooltip = this.getItemPartialDetail(ob_loadph);
			
			//logger.info("dataSource 6.2");
			
			//g3.crearColumna(1,hora,stePedido,nroPedido,color,desc_tooltip);
			switch(lv_contador_plants){
				case 1:
					g1.crearColumna(1,hora,stePedido,nroPedido,min,posnr,color,desc_tooltip);
					
					
					//g1.crearColumna(1,hora,stePedido,nroPedido,color,desc_tooltip);							
					view.txt_planta.text = werks;							
					gv_plants[lv_contador_plants] = werks;
					switch(stePedido){
						case 4:
							lv_items_completed1 = lv_items_completed1 + 1;
							break;
						case 10:
							lv_items_inProgress1 = lv_items_inProgress1 + 1;
							break;
						case 3:
							lv_items_pending1= lv_items_pending1 +1 ;
							break;	
					}
					//this.setResumeItemsGraph(stePedido, lv_items_completed1, lv_items_inProgress1,lv_items_pending1);
					break;
				
				case 2:
					g2.crearColumna(1,hora,stePedido,nroPedido,min,posnr,color,desc_tooltip);
					
					//g2.crearColumna(1,hora,stePedido,nroPedido,color,desc_tooltip);
					view.txt_planta2.text = werks;
					gv_plants[lv_contador_plants] = werks;
					switch(stePedido){
						case 4:
							lv_items_completed2 = lv_items_completed2 + 1;
							break;
						case 10:
							lv_items_inProgress2 = lv_items_inProgress2 + 1;
							break;
						case 3:
							lv_items_pending2= lv_items_pending2 +1 ;
							break;	
					}
					break;
				case 3:
					g3.crearColumna(1,hora,stePedido,nroPedido,min,posnr,color,desc_tooltip);
					
					//g3.crearColumna(1,hora,stePedido,nroPedido,color,desc_tooltip);
					view.txt_planta3.text = werks;
					gv_plants[lv_contador_plants] = werks;
					switch(stePedido){
						case 4:
							lv_items_completed3 = lv_items_completed3 + 1;
							break;
						case 10:
							lv_items_inProgress3 = lv_items_inProgress3 + 1;
							break;
						case 3:
							lv_items_pending3= lv_items_pending3 +1 ;
							break;	
					}
					
					break;
				default:
					gv_plantsList[lv_contador_listPlants] = werks;
					break;
			}  
			//g1.crearColumna(1,hora,stePedido,nroPedido,color,desc_tooltip);
		}
		//g1.capacidadMaximaAnim([7,8,10,11]
		// Alimenta el listado de plantas a la izquierda
		view.list_plants.dataProvider = gv_plantsList;
		
		
		// Configura el resumen de estados de cada una de las gráficas
		var str_items_resume:String = new String();
		view.lbl_resume.text = "Completed: " 	+ lv_items_completed1 	+ " m3 / "+
			"In Progress: " + lv_items_inProgress1 	+ " m3 / "+
			"Pending: "		+ lv_items_pending1		+ " m3 / ";
		
		view.lbl_resume2.text = "Completed: " + lv_items_completed2 	+ " m3 / "+
			"In Progress: " 	 + lv_items_inProgress2 	+ " m3 / "+
			"Pending: "			 + lv_items_pending2		+ " m3 / ";
		
		view.lbl_resume3.text = "Completed: " 	+ lv_items_completed3 	+ " m3 / "+
			"In Progress: " 	+ lv_items_inProgress3 	+ " m3 / "+
			"Pending: "			+ lv_items_pending3		+ " m3 / ";
		
		
		
		services.getFlashIslandService().resetActionExecuted();
		//FlashIsland.fireEvent(dataSourceGlobalData, "resetActionExecuted");
		//Imprimir  la capacidad máxima de cada planta
		lv_capMax = this.getPlantCapMax(gv_plants[1]);
		lv_delta  = this.getHighLimitPlant(gv_plants[1]);
		lv_highLimit = lv_capMax + (lv_capMax * lv_delta/100);
		
		g1.capacidadMaximaDeCargas(lv_highLimit);
		g1.maximumPlantCapacity(lv_capMax);
		g1.capacidadMaximaAnim([1,1,1,13,12,11,11,13,13,10,10,10,10,10,10,15,15,15,15,8,9,8,8]);
		
		
		lv_capMax = this.getPlantCapMax(gv_plants[2]);
		lv_delta  = this.getHighLimitPlant(gv_plants[2]);
		lv_highLimit = lv_capMax + (lv_capMax * lv_delta/100);
		g2.capacidadMaximaDeCargas(lv_highLimit);
		g2.maximumPlantCapacity(lv_capMax);
		g2.capacidadMaximaAnim([1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]);
		
		
		lv_capMax = this.getPlantCapMax(gv_plants[3]);
		lv_delta  = this.getHighLimitPlant(gv_plants[3]);
		lv_highLimit = lv_capMax + (lv_capMax * lv_delta/100);
		g3.capacidadMaximaDeCargas(lv_highLimit);
		g3.maximumPlantCapacity(lv_capMax);
		g3.capacidadMaximaAnim([8,8,12,12,4,8,7,13,13,9,9,8,8,5,4,5,5,5,9,9,6,6,10]);
		
	}
	
	protected function setLabelResumeStates(p_label:Label, p_text:String):void{
		p_label.text = p_text;
	}
	
	public function setResumeItemsGraph(stePedido:int, p_completed:int, p_inprogress:int, p_pending:int):void{
		switch(stePedido){
			case 4:
				p_completed = p_completed + 1;
				break;
			case 10:
				p_inprogress = p_inprogress + 1;
				break;
			case 3:
				p_pending = p_pending +1 ;
				break;	
		}
	}
	/*********************************************************************************
	 * drawGraph(graph, dataSource, plant) Pinta la grafica de la planta seleccionada
	 * *******************************************************************************/
	public function drawGraph(graph:ILoadsPerHourChart, posGraph:int, data:ArrayCollection, plant:String):void{
		var ob_loadph:Object = null;
		var lv_num_reg:int = 0;
		var hora:int      = 0;
		var min:int       = 0;
		var nroPedido:int = 0;
		var posnr:int     = 0;
		var stePedido:int = 0;
		var color:uint;
		var ob_globalData:Object = null;
		var desc_tooltip:String = null;
		var currentTime:Date = new Date();		// Hora Actual
		
		var lv_items_completed:int = 0;
		var lv_items_inProgress:int = 0;
		var lv_items_pending:int = 0;
		
		// Limpia la gráfica actual
		graph.limpiar();
		
		//Recorre los elementos del dataSource para solo ingresar aquellso que corresponden a 
		// la planta pasada por parámetro
		lv_num_reg = data.length;
		for(var i:Number=0; i<lv_num_reg; i++){
			ob_loadph = data.getItemAt(i);
			if(ob_loadph["WERKS"].toString() == plant){
				hora 	  = parseInt(ob_loadph["UALBG"].toString().substring(0,2));
				min		  = parseInt(ob_loadph["UALBG"].toString().substring(2,4));
				nroPedido = parseInt(ob_loadph["VBELN"].toString());
				posnr     = parseInt(ob_loadph["POSNR"].toString());
				stePedido = this.getStatePedido(ob_loadph["OBJNR"].toString());
				color     = ob_loadph["COLOR"];		
				desc_tooltip = this.getItemPartialDetail(ob_loadph);
				graph.crearColumna(1,hora,stePedido,nroPedido,min,posnr,color,desc_tooltip);
				//graph.crearColumna(1,hora,stePedido,nroPedido,color,desc_tooltip);
				switch(stePedido){
					case 4:
						lv_items_completed = lv_items_completed + 1;
						break;
					case 10:
						lv_items_inProgress = lv_items_inProgress + 1;
						break;
					case 3:
						lv_items_pending= lv_items_pending +1 ;
						break;	
				}
				
			}
		}
		
		//configura el resumen de items de la planta
		var str_resumeItems:String = "Completed: " 	+ lv_items_completed 	+ " m3 / "+
			"In Progress: " 	+ lv_items_inProgress 	+ " m3 / "+
			"Pending: "			+ lv_items_pending		+ " m3 / ";
		
		switch(posGraph){
			case 1:
				view.lbl_resume.text = str_resumeItems;
				break;
			case 2:
				view.lbl_resume2.text = str_resumeItems;
				break;
			case 3:
				view.lbl_resume3.text = str_resumeItems;
				break;
			
		}
		
	}
	
	/****************************************************************************************
	 *  getValuesOrderDetail - Obtiene los valores del detalle concatenadas en un solo string
	 * **************************************************************************************/
	public function getValuesOrderDetail():String{
		var lv_valuesDetail:String = "";
		
		var ob_detail:Object = null;
		var ob_line:Object = null;
		
		var lv_datgb:String = "";
		var lv_ezeit:String = "";
		var lv_uatbg:String = "";
		var lv_lddat:String = "";
		var lv_lduhr:String = "";
		var lv_ualbg:String = "";
		var lv_werks:String = "";
		var lv_name:String  = "";
		var lv_zzwerkool:String = "";
		var lv_vbeln:String = "";
		var lv_posnr:String = "";
		var lv_namecust:String = "";
		var lv_nameobj:String = "";
		var lv_arktx:String = "";
		var lv_matnr:String = "";
		var lv_atwrt:String = "";
		var lv_kwmeng:String = "";
		var lv_objnr:String = "";
		var lv_obser:String ="";
		var lv_zz_pump_assing:String = "";
		var lv_sign1:String = "";
		var lv_Exit1:String = "";
		
		
		var dataSourceAnOrderDetail:ArrayCollection = services.getFlashIslandService().getDataSourceAnOrderDetail();	
		
		
		if(dataSourceAnOrderDetail.length!=0){
			//Alert.show();
			ob_detail = dataSourceAnOrderDetail.getItemAt(0);
			//Alert.show(ReflectionHelper.object2XML(ob_detail));
		}else{
			Alert.show("No hay data para mostrar");
		}
		
		if(ob_detail["DATGB"]!=null){
			lv_datgb = ob_detail["DATGB"].toString();
		}
		
		if(ob_detail["EZEIT"]!=null){
			lv_ezeit = ob_detail["EZEIT"].toString();
		}
		
		if(ob_detail["LDDAT"]!=null){
			lv_lddat = ob_detail["LDDAT"].toString();
		}
		
		if(ob_detail["LDUHR"]!=null){
			lv_lduhr = ob_detail["LDUHR"].toString();
		}
		
		if( ob_detail["UALBG"]!=null){
			lv_ualbg =  ob_detail["UALBG"].toString();
		}
		
		//g1.crearColumna(
		
		if( ob_detail["WERKS"]!=null){
			lv_ualbg = ob_detail["WERKS"].toString();
		}
		
		if(ob_detail["ZZ_WERKOOL"]!=null){
			lv_zzwerkool = ob_detail["ZZ_WERKOOL"].toString();
		}
		
		if(ob_detail["VBELN"]!=null){
			lv_vbeln = ob_detail["VBELN"].toString();
		}
		
		if(ob_detail["POSNR"]!=null){
			lv_posnr = ob_detail["POSNR"].toString();
		}
		
		if(ob_detail["NAMEOBJ"]!=null){
			lv_nameobj = ob_detail["NAMEOBJ"].toString();
		}
		
		if(ob_detail["MATNR"]!=null){
			lv_matnr = ob_detail["MATNR"].toString();
		}
		
		if(ob_detail["ATWRT"]!=null){
			lv_atwrt = ob_detail["ATWRT"].toString();
		}
		
		if(ob_detail["KWMENG"]!=null){
			lv_kwmeng = ob_detail["KWMENG"].toString();
		}
		
		if(ob_detail["OBJNR"]!=null){
			lv_objnr = ob_detail["OBJNR"].toString();
		}
		
		lv_valuesDetail = 	lv_datgb + "\n" +
			lv_ezeit + "\n" + 
			lv_uatbg + "\n" + 
			lv_lddat + "\n" + 
			lv_lduhr + "\n" + 
			lv_ualbg + "\n" + 
			lv_werks + "\n" + 
			lv_name + "\n" + 
			lv_zzwerkool + "\n" + 
			lv_vbeln + "\n" + 
			lv_posnr + "\n" + 
			lv_namecust + "\n" + 
			lv_nameobj + "\n" + 
			lv_arktx + "\n" + 
			lv_matnr + "\n" + 
			lv_atwrt + "\n" + 
			lv_kwmeng + "\n" + 
			lv_objnr + "\n" + 
			lv_obser + "\n" + 
			lv_zz_pump_assing + "\n" +
			lv_sign1 + "\n" +
			lv_Exit1;
		
		return lv_valuesDetail;
	}
	
	/****************************************************************************************
	 *  GetObsOrderDetail - Obtiene las observaciones concatenadas en un solo string        *
	 * **************************************************************************************/
	public function getObsOrderDetail():String{
		var ob_lines:Object = null;					//Objeto Línea
		var num_reg:Number = 0;						//Número Registros 
		var	lv_obsDetail:String = "";				//Variable de retorno
		
		var dataSourceLinesDetail:ArrayCollection = services.getFlashIslandService().getDataSourceLinesDetail();	
		
		//valida que haya datos para leer en el nodo lines
		if(dataSourceLinesDetail.length!=0){
			num_reg = dataSourceLinesDetail.length;
			//Recorre las lineas
			for(var i:Number = 0; i<num_reg; i++){
				ob_lines = dataSourceLinesDetail.getItemAt(i);
				//Alert.show(ob_lines.toString());
				//Alert.show(ReflectionHelper.object2XML(ob_lines));
				//Valida que el campo no sea null
				//Alert.show(ReflectionHelper.object2XML(ob_lines));
				if(ob_lines["TDLINE"]!=null){
					lv_obsDetail = lv_obsDetail + ob_lines["TDLINE"].toString();
				}						
			}
			//ob_lines = dataSourceLinesDetail.getItemAt(0);
		}else{
			Alert.show("No hay data en lines para mostrar");
		}
		return lv_obsDetail;
		
	}
	
	/****************************************************************************************
	 *  getLabelsOrderDetail - Obtiene los label del detalle concatenadas en un solo string *
	 * **************************************************************************************/
	public function getLabelsOrderDetail():String{
		var lv_label:String = null;
		
		var ob_detail:Object = null;
		var ob_line:Object = null;
		
		var lv_datgb_desc:String = "Current date for start of shippment";
		var lv_ezeit_desc:String = "Arrival time";
		var lv_uatbg_desc:String = "Actual transport start time";
		var lv_lddat_desc:String = "Loading Date";
		var lv_lduhr_desc:String = "Loading Time";
		var lv_ualbg_desc:String = "Actual loading start time";
		var lv_werks_desc:String = "Plant( Own or External )";
		var lv_name_desc:String  = "Name";
		var lv_zzwerkool_desc:String = "Optimun Plant";
		var lv_vbeln_desc:String = "Sales document";
		var lv_posnr_desc:String = "Sales document item";
		var lv_namecust_desc:String = "Name";
		var lv_nameobj_desc:String = "name";
		var lv_arktx_desc:String = "Short text for sales order item";
		var lv_matnr_desc:String = "Material Number";
		var lv_atwrt_desc:String = "Characteristic value";
		var lv_kwmeng_desc:String = "Cumulative order quantity in sales Units";
		var lv_objnr_desc:String = "Object number at item level";
		var lv_obser:String ="Text_Line";
		var lv_zz_pump_assing:String = "Assignent pump";
		var lv_sign1_desc:String = "Container ID";
		var lv_Exit1_desc:String = "Vehicle Plate 1";
		
		lv_label = 	lv_datgb_desc + "\n" +
			lv_ezeit_desc + "\n" + 
			lv_uatbg_desc + "\n" + 
			lv_lddat_desc + "\n" + 
			lv_lduhr_desc + "\n" + 
			lv_ualbg_desc + "\n" + 
			lv_werks_desc + "\n" + 
			lv_name_desc + "\n" + 
			lv_zzwerkool_desc + "\n" + 
			lv_vbeln_desc + "\n" + 
			lv_posnr_desc + "\n" + 
			lv_namecust_desc + "\n" + 
			lv_nameobj_desc + "\n" + 
			lv_arktx_desc + "\n" + 
			lv_matnr_desc + "\n" + 
			lv_atwrt_desc + "\n" + 
			lv_kwmeng_desc + "\n" + 
			lv_objnr_desc + "\n" + 
			lv_obser + "\n" + 
			lv_zz_pump_assing + "\n" +
			lv_sign1_desc + "\n" +
			lv_Exit1_desc;
		
		
		
		
		
		
		/* for(var i:Number = 0; i<dataSourceAnOrderDetail.length; i++){
		ob_detail = dataSourceAnOrderDetail.getItemAt(i);
		}
		
		for(var i:Number = 0; i<dataSourceLinesDetail.length; i++){
		ob_line = dataSourceLinesDetail.getItemAt(i);
		} */
		
		return lv_label;
	}
	
	public function getAnOrderFullDetail():String{
		return null;
	}
	
	/****************************************************************************************
	 *  getItemPartialDetail - Obtiene los el detalle parcial                               *
	 * **************************************************************************************/
	public function getItemPartialDetail(ob_loadph:Object):String{
		var lv_tooltip:String = null;
		var lv_datgb_desc:String = "Current date for start of shippment";
		var lv_ezeit_desc:String = "Arrival time";
		var lv_lddat_desc:String = "Loading Date";
		var lv_lduhr_desc:String = "Loading Time";
		var lv_ualbg_desc:String = "Actual loading start time";
		var lv_werks_desc:String = "Plant( Own or External )";
		var lv_zzwerkool_desc:String = "Optimun Plant";
		var lv_vbeln_desc:String = "Sales document";
		var lv_posnr_desc:String = "Sales document item";
		var lv_nameobj_desc:String = "name";
		var lv_matnr_desc:String = "Material Number";
		var lv_atwrt_desc:String = "Characteristic value";
		var lv_kwmeng_desc:String = "Cumulative order quantity in sales Units";
		var lv_objnr_desc:String = "Object number at item level";
		
		var lv_datgb:String = "";
		var lv_ezeit:String = "";
		var lv_lddat:String = "";
		var lv_lduhr:String = "";
		var lv_ualbg:String = "";
		var lv_werks:String = "";
		var lv_zzwerkool:String = "";
		var lv_vbeln:String = "";
		var lv_posnr:String = "";
		var lv_nameobj:String = "";
		var lv_matnr:String = "";
		var lv_atwrt:String = "";
		var lv_kwmeng:String = "";
		var lv_objnr:String = "";
		
		
		if(ob_loadph["DATGB"]!=null){
			lv_datgb = ob_loadph["DATGB"].toString();
		}
		
		if(ob_loadph["EZEIT"]!=null){
			lv_ezeit = ob_loadph["EZEIT"].toString();
		}
		
		if(ob_loadph["LDDAT"]!=null){
			lv_lddat = ob_loadph["LDDAT"].toString();
		}
		
		if(ob_loadph["LDUHR"]!=null){
			lv_lduhr = ob_loadph["LDUHR"].toString();
		}
		
		if( ob_loadph["UALBG"]!=null){
			lv_ualbg =  ob_loadph["UALBG"].toString();
		}
		
		if( ob_loadph["WERKS"]!=null){
			lv_ualbg = ob_loadph["WERKS"].toString();
		}
		
		if(ob_loadph["ZZ_WERKOOL"]!=null){
			lv_zzwerkool = ob_loadph["ZZ_WERKOOL"].toString();
		}
		
		if(ob_loadph["VBELN"]!=null){
			lv_vbeln = ob_loadph["VBELN"].toString();
		}
		
		if(ob_loadph["POSNR"]!=null){
			lv_posnr = ob_loadph["POSNR"].toString();
		}
		
		if(ob_loadph["NAMEOBJ"]!=null){
			lv_nameobj = ob_loadph["NAMEOBJ"].toString();
		}
		
		if(ob_loadph["MATNR"]!=null){
			lv_matnr = ob_loadph["MATNR"].toString();
		}
		
		if(ob_loadph["ATWRT"]!=null){
			lv_atwrt = ob_loadph["ATWRT"].toString();
		}
		
		if(ob_loadph["KWMENG"]!=null){
			lv_kwmeng = ob_loadph["KWMENG"].toString();
		}
		
		if(ob_loadph["OBJNR"]!=null){
			lv_objnr = ob_loadph["OBJNR"].toString();
		}
		
		lv_tooltip = lv_datgb_desc + ":\t" + lv_datgb +"\n";
		lv_tooltip = lv_tooltip + lv_ezeit_desc + ":\t" + lv_ezeit +"\n";
		lv_tooltip = lv_tooltip + lv_lddat_desc + ":\t" + lv_lddat +"\n";
		lv_tooltip = lv_tooltip + lv_lduhr_desc + ":\t" + lv_lduhr +"\n";
		lv_tooltip = lv_tooltip + lv_ualbg_desc + ":\t" + lv_ualbg +"\n";
		lv_tooltip = lv_tooltip + lv_werks_desc + ":\t" + lv_werks +"\n";
		lv_tooltip = lv_tooltip + lv_zzwerkool_desc + ":\t" + lv_zzwerkool +"\n";
		lv_tooltip = lv_tooltip + lv_vbeln_desc + ":\t" + lv_vbeln +"\n";
		lv_tooltip = lv_tooltip + lv_posnr_desc + ":\t" + lv_posnr+"\n";
		lv_tooltip = lv_tooltip + lv_nameobj_desc + ":\t" + lv_nameobj +"\n";
		lv_tooltip = lv_tooltip + lv_matnr_desc + ":\t" + lv_matnr +"\n";
		lv_tooltip = lv_tooltip + lv_atwrt_desc + ":\t" + lv_atwrt +"\n";
		lv_tooltip = lv_tooltip + lv_kwmeng_desc + ":\t" + lv_kwmeng +"\n";
		lv_tooltip = lv_tooltip + lv_objnr_desc + ":\t" + lv_objnr;
		
		return lv_tooltip;
	}
	
	
	/****************************************************************************************
	 *  iniApp - Inicializa el aplicativo                                                   *
	 * **************************************************************************************
	public function initApp():void{
		var lv_str1:String = null;
		var lv_str2:String = null;
		var lv_str3:String = null;
		
		
		FlashIsland.register(this);

		this.addEventListener(FillChartEven.FILL_CHART,fillChartHandler);
		//gv_plantsList= new Array(["D161", "D162", "D164", "D165", "D166"]);
	}
	*/
	
	
	/****************************************************************************************
	 *  application1_creationCompleteHandler -                                              *
	 * **************************************************************************************/
	protected function registerChartEvents():void
	{
		//Alert.show("registerChartEvents");
		// TODO Auto-generated method stub
		//var algo:* = grafica.content["getChildAt"](0).content;
		loadsPerHourChart = view.grafica.content["getChildAt"](0).content;
		
		//Alert.show("loadsPerHourChart:"+loadsPerHourChart["graficaUno"]);
		//Alert.show("loadsPerHourChart:"+loadsPerHourChart["graficaDos"]);
		//Alert.show("loadsPerHourChart:"+loadsPerHourChart["graficaTres"]);
		
		g1 = loadsPerHourChart["graficaUno"] as ILoadsPerHourChart;
		g2 = loadsPerHourChart["graficaDos"] as ILoadsPerHourChart;
		g3 = loadsPerHourChart["graficaTres"] as ILoadsPerHourChart;
		
		
			logger.info(g1+":loadsPerHourChart:"+loadsPerHourChart["graficaUno"]);
			logger.info(g2+":loadsPerHourChart:"+loadsPerHourChart["graficaDos"]);
			logger.info(g3+":loadsPerHourChart:"+loadsPerHourChart["graficaTres"]);
			
		logger.info("g1"+g1);
		logger.info("g2"+g2);
		logger.info("g3"+g3);
		
		//this.addEventListener(LoadsPerHourChartEvent.ABRIR_MEP, showMep);
		g1["addEventListener"](LoadsPerHourChartEvent.ABRIR_MEP, showMep); 
		g1["addEventListener"](LoadsPerHourChartEvent.FULL_DETAIL,showFullDetail);
		g1["addEventListener"](LoadsPerHourChartEvent.PEDIDO_CHANGE, changeSalesOrderHandler);
		g2["addEventListener"](LoadsPerHourChartEvent.ABRIR_MEP, showMep); 
		g2["addEventListener"](LoadsPerHourChartEvent.FULL_DETAIL,showFullDetail);
		g2["addEventListener"](LoadsPerHourChartEvent.PEDIDO_CHANGE, changeSalesOrderHandler);
		g3["addEventListener"](LoadsPerHourChartEvent.ABRIR_MEP, showMep); 
		g3["addEventListener"](LoadsPerHourChartEvent.FULL_DETAIL,showFullDetail);
		g3["addEventListener"](LoadsPerHourChartEvent.PEDIDO_CHANGE, changeSalesOrderHandler);
		
		loadsPerHourChart["addEventListener"](ChartIdentifierEvent.CHART_SELECTED, chartIdentifierHandler);
		//g1.addEventListener(loadsPerHourChartEvent.FULL_DETAIL, showFullDetail);
		//g1.addEventListener(loadsPerHourChartEvent.PEDIDO_CHANGE, changeSalesOrder);
	}
	
	private var lastPosSelected:int = -1;
	public function chartIdentifierHandler(event:ChartIdentifierEvent):void{
		//Alert.show(String(event.numeroGrafica));
		gv_pos_selected = 0;
		if(gv_plants.length != 0 ){
			
			gv_pos_selected = event.numeroGrafica;
			if (lastPosSelected != gv_pos_selected){
				
				gv_current_graph = gv_pos_selected;
				//Alert.Show(gv_pos_selected.toString());					
				gv_plant_selected = gv_plants[gv_pos_selected];
				view.lbl_current_graph.text = gv_plant_selected;
				
				//Alert.show(""+services.getFlashIslandService());
				var dataSourceGlobalData:Object = services.getFlashIslandService().getDataSourceGlobalData();
				dataSourceGlobalData["PLANT_SELECTED"] = gv_plant_selected;					
				
				services.getFlashIslandService().plantSelected();
				lastPosSelected = gv_pos_selected;
			}
			
			
			//FlashIsland.fireEvent(dataSourceGlobalData, "PlantSelected");
		}
	}
	
	public function showFullDetail(event:LoadsPerHourChartEvent):void{
		var ob_ordersDet:Object = null;
		var ob_itemsChg:Object = new Object();
		var ob_loadh:Object = null;
		var ob_fullDetail:OrderFullDetail = new OrderFullDetail();
		var lv_pedido:int = 0;
		var lv_posicion:String = "0010";
		
		var dataSourceOrdersDetail:ArrayCollection = services.getFlashIslandService().getDataSourceOrdersDetail();
		ob_ordersDet = dataSourceOrdersDetail.getItemAt(0);
		
		//determina la acción que está ejecutando
		//pregunta si la hora es diferente a la anterior.
		if(event.dummyData["horario"]==event.dummyData["horarioOriginal"]){
			//esta solicitando el full detail
			ob_ordersDet["VBELN"] = "470";
			ob_ordersDet["POSNR"] = "0010";
			
			services.getFlashIslandService().getRMSDetail();
			//FlashIsland.fireEvent(dataSourceOrdersDetail, "GetRMSDetail");
		}else{
			// Agrega un nuevo cambio relacionado a CHG_HR
			// Recorre cada uno de los items relacionados al cambio
			var lv_dif:int = 0;
			var lv_new_hour:int = 0;
			var lv_str_new_hour:String = new String(6);
			lv_pedido = event.dummyData["numeroPedido"];
			
			var dataSourceItemsChanged:ArrayCollection = services.getFlashIslandService().getDataSourceItemsChanged();
			
			for(var i:Number = 0; i<dataSourceIniLoadsPerHour.length; i++){
				ob_loadh = dataSourceIniLoadsPerHour.getItemIndex(i);
				if(ob_loadh["WERKS"]!=null){
					if(ob_loadh["WERKS"].toString()==gv_plant_selected){
						if(parseInt(ob_loadh["VBELN"].toString())==lv_pedido){
							lv_dif = event.dummyData["horario"] - event.dummyData["horarioOriginal"];
							ob_itemsChg["ORDER_NUMBER"] = event.dummyData["numeroPedido"].toString();
							ob_itemsChg["ITEM_NUMBER"] = event.dummyData["posicion"].toString();
							ob_itemsChg["CHANGE_TYPE"] = "CHG_HR";
							//Obtiene la hora de loadh
							lv_new_hour = parseInt(ob_loadh["UALBGR"].toString().substring(0,2)) + lv_dif;
							lv_str_new_hour = lv_new_hour + ob_loadh["UALBGR"].toString().substring(2,6);
							ob_itemsChg["NEW_VALUE"]   = lv_str_new_hour;
							dataSourceItemsChanged.addItem(ob_itemsChg);
							gv_num_items_changed = gv_num_items_changed + 1;
						}
					}
				}
			}
		}
		
	}
	
	
	public function changeSalesOrderHandler(event:LoadsPerHourChartEvent):void{
		//Adicionar un registro a los cambios
		
		var ob_itemsChg:Object = new Object();
		
		if(event.dummyData["numeroPedido"]!=null){
			ob_itemsChg["ORDER_NUMBER"] = event.dummyData["numeroPedido"].toString();
		}
		
		if(event.dummyData["posicion"]!=null){
			ob_itemsChg["ITEM_NUMBER"] = event.dummyData["posicion"].toString();
		}
		
		ob_itemsChg["CHANGE_TYPE"] = "CHG_PLT";				
		ob_itemsChg["NEW_VALUE"]   = gv_plant_selected;
		
		var dataSourceItemsChanged:ArrayCollection = services.getFlashIslandService().getDataSourceItemsChanged();
		
		dataSourceItemsChanged.addItem(ob_itemsChg);
		gv_num_items_changed = gv_num_items_changed + 1;
		
		
		// dataSourceItemsChanged.
		//FlashIsland.fireEvent(dataSourceOrdersDetail, "requestMovOrd");
		
		services.getFlashIslandService().requestMovOrd();
		
		//Alert.show("changeSalesOrder");
	}
	
	public function changeSalesOrder(p_salesOrder:String, p_graph_ini:int, p_graph_fin:int):void{
		
		//change
		//Recorre el nodo principal para determinar cuantas posiciones tiene el pedido
		var ob_loadph:Object = null;
		var lv_plant:String = gv_plants[p_graph_ini];
		var lv_graph_ini:ILoadsPerHourChart = null;
		var lv_graph_fin:ILoadsPerHourChart = null;
		var hora:int      = 0;
		var min:int		  = 0;
		var nroPedido:int = 0;
		var posnr:int     = 0;
		var stePedido:int = 0;
		var color:uint;
		var ob_globalData:Object = null;
		var desc_tooltip:String = null;
		
		switch(p_graph_ini){
			case 1:
				lv_graph_ini = g1;
				break;
			case 2:
				lv_graph_ini = g2;
				break;
			case 3:
				lv_graph_ini = g3;
				break;
		}
		
		switch(p_graph_fin){
			case 1:
				lv_graph_fin = g1;
				break;
			case 2:
				lv_graph_fin = g2;
				break;
			case 3:
				lv_graph_fin = g3;
				break;
		}
		
		
		for(var i:Number = 0; i<dataSourceIniLoadsPerHour.length; i++){
			ob_loadph = dataSourceIniLoadsPerHour.getItemIndex(i);
			if(ob_loadph["WERKS"].toString()==lv_plant &&
				ob_loadph["VBELN"].toString()==p_salesOrder){
				hora 	  = parseInt(ob_loadph["UALBG"].toString().substring(0,2));
				min       = parseInt(ob_loadph["UALBG"].toString().substring(2,4));
				nroPedido = parseInt(ob_loadph["VBELN"].toString());
				posnr     = parseInt(ob_loadph["POSNR"].toString());
				stePedido = this.getStatePedido(ob_loadph["OBJNR"].toString());
				color     = ob_loadph["COLOR"].toString();		
				desc_tooltip = this.getItemPartialDetail(ob_loadph);
				
				lv_graph_fin.crearColumna(1,hora,stePedido,nroPedido,min,posnr,color,desc_tooltip);
				//ob_loadph["NEW_WERKS"] = gb_plants[p_graph_fin];
				
			}					
		}
		
		//lo copia en el dataSource en las variables new plant
		
		
		//despues de copiarlas las borra
		lv_graph_ini.borrarPorPedido(parseInt(p_salesOrder));
		
		services.getFlashIslandService().validaMEP();
		//FlashIsland.fireEvent(dataSourceOrdersDetail, "validaMEP");
		
		
	}
	
	public function btn_showMep():void{
			
		services.getFlashIslandService().showMEPWindows();
		services.getFlashIslandService().resetActionExecuted();
		
		//FlashIsland.fireEvent(dataSourceOrdersDetail, "showMepWindows");
		//FlashIsland.fireEvent(dataSourceGlobalData, "resetActionExecuted");
	}
	
	
	public function showMep(event:LoadsPerHourChartEvent):void{
		// COnfigura los parametros de entrada del MEP
		
		
		var dataSourceInputMEP:Object = services.getFlashIslandService().getDataSourceInputMEP();
		
		
		dataSourceInputMEP["WERKS"] = gv_plants[gv_pos_selected];
		dataSourceInputMEP["VBELN"] = event.dummyData["numeroPedido"];
		dataSourceInputMEP["POSNR"] = event.dummyData["posicion"];
		
		view.lbl_pedido.text =  event.dummyData["numeroPedido"];
		view.lbl_posicion.text = event.dummyData["posicion"];
		//FlashIsland.fireEvent(dataSourceOrdersDetail, "salesOrderSelected");
		services.getFlashIslandService().salesOrderSelected();
		//lbl_pedido.text = 
		/* Alert.show("ShowMep");
		if(event.target==g1){
		Alert.show("ShowMap From grafica 1");
		}else{
		if(event.target==g2){
		Alert.show("ShowMap From grafica 2");						
		}else{
		Alert.show("ShowMap From grafica 3");
		}
		} */
	}
	
	
	//protected function showMep(event:Load
	//copiar com.neoris.events
	
	//function func(event:load...)
	public function addItemsColumna(hora:int, columna:Array, datSource:ArrayCollection):void{
		var ob_loadph:Object = new Object();
		for(var i:Number=0; i<columna.length; i++){
			ob_loadph = new Object();					
			ob_loadph["UALBG"] = hora.toString()+":00:00";				//Hora
			ob_loadph["VBELN"] = new String(columna[i].pedido);
			ob_loadph["OBJNR"] = this.getObjnrFromStateNumber(columna[i].tipo);
			datSource.addItem(ob_loadph);
		}
	}
	
	/*****************************************************************************
	 * setAditionalDataLoadPerHour : Recorre la carga inicial y busca el registro 
	 * que corresponda con la planta, pedido, posición 
	 * ***************************************************************************/
	public function setAditionalDataLoadPerHour(ob_loadph:Object):void{
		var ob_loadph_ini:Object = null;
		var lv_num_reg:Number = dataSourceIniLoadsPerHour.length;
		
		for(var i:Number=0; i<lv_num_reg; i++){
			ob_loadph_ini = dataSourceIniLoadsPerHour.getItemAt(i);
			if(ob_loadph["WERKS"].toString()==ob_loadph_ini["WERKS"].toString() &&
				ob_loadph["VBELN"].toString()==ob_loadph_ini["VBELN"].toString() &&
				ob_loadph["POSNR"].toString()==ob_loadph_ini["POSNR"].toString()){
				// Copia los demás valores
				ob_loadph["DATGB"] = ob_loadph_ini["DATGB"];
				ob_loadph["EZEIT"] = ob_loadph_ini["EZEIT"];
				ob_loadph["EZEIT"] = ob_loadph_ini["EZEIT"];
				ob_loadph["LDDAT"] = ob_loadph_ini["LDDAT"];
				ob_loadph["LDUHR"] = ob_loadph_ini["LDUHR"];
				
				ob_loadph["WERKS"] = ob_loadph_ini["WERKS"];
				ob_loadph["ZZ_WERKOOL"] = ob_loadph_ini["ZZ_WERKOOL"];
				ob_loadph["VBELN"] = ob_loadph_ini["VBELN"];
				ob_loadph["POSNR"] = ob_loadph_ini["POSNR"];
				ob_loadph["NAMEOBJ"] = ob_loadph_ini["NAMEOBJ"];
				ob_loadph["MATNR"] = ob_loadph_ini["MATNR"];
				ob_loadph["ATWRT"] = ob_loadph_ini["ATWRT"];
				ob_loadph["KWMENG"] = ob_loadph_ini["KWMENG"];
			}						
		}
	}
	
	
	/******************************************************************************************************
	 * getDataSourceGraph: Configura un dataSource de acuerdo a los items de una columna                  *
	 * ****************************************************************************************************/	
	public function getDataSourceGraph(graph:ILoadsPerHourChart, planta:String, dataSource:ArrayCollection):void{
		var ob_loadph:Object= new Object();
		var la_columna:Array = new Array();
		// Recorre las 24 columnas de la gráfica
		for(var i:Number = 0 ; i<24; i++){
			la_columna = graph.getColumna(i);
			
			
			//Recorre cada uno de los items encontrados en una columna
			for(var j:Number=0; j<la_columna.length; i++){
				ob_loadph = new Object();		
				ob_loadph["WERKS"] = planta;
				ob_loadph["UALBG"] = i.toString()+":00:00";				//Hora
				ob_loadph["VBELN"] = new String(la_columna[j].pedido);
				ob_loadph["POSNR"] = new String(la_columna[j].posnr);
				ob_loadph["OBJNR"] = this.getObjnrFromStateNumber(la_columna[j].tipo);
				
				dataSource.addItem(ob_loadph);
			}
		}
	}
	
	
	public function searchItem(plant:String, pedido:String, posnr:String):void{
		// recorre todas las posiciones del elemento fin 
	}
	
	
	
	
	protected function btn_send_clickHandler(event:MouseEvent):void
	{
		var ob_loadph_ini:Object = null;
		var ob_loadph_fin:Object = null;
		var la_columna1:Array = null;
		var la_columna2:Array = null;
		var la_columna3:Array = null;
		var lv_total_horas:Number = 24;
		
		//Obtiene los valores de la gráfica1
		dataSourceFinLoadsPerHour.removeAll();
		var i:Number = 0;
		//Recorre cada una de las columnas y agrega sus items en dataSourceFinLoadsPerHour
		for(i=0; i<lv_total_horas; i++){
			this.addItemsColumna(i,g1.getColumna(i), dataSourceFinLoadsPerHour);
			this.addItemsColumna(i,g2.getColumna(i), dataSourceFinLoadsPerHour);
			this.addItemsColumna(i,g3.getColumna(i), dataSourceFinLoadsPerHour);
		}	
		
		// Recorre las 3 graficas y obtiene el detalle de cada uno de los items
		
		//Recorre cada uno de los items de la carga inicial y pregunta si está o no en la final
		for(i=0; i<dataSourceIniLoadsPerHour.length; i++){
			ob_loadph_ini = dataSourceIniLoadsPerHour.getItemAt(i);
			//Busca el objeto por la planta, pedido y posición en la carga final
			//Si existe, copia todos los valores adicionales de la carga INI a FIN
			//ob_loadph_fin = 
		}
		
		//this.dataSource = _dataSource;*/
		
		services.getFlashIslandService().updateData();
		//FlashIsland.fireEvent(dataSource, "updateData");
	}
	
	protected function button1_clickHandler(event:MouseEvent):void
	{
		// TODO Auto-generated method stub
		//fill_chart();
	}
	
	protected function fill_test():void{
		
		Alert.show(ReflectionHelper.object2XML(services.getFlashIslandService().getDataSourceMaxPlant()));
		
		
		/* var ob_globalData:Object = null;
		for(var i:Number=0; i<dataSourceGlobalData.length; i++){
		
		ob_globalData = dataSourceGlobalData.getItemAt(i);
		lbl_test.text = ob_globalData["ACTION_EXECUTED"].toString();
		} */
	}
	
	protected function validaCambiosPlanta():void{
		
	}
	
	protected function setPlantParameters():void{
		var ob_MaxPlant:Object = null;
		
		
		
		var dataSourceMaxPlant:ArrayCollection = services.getFlashIslandService().getDataSourceMaxPlant();
		
		
		for(var i:Number = 0; i<dataSourceMaxPlant.length; i++){
			ob_MaxPlant	= dataSourceMaxPlant.getItemAt(i);
			switch(ob_MaxPlant["PARAM"].toString()){
				case "MAX_LOAD_M3":
					gv_capMax = parseInt(ob_MaxPlant["VALUE"].toString());
				case "PLANT_NAME":
					//Alert.show(ob_MaxPlant["VALUE"].toString());
					view.txt_planta.text = ob_MaxPlant["VALUE"].toString();
					//gv_plantName = ob_MaxPlant["VALUE"].toString();
			}
			
		}
	}
	
	
	protected function getPlantCapMax(p_plant:String):int{
		var ob_MaxPlant:Object = null;
		
	
		var dataSourceMaxPlant:ArrayCollection = services.getFlashIslandService().getDataSourceMaxPlant();
		for(var i:Number = 0; i<dataSourceMaxPlant.length; i++){
			ob_MaxPlant	= dataSourceMaxPlant.getItemAt(i);
			if(ob_MaxPlant["PARAM"].toString()=="MAX_LOAD_M3" &&
				ob_MaxPlant["WERKS"].toString()==p_plant){
				return parseInt(ob_MaxPlant["VALUE"].toString());
			}
		}
		
		return 0;
	}
	
	protected function getHighLimitPlant(p_plant:String):int{
		var ob_MaxPlant:Object = null;
		var lv_high_limit:int  = 0;
		var dataSourceMaxPlant:ArrayCollection = services.getFlashIslandService().getDataSourceMaxPlant();
		for(var i:Number = 0; i<dataSourceMaxPlant.length; i++){
			ob_MaxPlant	= dataSourceMaxPlant.getItemAt(i);
			
			if(ob_MaxPlant["PARAM"].toString()=="PLT_HIGH_LIMIT_P" &&
				ob_MaxPlant["WERKS"].toString() == p_plant){
				lv_high_limit = parseInt(ob_MaxPlant["VALUE"].toString().substring(0,2));
				//return parseInt(ob_MaxPlant["VALUE"].toString());
			}
		}
		
		return lv_high_limit;
		
	}
	
	protected function getObjnrFromStateNumber(p_tipo:int):String{
		var lv_objnr:String = null;
		switch(p_tipo){
			case 0:
				lv_objnr = "CMPO"
				break;
			case 1:
				lv_objnr = "CNCO";
				break;
			case 2:
				lv_objnr = "NOST";
				break;
			case 3:
				lv_objnr = "TBCL";
				break;
			case 4:
				lv_objnr = "CMPL";
				break;
			case 5:
				lv_objnr = "NOPA";
				break;
			case 6:
				lv_objnr = "TJST";
				break;
			case 7:
				lv_objnr = "OJOB";
				break;
			case 8:
				lv_objnr = "UNLD";
				break;
			case 9:
				lv_objnr = "SIML";
				break;
			case 10:
				lv_objnr = "LDNG";
				break;
			case 11:
				lv_objnr = "SYST";
				break;
		}
		return lv_objnr;
	}
	
	protected function showFullDetailPopUp():void{
		
		var ob_fullDetail:OrderFullDetail = new OrderFullDetail();
		
		//ob_fullDetail.setLabelsDetail(this.getLabelsOrderDetail());
		//ob_fullDetail.labelsDetail = "Mierda";
		Alert.show(	this.getLabelsOrderDetail() );
		detailPopUP = PopUpManager.createPopUp(view, OrderFullDetail, true); 
		PopUpManager.centerPopUp(detailPopUP);
		
	}
	
	protected function getStatePedido(p_objnr:String):int{
		var lv_status:int = 0;
		switch(p_objnr){
			case "CMPO" :
				lv_status = 0;
				break;
			case "CNCO":
				lv_status = 1;
				break;
			case "NOST":
				lv_status = 2;
				break;
			case "TBCL":
				lv_status = 3;
				break;
			case "CMPL":
				lv_status = 4;
				break;
			case "NOPA":
				lv_status = 5;
				break;
			case "TJST":
				lv_status = 6;
				break;
			case "OJOB":
				lv_status = 7;
				break;
			case "UNLD":
				lv_status = 8;
				break;
			case "SIML":
				lv_status = 9;
				break;
			case "LDNG":
				lv_status = 10;
				break;
			case "SYST":
				lv_status = 11;
				break;
		}
		return lv_status;				
	}
	
	public function setSimParamsToAPlant():void{
		/* var lv_plant:String = "D164";
		//var ob_GlobalData:Object = null;
		//if(dataSourceGlobalData!=null){
		gv_plant_selected = txt_plant_selected.text;
		dataSourceGlobalData["PLANT_SELECTED"] = txt_plant_selected.text;
		//}
		FlashIslan d.fireEvent(dataSourceGlobalData, "PlantSelected");*/
		
	}
	
	/**********************************************************************************
	 * dragComplete
	 * ********************************************************************************/
	private function dragCompleteHandler(event:DragEvent):void {
		if (event.action == DragManager.MOVE){
			//plants1.removeChildAt(plants1.getFocus());
			//var lv_obj = new Object();
			//lv_obj = new String("Eureka");
			//plants1.removeChildAt(0);
			//plants1.addChild(lv_obj);
			//plants1.dataProvider = gv_plantsList;
			
		}
		//dragInitCanvas.removeChild(draggedImage);
	}
	
	private function dragOverHandler(event:DragEvent):void
	{
		DragManager.showFeedback(DragManager.MOVE);
	}
	
	private function dragEnterHandler(event:DragEvent):void {
		DragManager.acceptDragDrop(Canvas(event.currentTarget));				
	}
	
	private function dragDropHandler(event:DragEvent):void {
		// Determina si se seleccionó una planta de la lista
		if(view.list_plants.selectedItem!=0){
			//Determina la planta selecciona 
			switch(gv_pos_selected){
				case 1:
					gv_plantsList[view.list_plants.selectedIndex] = view.txt_planta.text;
					view.txt_planta.text = view.list_plants.selectedItem.toString();
					gv_plants[1] = view.list_plants.selectedItem.toString();
					this.drawGraph(g1, gv_pos_selected, dataSourceFinLoadsPerHour,view.list_plants.selectedItem.toString());
					//Llama la función para pintar la grafica en la planta seleccionada							
					break;
				case 2:
					gv_plantsList[view.list_plants.selectedIndex] = view.txt_planta2.text;
					view.txt_planta2.text = view.list_plants.selectedItem.toString();
					gv_plants[2]=view.list_plants.selectedItem.toString();
					this.drawGraph(g2, gv_pos_selected, dataSourceFinLoadsPerHour,view.list_plants.selectedItem.toString());
					break;
				case 3:
					gv_plantsList[view.list_plants.selectedIndex] = view.txt_planta3.text;
					view.txt_planta3.text = view.list_plants.selectedItem.toString();
					gv_plants[3]=view.list_plants.selectedItem.toString();
					this.drawGraph(g3, gv_pos_selected, dataSourceFinLoadsPerHour,view.list_plants.selectedItem.toString());
					break;						
			}
			
			view.list_plants.dataProvider = gv_plantsList;
			
			
		}
		//txt_planta.text = plants1.getChildAt().toString();
		/*var lv_planta_tmp = txt_planta.text;				
		txt_planta.text = gv_plantsList[plants1.getFocus()];
		gv_plantsList[plants1.getFocus()] = lv_planta_tmp;*/
	}
	
	
	}
	
}