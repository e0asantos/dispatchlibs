package com.cemex.rms.loadsPerHour
{
	import com.cemex.rms.common.logging.LoggerFactory;
	import com.cemex.rms.common.services.IServiceFactory;
	import com.cemex.rms.common.services.ServiceFactory;
	import com.cemex.rms.common.services.events.ServiceManagerEvent;
	import com.cemex.rms.loadsPerHour.controller.StartupCommand;
	import com.cemex.rms.loadsPerHour.services.ChartServiceFactory;
	import com.cemex.rms.loadsPerHour.services.ChartServicesModel;
	import com.cemex.rms.loadsPerHour.services.IChartServiceFactory;
	import com.cemex.rms.loadsPerHour.views.ChartView;
	import com.cemex.rms.loadsPerHour.views.mediators.ChartViewMediator;
	
	import org.robotlegs.base.ContextEvent;
	import org.robotlegs.mvcs.Context;
	
	public class MainContext extends Context
	{
		
	import flash.display.DisplayObjectContainer;
		
		public function MainContext(contextView:DisplayObjectContainer=null, autoStartup:Boolean=true)
		{
			super(contextView, autoStartup);			
		}
		
		
		
		override public function startup():void
		{
			
			
			LoggerFactory.defaultLogger = LoggerFactory.LOGGER_TYPE_EVENT;
			// SE inicializa el factory de servicios
			var factory:ServiceFactory = new ServiceFactory();
			
			
			// Se le settea el dispatcher del contexto a los servicios
			// para poder utilizar los listeners de los servicios con la arquitectura de 
			// Robotlegs
			factory.dispatcher = eventDispatcher;
			factory.model = new ChartServicesModel();
			// Se settea el Factory de Servicios
			injector.mapValue(IServiceFactory, factory);
			
			
			var chartService:ChartServiceFactory = new ChartServiceFactory();
			//injector.injectInto(ganttService);
			chartService.serviceFactory = factory;
			injector.mapValue(IChartServiceFactory,chartService);
			
/*			DispatcherColorHelper.service 	= ganttService;
			GanttTaskMoveHelper.service 	= ganttService;
			TooltipHelper.service 			= ganttService;
			LoadPerOrderTaskHelper.service 	= ganttService;
			ILogDateFormatterHelper.service = ganttService;
			OrderLoad.service 				= ganttService;
*/
			
			// Se settea el mapa de Vistas mediador

			
			//mediatorMap.mapView(TaskBranchRenderer, TaskBranchRendererMediator);
			
			mediatorMap.mapView(ChartView, ChartViewMediator);
			// se crea el mediador de la vista principal el FlashIsland Settea datos
			// en cuanto se manda a cargar el servicio de FlashIslands
			mediatorMap.createMediator((contextView as MainModule).view );
			
			
			
			// Se mappean los eventos con comandos
			/*
			commandMap.mapEvent(ServiceManagerEvent.ALL_SERVICES_READY, ServicesReadyCommand);
			
			
			commandMap.mapEvent(DataReloadRequestEvent.DATA_RELOAD_REQUEST, DataReloadCommand);
			commandMap.mapEvent(OrderOperationRequestEvent.ORDER_ON_HOLD_REQUEST, OrderOnHoldCommand);
			commandMap.mapEvent(OrderOperationRequestEvent.CHANGE_PLANT_REQUEST, ChangePlantCommand);
			commandMap.mapEvent(OrderOperationRequestEvent.CHANGE_DELIVERY_TIME_REQUEST, ChangeDeliveryTimeCommand);
			
			commandMap.mapEvent(OrderOperationRequestEvent.REUSE_CONCRETE_REQUEST, ReuseConcreteCommand);
			commandMap.mapEvent(OrderOperationRequestEvent.REDIRECT_LOAD_REQUEST, RedirectLoadCommand);
			commandMap.mapEvent(OrderOperationRequestEvent.ASSIGN_PLANT_REQUEST, AssignPlantCommand);
			commandMap.mapEvent(OrderOperationRequestEvent.CHANGE_VOLUME_REQUEST, ChangeVolumeCommand);
			commandMap.mapEvent(OrderOperationRequestEvent.ASSIGN_VEHICLE_REQUEST, AssignVehicleCommand);
			
			
			
			commandMap.mapEvent(GanttTaskMovementEvent.GANTT_TASK_MOVE,GanttTaskMovementCommand);
			commandMap.mapEvent(LCDSReconnectEvent.LCDS_RECONNECT_REQUEST,LCDSReconnectCommand);
			
			commandMap.mapEvent(FlashIslandsDataEvent.FLASHISLAND_DATA_CONFIRM,BroadCastFlashIslandConfirmCommand);
			*/
			
			commandMap.mapEvent(ContextEvent.STARTUP, StartupCommand, ContextEvent, true);
			// Se mandan a inicializar las cosas
			dispatchEvent( new ContextEvent( ContextEvent.STARTUP ) );
			
			
			
		}
	}
}