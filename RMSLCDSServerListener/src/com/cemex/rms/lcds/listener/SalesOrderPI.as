package com.cemex.rms.lcds.listener
{
	import com.cemex.rms.common.PushInfo;
	import com.cemex.rms.common.lcds.AbstractReceiverService;
	import com.cemex.rms.common.reflection.ReflectionHelper;
	
	import mx.messaging.ChannelSet;
	import mx.messaging.messages.IMessage;
	
	public class SalesOrderPI extends AbstractReceiverService
	{
		public function SalesOrderPI()
		{
			super();
		}
		
		public override function getDestinationName():String{
			return "SalesOrderPI";
		}
		public override function getChannelSet():ChannelSet{
			return servicesContext.getValue("ChannelSetLCDS") as ChannelSet;
		}
		
		override protected function receiveIMessage(message:IMessage):void{		
			logger.debug(ReflectionHelper.object2XML(message,getDestinationName()));
		}
		
		
	}
}