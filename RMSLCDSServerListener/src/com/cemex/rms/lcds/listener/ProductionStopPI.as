package com.cemex.rms.lcds.listener
{
	import com.cemex.rms.common.PushInfo;
	import com.cemex.rms.common.lcds.AbstractReceiverService;
	
	import mx.messaging.ChannelSet;
	import mx.messaging.messages.IMessage;
	
	public class ProductionStopPI extends AbstractReceiverService
	{
		public function ProductionStopPI()
		{
			super();
		}
		
		public override function getDestinationName():String{
			return "ProductionStopPI";
		}
		public override function getChannelSet():ChannelSet{
			return servicesContext.getValue("ChannelSetLCDS") as ChannelSet;
		}
		
		override protected function receiveIMessage(message:IMessage):void{		
			PushInfo.logObjectInfo(type,getDestinationName(),message);
		}
	}
}