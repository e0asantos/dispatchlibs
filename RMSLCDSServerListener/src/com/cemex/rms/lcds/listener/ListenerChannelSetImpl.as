package com.cemex.rms.lcds.listener
{
	import com.cemex.rms.common.push.AbstractChannelSetService;
	import com.cemex.rms.common.push.PushConfigHelper;
	
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.messaging.ChannelSet;

	public class ListenerChannelSetImpl extends AbstractChannelSetService
	{
		public function ListenerChannelSetImpl()
		{
			super();
			
			var lcds:Object ={
				
				LCDS_ENDPOINT:
				{	
					//URL:"rtmp://mxoccrmsrid01.noam.cemexnet.com:1935/weborb",
					//URL:"rtmp://mxoccrmsrid01.noam.cemexnet.com:2038",
					//URL:"rtmp://10.26.29.177:2038",
					//CHANNEL_NAME:"RMS_RTMP_CHANNEL",
					//CHANNEL_TYPE:"rtmp"
					URL:"rtmp://10.26.29.177:1953/weborb",
					CHANNEL_NAME:"weborb-rtmp-messaging",
					CHANNEL_TYPE:"weborb"
					
					
					//URL:"rtmp://mxoccrmsrid01.noam.cemexnet.com:1938/weborb",
					//CHANNEL_NAME:"weborb-rtmp-messaging",
					//CHANNEL_TYPE:"weborb"
				}
				
				
			};
			PushConfigHelper.initLcdsEndpoint(lcds);
			
		}
		
		protected override function setChannelSet(channelSet:ChannelSet):void{
			
			servicesContext.setValue("ChannelSetLCDS",channelSet);
			
		} 
		
	}
}