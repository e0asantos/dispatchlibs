package com.cemex.rms.weborb.listener
{
	import com.cemex.rms.common.push.AbstractChannelSetService;
	import com.cemex.rms.common.push.PushConfigHelper;
	
	import mx.messaging.ChannelSet;

	public class ListenerChannelSetImpl extends AbstractChannelSetService
	{
		public function ListenerChannelSetImpl()
		{
			super();
			
			var lcds:Object ={
				
				
				LCDS_ENDPOINT:
				{
					
					//URL:"rtmp://localhost:1935/weborb",
					//rtmp://mxoccrmsrpd01.noam.cemexnet.com:1953/weborb
					//URL:"rtmp://10.26.29.177:1946/weborb/",
					//mxoccrmsrid01.noam.cemexnet.com:1953/weborb
					//rtmp://mxoccrmsrpd01.noam.cemexnet.com:1952/weborb
					URL:RMSLCDSServerListener.RTMP,
					//URL:"rtmp://mxoccrmsrpd01.noam.cemexnet.com:1973/weborb",
					CHANNEL_NAME:"weborb-rtmp-messaging",
					CHANNEL_TYPE:"weborb"
				}
			};
			PushConfigHelper.initLcdsEndpoint(lcds);
		}
		
		protected override function setChannelSet(channelSet:ChannelSet):void {
			servicesContext.setValue("ChannelSetWEBORB",channelSet);
			
		} 
		
	}
}