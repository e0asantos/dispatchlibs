package com.cemex.rms.weborb.listener
{
	import com.cemex.rms.common.PushInfo;
	import com.cemex.rms.common.lcds.AbstractReceiverService;
	import com.cemex.rms.common.reflection.ReflectionHelper;
	
	import mx.messaging.ChannelSet;
	import mx.messaging.messages.IMessage;
	
	public class PlantPI extends AbstractReceiverService
	{
		public function PlantPI()
		{
			super();
		}
		
		public override function getDestinationName():String{
			return "PlantPI_CR";
		}
		public override function getChannelSet():ChannelSet{
			return servicesContext.getValue("ChannelSetWEBORB") as ChannelSet;
		}
		override protected function receiveIMessage(message:IMessage):void{
			RMSLCDSServerListener.logAnyWhere(getDestinationName(),ReflectionHelper.object2XML(message.headers,getDestinationName()),false);
			RMSLCDSServerListener.logAnyWhere(getDestinationName(),ReflectionHelper.object2XML(message.body,getDestinationName()));
			//PushInfo.logObjectInfo(type,getDestinationName(),message);
		}
		
		
	}
}