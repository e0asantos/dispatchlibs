package com.cemex.rms.weborb.listener
{
	import com.cemex.rms.common.PushInfo;
	import com.cemex.rms.common.lcds.AbstractReceiverService;
	import com.cemex.rms.common.reflection.ReflectionHelper;
	
	import mx.messaging.ChannelSet;
	import mx.messaging.messages.IMessage;
	
	public class SampleRTMPDestination extends AbstractReceiverService
	{
		public function SampleRTMPDestination()
		{
			super();
		}
		
		public override function getDestinationName():String{
			return "SampleRTMPDestination";
		}
		public override function getChannelSet():ChannelSet{
			return servicesContext.getValue("ChannelSetWEBORB") as ChannelSet;
		}
		override protected function receiveIMessage(message:IMessage):void{		
			RMSLCDSServerListener.logAnyWhere(getDestinationName(),ReflectionHelper.object2XML(message.body,getDestinationName()));
			//PushInfo.logObjectInfo(type,getDestinationName(),message);
		}
		
	}
}