package com.cemex.rms.weborb.listener
{
	import com.cemex.rms.common.PushInfo;
	import com.cemex.rms.common.lcds.AbstractReceiverService;
	
	import mx.messaging.ChannelSet;
	import mx.messaging.messages.IMessage;
	
	public class PoConfirmationPIRID extends AbstractReceiverService
	{
		public function PoConfirmationPIRID()
		{
			super();
		}
		public override function getDestinationName():String{
			return "PoConfirmationPIRID";
		}
		public override function getChannelSet():ChannelSet{
			
			return servicesContext.getValue("ChannelSetWEBORB") as ChannelSet;
		}
		override protected function receiveIMessage(message:IMessage):void{		
			//PushInfo.logObjectInfo(type,getDestinationName(),message);
		}
	}
}