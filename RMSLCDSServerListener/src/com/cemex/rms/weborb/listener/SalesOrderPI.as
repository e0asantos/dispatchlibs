package com.cemex.rms.weborb.listener
{
	import com.cemex.rms.common.PushInfo;
	import com.cemex.rms.common.lcds.AbstractReceiverService;
	import com.cemex.rms.common.reflection.ReflectionHelper;
	
	import mx.messaging.ChannelSet;
	import mx.messaging.messages.IMessage;
	
	public class SalesOrderPI extends AbstractReceiverService
	{
		public function SalesOrderPI()
		{
			super();
		}
		
		public override function getDestinationName():String{
			return "SalesOrderPI_CR";
		}
		public override function getChannelSet():ChannelSet{
			return servicesContext.getValue("ChannelSetWEBORB") as ChannelSet;
		}
		
		override protected function receiveIMessage(message:IMessage):void{
			//RMSLCDSServerListener.logAnyWhere(this.name,message);
			//logger.debug(ReflectionHelper.object2XML(message,getDestinationName()));
			RMSLCDSServerListener.logAnyWhere(getDestinationName(),ReflectionHelper.object2XML(message.headers,getDestinationName()),false);
			RMSLCDSServerListener.logAnyWhere(getDestinationName(),ReflectionHelper.object2XML(message.body,getDestinationName()));
		}
		
	}
}