package com.cemex.rms.weborb.listener
{
	import com.cemex.rms.common.PushInfo;
	import com.cemex.rms.common.lcds.AbstractReceiverService;
	
	import mx.messaging.ChannelSet;
	import mx.messaging.messages.IMessage;
	
	public class ProductionStatusPIRID extends AbstractReceiverService
	{
		public function ProductionStatusPIRID()
		{
			super();
		}
		
		public override function getDestinationName():String{
			return "ProductionStatusPIRID";
		}
		public override function getChannelSet():ChannelSet{
			return servicesContext.getValue("ChannelSetWEBORB") as ChannelSet;
		}
		override protected function receiveIMessage(message:IMessage):void{		
			PushInfo.logObjectInfo(type,getDestinationName(),message);
		}
		
	}
}