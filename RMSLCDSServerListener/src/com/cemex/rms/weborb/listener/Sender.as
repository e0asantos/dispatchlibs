package com.cemex.rms.weborb.listener
{
	import com.cemex.rms.common.lcds.AbstractProducerService;
	
	import mx.messaging.ChannelSet;
	
	public class Sender extends AbstractProducerService
	{
		public function Sender()
		{
			super();
		}
		
		public override function getDestinationName():String{
			return "AbnormalConsumptionPI_WEBORB";
		}
		public override function getChannelSet():ChannelSet{
			return servicesContext.getValue("ChannelSetWEBORB") as ChannelSet;
		}
	}
}