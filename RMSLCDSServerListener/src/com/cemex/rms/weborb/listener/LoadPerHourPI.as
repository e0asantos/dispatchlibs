package com.cemex.rms.weborb.listener
{
	import com.cemex.rms.common.PushInfo;
	import com.cemex.rms.common.lcds.AbstractReceiverService;
	import com.cemex.rms.common.reflection.ReflectionHelper;
	
	import mx.messaging.ChannelSet;
	import mx.messaging.messages.IMessage;
	
	public class LoadPerHourPI extends AbstractReceiverService
	{
		public function LoadPerHourPI()
		{
			super();
		}
		
		public override function getDestinationName():String{
			return "LoadPerHourPI_SR";
		}
		public override function getChannelSet():ChannelSet{
			return servicesContext.getValue("ChannelSetWEBORB") as ChannelSet;
		}
		override protected function receiveIMessage(message:IMessage):void{
			RMSLCDSServerListener.logAnyWhere(getDestinationName(),ReflectionHelper.object2XML(message.body,getDestinationName()));
			//PushInfo.logObjectInfo(type,getDestinationName(),message);
		}
		
		
	}
}