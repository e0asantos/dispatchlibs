package com.cemex.rms.weborb.listener
{
	import com.cemex.rms.common.PushInfo;
	import com.cemex.rms.common.lcds.AbstractReceiverService;
	
	import mx.messaging.ChannelSet;
	import mx.messaging.messages.IMessage;
	
	public class RTMPDestination extends AbstractReceiverService
	{
		public function RTMPDestination()
		{
			super();
		}
		public override function getDestinationName():String{
			return "MultiPurposeRTMPDestination";
		}
		public override function getChannelSet():ChannelSet{
			
			return servicesContext.getValue("ChannelSetWEBORB") as ChannelSet;
		}
		override protected function receiveIMessage(message:IMessage):void{		
			PushInfo.logObjectInfo(type,getDestinationName(),message);
		}
	}
}