package com.cemex.rms.weborb.listener
{
	import com.cemex.rms.common.PushInfo;
	import com.cemex.rms.common.lcds.AbstractReceiverService;
	
	import mx.messaging.ChannelSet;
	import mx.messaging.messages.IMessage;
	
	public class TestPIRID extends AbstractReceiverService
	{
		public function TestPIRID()
		{
			super();
		}
		
		public override function getDestinationName():String{
			return "TestPIRID";
		}
		public override function getChannelSet():ChannelSet{
			
			return servicesContext.getValue("ChannelSetWEBORB") as ChannelSet;
		}
		override protected function receiveIMessage(message:IMessage):void{		
			PushInfo.logObjectInfo(type,getDestinationName(),message);
		}
	}
}