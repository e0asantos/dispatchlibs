package com.cemex.rms.weborb.listener
{
	import com.cemex.rms.common.PushInfo;
	import com.cemex.rms.common.lcds.AbstractReceiverService;
	import com.cemex.rms.common.reflection.ReflectionHelper;
	
	import mx.messaging.ChannelSet;
	import mx.messaging.messages.IMessage;
	
	public class SalesOrderPIRID extends AbstractReceiverService
	{
		public function SalesOrderPIRID()
		{
			super();
		}
		
		public override function getDestinationName():String{
			return "VehiclePIMR2URL";
		}
		public override function getChannelSet():ChannelSet{
			return servicesContext.getValue("ChannelSetWEBORB") as ChannelSet;
		}
		
		override protected function receiveIMessage(message:IMessage):void{		
			logger.debug(ReflectionHelper.object2XML(message,getDestinationName()));
		}
		
	}
}