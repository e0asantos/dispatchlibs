package com.cemex.rms.weborb.listener
{
	import com.cemex.rms.common.PushInfo;
	import com.cemex.rms.common.lcds.AbstractReceiverService;
	
	import mx.messaging.ChannelSet;
	import mx.messaging.messages.IMessage;
	
	public class AbnormalConsumptionPIRID extends AbstractReceiverService
	{
		public function AbnormalConsumptionPIRID()
		{
			super();
		}
		
		public override function getDestinationName():String{
			return "AbnormalConsumptionPIRID";
		}
		public override function getChannelSet():ChannelSet{
			return servicesContext.getValue("ChannelSetWEBORB") as ChannelSet;
		}
		override protected function receiveIMessage(message:IMessage):void{		
			PushInfo.logObjectInfo(type,getDestinationName(),message);
		}
		
	}
}