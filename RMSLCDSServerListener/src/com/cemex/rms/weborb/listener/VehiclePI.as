package com.cemex.rms.weborb.listener
{
	import com.cemex.rms.common.PushInfo;
	import com.cemex.rms.common.lcds.AbstractReceiverService;
	import com.cemex.rms.common.reflection.ReflectionHelper;
	
	import mx.messaging.ChannelSet;
	import mx.messaging.messages.IMessage;
	
	public class VehiclePI extends AbstractReceiverService
	{
		public function VehiclePI()
		{
			super();
		}
		
		public override function getDestinationName():String{
			return "VehiclePI_CR";
		}
		public override function getChannelSet():ChannelSet{
			return servicesContext.getValue("ChannelSetWEBORB") as ChannelSet;
		}
		override protected function receiveIMessage(message:IMessage):void{		
			//PushInfo.logObjectInfo(type,getDestinationName(),message);
			RMSLCDSServerListener.logAnyWhere(getDestinationName(),ReflectionHelper.object2XML(message.headers,getDestinationName()));
			RMSLCDSServerListener.logAnyWhere(getDestinationName(),ReflectionHelper.object2XML(message.body,getDestinationName()));
		}
	}
}