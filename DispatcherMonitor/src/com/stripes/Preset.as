﻿package com.stripes
{
    import com.ed.gfx.*;
    import flash.display.*;
    import flash.events.*;
    import flash.geom.*;

    public class Preset extends Sprite
    {
        private var c:Sprite;
        private var bg:Shape;
        private var m:Matrix;
        public var stripesArray:Array;
        private var stripe:Stripes;

        public function Preset() : void
        {
            stripesArray = [];
            if (stage)
            {
                init();
            }
            else
            {
                addEventListener(Event.ADDED_TO_STAGE, init, false, 0, true);
            }
            return;
        }// end function

        private function init(event:Event = null) : void
        {
            c = new Sprite();
            addChild(c);
            m = new Matrix();
            m.createGradientBox(stage.stageWidth, stage.stageHeight, 90 * Math.PI / 180, 0, 0);
            bg = new Shape();
            bg.graphics.beginGradientFill(GradientType.LINEAR, [10027263, 10027212], [1, 1], [0, 255], m, SpreadMethod.PAD);
            bg.graphics.drawRect(0, 0, stage.stageWidth, stage.stageHeight);
            bg.graphics.endFill();
            c.addChild(bg);
            stripe = new Stripes(1, 2, {a:0}, Stripes.RIGHT);
            stripe.draw(stage.stageWidth, stage.stageHeight);
            c.addChild(stripe);
            stripe = new Stripes(1, 5, {a:0}, Stripes.LEFT);
            stripe.draw(stage.stageWidth, stage.stageHeight);
            c.addChild(stripe);
            stripesArray.push(c);
            c = new Sprite();
            addChild(c);
            stripe = new Stripes(1, 5, {a:16777215}, Stripes.LEFT);
            stripe.draw(stage.stageWidth, stage.stageHeight);
            c.addChild(stripe);
            stripe = new Stripes(1, 1, {a:16711680}, Stripes.HORIZONTAL);
            stripe.draw(stage.stageWidth, stage.stageHeight);
            c.addChild(stripe);
            stripe = new Stripes(1, 1, {a:16711935}, Stripes.VERTICAL);
            stripe.draw(stage.stageWidth, stage.stageHeight);
            c.addChild(stripe);
            stripesArray.push(c);
            c = new Sprite();
            addChild(c);
            m = new Matrix();
            m.createGradientBox(stage.stageWidth, stage.stageHeight, 90 * Math.PI / 180, 0, 0);
            bg = new Shape();
            bg.graphics.beginGradientFill(GradientType.LINEAR, [26367, 0], [1, 1], [0, 255], m, SpreadMethod.PAD);
            bg.graphics.drawRect(0, 0, stage.stageWidth, stage.stageHeight);
            bg.graphics.endFill();
            c.addChild(bg);
            stripe = new Stripes(3, 4, {a:0}, Stripes.HORIZONTAL);
            stripe.draw(stage.stageWidth, stage.stageHeight);
            c.addChild(stripe);
            stripesArray.push(c);
            c = new Sprite();
            addChild(c);
            bg = new Shape();
            bg.graphics.beginFill(0);
            bg.graphics.drawRect(0, 0, stage.stageWidth, stage.stageHeight);
            bg.graphics.endFill();
            c.addChild(bg);
            stripe = new Stripes(1, 5, {a:16777215}, Stripes.RIGHT);
            stripe.draw(stage.stageWidth, stage.stageHeight);
            c.addChild(stripe);
            stripe = new Stripes(10, 50, {a:0}, Stripes.RIGHT);
            stripe.draw(stage.stageWidth, stage.stageHeight);
            c.addChild(stripe);
            stripe = new Stripes(1, 10, {a:16711680}, Stripes.RIGHT);
            stripe.draw(stage.stageWidth, stage.stageHeight);
            c.addChild(stripe);
            stripesArray.push(c);
            c = new Sprite();
            addChild(c);
            bg = new Shape();
            bg.graphics.beginFill(0);
            bg.graphics.drawRect(0, 0, stage.stageWidth, stage.stageHeight);
            bg.graphics.endFill();
            c.addChild(bg);
            stripe = new Stripes(1, 5, {a:6749952}, Stripes.HORIZONTAL);
            stripe.draw(stage.stageWidth, stage.stageHeight);
            stripe.alpha = 0.2;
            c.addChild(stripe);
            stripe = new Stripes(1, 5, {a:6749952}, Stripes.VERTICAL);
            stripe.draw(stage.stageWidth, stage.stageHeight);
            stripe.alpha = 0.2;
            c.addChild(stripe);
            stripesArray.push(c);
            c = new Sprite();
            addChild(c);
            bg = new Shape();
            bg.graphics.beginFill(16764159);
            bg.graphics.drawRect(0, 0, stage.stageWidth, stage.stageHeight);
            bg.graphics.endFill();
            c.addChild(bg);
            stripe = new Stripes(1, 15, {a:0}, Stripes.LEFT);
            stripe.draw(stage.stageWidth, stage.stageHeight);
            c.addChild(stripe);
            stripesArray.push(c);
            c = new Sprite();
            addChild(c);
            bg = new Shape();
            bg.graphics.beginFill(16764159);
            bg.graphics.drawRect(0, 0, stage.stageWidth, stage.stageHeight);
            bg.graphics.endFill();
            c.addChild(bg);
            stripe = new Stripes(1, 13, {a:0}, Stripes.LEFT);
            stripe.draw(stage.stageWidth, stage.stageHeight);
            c.addChild(stripe);
            stripe = new Stripes(1, 13, {a:0}, Stripes.RIGHT);
            stripe.draw(stage.stageWidth, stage.stageHeight);
            c.addChild(stripe);
            stripe = new Stripes(1, 1, {a:10027263}, Stripes.HORIZONTAL);
            stripe.draw(stage.stageWidth, stage.stageHeight);
            c.addChild(stripe);
            stripe = new Stripes(1, 1, {a:3342336}, Stripes.VERTICAL);
            stripe.draw(stage.stageWidth, stage.stageHeight);
            c.addChild(stripe);
            stripesArray.push(c);
            c = new Sprite();
            addChild(c);
            bg = new Shape();
            bg.graphics.beginFill(16764159);
            bg.graphics.drawRect(0, 0, stage.stageWidth, stage.stageHeight);
            bg.graphics.endFill();
            c.addChild(bg);
            stripe = new Stripes(1, 1, {a:0}, Stripes.LEFT);
            stripe.draw(stage.stageWidth, stage.stageHeight);
            c.addChild(stripe);
            stripesArray.push(c);
            c = new Sprite();
            addChild(c);
            m = new Matrix();
            m.createGradientBox(stage.stageWidth, stage.stageHeight, 90 * Math.PI / 180, 0, 0);
            bg = new Shape();
            bg.graphics.beginGradientFill(GradientType.LINEAR, [0, 0], [1, 1], [0, 255], m, SpreadMethod.PAD);
            bg.graphics.drawRect(0, 0, stage.stageWidth, stage.stageHeight);
            bg.graphics.endFill();
            c.addChild(bg);
            stripe = new Stripes(1, 5, {a:16777215}, Stripes.RIGHT);
            stripe.draw(stage.stageWidth, stage.stageHeight);
            c.addChild(stripe);
            stripe = new Stripes(10, 50, {a:0}, Stripes.LEFT);
            stripe.draw(stage.stageWidth, stage.stageHeight);
            c.addChild(stripe);
            stripe = new Stripes(1, 10, {a:16711680}, Stripes.LEFT);
            stripe.draw(stage.stageWidth, stage.stageHeight);
            c.addChild(stripe);
            stripesArray.push(c);
            c = new Sprite();
            addChild(c);
            m = new Matrix();
            m.createGradientBox(stage.stageWidth, stage.stageHeight, 90 * Math.PI / 180, 0, 0);
            bg = new Shape();
            bg.graphics.beginGradientFill(GradientType.LINEAR, [10027263, 10027212], [1, 1], [0, 255], m, SpreadMethod.PAD);
            bg.graphics.drawRect(0, 0, stage.stageWidth, stage.stageHeight);
            bg.graphics.endFill();
            c.addChild(bg);
            stripe = new Stripes(5, 5, {a:0}, Stripes.RIGHT);
            stripe.draw(stage.stageWidth, stage.stageHeight);
            c.addChild(stripe);
            stripe = new Stripes(12, 1, {a:0}, Stripes.LEFT);
            stripe.draw(stage.stageWidth, stage.stageHeight);
            c.addChild(stripe);
            stripesArray.push(c);
            c = new Sprite();
            addChild(c);
            bg = new Shape();
            bg.graphics.beginFill(16724991);
            bg.graphics.drawRect(0, 0, stage.stageWidth, stage.stageHeight);
            bg.graphics.endFill();
            c.addChild(bg);
            stripe = new Stripes(10, 20, {a:0}, Stripes.LEFT);
            stripe.draw(1120, 660);
            c.addChild(stripe);
            stripe = new Stripes(10, 20, {a:0}, Stripes.RIGHT);
            stripe.draw(1120, 660);
            c.addChild(stripe);
            stripe = new Stripes(6, 1, {a:0}, Stripes.HORIZONTAL);
            stripe.draw(1120, 660);
            c.addChild(stripe);
            stripesArray.push(c);
            c = new Sprite();
            addChild(c);
            m = new Matrix();
            m.createGradientBox(stage.stageWidth, stage.stageHeight, 90 * Math.PI / 180, 0, 0);
            bg = new Shape();
            bg.graphics.beginGradientFill(GradientType.LINEAR, [13311, 0], [1, 1], [0, 255], m, SpreadMethod.PAD);
            bg.graphics.drawRect(0, 0, stage.stageWidth, stage.stageHeight);
            bg.graphics.endFill();
            c.addChild(bg);
            stripe = new Stripes(10, 1, {a:0, b:13311}, Stripes.HORIZONTAL);
            stripe.draw(1220, 620);
            stripe.alpha = 0.4;
            c.addChild(stripe);
            stripe = new Stripes(1, 1, {a:10066380, b:0}, Stripes.VERTICAL);
            stripe.draw(1240, 620);
            c.addChild(stripe);
            stripe = new Stripes(9, 19, {a:10092543, b:13311}, Stripes.LEFT);
            stripe.draw(1220, 620);
            stripe.alpha = 0.2;
            c.addChild(stripe);
            stripesArray.push(c);
            c = new Sprite();
            addChild(c);
            m = new Matrix();
            m.createGradientBox(stage.stageWidth, stage.stageHeight, 90 * Math.PI / 180, 0, 0);
            bg = new Shape();
            bg.graphics.beginGradientFill(GradientType.LINEAR, [13311, 0], [1, 1], [0, 255], m, SpreadMethod.PAD);
            bg.graphics.drawRect(0, 0, stage.stageWidth, stage.stageHeight);
            bg.graphics.endFill();
            c.addChild(bg);
            stripe = new Stripes(10, 1, {a:0, b:13311}, Stripes.HORIZONTAL);
            stripe.draw(1220, 620);
            stripe.alpha = 0.4;
            c.addChild(stripe);
            stripe = new Stripes(1, 1, {a:10066380, b:0}, Stripes.VERTICAL);
            stripe.draw(1240, 620);
            c.addChild(stripe);
            stripe = new Stripes(20, 20, {a:0}, Stripes.LEFT);
            stripe.draw(1220, 620);
            stripe.alpha = 0.2;
            c.addChild(stripe);
            stripesArray.push(c);
            c = new Sprite();
            addChild(c);
            m = new Matrix();
            m.createGradientBox(stage.stageWidth, stage.stageHeight, 90 * Math.PI / 180, 0, 0);
            bg = new Shape();
            bg.graphics.beginGradientFill(GradientType.LINEAR, [13311, 0], [1, 1], [0, 255], m, SpreadMethod.PAD);
            bg.graphics.drawRect(0, 0, stage.stageWidth, stage.stageHeight);
            bg.graphics.endFill();
            c.addChild(bg);
            stripe = new Stripes(10, 1, {a:0, b:13311}, Stripes.HORIZONTAL);
            stripe.draw(1220, 620);
            stripe.alpha = 0.4;
            c.addChild(stripe);
            stripe = new Stripes(20, 20, {a:0}, Stripes.LEFT);
            stripe.draw(1220, 620);
            stripe.alpha = 0.2;
            c.addChild(stripe);
            stripe = new Stripes(1, 1, {a:0}, Stripes.RIGHT);
            stripe.draw(1220, 620);
            c.addChild(stripe);
            stripe = new Stripes(1, 1, {a:10027008, b:10027263}, Stripes.HORIZONTAL);
            stripe.draw(1220, 620);
            stripe.alpha = 0.4;
            c.addChild(stripe);
            stripesArray.push(c);
            c = new Sprite();
            addChild(c);
            m = new Matrix();
            m.createGradientBox(stage.stageWidth, stage.stageHeight, 90 * Math.PI / 180, 0, 0);
            bg = new Shape();
            bg.graphics.beginGradientFill(GradientType.LINEAR, [13311, 0], [1, 1], [0, 255], m, SpreadMethod.PAD);
            bg.graphics.drawRect(0, 0, stage.stageWidth, stage.stageHeight);
            bg.graphics.endFill();
            c.addChild(bg);
            stripe = new Stripes(2, 2, {a:13311, b:51}, Stripes.HORIZONTAL);
            stripe.draw(1220, 620);
            stripe.alpha = 0.3;
            c.addChild(stripe);
            stripe = new Stripes(2, 1, {a:3154470, b:7565166}, Stripes.VERTICAL);
            stripe.draw(1220, 620);
            stripe.alpha = 0.7;
            c.addChild(stripe);
            stripe = new Stripes(1, 2, {a:13311, b:102}, Stripes.LEFT);
            stripe.draw(1220, 620);
            c.addChild(stripe);
            stripe = new Stripes(1, 1, {a:13311, b:51}, Stripes.LEFT);
            stripe.draw(1220, 620);
            stripe.alpha = 0;
            c.addChild(stripe);
            stripe = new Stripes(21, 1, {a:51, b:0}, Stripes.LEFT);
            stripe.draw(1220, 620);
            stripe.alpha = 0.5;
            c.addChild(stripe);
            stripesArray.push(c);
            c = new Sprite();
            addChild(c);
            bg = new Shape();
            bg.graphics.beginFill(0);
            bg.graphics.drawRect(0, 0, stage.stageWidth, stage.stageHeight);
            bg.graphics.endFill();
            c.addChild(bg);
            stripe = new Stripes(17, 1, {a:10027008, b:3342336}, Stripes.LEFT);
            stripe.draw(1040, 720);
            c.addChild(stripe);
            stripesArray.push(c);
            c = new Sprite();
            addChild(c);
            m = new Matrix();
            m.createGradientBox(stage.stageWidth, stage.stageHeight, 90 * Math.PI / 180, 0, 0);
            bg = new Shape();
            bg.graphics.beginGradientFill(GradientType.LINEAR, [3342489, 10027059], [1, 1], [0, 255], m, SpreadMethod.PAD);
            bg.graphics.drawRect(0, 0, stage.stageWidth, stage.stageHeight);
            bg.graphics.endFill();
            c.addChild(bg);
            stripe = new Stripes(2, 27, {a:0}, Stripes.LEFT);
            stripe.draw(1120, 660);
            c.addChild(stripe);
            stripe = new Stripes(2, 20, {a:0}, Stripes.VERTICAL);
            stripe.draw(1120, 660);
            c.addChild(stripe);
            stripesArray.push(c);
            m = new Matrix();
            m.createGradientBox(stage.stageWidth, stage.stageHeight, 90 * Math.PI / 180, 0, 0);
            bg = new Shape();
            bg.graphics.beginGradientFill(GradientType.LINEAR, [16764159, 10053222], [1, 1], [0, 255], m, SpreadMethod.PAD);
            bg.graphics.drawRect(0, 0, stage.stageWidth, stage.stageHeight);
            bg.graphics.endFill();
            c.addChild(bg);
            stripe = new Stripes(1, 1, {a:0}, Stripes.HORIZONTAL);
            stripe.draw(1180, 680);
            c.addChild(stripe);
            stripe = new Stripes(22, 13, {a:0}, Stripes.LEFT);
            stripe.draw(1180, 680);
            c.addChild(stripe);
            stripe = new Stripes(1, 1, {a:6697881}, Stripes.VERTICAL);
            stripe.draw(1180, 740);
            stripe.alpha = 0.3;
            c.addChild(stripe);
            c = new Sprite();
            addChild(c);
            bg = new Shape();
            bg.graphics.beginFill(6684723);
            bg.graphics.drawRect(0, 0, stage.stageWidth, stage.stageHeight);
            bg.graphics.endFill();
            c.addChild(bg);
            stripe = new Stripes(10, 50, {a:0}, Stripes.LEFT);
            stripe.draw(1120, 660);
            c.addChild(stripe);
            stripe = new Stripes(10, 50, {a:3342438}, Stripes.LEFT);
            stripe.draw(1140, 660);
            stripe.x = -35;
            c.addChild(stripe);
            stripesArray.push(c);
            c = new Sprite();
            addChild(c);
            m = new Matrix();
            m.createGradientBox(stage.stageWidth, stage.stageHeight, 90 * Math.PI / 180, 0, 0);
            bg = new Shape();
            bg.graphics.beginGradientFill(GradientType.LINEAR, [16764159, 10053222], [1, 1], [0, 255], m, SpreadMethod.PAD);
            bg.graphics.drawRect(0, 0, stage.stageWidth, stage.stageHeight);
            bg.graphics.endFill();
            c.addChild(bg);
            stripe = new Stripes(10, 70, {a:0}, Stripes.LEFT);
            stripe.draw(1180, 680);
            c.addChild(stripe);
            stripe = new Stripes(10, 70, {a:10027161}, Stripes.LEFT);
            stripe.draw(1220, 680);
            stripe.x = -46;
            c.addChild(stripe);
            stripesArray.push(c);
            hideAll();
            return;
        }// end function

        public function showStripe(param1:int) : void
        {
            param1 = param1 - 1;
            var _loc_2:uint = 0;
            while (_loc_2 < stripesArray.length)
            {
                
                if (_loc_2 == param1)
                {
                    stripesArray[_loc_2].visible = true;
                }
                else
                {
                    stripesArray[_loc_2].visible = false;
                }
                _loc_2 = _loc_2 + 1;
            }
            return;
        }// end function

        public function resize() : void
        {
            var _loc_2:uint = 0;
            var _loc_1:uint = 0;
            while (_loc_1 < stripesArray.length)
            {
                
                _loc_2 = 0;
                while (_loc_2 < stripesArray[_loc_1].numChildren)
                {
                    
                    if (stripesArray[_loc_1].getChildAt(_loc_2) is Stripes)
                    {
                        Stripes(stripesArray[_loc_1].getChildAt(_loc_2)).draw(stage.stageWidth + 100, stage.stageHeight + 100);
                    }
                    else
                    {
                        Shape(stripesArray[_loc_1].getChildAt(_loc_2)).width = stage.stageWidth;
                        Shape(stripesArray[_loc_1].getChildAt(_loc_2)).height = stage.stageHeight;
                    }
                    _loc_2 = _loc_2 + 1;
                }
                _loc_1 = _loc_1 + 1;
            }
            this.parent.addChild(this);
            return;
        }// end function

        public function hideAll() : void
        {
            var _loc_1:uint = 0;
            while (_loc_1 < stripesArray.length)
            {
                
                stripesArray[_loc_1].visible = false;
                _loc_1 = _loc_1 + 1;
            }
            return;
        }// end function

    }
}
