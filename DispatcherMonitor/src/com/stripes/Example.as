﻿package com.stripes
{
    import flash.display.*;
    import flash.events.*;
    import flash.text.*;

    public class Example extends Sprite
    {
        private var c:Sprite;
        public var _bg:MovieClip;
        public var bg:Sprite;
        private var btn:Btn;
        private var btnArray:Array;

        public function Example(param1:int) : void
        {
            btnArray = [];
            bg = Sprite(this.getChildByName("_bg"));
            c = new Sprite();
            addChild(c);
            var _loc_2:uint = 0;
            while (_loc_2 < param1)
            {
                
                btn = new Btn();
                btn.num.text = ((_loc_2 + 1)).toString();
                btn.num.autoSize = TextFieldAutoSize.LEFT;
                btn.x = _loc_2 * 20;
                btn.mouseChildren = false;
                btn.buttonMode = true;
                c.addChild(btn);
                btnArray.push(btn);
                btn.addEventListener(MouseEvent.CLICK, btnClicked);
                _loc_2 = _loc_2 + 1;
            }
            c.x = 70;
            c.y = 5;
            return;
        }// end function

        private function btnClicked(event:MouseEvent) : void
        {
            dispatchEvent(new ExampleEvent(ExampleEvent.CHANGED, (btnArray.indexOf(event.target) + 1)));
            return;
        }// end function

    }
}
