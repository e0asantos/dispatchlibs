﻿package com.stripes
{
    import fl.controls.*;
    import flash.display.*;
    import flash.events.*;
    import flash.geom.*;

    public class MainComponent extends Sprite
    {
        private var matrix:Matrix;
        public var _cb:CheckBox;
        private var bg:Sprite;
        private var ySpace:uint = 88;
        private var grad:Shape;
        public var comp:Sprite;
        public var colorPicker:ColorPicker;
        private var msk:Shape;
        public var addInstance:Sprite;
        public var _addInstance:MovieClip;
        public var cb:CheckBox;
        public var _hide:MovieClip;
        public var _bg:MovieClip;
        public var colorPickerContainer:Sprite;
        public var _asset:MovieClip;
        public var bgString:String = "//DRAWS THE BACKGROUND\nvar bg:Shape = new Shape();\nbg.graphics.beginFill(0xffccff);\nbg.graphics.drawRect(0, 0, stage.stageWidth, stage.stageHeight);\nbg.graphics.endFill();\naddChild(bg);";
        private var asset:Sprite;
        private var isGradientOn:Boolean = false;
        public var hideBtn:Sprite;
        public var bgContainer:Sprite;
        public static const SET_POS:String = "setPos";

        public function MainComponent() : void
        {
            bg = Sprite(this.getChildByName("_bg"));
            asset = Sprite(this.getChildByName("_asset"));
            hideBtn = Sprite(this.getChildByName("_hide"));
            addInstance = Sprite(this.getChildByName("_addInstance"));
            addInstance.addEventListener(MouseEvent.CLICK, addInstanceClick);
            addInstance.buttonMode = true;
            cb = CheckBox(this.getChildByName("_cb"));
            cb.addEventListener(Event.CHANGE, gradientSelected);
            cb.addEventListener(Event.CHANGE, drawStripes);
            bgContainer = new Sprite();
            addChild(bgContainer);
            comp = new Sprite();
            addChild(comp);
            hideBtn.getChildByName("_unhide").visible = false;
            hideBtn.addEventListener(MouseEvent.CLICK, hideClick);
            hideBtn.buttonMode = true;
            if (stage)
            {
                init();
            }
            else
            {
                addEventListener(Event.ADDED_TO_STAGE, init, false, 0, true);
            }
            __setProp__cb_main_Layer2_0();
            return;
        }// end function

        public function drawStripes(event:Event = null) : void
        {
            var _loc_2:uint = 0;
            var _loc_3:uint = 0;
            if (isGradientOn == true)
            {
                ColorPicker(Sprite(colorPickerContainer).getChildByName("b")).visible = true;
                _loc_2 = ColorPicker(Sprite(colorPickerContainer).getChildByName("a")).selectedColor;
                _loc_3 = ColorPicker(Sprite(colorPickerContainer).getChildByName("b")).selectedColor;
            }
            else
            {
                var _loc_4:* = ColorPicker(Sprite(colorPickerContainer).getChildByName("a")).selectedColor;
                _loc_2 = ColorPicker(Sprite(colorPickerContainer).getChildByName("a")).selectedColor;
                var _loc_4:* = _loc_4;
                _loc_3 = _loc_4;
                _loc_2 = _loc_4;
                ColorPicker(Sprite(colorPickerContainer).getChildByName("b")).visible = false;
            }
            drawBG(_loc_2, _loc_3);
            return;
        }// end function

        private function init(event:Event = null) : void
        {
            removeEventListener(Event.ADDED_TO_STAGE, init);
            drawColorPicker();
            drawMask();
            return;
        }// end function

        private function addInstanceClick(event:MouseEvent) : void
        {
            Object(this.parent).createComponent();
            return;
        }// end function

        private function drawBG(param1:uint, param2:uint) : void
        {
            matrix = new Matrix();
            var _loc_3:int = 90;
            matrix.createGradientBox(stage.stageWidth, stage.stageHeight, _loc_3 * Math.PI / 180, 0, 0);
            grad = new Shape();
            grad.graphics.beginGradientFill(GradientType.LINEAR, [param1, param2], [1, 1], [0, 255], matrix, SpreadMethod.PAD);
            grad.graphics.drawRect(0, 0, stage.stageWidth, stage.stageHeight);
            grad.graphics.endFill();
            bgContainer.addChild(grad);
            if (isGradientOn == true)
            {
                bgString = "//DRAWS THE BACKGROUND\nvar m:Matrix = new Matrix();\nm.createGradientBox(stage.stageWidth, stage.stageHeight, (90 * Math.PI / 180), 0, 0);\nvar bg:Shape = new Shape();\nbg.graphics.beginGradientFill(GradientType.LINEAR, [0x" + ColorPicker(Sprite(colorPickerContainer).getChildByName("a")).hexValue + ", 0x" + ColorPicker(Sprite(colorPickerContainer).getChildByName("b")).hexValue + "]" + ", [1, 1] , [0x00, 0xFF], m, SpreadMethod.PAD);\nbg.graphics.drawRect(0, 0, stage.stageWidth, stage.stageHeight);\nbg.graphics.endFill();\naddChild(bg);";
            }
            else
            {
                bgString = "//DRAWS THE BACKGROUND\nvar bg:Shape = new Shape();\nbg.graphics.beginFill(0x" + ColorPicker(Sprite(colorPickerContainer).getChildByName("a")).hexValue + ");\nbg.graphics.drawRect(0, 0, stage.stageWidth, stage.stageHeight);\nbg.graphics.endFill();\naddChild(bg);";
            }
            if (bgContainer.numChildren > 1)
            {
                bgContainer.removeChild(bgContainer.getChildAt(0));
            }
            Object(this.parent).addCode();
            return;
        }// end function

        function __setProp__cb_main_Layer2_0()
        {
            try
            {
                _cb["componentInspectorSetting"] = true;
            }
            catch (e:Error)
            {
            }
            _cb.enabled = true;
            _cb.label = "";
            _cb.labelPlacement = "right";
            _cb.selected = false;
            _cb.visible = true;
            try
            {
                _cb["componentInspectorSetting"] = false;
            }
            catch (e:Error)
            {
            }
            return;
        }// end function

        private function hideClick(event:MouseEvent) : void
        {
            if (hideBtn.getChildByName("_unhide").visible == false)
            {
                hideBtn.getChildByName("_unhide").visible = true;
                hideBtn.getChildByName("_hide").visible = false;
                msk.height = 30;
            }
            else
            {
                msk.height = bg.height;
                hideBtn.getChildByName("_unhide").visible = false;
                hideBtn.getChildByName("_hide").visible = true;
            }
            dispatchEvent(new Event(SET_POS));
            return;
        }// end function

        private function drawMask() : void
        {
            msk = new Shape();
            msk.graphics.beginFill(16711935, 1);
            msk.graphics.drawRect(0, 0, 182, bg.height);
            msk.graphics.endFill();
            addChild(msk);
            comp.mask = msk;
            return;
        }// end function

        private function gradientSelected(event:Event) : void
        {
            isGradientOn = event.target.selected;
            return;
        }// end function

        private function drawColorPicker() : void
        {
            colorPickerContainer = new Sprite();
            colorPicker = new ColorPicker();
            colorPicker.selectedColor = 16764159;
            colorPicker.addEventListener(Event.CHANGE, drawStripes, false, 0, true);
            colorPickerContainer.addChild(colorPicker);
            colorPicker.name = "a";
            colorPicker = new ColorPicker();
            colorPickerContainer.addChild(colorPicker);
            colorPicker.addEventListener(Event.CHANGE, drawStripes, false, 0, true);
            colorPicker.name = "b";
            colorPicker.x = 60;
            if (cb.selected == false)
            {
                colorPicker.visible = false;
            }
            comp.addChild(bg);
            comp.addChild(colorPickerContainer);
            comp.addChild(hideBtn);
            comp.addChild(addInstance);
            comp.addChild(asset);
            comp.addChild(cb);
            colorPickerContainer.x = 13;
            colorPickerContainer.y = ySpace;
            ySpace = ySpace + (colorPicker.height + 10);
            return;
        }// end function

    }
}
