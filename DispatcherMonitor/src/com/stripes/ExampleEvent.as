﻿package com.stripes
{
    import flash.events.*;

    public class ExampleEvent extends Event
    {
        public var i:int;
        public static const CHANGED:String = "changed";

        public function ExampleEvent(param1:String, param2:int) : void
        {
            super(param1);
            this.i = param2;
            return;
        }// end function

    }
}
