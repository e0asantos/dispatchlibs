﻿package com.stripes
{
    import com.ed.gfx.*;
    import fl.controls.*;
    import flash.display.*;
    import flash.events.*;

    public class StripeComponent extends Sprite
    {
        private var stripeWidth:NumericStepper;
        private var msk:Shape;
        private var ySpace:int = 270;
        public var comp:Sprite;
        private var sWidth:NumericStepper;
        private var stripeX:NumericStepper;
        private var stripeY:NumericStepper;
        private var colorPickerContainer:Sprite;
        private var deleteBtn:Sprite;
        private var _stripeHeight:uint;
        private var _stripeWidth:uint;
        private var gradCB:CheckBox;
        private var hideBtn:Sprite;
        public var _con:MovieClip;
        private var bg:Sprite;
        private var alphaNum:NumericStepper;
        private var stripe:Stripes;
        private var colorPicker:ColorPicker;
        private var sSpace:NumericStepper;
        private var stripeHeight:NumericStepper;
        private var orientation:ComboBox;
        public var stripeCode:String;
        public var s:Sprite;
        private var isGradientOn:Boolean = false;
        public static const REMOVE_STRIPE:String = "removeStripe";

        public function StripeComponent() : void
        {
            comp = Sprite(this.getChildByName("_con"));
            hideBtn = Sprite(comp.getChildByName("_hideBtn"));
            bg = Sprite(comp.getChildByName("_bg"));
            deleteBtn = Sprite(comp.getChildByName("_delete"));
            alphaNum = NumericStepper(comp.getChildByName("_alphaNum"));
            sWidth = NumericStepper(comp.getChildByName("_sWidth"));
            sSpace = NumericStepper(comp.getChildByName("_sSpace"));
            stripeX = NumericStepper(comp.getChildByName("_stripeX"));
            stripeY = NumericStepper(comp.getChildByName("_stripeY"));
            stripeWidth = NumericStepper(comp.getChildByName("_stripeWidth"));
            stripeHeight = NumericStepper(comp.getChildByName("_stripeHeight"));
            orientation = ComboBox(comp.getChildByName("_combo"));
            gradCB = CheckBox(comp.getChildByName("_gradCB"));
            if (stage)
            {
                init();
            }
            else
            {
                addEventListener(Event.ADDED_TO_STAGE, init, false, 0, true);
            }
            return;
        }// end function

        public function setDisplay() : void
        {
            addChild(comp);
            return;
        }// end function

        public function resizeStripes() : void
        {
            stripe.draw(stage.stageWidth, stage.stageHeight);
            stripeWidth.value = stage.stageWidth;
            stripeHeight.value = stage.stageHeight;
            return;
        }// end function

        public function removeCompDisplay() : void
        {
            this.parent.removeChild(comp);
            return;
        }// end function

        public function init(event:Event = null) : void
        {
            hideBtn.getChildByName("_unhide").visible = false;
            hideBtn.addEventListener(MouseEvent.CLICK, hideClick);
            hideBtn.buttonMode = true;
            deleteBtn.addEventListener(MouseEvent.CLICK, deleteClick);
            deleteBtn.buttonMode = true;
            removeEventListener(Event.ADDED_TO_STAGE, init);
            drawMask();
            createContainer();
            createComponents();
            addColor();
            createStripe(20, 20, Stripes.LEFT);
            return;
        }// end function

        private function deleteClick(event:MouseEvent) : void
        {
            dispatchEvent(new Event(REMOVE_STRIPE));
            return;
        }// end function

        private function createComponents() : void
        {
            sWidth.stepSize = 1;
            sWidth.minimum = 1;
            sWidth.maximum = 1000;
            sWidth.value = 20;
            sSpace.stepSize = 1;
            sSpace.minimum = 1;
            sSpace.maximum = 1000;
            sSpace.value = 20;
            stripeX.stepSize = 1;
            stripeX.minimum = -3000;
            stripeX.maximum = 3000;
            stripeX.value = 0;
            stripeY.stepSize = 1;
            stripeY.minimum = -3000;
            stripeY.maximum = 3000;
            stripeY.value = 0;
            stripeWidth.stepSize = 20;
            stripeWidth.minimum = 1;
            stripeWidth.maximum = 3000;
            stripeWidth.value = stage.stageWidth;
            stripeHeight.stepSize = 20;
            stripeHeight.minimum = 1;
            stripeHeight.maximum = 3000;
            stripeHeight.value = stage.stageHeight;
            orientation.addItem({label:"Left", val:"left"});
            orientation.addItem({label:"Right", val:"right"});
            orientation.addItem({label:"Horizontal", val:"horizontal"});
            orientation.addItem({label:"Vertical", val:"vertical"});
            sWidth.addEventListener(Event.CHANGE, drawStripes);
            sSpace.addEventListener(Event.CHANGE, drawStripes);
            stripeX.addEventListener(Event.CHANGE, drawStripes);
            stripeY.addEventListener(Event.CHANGE, drawStripes);
            stripeWidth.addEventListener(Event.CHANGE, drawStripes);
            stripeHeight.addEventListener(Event.CHANGE, drawStripes);
            orientation.addEventListener(Event.CHANGE, drawStripes);
            gradCB.addEventListener(Event.CHANGE, gradientSelected);
            alphaNum.addEventListener(Event.CHANGE, drawStripes);
            return;
        }// end function

        private function addColor(event:MouseEvent = null) : void
        {
            colorPickerContainer = new Sprite();
            colorPicker = new ColorPicker();
            colorPicker.addEventListener(Event.CHANGE, drawStripes, false, 0, true);
            colorPickerContainer.addChild(colorPicker);
            colorPicker.name = "a";
            colorPicker = new ColorPicker();
            colorPickerContainer.addChild(colorPicker);
            colorPicker.addEventListener(Event.CHANGE, drawStripes, false, 0, true);
            colorPicker.name = "b";
            colorPicker.x = 60;
            if (gradCB.selected == false)
            {
                colorPicker.visible = false;
            }
            comp.addChild(colorPickerContainer);
            colorPickerContainer.x = 13;
            colorPickerContainer.y = ySpace;
            ySpace = ySpace + (colorPicker.height + 10);
            return;
        }// end function

        private function drawMask() : void
        {
            msk = new Shape();
            msk.graphics.beginFill(16711935, 1);
            msk.graphics.drawRect(0, 0, 182, bg.height);
            msk.graphics.endFill();
            return;
        }// end function

        private function createContainer() : void
        {
            _stripeWidth = stage.stageWidth;
            _stripeHeight = stage.stageHeight;
            s = new Sprite();
            addChild(s);
            addChild(comp);
            comp.addChild(msk);
            comp.mask = msk;
            return;
        }// end function

        private function drawStripes(event:Event) : void
        {
            _stripeWidth = stripeWidth.value;
            _stripeHeight = stripeHeight.value;
            createStripe(sWidth.value, sSpace.value, orientation.selectedItem.val);
            return;
        }// end function

        private function gradientSelected(event:Event) : void
        {
            isGradientOn = event.target.selected;
            if (event.target.selected == true)
            {
                ColorPicker(Sprite(colorPickerContainer).getChildByName("b")).visible = true;
            }
            else
            {
                ColorPicker(Sprite(colorPickerContainer).getChildByName("b")).visible = false;
            }
            createStripe(sWidth.value, sSpace.value, orientation.selectedItem.val);
            return;
        }// end function

        private function createStripe(param1:int, param2:int, param3:String) : void
        {
            var _loc_4:uint = 0;
            var _loc_5:uint = 0;
            if (isGradientOn == true)
            {
                _loc_4 = ColorPicker(Sprite(colorPickerContainer).getChildByName("a")).selectedColor;
                _loc_5 = ColorPicker(Sprite(colorPickerContainer).getChildByName("b")).selectedColor;
            }
            else
            {
                var _loc_6:* = ColorPicker(Sprite(colorPickerContainer).getChildByName("a")).selectedColor;
                _loc_4 = ColorPicker(Sprite(colorPickerContainer).getChildByName("a")).selectedColor;
                var _loc_6:* = _loc_6;
                _loc_5 = _loc_6;
                _loc_4 = _loc_6;
            }
            stripe = new Stripes(param1, param2, {a:_loc_4, b:_loc_5}, param3);
            stripe.draw(_stripeWidth + 20, _stripeHeight + 20);
            s.addChild(stripe);
            s.x = stripeX.value;
            s.y = stripeY.value;
            s.alpha = alphaNum.value;
            if (isGradientOn == false)
            {
                stripeCode = "stripe = new Stripes(" + param1 + ", " + param2 + ", " + "{a:0x" + ColorPicker(Sprite(colorPickerContainer).getChildByName("a")).hexValue + "}, " + orientFun(param3) + ");";
            }
            else
            {
                stripeCode = "stripe = new Stripes(" + param1 + ", " + param2 + ", " + "{a:0x" + ColorPicker(Sprite(colorPickerContainer).getChildByName("a")).hexValue + ", b:0x" + ColorPicker(Sprite(colorPickerContainer).getChildByName("b")).hexValue + "}, " + orientFun(param3) + ");";
            }
            stripeCode = stripeCode + ("\nstripe.draw(" + _stripeWidth + ", " + _stripeHeight + ");");
            if (s.x != 0)
            {
                stripeCode = stripeCode + ("\nstripe.x = " + s.x + ";");
            }
            if (s.y != 0)
            {
                stripeCode = stripeCode + ("\nstripe.y = " + s.y + ";");
            }
            if (s.alpha != 1)
            {
                stripeCode = stripeCode + ("\nstripe.alpha = " + Math.round(s.alpha * 100) / 100 + ";");
            }
            stripeCode = stripeCode + "\naddChild(stripe);";
            if (s.numChildren > 1)
            {
                s.removeChild(s.getChildAt(0));
            }
            Object(this.parent).addCode();
            return;
        }// end function

        private function hideClick(event:MouseEvent) : void
        {
            if (hideBtn.getChildByName("_unhide").visible == false)
            {
                hideBtn.getChildByName("_unhide").visible = true;
                hideBtn.getChildByName("_hide").visible = false;
                msk.height = 30;
            }
            else
            {
                msk.height = bg.height;
                hideBtn.getChildByName("_unhide").visible = false;
                hideBtn.getChildByName("_hide").visible = true;
            }
            return;
        }// end function

        private function orientFun(param1:String) : String
        {
            var _loc_2:String = null;
            switch(param1)
            {
                case "horizontal":
                {
                    _loc_2 = "Stripes.HORIZONTAL";
                    break;
                }
                case "vertical":
                {
                    _loc_2 = "Stripes.VERTICAL";
                    break;
                }
                case "right":
                {
                    _loc_2 = "Stripes.RIGHT";
                    break;
                }
                case "left":
                {
                    _loc_2 = "Stripes.LEFT";
                    break;
                }
                default:
                {
                    break;
                }
            }
            return _loc_2;
        }// end function

    }
}
