﻿package com.ed.gfx
{
    import flash.display.*;

    public class Stripes extends Sprite
    {
        private var orientation:String;
        private var bd:BitmapData;
        private var s:Stripe;
        private var t:Sprite;
        public static const HORIZONTAL:String = "horizontal";
        public static const VERTICAL:String = "vertical";
        public static const LEFT:String = "left";
        public static const RIGHT:String = "right";

        public function Stripes(param1:int, param2:int, param3:Object = null, param4:String = "left") : void
        {
            var _loc_5:int = 0;
            var _loc_6:int = 0;
            this.orientation = param4;
            if (param4 == "left" || param4 == "right")
            {
                param2 = param1 * 2 + param2;
            }
            s = new Stripe(param1, param2, param3, param4);
            if (param4 == "horizontal")
            {
                _loc_5 = param2;
                _loc_6 = param2 + param1;
            }
            else if (param4 == "vertical")
            {
                _loc_5 = param2 + param1;
                _loc_6 = param2;
            }
            else
            {
                _loc_5 = param2;
                _loc_6 = param2;
            }
            bd = new BitmapData(_loc_5, _loc_6, true, 0);
            bd.draw(s);
            return;
        }// end function

        public function draw(param1:Number, param2:Number) : void
        {
            var _loc_3:* = t;
            t = new Sprite();
            t.graphics.beginBitmapFill(bd);
            t.graphics.drawRect(0, 0, param1, param2);
            t.graphics.endFill();
            addChild(t);
            if (_loc_3 != null && _loc_3 != t)
            {
                removeChild(_loc_3);
                _loc_3 = null;
            }
            if (orientation == "right")
            {
            }
            return;
        }// end function

    }
}
