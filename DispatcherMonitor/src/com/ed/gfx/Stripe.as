﻿package com.ed.gfx
{
    import flash.display.*;
    import flash.geom.*;

    public class Stripe extends Shape
    {
        private var space:int;
        private var orientation:String;
        private var colorO:Object;
        private var gradientBoxMatrix:Matrix;
        private var a:int;
        private var b:int;
        private var sw:int;
        private var colorA:uint;
        private var colorB:uint;

        public function Stripe(param1:int, param2:int, param3:Object = null, param4:String = "LEFT") : void
        {
            colorO = {a:uint, b:uint};
            sw = param1;
            this.space = param2;
            this.colorO = param3;
            this.orientation = param4;
            init();
            return;
        }// end function

        private function gradientVertical() : void
        {
            gradientBoxMatrix = new Matrix();
            gradientBoxMatrix.createGradientBox(sw, space, Math.PI, 0, 0);
            this.graphics.beginGradientFill(GradientType.LINEAR, [colorA, colorB], [1, 1], [0, 255], gradientBoxMatrix);
            this.graphics.drawRect(0, 0, sw, space);
            return;
        }// end function

        private function linearLeft() : void
        {
            var _loc_1:int = 0;
            while (_loc_1 < space)
            {
                
                this.graphics.beginFill(colorA);
                this.graphics.drawRect(_loc_1, _loc_1, sw, sw);
                _loc_1++;
            }
            a = space - sw;
            var _loc_2:int = 0;
            while (_loc_2 < sw)
            {
                
                var _loc_5:* = a + 1;
                a = _loc_5;
                this.graphics.beginFill(colorA);
                this.graphics.drawRect(0, a, (_loc_2 + 1), (_loc_2 + 1));
                _loc_2++;
            }
            a = space - sw;
            var _loc_3:int = 0;
            while (_loc_3 < sw)
            {
                
                var _loc_5:* = a + 1;
                a = _loc_5;
                this.graphics.beginFill(colorA);
                this.graphics.drawRect(a, 0, (_loc_3 + 1), (_loc_3 + 1));
                _loc_3++;
            }
            return;
        }// end function

        private function init() : void
        {
            if (colorO.a != null && colorO.b != null)
            {
                colorA = colorO.a;
                colorB = colorO.b;
                gradient();
            }
            else if (colorO.a != null && colorO.b == null)
            {
                colorA = colorO.a;
                linear();
            }
            else if (colorO.b != null && colorO.a == null)
            {
                colorA = colorO.b;
                linear();
            }
            else
            {
                colorA = 0;
                linear();
            }
            return;
        }// end function

        private function gradientLeft() : void
        {
            var _loc_1:int = 0;
            while (_loc_1 < space)
            {
                
                gradientBoxMatrix = new Matrix();
                gradientBoxMatrix.createGradientBox(sw, sw, (-Math.PI) / 4, 0, 0);
                this.graphics.beginGradientFill(GradientType.LINEAR, [colorA, colorB], [1, 1], [0, 255], gradientBoxMatrix);
                this.graphics.drawRect(_loc_1, _loc_1, sw, sw);
                _loc_1++;
            }
            a = space - sw;
            b = -sw;
            var _loc_2:int = 0;
            while (_loc_2 < sw)
            {
                
                var _loc_5:* = a + 1;
                a = _loc_5;
                var _loc_5:* = b + 1;
                b = _loc_5;
                gradientBoxMatrix = new Matrix();
                gradientBoxMatrix.createGradientBox(sw, sw, (-Math.PI) / 4, b, a);
                this.graphics.beginGradientFill(GradientType.LINEAR, [colorA, colorB], [1, 1], [0, 255], gradientBoxMatrix);
                this.graphics.drawRect(b, a, sw, sw);
                _loc_2++;
            }
            a = space - sw;
            b = -sw;
            var _loc_3:int = 0;
            while (_loc_3 < sw)
            {
                
                var _loc_5:* = a + 1;
                a = _loc_5;
                var _loc_5:* = b + 1;
                b = _loc_5;
                gradientBoxMatrix = new Matrix();
                gradientBoxMatrix.createGradientBox(sw, sw, (-Math.PI) / 4, a, b);
                this.graphics.beginGradientFill(GradientType.LINEAR, [colorA, colorB], [1, 1], [0, 255], gradientBoxMatrix);
                this.graphics.drawRect(a, b, sw, sw);
                _loc_3++;
            }
            return;
        }// end function

        private function linear() : void
        {
            switch(orientation)
            {
                case "horizontal":
                {
                    linearHorizontal();
                    break;
                }
                case "vertical":
                {
                    linearVertical();
                    break;
                }
                case "right":
                {
                    linearRight();
                    break;
                }
                case "left":
                {
                    linearLeft();
                    break;
                }
                default:
                {
                    break;
                }
            }
            this.graphics.endFill();
            return;
        }// end function

        private function linearHorizontal() : void
        {
            this.graphics.beginFill(colorA);
            this.graphics.drawRect(0, 0, space, sw);
            this.graphics.beginFill(colorA, 0);
            this.graphics.drawRect(0, sw, space, space);
            return;
        }// end function

        private function gradientRight() : void
        {
            a = space - sw + 1;
            var _loc_1:int = 0;
            while (_loc_1 < space)
            {
                
                var _loc_5:* = a - 1;
                a = _loc_5;
                gradientBoxMatrix = new Matrix();
                gradientBoxMatrix.createGradientBox(sw, sw, Math.PI / 4, a, _loc_1);
                this.graphics.beginGradientFill(GradientType.LINEAR, [colorA, colorB], [1, 1], [0, 255], gradientBoxMatrix);
                this.graphics.drawRect(a, _loc_1, sw, sw);
                _loc_1++;
            }
            a = space - sw - 1;
            var _loc_2:int = 0;
            while (_loc_2 < sw)
            {
                
                var _loc_5:* = a + 1;
                a = _loc_5;
                gradientBoxMatrix = new Matrix();
                gradientBoxMatrix.createGradientBox(sw, sw, Math.PI / 4, a, space - _loc_2);
                this.graphics.beginGradientFill(GradientType.LINEAR, [colorA, colorB], [1, 1], [0, 255], gradientBoxMatrix);
                this.graphics.drawRect(a, space - _loc_2, sw, sw);
                _loc_2++;
            }
            a = space - sw + 1;
            var _loc_3:int = 0;
            while (_loc_3 < space)
            {
                
                var _loc_5:* = a - 1;
                a = _loc_5;
                gradientBoxMatrix = new Matrix();
                gradientBoxMatrix.createGradientBox(sw, sw, Math.PI / 4, -space + _loc_3, a);
                this.graphics.beginGradientFill(GradientType.LINEAR, [colorA, colorB], [1, 1], [0, 255], gradientBoxMatrix);
                this.graphics.drawRect(-space + _loc_3, a, sw, sw);
                _loc_3++;
            }
            return;
        }// end function

        private function gradient() : void
        {
            switch(orientation)
            {
                case "horizontal":
                {
                    gradientHorizontal();
                    break;
                }
                case "vertical":
                {
                    gradientVertical();
                    break;
                }
                case "right":
                {
                    gradientRight();
                    break;
                }
                case "left":
                {
                    gradientLeft();
                    break;
                }
                default:
                {
                    break;
                }
            }
            this.graphics.endFill();
            return;
        }// end function

        private function gradientHorizontal() : void
        {
            gradientBoxMatrix = new Matrix();
            gradientBoxMatrix.createGradientBox(space, sw, (-Math.PI) / 2, 0, 0);
            this.graphics.beginGradientFill(GradientType.LINEAR, [colorA, colorB], [1, 1], [0, 255], gradientBoxMatrix);
            this.graphics.drawRect(0, 0, space, sw);
            return;
        }// end function

        private function linearRight() : void
        {
            a = space - sw + 1;
            var _loc_1:int = 0;
            while (_loc_1 < space)
            {
                
                var _loc_5:* = a - 1;
                a = _loc_5;
                this.graphics.beginFill(colorA);
                this.graphics.drawRect(a, _loc_1, sw, sw);
                _loc_1++;
            }
            a = space - sw - 1;
            var _loc_2:int = 0;
            while (_loc_2 < sw)
            {
                
                var _loc_5:* = a + 1;
                a = _loc_5;
                this.graphics.beginFill(colorA);
                this.graphics.drawRect(a, space - _loc_2, (_loc_2 + 1), (_loc_2 + 1));
                _loc_2++;
            }
            a = sw;
            var _loc_3:int = 0;
            while (_loc_3 < sw)
            {
                
                var _loc_5:* = a - 1;
                a = _loc_5;
                var _loc_5:* = a - 1;
                a = _loc_5;
                this.graphics.beginFill(colorA);
                this.graphics.drawRect(0, a, (_loc_3 + 1), (_loc_3 + 1));
                _loc_3++;
            }
            return;
        }// end function

        private function linearVertical() : void
        {
            this.graphics.beginFill(colorA);
            this.graphics.drawRect(0, 0, sw, space);
            this.graphics.beginFill(colorA, 0);
            this.graphics.drawRect(0, sw, space, space);
            return;
        }// end function

    }
}
