package com.cemex.rms.dispatcher.views.mediators
{
	import com.cemex.rms.common.date.FlexDateHelper;
	import com.cemex.rms.common.filters.FilterCondition;
	import com.cemex.rms.common.filters.FilterHelper;
	import com.cemex.rms.common.flashislands.IFlashIslandEventObject;
	import com.cemex.rms.common.gantt.GanttTask;
	import com.cemex.rms.common.helpers.PopupHelper;
	import com.cemex.rms.common.logging.ILogger;
	import com.cemex.rms.common.logging.LoggerFactory;
	import com.cemex.rms.common.reflection.ReflectionHelper;
	import com.cemex.rms.common.utils.DictionaryMap;
	import com.cemex.rms.dispatcher.constants.ExtrasConstants;
	import com.cemex.rms.dispatcher.constants.OTRConstants;
	import com.cemex.rms.dispatcher.constants.ParamtersConstants;
	import com.cemex.rms.dispatcher.constants.SecurityConstants;
	import com.cemex.rms.dispatcher.constants.TVARVCConstants;
	import com.cemex.rms.dispatcher.constants.TVARVCPreffixes;
	import com.cemex.rms.dispatcher.events.DispatcherConstants;
	import com.cemex.rms.dispatcher.helpers.GanttServiceReference;
	import com.cemex.rms.dispatcher.helpers.TooltipHelper;
	import com.cemex.rms.dispatcher.services.flashislands.helper.WDRequestHelper;
	import com.cemex.rms.dispatcher.services.flashislands.impl.DispatcherIslandImpl;
	import com.cemex.rms.dispatcher.services.flashislands.requests.dispatch.ChangePlantRequest;
	import com.cemex.rms.dispatcher.services.flashislands.vo.Equipment;
	import com.cemex.rms.dispatcher.services.flashislands.vo.Plant;
	import com.cemex.rms.dispatcher.services.flashislands.vo.ProductionData;
	import com.cemex.rms.dispatcher.services.flashislands.vo.Workcenter;
	import com.cemex.rms.dispatcher.views.assignedLoads.AssignedLoadsTaskRenderer;
	import com.cemex.rms.dispatcher.views.assignedLoads.mediators.AssignedLoadsTaskRendererMediator;
	import com.cemex.rms.dispatcher.views.loadsPerVehicle.mediators.LoadPerVehicleTaskRendererMediator;
	import com.cemex.rms.dispatcher.views.tree.OrderLoadGanttTask;
	import com.cemex.rms.dispatcher.vo.OrderLoad;
	
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	import ilog.gantt.TaskItem;
	
	import mx.binding.utils.BindingUtils;
	import mx.collections.ArrayCollection;
	import mx.collections.Sort;
	import mx.controls.Alert;
	import mx.controls.Menu;
	import mx.core.UIComponent;
	import mx.events.MenuEvent;
	import mx.events.ToolTipEvent;
	import mx.managers.ToolTipManager;
	import mx.utils.ObjectUtil;
	
	import org.robotlegs.mvcs.Mediator;
	
	public class GenericTaskRendererMediator extends Mediator
	{
		public function GenericTaskRendererMediator()
		{
			super();
			
		}
		
		
		
		public function getTooltiper():UIComponent{
			return getView();
		}
		
		public function tooltipStart(e:ToolTipEvent):void{
			
			if (getTooltiper() != null && e.currentTarget["data"]["data"]["payload"]["workAroundDestroy"]!="DELE"){
				getTooltiper().toolTip = getTooltip();
			}
		}
		public function getViewName():String{
			throw new Error("el metodo getViewName: " + this + "debe ser sobre escrito");
		}
		
		public function getTooltip():String{
			if (getGanttTask() != null){
				
				if (GanttServiceReference.isVisibleField(getViewName(),SecurityConstants.LOAD_TOOLTIP,null)){
					return TooltipHelper.getLoadTooltip(getGanttTask());
					//return "hello world 1";
				}
				else if (GanttServiceReference.isVisibleField(getViewName(),SecurityConstants.LOAD_TOOLTIP_PRODUCTION,null)){
					return TooltipHelper.getProdLoadTooltip(getGanttTask());
					//return "hello world 2";
				}
				else {
					return "";
				}
				
				
				
			}
			else {
				return "X";
			}
		}
		public function tooltipShow(e:ToolTipEvent):void{
			if (getTooltiper() != null && e.currentTarget["data"]["data"]["payload"]["workAroundDestroy"]!="DELE"){
				e.toolTip.text = getTooltiper().toolTip ;
			}
		}
		
		
		
		public function getLabel(label:String):String{
			return GanttServiceReference.getLabel(label);
		}
		
		
		public function popup(event:MouseEvent):void {
			if (getGanttTask() != null && getGanttTask().step != GanttTask.STEP_GHOSTX && event.currentTarget["data"]["data"]["payload"]["workAroundDestroy"]!="DELE"){
				var taskActions:Menu = PopupHelper.popupFromXML(event,"@label",getTaskActions());
				taskActions.addEventListener(MenuEvent.ITEM_CLICK,menuClick);
				//taskActions.addEventListener(MenuEvent.MENU_SHOW,makeRoundMenu);
				makeRoundMenu(taskActions);
			} 
		}
		
		private function makeRoundMenu(popmenu:Menu):void{
			var menu:Menu=popmenu as Menu;
			menu.cacheAsBitmap=false;
			
			if (!menu.mask){
				var maskx:uint = menu.x;
				var masky:uint = menu.y;
				var maskw:uint = menu.getExplicitOrMeasuredWidth();
				var maskh:uint = menu.getExplicitOrMeasuredHeight();
				var rad:int = menu.getStyle("cornerRadius") * 2;
				
				var roundRect:Sprite = new Sprite();
				roundRect.graphics.beginFill(0xFFFFFF);
				roundRect.graphics.drawRoundRect(maskx,masky,maskw,maskh,rad);
				roundRect.graphics.endFill();
				menu.mask = roundRect;
			}
		}
		public function getTaskActions():XML {
			
			var result:XML = PopupHelper.getXML("root");
			result.appendChild(PopupHelper.getMenuItem(getExtraInfoIfOverlap(getGanttTask()),null,null,null,0,false));
			add2(result,PopupHelper.getMenuItemSepataror());
			
			var overlaps:ArrayCollection = getOverlap();
			
			addMenu(result,overlaps);
			
			return result;
		}
		
		public static var isWaitingFakePush:Boolean=false;
		public function addMenu(result:XML,overlaps:ArrayCollection) :void {
			add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.ORDER_DETAIL), WDRequestHelper.SHOW_BITACORA_REQUEST,SecurityConstants.MENU_ASSIGN_PLANT));
			if(!DispatcherIslandImpl.areMenusAvailable){
				return;
			}
			
			var temp:XML = null;
			
			add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.ASSIGN_TO_PLANT), WDRequestHelper.ASSIGN_PLANT_REQUEST,SecurityConstants.MENU_ASSIGN_PLANT));
			add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.ANTICIPATED_LOADS), WDRequestHelper.ANTICIPATED_LOADS_REQUEST,SecurityConstants.MENU_ANTICIPATED_LOADS));
			add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.RENEGOTIATE), WDRequestHelper.RENEGOTIATE_LOAD_REQUEST,SecurityConstants.MENU_RENEGOTIATE_LOAD));
			add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.UNASSIGN_TO_PLANT), WDRequestHelper.UNASSIGN_PLANT_REQUEST,SecurityConstants.MENU_UNASSIGN_PLANT));
			add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.UNASSIGN), WDRequestHelper.UNASSIGN_QUICK_REQUEST,SecurityConstants.MENU_UNASSIGN));
			add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.BOM_REQUEST), WDRequestHelper.BOM_REQUEST,SecurityConstants.BOM_REQUEST));
			//add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.CHANGE_PLANT), WDRequestHelper.CHANGE_PLANT_REQUEST_MENU));
			var changePlant:XML = PopupHelper.getMenuItem(getLabel(OTRConstants.CHANGE_PLANT),null,null,null,-1,true,null,false);
			
			
			var changePlantChildren:Array = new Array();
			//changePlantChildren.push(getMenuItemOverLap(overlaps,DispatcherConstants.ORDER_SCOPE,getLabel(OTRConstants.ORDER_LABEL), WDRequestHelper.CHANGE_PLANT_REQUEST_MENU,SecurityConstants.MENU_CHANGE_PLANT));
			var algo:*=getViewName();
			
			if(getGanttTask().data.itemCategory!="ZTNR" && getGanttTask().data.itemCategory!="ZBR1" && getGanttTask().data.itemCategory!="ZRM2" && getGanttTask().data.itemCategory!="ZRWR" && getViewName()!="FXD_ASL"){
				changePlantChildren.push(getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.ORDER_LABEL), WDRequestHelper.CHANGE_PLANT_REQUEST_MENU,SecurityConstants.MENU_CHANGE_PLANT));
			}
			changePlantChildren.push(getMenuItemOverLap(overlaps,DispatcherConstants.LOAD_SCOPE,getLabel(OTRConstants.LOAD_LABEL), WDRequestHelper.CHANGE_PLANT_REQUEST_MENU,SecurityConstants.MENU_CHANGE_PLANT));
			logger.debug("changePlantChildren");
			add2Parent(result,changePlant,changePlantChildren);
			//en
			
			add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.CHANGE_VOLUME), WDRequestHelper.CHANGE_VOLUME_REQUEST,SecurityConstants.MENU_CHANGE_VOLUME));
			
			//add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.RE_USE), WDRequestHelper.REUSE_CONCRETE_REQUEST,SecurityConstants.MENU_REUSE_CONCRETE));
			add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.ORDER_ON_HOLD), WDRequestHelper.ORDER_ON_HOLD_REQUEST,SecurityConstants.MENU_ORDER_ON_HOLD,"check"));
			
			
			/////---->
			add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.CHANGE_DELIVERY_TIME), WDRequestHelper.CHANGE_DELIVERY_TIME_REQUEST,SecurityConstants.MENU_CHANGE_DELIVERY_TIME));
			
			add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.CHANGE_FREQUENCY), WDRequestHelper.CHANGE_FREQUENCY_REQUEST,SecurityConstants.MENU_CHANGE_FREQUENCY));
			
			
			add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.ASSIGN_VEHICLE), WDRequestHelper.ASSIGN_VEHICLE_REQUEST,SecurityConstants.MENU_ASSIGN_VEHICLE));
			
			//menu para mostrar el anticipated loads
			//add2(result,getMenuItemOverLap(overlaps,null,"lalalalala", WDRequestHelper.ANTICIPATED_LOADS_REQUEST,SecurityConstants.MENU_ASSIGN_VEHICLE));
			
			addAssignVehicles(result,overlaps);
			
			
			var plants:DictionaryMap = filterPlants(GanttServiceReference.getPlants(), overlaps);
			var keys:ArrayCollection = plants.getAvailableKeys();
			for (var i:int = 0 ; i < keys.length ; i ++ ) {
				var key:String = keys.getItemAt(i) as String;
				var plant:Plant = plants.get(key);
				var wc:ArrayCollection = plant.workcenter;
				for (var j:int  = 0; j < wc.length; j++ ){
					var workcenter:Workcenter = wc.getItemAt(j) as Workcenter;
					add2(result,getMenuItemOverLap(overlaps,workcenter.arbpl,getLabel(OTRConstants.BATCH) + ":" + workcenter.arbpl, WDRequestHelper.BATCH_REQUEST,SecurityConstants.MENU_BATCH));
				}
			}
			add2(result,
				getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.MANUAL_BATCH), WDRequestHelper.MANUAL_BATCH_REQUEST,SecurityConstants.MENU_MANUAL_BATCH)
			);
			
			add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.RE_USE), WDRequestHelper.RE_USE_REQUEST,SecurityConstants.MENU_RE_USE));
			add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.CONTINUE_PRODUCTION), WDRequestHelper.CONTINUE_PRODUCTION_REQUEST,SecurityConstants.MENU_CONTINUE_PRODUCTION));
			add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.RESTART_LOAD), WDRequestHelper.RESTART_LOAD_REQUEST,SecurityConstants.MENU_RESTART_LOAD));
			add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.QUALITY_ADJUSTMENT), WDRequestHelper.QUALITY_ADJUSTMENT_REQUEST,SecurityConstants.MENU_QUALITY_ADJUSTMENT));
			
			
			add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.RE_PRINT), WDRequestHelper.RE_PRINT_REQUEST,SecurityConstants.MENU_RE_PRINT));
			
			add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.DISPATCH), WDRequestHelper.DISPATCH_REQUEST,SecurityConstants.MENU_DISPATCH));
			
			// Este siempre se despliega
			add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.DISPLAY_PO), WDRequestHelper.DISPLAY_PO_REQUEST,SecurityConstants.MENU_DISPLAY_PO));
			
			//add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.BATCH), WDRequestHelper.BATCH_REQUEST,SecurityConstants.MENU_BATCH));
			
			add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.START_BATCHING), WDRequestHelper.START_BATCHING_REQUEST,SecurityConstants.MENU_START_BATCHING));
			trace("START BATCHING:"+SecurityConstants.MENU_START_BATCHING);
			
			
			temp = getMenuItemOverLap(overlaps,null,"Show Data",WDRequestHelper.SHOW_DATA_REQUEST,SecurityConstants.MENU_SHOW_DATA,null,PopupHelper.MENU_ACTION_FUNCTION);
			if (temp != null){
				add2(result,PopupHelper.getMenuItemSepataror());
				//add2(result,temp);
			}
			
			
			
			
		}
		public function filterPlants(plants:ArrayCollection , overlaps:ArrayCollection) :DictionaryMap {
			var result:DictionaryMap =  new DictionaryMap();
			for(var i:int  = 0 ; i < overlaps.length ; i ++){
				var task:GanttTask = overlaps.getItemAt(i) as GanttTask;
				for (var j:int = 0 ; j < plants.length ; j ++){
					var plant:Plant = plants.getItemAt(j) as Plant;
					var payload:OrderLoad = task.data as OrderLoad;
					if (plant.plantId == payload.plant && result.get(plant.plantId) == null){
						result.put(plant.plantId , plant);
					}
				}
			}
			return result;
		}
		
		public function showMenu(action:String, task:GanttTask,extra:String):Boolean{
			
			var payload:OrderLoad = task.data as OrderLoad;
			var plant:String = payload.plant;
			var operationType:String = GanttServiceReference.getPlantType(plant);
			
			
			var extraSplit:Array;
			var prodData:ProductionData = GanttServiceReference.getProductionData(payload);
			if(prodData==null && payload.prodOrder!="" && payload.prodOrder!=null){
				prodData=new ProductionData();
				prodData.aufnr=payload.prodOrder;
				prodData.txt04=payload.txtStatusProdOrder;
				prodData.posnr=Number(payload.codStatusProdOrder);
				prodData.stonr=payload.codStatusProdOrder;
			}
			
			

			switch(action){
				case WDRequestHelper.INTERCHANGE_VEHICLE_REQUEST:
					if((payload.loadStatus=="CMPL" || payload.loadStatus=="TJST") && Number(payload.codStatusProdOrder)>=80){
						return true;
					}
					break;
				case WDRequestHelper.BOM_REQUEST:
					//if(payload.orderStatus=="ADBO"){
					if(payload.bomValid=="1" || payload.bomValid=="3" || payload.bomValid=="2" || payload.bomValid=="4"){
						//var viewr:Boolean=GanttServiceReference.isVisibleField(getViewName(),SecurityConstants.MENU_ANTICIPATED_LOADS,operationType);
						
						return true;
					} else {
						return false;
					}
					break;
				case WDRequestHelper.UNASSIGN_QUICK_REQUEST:
					if(payload.loadStatus=="ASSG"){
						var viewr:Boolean=GanttServiceReference.isVisibleField(getViewName(),SecurityConstants.MENU_ANTICIPATED_LOADS,operationType);
						return viewr;
					} else {
						return false;
					}
					break;
				case WDRequestHelper.SHOW_DATA_REQUEST:
					//return GanttServiceReference.isVisibleField(getViewName(),SecurityConstants.MENU_SHOW_DATA,operationType);
					return false;
					break;
				case WDRequestHelper.ANTICIPATED_LOADS_REQUEST:
					if(payload.loadStatus=="NOST"){
						return GanttServiceReference.isVisibleField(getViewName(),SecurityConstants.MENU_ANTICIPATED_LOADS,operationType);
					} else {
						return false;
					}
					break;
				case WDRequestHelper.ASSIGN_VEHICLE_REQUEST:
					if ((prodData == null || prodData.stonr == "10" || prodData.stonr == "20" || payload.constructionProduct.indexOf("P")!=-1 || payload.codStatusProdOrder=="0") && (payload.loadStatus!="ASSG" && payload.orderStatus!="RNGT")){
						return GanttServiceReference.isVisibleField(getViewName(),SecurityConstants.MENU_ASSIGN_VEHICLE,operationType);
					}
					break;
				case WDRequestHelper.ASSIGN_ONE_VEHICLE_REQUEST:
					//requiere datos extras -extraSplit-
					extraSplit = extra.split(":");
					if ((prodData == null || prodData.stonr == "10" || prodData.stonr == "20" || payload.constructionProduct.indexOf("CP")!=-1 || payload.codStatusProdOrder=="0") && payload.orderStatus!="RNGT"){
						if ((payload.shipConditions == GanttServiceReference.getTVARVCParam(TVARVCConstants.SHIPCOND).low && extraSplit[5] == "W")
							|| (payload.shipConditions != GanttServiceReference.getTVARVCParam(TVARVCConstants.SHIPCOND).low && extraSplit[5] == "V") ){
							if (extraSplit[0] == plant){
								return GanttServiceReference.isVisibleField(getViewName(),SecurityConstants.MENU_ASSIGN_ONE_VEHICLE,operationType);
							}
						}
					}
					break;
				case WDRequestHelper.ASSIGN_PLANT_REQUEST:
					if ((prodData == null || prodData.stonr == "10" || payload.constructionProduct.indexOf("CP")!=-1 || payload.codStatusProdOrder=="0") && payload.orderStatus!="RNGT"){
						return GanttServiceReference.isVisibleField(getViewName(),SecurityConstants.MENU_ASSIGN_PLANT,operationType);
					}
					break;
				case WDRequestHelper.CHANGE_PLANT_REQUEST_MENU:
					if (prodData == null || prodData.stonr == "10" || (getViewName()==TVARVCPreffixes.ASSIGNED_LOADS_VIEW_ID && Number(prodData.stonr)>=20)){
						return GanttServiceReference.isVisibleField(getViewName(),SecurityConstants.MENU_CHANGE_PLANT,operationType);
					}
					break;
				case WDRequestHelper.CHANGE_VOLUME_REQUEST:
					if (prodData == null || prodData.stonr == "10"){
						return GanttServiceReference.isVisibleField(getViewName(),SecurityConstants.MENU_CHANGE_VOLUME,operationType);
					}
					break;
				case WDRequestHelper.CHANGE_FREQUENCY_REQUEST:
					if (prodData == null || prodData.stonr == "10"){
						return GanttServiceReference.isVisibleField(getViewName(),SecurityConstants.MENU_CHANGE_FREQUENCY,operationType);
					}
					break;
				case WDRequestHelper.ORDER_ON_HOLD_REQUEST:
					if (prodData == null || prodData.stonr == "10"){
						return GanttServiceReference.isVisibleField(getViewName(),SecurityConstants.MENU_ORDER_ON_HOLD,operationType);
					}
					break;
				case WDRequestHelper.CHANGE_DELIVERY_TIME_REQUEST:
					if (prodData == null || prodData.stonr == "10"){
						return GanttServiceReference.isVisibleField(getViewName(),SecurityConstants.MENU_CHANGE_DELIVERY_TIME,operationType);
					}
					break;
				case WDRequestHelper.DISPLAY_PO_REQUEST:
					if (prodData != null){
						return GanttServiceReference.isVisibleField(getViewName(),SecurityConstants.MENU_DISPLAY_PO,operationType);
					}
					break;
				case WDRequestHelper.DISPATCH_REQUEST:
					return true;
					/*if ((prodData == null || (payload.loadStatus=="TJST" && payload.loadNumber.indexOf("L")!=-1)) && DispatcherIslandImpl.isBatcher){
						return true;//GanttServiceReference.isVisibleField(getViewName(),SecurityConstants.MENU_DISPATCH,operationType);
					}*/
					break;
				case WDRequestHelper.START_BATCHING_REQUEST:
					if (prodData != null && (payload.loadStatus!="TJST" && payload.constructionProduct.indexOf("P")==-1) && DispatcherIslandImpl.isBatcher){
						return GanttServiceReference.isVisibleField(getViewName(),SecurityConstants.MENU_START_BATCHING,operationType);
					}
					break;
				case WDRequestHelper.BATCH_REQUEST:
					if (prodData != null && prodData.stonr == "20" && (payload.loadStatus!="TJST" && payload.constructionProduct.indexOf("P")==-1) && DispatcherIslandImpl.isBatcher){
						return GanttServiceReference.isVisibleField(getViewName(),SecurityConstants.MENU_BATCH,operationType);
					}
					break;
				case WDRequestHelper.MANUAL_BATCH_REQUEST:
					if (prodData != null && prodData.stonr == "20"){
						return GanttServiceReference.isVisibleField(getViewName(),SecurityConstants.MENU_MANUAL_BATCH,operationType);
					}
					break;
				case WDRequestHelper.RE_PRINT_REQUEST:
					if (prodData != null && Number(prodData.stonr) >= 40 ){
						return GanttServiceReference.isVisibleField(getViewName(),SecurityConstants.MENU_RE_PRINT,operationType);
					}
					break;
				case WDRequestHelper.RE_USE_REQUEST:
					if (prodData != null && prodData.stonr == "70"){
						return GanttServiceReference.isVisibleField(getViewName(),SecurityConstants.MENU_RE_USE,operationType);
					}
					break;
				case WDRequestHelper.CONTINUE_PRODUCTION_REQUEST:
					if (prodData != null && prodData.stonr == "70"){
						return GanttServiceReference.isVisibleField(getViewName(),SecurityConstants.MENU_CONTINUE_PRODUCTION,operationType);
					}	
					break;
				case WDRequestHelper.RESTART_LOAD_REQUEST:
					if (prodData != null && prodData.stonr == "70"){
						return GanttServiceReference.isVisibleField(getViewName(),SecurityConstants.MENU_RESTART_LOAD,operationType);
					}
					break;
				case WDRequestHelper.QUALITY_ADJUSTMENT_REQUEST:
					if (prodData != null && prodData.stonr == "80"){
						return GanttServiceReference.isVisibleField(getViewName(),SecurityConstants.MENU_QUALITY_ADJUSTMENT,operationType);
					}
					break;

				case WDRequestHelper.RENEGOTIATE_LOAD_REQUEST:
					if(payload.orderStatus!="RNGT"){
						var renegBoolean:Boolean=GanttServiceReference.isVisibleField(getViewName(),SecurityConstants.MENU_RENEGOTIATE_LOAD,operationType);
						return renegBoolean;
					} else {
						return false;
					}
					break;
				case WDRequestHelper.UNASSIGN_PLANT_REQUEST:
					return GanttServiceReference.isVisibleField(getViewName(),SecurityConstants.MENU_UNASSIGN_PLANT,operationType);
					break;
				case WDRequestHelper.SHOW_BITACORA_REQUEST:
					return GanttServiceReference.isVisibleField(getViewName(),SecurityConstants.MENU_ASSIGN_PLANT,operationType);
			}
			
			
			
			//Alert.show("Show:" +action);
			return false;
			
		}
		public static function compareEquipments(a:Object,b:Object,fields:Array = null):int
		{
			//fnDtfParceFunct is a Date parse function avail in DownloadCode.
			var eq1:Equipment = a as Equipment;
			var eq2:Equipment= b as Equipment;
			if (eq1 == null || eq2 == null){
				if (eq1 == null && eq2 == null){
					return 0;
				}
				else {
					if (eq1 == null){
						return ObjectUtil.compare(null, eq2.turnTimestamp);
					}
					else {
						return ObjectUtil.compare(eq1.turnTimestamp, null);
					}
				}
			}
			
			var comp:int = ObjectUtil.dateCompare(eq1.turnTimestamp, eq2.turnTimestamp);
			
			if(comp == 0){
				comp = ObjectUtil.compare(eq1.equipment,eq2.equipment);
			}
			return comp;
		}
		public function addAssignVehicles(result:XML,overlaps:ArrayCollection):void{
			var plants:DictionaryMap = filterPlants(GanttServiceReference.getPlants(), overlaps);
			var keys:ArrayCollection = plants.getAvailableKeys();
			
			for (var i:int = 0 ; i < keys.length ; i ++ ) {
				var key:String = keys.getItemAt(i) as String;
				var plant:Plant = plants.get(key);
				
				var eq:ArrayCollection = plant.equipments;
				var sort:Sort =  new Sort();
				sort.compareFunction = compareEquipments;
				eq.sort = sort; 
				eq.refresh();
				// sort
				
				/*
				Inicialmente los vehiculos debían estar en estado disponible
				pero ahora cualquier pedido puede ser asignado
				*/
				//----------deprecated---->>>>>>
				var availableVehicles:ArrayCollection = new ArrayCollection();
				
				availableVehicles.addItem(FilterHelper.getFilterConditionTVARVC("status",new ArrayCollection([GanttServiceReference.getTVARVCParam(TVARVCConstants.VEHICLES_STATUS_AVAILABLE)]),FilterCondition.EQUALS));
				
				if(!GanttServiceReference.getCollectFilterStatus()){
					availableVehicles.addItem(FilterHelper.getFilterCondition("equicatgry",new ArrayCollection(["V"]),FilterCondition.EQUALS));
				}
				var maxVehicles:Number = GanttServiceReference.getParamNumber(ParamtersConstants.NUM_VEHI_DISP);
				var count:int = 0 ;
				//<<<<<<------------------
				/*for (var k:int  = 0; k < eq.length && count < maxVehicles; k++ ){
					var equipment:Equipment = eq.getItemAt(k) as Equipment;
					
					if (FilterHelper.matchFilters(equipment,availableVehicles)){
						add2(result,getMenuItemOverLap(overlaps,WDRequestHelper.getAssignOneVehicleExtraID(plant,equipment) ,getLabel(OTRConstants.ASSIGN_VEHICLE)+":"+plant.plantId + ":" + equipment.equipment, WDRequestHelper.ASSIGN_ONE_VEHICLE_REQUEST,SecurityConstants.MENU_ASSIGN_ONE_VEHICLE));
						count++;
					}					
				}*/
				if(DispatcherViewMediator.vehiculosCorrectos==null){
					return;
				}
				if(overlaps.length==1){
					var notVisibleItem:Object=overlaps.getItemAt(0);
					if(notVisibleItem.data.loadStatus=="ASSG"){
						return;
					}
				}
				for(var k:int=0;k<DispatcherViewMediator.vehiculosCorrectos.length && count < maxVehicles;k++){
					if(DispatcherViewMediator.vehiculosCorrectos.getItemAt(k).plantId==plant.plantId && DispatcherViewMediator.vehiculosCorrectos.getItemAt(k).equipStatus==ExtrasConstants.VEHICLE_DISPONIBLE && DispatcherViewMediator.vehiculosCorrectos.getItemAt(k).fleetNum!="RMS-DUMMY"){
						var d:*=DispatcherViewMediator.vehiculosCorrectos.getItemAt(k);
						add2(result,getMenuItemOverLap(overlaps,WDRequestHelper.getAssignOneVehicleExtraID(plant,DispatcherViewMediator.vehiculosCorrectos.getItemAt(k)) ,getLabel(OTRConstants.ASSIGN_VEHICLE)+":"+plant.plantId + ":" + DispatcherViewMediator.vehiculosCorrectos.getItemAt(k).equipLabel+": ("+DispatcherViewMediator.vehiculosCorrectos.getItemAt(k).loadVol+" "+DispatcherViewMediator.vehiculosCorrectos.getItemAt(k).volUnit+" )", WDRequestHelper.ASSIGN_ONE_VEHICLE_REQUEST,SecurityConstants.MENU_ASSIGN_ONE_VEHICLE));
						count++;
					}
				}
			}
			
		}
		
		protected function getView():UIComponent{
			
			throw new Error("This method should be overriden");
		}
		
		override public function onRegister():void {
			if(getView().visible){
				BindingUtils.bindSetter(setParent,getView(),["data","data"])
				getView().addEventListener(MouseEvent.DOUBLE_CLICK, popup);
				//getTooltiper().addEventListener(ToolTipEvent.TOOL_TIP_CREATE,createTooltip);
				getTooltiper().addEventListener(ToolTipEvent.TOOL_TIP_SHOW,tooltipShow);
				getTooltiper().addEventListener(ToolTipEvent.TOOL_TIP_START,tooltipStart);
				//getTooltiper().addEventListener(,tooltipStart);
				getTooltiper().toolTip = getTooltip();
				/*ToolTipManager.enabled=false;
				getTooltiper().addEventListener(MouseEvent.MOUSE_OVER, onMouseOverHandler);
				getTooltiper().addEventListener(MouseEvent.MOUSE_OUT, onMouseOutHandler);*/	
			}
			
			
		}
		public function setParent(data:*):void {
			if(data != null && getGanttTask() != null) {
				getGanttTask().setTaskItem(getTaskItem());
				//getGanttTask().updateTooltip();
				//BindingUtils.bindProperty(getTooltiper(),"toolTip", getGanttTask(),["tooltip"]);
			}
		}
		
		
		private var logger:ILogger = LoggerFactory.getLogger("GenericTaskRenderMediator("+this+")");
		
		
		/*
		public function checkDelayedTime(delayTime:Date):void {
			
			
			if (getGanttTask() != null ){ 
				var task:GanttTask = getGanttTask();
	
				if (task.isDelayed) {
					
					getGanttTask().startTime =  FlexDateHelper.copyDate(delayTime);
					getTaskItem().startTime = getGanttTask().startTime;
					getTaskItem().endTime = getGanttTask().endTime;
				}
				updateColorsTask();
			}
		}
		
		*/
		public function updateColorsTask():void{
			getGanttTask().getAllTasks().itemUpdated(getGanttTask());
		}
		
		
		
		
		
		public function add2(root:XML, child:XML):void{
			if (child != null) {
				root.appendChild(child);
			}
		}
		
		public function add2Parent(root:XML, parent:XML,children:Array):void{
			if (children != null) {
				var count:Number = 0 ;
				for (var i:int = 0 ; i < children.length ; i ++){
					var child:XML = children[i] as XML;
					if (child != null){
						count++;
						logger.debug("ChangePlant :" + child);
						parent.appendChild(child);
					}
				}
				if (count > 0){
					logger.debug("RootChangePlant :" + parent);
					root.appendChild(parent);
				}
			}
		}
		
		
		public function getOverlap():ArrayCollection {
			
			var result:ArrayCollection =  new ArrayCollection();
			var task:GanttTask = getGanttTask();
			//var my:Number=task.getTaskItem().
			var tiempo:String = FlexDateHelper.getTimestampString(task.startTime);
			for (var i:int = 0 ; i < task.getContainer().length ; i ++ ){
				var temp:GanttTask = task.getContainer().getItemAt(i) as GanttTask; 
				if (FlexDateHelper.getTimestampString(temp.startTime) == tiempo && temp.data.indexPosition==task.data.indexPosition){
					result.addItem(temp);
				} 
			}
			return result;
		}
		
		
		public  function getMenuItemOverLap(overlaps:ArrayCollection,extra:String,label:String,action:String,enabledSecurity:String,type:String=null,callType:int=1):XML{
			
			var payload:OrderLoad;
			var plant:String;
			var operationType:String;
			var enables:Boolean;
			var menu:XML = null;
			
			var show:Boolean;
			var temp:GanttTask;
			
			if (overlaps.length > 1 && label!=getLabel(OTRConstants.ORDER_LABEL)) {
				
				menu = PopupHelper.getMenuItem(label,null,extra,action,callType,true,type,showChecked(action,getGanttTask()));
				
				var some:Boolean = false;
				for (var i:int = 0; i < overlaps.length ; i ++ ) {
					
					temp = overlaps.getItemAt(i) as GanttTask;
					show = showMenu(action,temp,extra);
					
					
					payload = temp.data as OrderLoad;
					
					plant = payload.plant;
					operationType = GanttServiceReference.getPlantType(plant);
					if(enabledSecurity=="BTN_ANTICIPATED_LOADS"){
						//preparing to debug
						var algo:*=".";
					}
					enables = GanttServiceReference.isEnabledField(getViewName(),enabledSecurity,operationType);
					var esma:Boolean=DispatcherIslandImpl.isTomorrow;
					if(((payload.itemCategory=="ZTX0" || payload.itemCategory=="ZRWN" || DispatcherIslandImpl.isTomorrow) && (label==getLabel(OTRConstants.CHANGE_PLANT) 
						|| label==getLabel(OTRConstants.CHANGE_DELIVERY_TIME) 
						|| label==getLabel(OTRConstants.CHANGE_VOLUME) 
						|| label==getLabel(OTRConstants.ASSIGN_VEHICLE) 
						|| label==getLabel(OTRConstants.ORDER_LABEL) 
						|| label==getLabel(OTRConstants.LOAD_LABEL))) ){
						show=false;
						enables=false;
					}
					if((label==getLabel(OTRConstants.CHANGE_DELIVERY_TIME) 
						|| label==getLabel(OTRConstants.CHANGE_VOLUME) 
						|| label==getLabel(OTRConstants.ASSIGN_VEHICLE) 
						|| label==getLabel(OTRConstants.ORDER_LABEL) 
						|| label==getLabel(OTRConstants.LOAD_LABEL)
						|| label==getLabel(OTRConstants.ORDER_ON_HOLD)
						|| label==getLabel(OTRConstants.CHANGE_PLANT)
						|| action==WDRequestHelper.ASSIGN_ONE_VEHICLE_REQUEST
						|| label==getLabel(OTRConstants.CHANGE_FREQUENCY)
						|| label==getLabel(OTRConstants.ASSIGN_TO_PLANT)
						|| label==getLabel(OTRConstants.ANTICIPATED_LOADS)) 
						&& ((payload.loadStatus=="CMPL" || payload.loadStatus=="TJST" || payload.loadStatus=="OJOB" || payload.loadStatus=="UNLD") && !DispatcherIslandImpl.isBatcher)){
						show=false;
						enables=false;
					}
					if((payload.loadStatus=="BLCK" || payload.orderStatus=="BLCK") && (label==getLabel(OTRConstants.CHANGE_VOLUME)
						|| label==getLabel(OTRConstants.ASSIGN_VEHICLE)
						|| label==getLabel(OTRConstants.ORDER_LABEL)
						|| label==getLabel(OTRConstants.ASSIGN_TO_PLANT)
						|| action==WDRequestHelper.ASSIGN_ONE_VEHICLE_REQUEST	
						|| label==getLabel(OTRConstants.ANTICIPATED_LOADS)
						|| label==getLabel(OTRConstants.LOAD_LABEL))){
						show=false;
						enables=false;
						
					}
					if(DispatcherIslandImpl.isTomorrow && (label==getLabel(OTRConstants.ASSIGN_TO_PLANT)
						|| label==getLabel(OTRConstants.ASSIGN_VEHICLE) 
						|| label==getLabel(OTRConstants.ORDER_LABEL) 
						|| label==getLabel(OTRConstants.LOAD_LABEL)
						|| action==WDRequestHelper.ASSIGN_ONE_VEHICLE_REQUEST)){
						show=false;
						enables=false;
					}
					if(label==getLabel(OTRConstants.CHANGE_VOLUME) && payload.itemCategory=="ZTNR"){
						show=false;
						enables=false;
					}
					if(label==getLabel(OTRConstants.ORDER_DETAIL) || action==WDRequestHelper.DISPATCH_REQUEST){
						show=true;
						enables=true;
					}
					if(label==getLabel(OTRConstants.UNASSIGN) && payload.loadStatus=="ASSG"){
						enables=true;
					}
					if(label==getLabel(OTRConstants.BOM_REQUEST)){
						enables=true;
					}
					if(label==getLabel(OTRConstants.SHOW_INTERCHANGE_VEHICLE)){
						enables=true;
					}
					if (show){
						menu.appendChild(PopupHelper.getMenuItem(getExtraInfoIfOverlap(temp),temp,extra,action,callType,enables,type,showChecked(action,temp)));
						some = true;
					}
				}
				if (!some){
					menu = null;// PopupHelper.getMenuItem(label,null,extra,action,callType,false,type,showChecked(action,getGanttTask()));
				}
			}
			else {				
				temp = getGanttTask();
				
				show = showMenu(action,temp,extra);
				payload = temp.data as OrderLoad;
				plant = payload.plant;
				operationType = GanttServiceReference.getPlantType(plant);
				enables = GanttServiceReference.isEnabledField(getViewName(),enabledSecurity,operationType);
				var esma:Boolean=DispatcherIslandImpl.isTomorrow;
				if(((payload.itemCategory=="ZTX0" || payload.itemCategory=="ZRWN" || DispatcherIslandImpl.isTomorrow) && (label==getLabel(OTRConstants.CHANGE_PLANT) 
					|| label==getLabel(OTRConstants.CHANGE_DELIVERY_TIME) 
					|| label==getLabel(OTRConstants.CHANGE_VOLUME) 
					|| label==getLabel(OTRConstants.ASSIGN_VEHICLE) 
					|| label==getLabel(OTRConstants.ORDER_LABEL) 
					|| label==getLabel(OTRConstants.LOAD_LABEL))) ){
					show=false;
					enables=false;
				}
				if((label==getLabel(OTRConstants.CHANGE_DELIVERY_TIME) 
					|| label==getLabel(OTRConstants.CHANGE_VOLUME) 
					|| label==getLabel(OTRConstants.ASSIGN_VEHICLE) 
					|| label==getLabel(OTRConstants.ORDER_LABEL) 
					|| label==getLabel(OTRConstants.LOAD_LABEL)
					|| label==getLabel(OTRConstants.ORDER_ON_HOLD)
					|| label==getLabel(OTRConstants.CHANGE_PLANT)
					|| action==WDRequestHelper.ASSIGN_ONE_VEHICLE_REQUEST
					|| label==getLabel(OTRConstants.CHANGE_FREQUENCY)
					|| label==getLabel(OTRConstants.ASSIGN_TO_PLANT)
					|| label==getLabel(OTRConstants.ANTICIPATED_LOADS)) 
					&& ((payload.loadStatus=="CMPL" || payload.loadStatus=="TJST" || payload.loadStatus=="OJOB" || payload.loadStatus=="UNLD") && !DispatcherIslandImpl.isBatcher)){
					show=false;
					enables=false;
				}
				if((payload.loadStatus=="BLCK" || payload.orderStatus=="BLCK") && (label==getLabel(OTRConstants.CHANGE_VOLUME)
					|| label==getLabel(OTRConstants.ASSIGN_VEHICLE)
					|| label==getLabel(OTRConstants.ORDER_LABEL)
					|| label==getLabel(OTRConstants.ASSIGN_TO_PLANT)
					|| action==WDRequestHelper.ASSIGN_ONE_VEHICLE_REQUEST	
					|| label==getLabel(OTRConstants.ANTICIPATED_LOADS)
					|| label==getLabel(OTRConstants.LOAD_LABEL))){
					show=false;
					enables=false;
					
				}
				if(DispatcherIslandImpl.isTomorrow && (label==getLabel(OTRConstants.ASSIGN_TO_PLANT)
					|| label==getLabel(OTRConstants.ASSIGN_VEHICLE) 
					|| label==getLabel(OTRConstants.ORDER_LABEL) 
					|| label==getLabel(OTRConstants.LOAD_LABEL)
					|| action==WDRequestHelper.ASSIGN_ONE_VEHICLE_REQUEST)){
					show=false;
					enables=false;
				}
				if(label==getLabel(OTRConstants.CHANGE_VOLUME) && payload.itemCategory=="ZTNR"){
					show=false;
					enables=false;
				}
				if(label==getLabel(OTRConstants.ORDER_DETAIL)){
					show=true;
					enables=true;
				}
				if(label==getLabel(OTRConstants.UNASSIGN) && payload.loadStatus=="ASSG"){
					enables=true;
				}
				if(label==getLabel(OTRConstants.BOM_REQUEST)){
					enables=true;
				}
				if(label==getLabel(OTRConstants.SHOW_INTERCHANGE_VEHICLE)){
					enables=true;
				}
				if (show){
					menu = PopupHelper.getMenuItem(label,null,extra,action,callType,enables,type,showChecked(action,getGanttTask()));
				}
				
				
				
			}
			return menu;
		}
		
		public function showChecked(action:String, task:GanttTask):Boolean{
			return true;
		}
		
		
		public function getExtraInfoIfOverlap(task:GanttTask):String{
			var payload:OrderLoad = task.data  as OrderLoad;
			return payload.orderNumber+":"+payload.itemNumber ;
		}
		
		
	
		/**
		 * Esta menuClick lo que hace es mandar a llamar la funcion que esta configurada en el XML 
		 * 
		 */
		public function menuClick(event:MenuEvent):void {
			/*
			Esta linea verifica que los menus no esten bloqueados debido a la fecha
			actual
			*/
			if(!DispatcherIslandImpl.areMenusAvailable){
				return;
			}
			
			
			if (event.item.@callType != null && event.item.@ref != null  && 
				event.item.@callType != "" && event.item.@ref != "" ){
			 		
				var task:GanttTask = getSelectedGanttTask(""+event.item.@label);
				var extra:String = "";
				if (event.item.@extra != null){
					extra = "" + event.item.@extra;
				}
			 	 if (event.item.@callType == PopupHelper.MENU_ACTION_EVENT){
			 	 	
					dispatchOrderOperationRequestEvent(event.item.@ref,task,extra);
				}	
				else if (event.item.@callType == PopupHelper.MENU_ACTION_FUNCTION && this[event.item.@ref] != null){
					
					var funcion:Function = this[event.item.@ref] as Function;
					if (task != null){
						funcion.call(null,task);
					}
					else {
						funcion.call(null,getGanttTask());				
					}
				}
			}
		}
		public function dispatchOrderOperationRequestEvent(type:String,task:*,extra:String):void {
			
			if (task != null && task is GanttTask){
				//dispatch(new OrderOperationRequestEvent(type,task,extra))
				callWDEvent(type,task,extra);
			}
			else {
				callWDEvent(type,getGanttTask(),extra);
				//dispatch(new OrderOperationRequestEvent(type,getGanttTask(),extra))
			}
		}
			
		public function callWDEvent(type:String,task:GanttTask,fuaaa:String):void{
			var payload:OrderLoad = task.data as OrderLoad;
			
			var prodData:ProductionData = GanttServiceReference.getProductionData(payload);
			
			var toPlantType:String = GanttServiceReference.getPlantType(payload.plant);
			var func:Function = WDRequestHelper[type];
			var fio:IFlashIslandEventObject = func(payload,prodData,toPlantType,fuaaa) as IFlashIslandEventObject;
			if(fio is ChangePlantRequest){
				if(this is LoadPerVehicleTaskRendererMediator){
					fio["fromView"]="LV";
				} else if(this is AssignedLoadsTaskRendererMediator){
					fio["fromView"]="AL";
				} else {
					fio["fromView"]="LO";
				}
			}
			
			GanttServiceReference.dispatchIslandEvent(fio);
		}
		public function getSelectedGanttTask(label:String):GanttTask{
			var task:GanttTask = getGanttTask();
			for (var i:int = 0 ; i < task.getContainer().length ; i ++ ){
				var temp:GanttTask = task.getContainer().getItemAt(i) as GanttTask; 
				if (getExtraInfoIfOverlap(temp) == label){
					return temp;
				}
			}
			return task;
		}
		
		
		/*
		Obtiene una referencia de este taskItem
		*/
		protected function getGanttTask():GanttTask {
			
			if (getView() != null  && getView()["data"] != null){
				return getView()["data"]["data"] as GanttTask;
			}
			return null;
		}
		
		protected function getTaskItem():TaskItem{
			if (getView() != null  && getView()["data"] != null) {
				return getView()["data"] as TaskItem;
			}
			return null;
		}
		
		
		
		public function doShowData(task:*):void {
			Alert.show(ReflectionHelper.object2AS(task));
		}
		
		/*private function createTooltip(event:ToolTipEvent):void{
			event.toolTip=this.createTip(event.currentTarget.toolTip)
		}
		
		private function createTip(tip:String):ExtendToolTip{
			var imageToolTip:ExtendToolTip = new ExtendToolTip();
			//for embeded image
			//imageToolTip.ImageTip = tipImg;
			//for external image
			imageToolTip.ImageTip = "http://maps.googleapis.com/maps/api/staticmap?center=Berkeley,CA&zoom=14&size=90x90&sensor=false";
			//imageToolTip.TipText = getTooltiper().toolTip;
			if (getTooltiper() != null ){
				imageToolTip.TipText = getTooltip();
			}
			return imageToolTip;
		}*/
		
		public	var imageToolTip:ExtendToolTip = null
		protected function onMouseOverHandler(event:MouseEvent):void
		{
			
			imageToolTip=new ExtendToolTip();
			
			if(this.imageToolTip.haytooltip){
				imageToolTip.destroyToolTip();
			}
			//imageToolTip.ad
			//for embeded image
			//imageToolTip.ImageTip = tipImg;
			//for external image
			imageToolTip.ImageTip = "http://maps.googleapis.com/maps/api/staticmap?center=Monterrey,CA&zoom=14&size=90x90&sensor=false";
			//imageToolTip.TipText = getTooltiper().toolTip;
			imageToolTip.TipText = getTooltip();
			//	showTimeTooltip(imageToolTip);
			imageToolTip.createCustomTooltip(imageToolTip);
			
		}
		protected function onMouseOutHandler(event:MouseEvent):void
		{
			imageToolTip.mouseOverToolTipFlag=false
		}
		
	
		
		
		
	}
}