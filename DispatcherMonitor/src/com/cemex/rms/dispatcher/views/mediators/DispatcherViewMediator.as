package com.cemex.rms.dispatcher.views.mediators
{
	import com.adobe.fiber.runtime.lib.DateTimeFunc;
	import com.cemex.rms.common.date.FlexDateHelper;
	import com.cemex.rms.common.filters.FilterCondition;
	import com.cemex.rms.common.filters.FilterHelper;
	import com.cemex.rms.common.gantt.GanttResource;
	import com.cemex.rms.common.gantt.GanttTask;
	import com.cemex.rms.common.helpers.ContextMenuHelper;
	import com.cemex.rms.common.helpers.FullScreenHelper;
	import com.cemex.rms.common.lcds.AbstractReceiverService;
	import com.cemex.rms.common.logging.ILogger;
	import com.cemex.rms.common.logging.LoggerFactory;
	import com.cemex.rms.common.reflection.ReflectionHelper;
	import com.cemex.rms.common.services.config.IServiceModel;
	import com.cemex.rms.common.services.events.ServiceEvent;
	import com.cemex.rms.common.transformer.IDataTransformer;
	import com.cemex.rms.common.tree.GenericNode;
	import com.cemex.rms.common.tree.ResourceHierarchicalData;
	import com.cemex.rms.common.utils.DictionaryMap;
	import com.cemex.rms.common.views.GenericMediator;
	import com.cemex.rms.dispatcher.constants.ExtrasConstants;
	import com.cemex.rms.dispatcher.constants.Logger;
	import com.cemex.rms.dispatcher.constants.OTRConstants;
	import com.cemex.rms.dispatcher.constants.ParamtersConstants;
	import com.cemex.rms.dispatcher.constants.TVARVCConstants;
	import com.cemex.rms.dispatcher.constants.TVARVCPreffixes;
	import com.cemex.rms.dispatcher.constants.ValidateParameters;
	import com.cemex.rms.dispatcher.events.SearchBtnClickViewEvent;
	import com.cemex.rms.dispatcher.events.TogglePlaintViewEvent;
	import com.cemex.rms.dispatcher.events.lcds.LCDSReconnectEvent;
	import com.cemex.rms.dispatcher.helpers.ColorHelper;
	import com.cemex.rms.dispatcher.helpers.GanttServiceReference;
	import com.cemex.rms.dispatcher.helpers.TimeZoneHelper;
	import com.cemex.rms.dispatcher.services.flashislands.events.FlashIslandsDataEvent;
	import com.cemex.rms.dispatcher.services.flashislands.events.ReturnLogEvent;
	import com.cemex.rms.dispatcher.services.flashislands.impl.DispatcherIslandHelper;
	import com.cemex.rms.dispatcher.services.flashislands.impl.DispatcherIslandImpl;
	import com.cemex.rms.dispatcher.services.flashislands.requests.dispatch.ClearMessagesArea;
	import com.cemex.rms.dispatcher.services.flashislands.requests.dispatch.SearchOrderVehicleRequest;
	import com.cemex.rms.dispatcher.services.flashislands.requests.dispatch.testComponent1;
	import com.cemex.rms.dispatcher.services.flashislands.vo.ColorSetting;
	import com.cemex.rms.dispatcher.services.flashislands.vo.Equipment;
	import com.cemex.rms.dispatcher.services.flashislands.vo.ProductionData;
	import com.cemex.rms.dispatcher.services.lcds.events.PlantOperativeModeLCDSEvent;
	import com.cemex.rms.dispatcher.services.lcds.events.ProductionStatusLCDSEvent;
	import com.cemex.rms.dispatcher.services.lcds.events.SalesOrderLCDSEvent;
	import com.cemex.rms.dispatcher.services.lcds.events.VehicleLCDSEvent;
	import com.cemex.rms.dispatcher.views.DispatcherView;
	import com.cemex.rms.dispatcher.views.assignedLoads.transformers.AssignedLoadsDataTransformer;
	import com.cemex.rms.dispatcher.views.components.BottomTabNavigator;
	import com.cemex.rms.dispatcher.views.components.CanvasColor;
	import com.cemex.rms.dispatcher.views.components.HTMLToolTip;
	import com.cemex.rms.dispatcher.views.components.TestWindowSession;
	import com.cemex.rms.dispatcher.views.components.searchProductionOrders;
	import com.cemex.rms.dispatcher.views.loadsPerOrder.LoadPerOrderPlantOrderResourceRenderer;
	import com.cemex.rms.dispatcher.views.loadsPerOrder.transformers.LoadPerOrderOrderDataTransformer;
	import com.cemex.rms.dispatcher.views.loadsPerOrder.transformers.LoadPerOrderPlantOrderDataTransformer;
	import com.cemex.rms.dispatcher.views.loadsPerOrder.transformers.LoadsPerOrderAbstractDataTransformer;
	import com.cemex.rms.dispatcher.views.loadsPerOrder.tree.LoadsPerOrderResource;
	import com.cemex.rms.dispatcher.views.loadsPerVehicle.transformers.LoadPerVehiclePlantVehicleDataTransformer;
	import com.cemex.rms.dispatcher.views.loadsPerVehicle.tree.LoadsPerVehicleResource;
	import com.cemex.rms.dispatcher.views.reports.PlantReport;
	import com.cemex.rms.dispatcher.views.reports.ReportConfig;
	import com.cemex.rms.dispatcher.views.reports.VehicleReport;
	import com.cemex.rms.dispatcher.views.reports.ViewReportObject;
	import com.cemex.rms.dispatcher.vo.OrderLoad;
	
	import flash.display.Graphics;
	import flash.events.ContextMenuEvent;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.system.System;
	import flash.text.TextField;
	import flash.utils.Dictionary;
	import flash.utils.Timer;
	import flash.utils.getTimer;
	
	import mx.binding.utils.BindingUtils;
	import mx.collections.ArrayCollection;
	import mx.collections.HierarchicalCollectionView;
	import mx.collections.IHierarchicalCollectionViewCursor;
	import mx.collections.Sort;
	import mx.collections.SortField;
	import mx.containers.Canvas;
	import mx.containers.ViewStack;
	import mx.controls.Alert;
	import mx.controls.TextInput;
	import mx.controls.ToolTip;
	import mx.core.IFlexDisplayObject;
	import mx.core.IToolTip;
	import mx.core.UIComponent;
	import mx.events.EffectEvent;
	import mx.events.FlexEvent;
	import mx.formatters.CurrencyFormatter;
	import mx.managers.PopUpManager;
	import mx.managers.ToolTipManager;
	import mx.utils.ObjectUtil;
	
	import ilog.gantt.GanttSheetEvent;
	import ilog.gantt.GanttSheetEventReason;
	import ilog.gantt.TimeControllerGetter;
	import ilog.utils.TimeUnit;
	
	import weborb.data.Database;
	
	/**
	 * 
	 * DispatcherViewMediator es la clase mediador que le da funcionalidad a la vista, tambien
	 *  se encarga de hacer que los botones y la funcionalidad externa funcionen
	 * a demas que esta clase es la que se encarga de recibir los datos externos
	 * Es decir esta clase se encarga de afectar los datos y la vista del gantt
	 * 
	 * 
	 */
	public class DispatcherViewMediator extends GenericMediator {
		
		public function DispatcherViewMediator() {
			super();
			
		}
		
		
		/**
		 * La vista 
		 */
		[Inject]
		public var view : DispatcherView;
		
		
		
		
		/**
		 * 
		 */
		private var rawData:String="Init";
		
		
		/**
		 * the logger
		 */
		private var logger:ILogger = LoggerFactory.getLogger("DispatcherViewMediator");
		
		
		/**
		 * The date selected by the user
		 */
		private var baseDate:Date;
		
		
		// 
		
		/**
		 * Timer Display Objects content
		 */
		private var content:UIComponent;
		
		/**
		 * Timer Display Objects
		 */
		private var timeIndicators:UIComponent;
		
		/**
		 * Timer Display Objects Indicator
		 */
		private var timeIndicatorTimer:Timer;
		
		/**
		 * Timer Display Objects Controller
		 */
		public static var timeController:Object; 
		
		/**
		 * this var controls all the tasks instances
		 **/
		public static var taskInstances:Dictionary;
		
		/**
		* this var controls the instance of this class
		**/
		public static var currentInstance:DispatcherViewMediator;
		
		/**
		 * This method will get the View of this mediator 
		 */
		
		
		
		override public function getView():UIComponent{
			return view;
		}
		
		
		
		/**
		 * This method is the first executed
		 */
		override public function onRegister():void {
			
			
			// SEccion de eventos del mouse
			init();
			ToolTipManager.toolTipClass = HTMLToolTip;
			currentInstance=this;
			// SE settean los valores para que se cambie de pantalla
			view.loadsPerOrderLabel.name	= TVARVCPreffixes.LOADS_PER_ORDER_VIEW_ID;
			view.loadsPerVehicleLabel.name	= TVARVCPreffixes.LOADS_PER_VEHICLE_VIEW_ID;
			view.assignedLoadsLabel.name	= TVARVCPreffixes.ASSIGNED_LOADS_VIEW_ID;
			
			// SE registran los eventos de usuario
			view.zoomin.addEventListener(MouseEvent.CLICK,zoomin);
			view.zoomout.addEventListener(MouseEvent.CLICK,zoomout);
			view.showAll.addEventListener(MouseEvent.CLICK,showAll);
			view.refresh.addEventListener(MouseEvent.CLICK,refresh);
			view.currentTime.addEventListener(MouseEvent.CLICK,gotoCurrentTime);
			view.riseEvent.addEventListener(MouseEvent.CLICK,riseEventMediator);
			
			// SEccion de eventos Varios
			view.viewStack.addEventListener(Event.CHANGE,changeViewHandler);
			
			view.gantt.ganttSheet.addEventListener(GanttSheetEvent.ITEM_EDIT_END,itemEditEnd);
			/*view.gantt.ganttSheet.addEventListener(GanttSheetEvent.ITEM_EDIT_BEGIN,itemChangeAsDragAndDropBegin);*/
			view.gantt.ganttSheet.addEventListener(GanttSheetEvent.VISIBLE_TIME_RANGE_CHANGE,visibleTimeChanged);
			view.gantt.ganttSheet.addEventListener(MouseEvent.MOUSE_DOWN,cancelAllMovements);
			view.gantt.ganttSheet.addEventListener(MouseEvent.MOUSE_UP,continueAllMovements);
			view.gantt.ganttSheet.addEventListener(GanttSheetEvent.ITEM_EDIT_MOVE,itemIsBeingMoved);
			view.gantt.ganttSheet.taskBackToFrontSortFunction=taskSort;
			view.stage.addEventListener(KeyboardEvent.KEY_DOWN,addKeyForSearch)
			//eventos para crear pushes
			view.nu.addEventListener(MouseEvent.CLICK,seekInTreeView);
			view.del.addEventListener(MouseEvent.CLICK,borrarPush);
			// Collect Order Filtering 
			view.collect.addEventListener(MouseEvent.CLICK,collectFilter);
			view.cleanMsg.addEventListener(MouseEvent.CLICK,clearMessages);
			view.loadWithOutRecipe_cb.addEventListener(MouseEvent.CLICK,filterRecipes);
			
			
			
			// -------------SEccion de eventos -----
			/// Eventos de Plash Island
			eventMap.mapListener(eventDispatcher,FlashIslandsDataEvent.FLASHISLAND_DATA_READY,islandDataEventHandler);
			eventMap.mapListener(eventDispatcher,FlashIslandsDataEvent.FLASHISLAND_DATA_FAKE_PUSH,islandDataEventFake);
			eventMap.mapListener(eventDispatcher,FlashIslandsDataEvent.FLASHISLAND_DATA_CONFIRM,islandDataConfirmEventHandler);
			eventMap.mapListener(eventDispatcher,ReturnLogEvent.RETURN_LOG_EVENT,returnLogEvent);
			eventMap.mapListener(eventDispatcher,SearchBtnClickViewEvent.SEARCH_BTN_CLICK_EVENT,seekInTreeView);
			
			
			// Eventos de LCDS (Push)
			eventMap.mapListener(eventDispatcher,SalesOrderLCDSEvent.LCDS_DATA_RECEIVED,gotPushFromSalesOrder);
			eventMap.mapListener(eventDispatcher,VehicleLCDSEvent.LCDS_DATA_RECEIVED,gotPushFromVehicle);
			eventMap.mapListener(eventDispatcher,ProductionStatusLCDSEvent.LCDS_DATA_RECEIVED,gotPushFromProductionStatus);
			eventMap.mapListener(eventDispatcher,PlantOperativeModeLCDSEvent.LCDS_DATA_RECEIVED,gotPushFromPlantOperativeMode);
			
			// Eventos de pantalla especiales
			eventMap.mapListener(eventDispatcher,TogglePlaintViewEvent.TOGGLE_PLAIN_VIEW,togglePlainView);
			
			
			
			// Seccion del Timer
			content = view.gantt.ganttSheet.getChildByName("content")as UIComponent;
			timeIndicators = content.getChildByName("timeIndicators")as UIComponent;
			timeController = TimeControllerGetter.getTimeController(view.gantt.ganttSheet);
			
			
			timeIndicatorTimer = new Timer(0);
			timeIndicatorTimer.addEventListener(TimerEvent.TIMER,timeChanged)
			timeIndicatorTimer.start();
			refreshTime.addEventListener(TimerEvent.TIMER, doRefresh);
			
			// Click derecho
			FullScreenHelper.prepareFlashIslandFullScreen();
			ContextMenuHelper.addContextMenuOption("Clean log Mar 26 2013 RC", cleanLog);
			ContextMenuHelper.addContextMenuOption("Copy Log Mar 26 2013 RC", copyLog);
			ContextMenuHelper.addContextMenuOption("Activate event mapping",activateMapping);
			ContextMenuHelper.addContextMenuOption("Activated test mode:"+AbstractReceiverService.isTestMode, activeTestMode);
			ContextMenuHelper.addContextMenuOption("Activated data logging:"+DispatcherIslandImpl.dataLogging, activeDataLogging);
			ContextMenuHelper.addContextMenuOption("Create new name session",createSession);
			ContextMenuHelper.addContextMenuOption("Search production orders",searchProduction);
			ContextMenuHelper.addContextMenuOption("Copy Received Data",copyReceivedData);
			ContextMenuHelper.addContextMenuOption("Copy Data", copyToClipboard);
			ContextMenuHelper.addContextMenuOption("Toggle Full Screen View",fullScreenMenuHandler);
			tracetip=ToolTipManager.createToolTip("Loading time2:",100,100,"errorTipAbove") as ToolTip;
			tracetip.visible=false;
		}
		public var showNoRecipesOnly:Boolean=false;
		public function filterRecipes(evt:MouseEvent):void{
			DispatcherIslandImpl.masterKey=true;
			showNoRecipesOnly=!showNoRecipesOnly;
			requestRefresh();
		}
		public function riseEventMediator(evt:MouseEvent):void{
			var fakeEvent:testComponent1=new testComponent1();
			GanttServiceReference.dispatchIslandEvent(fakeEvent);
		}
		public function activateMapping(event:ContextMenuEvent):void{
			CemexDispatcherMonitor.activateMap=!CemexDispatcherMonitor.activateMap;
		}
		public static var tracetip:ToolTip=null;
		public function itemIsBeingMoved(evt:GanttSheetEvent):void{
			var itemmoved:Object=evt;
			//tracetip.text="Loading time:"+evt.item.startTime.toString();
			var calculatedDate:Date=timeController.getTime(evt.item.payload.graphicReference.x);
			tracetip.text=calculatedDate.toString();
			/*if(tracetip==null){
			tracetip.text="Loading time:"+evt.item.startTime.toString();
			//tracetip=ToolTipManager.createToolTip(evt.item.startTime.toString(),evt.item.x,evt.item.y) as ToolTip;
			//tracetip.setStyle("styleName","errorTipAbove");
			
			//this.view.gantt.dataGridBack.errorString="I am an error message";
			} else {
			tracetip.x=evt.item.x;
			tracetip.text=evt.item.startTime.toString();
			}
			trace(evt.item.startTime);*/
		}
		public function addKeyForSearch(evt:KeyboardEvent):void{
			eventDispatcher.dispatchEvent(evt);
		}
		private function seekInTreeView(evt:SearchBtnClickViewEvent):void{
			var searchEvent:SearchOrderVehicleRequest=new SearchOrderVehicleRequest();
			if(currentTransformer is AssignedLoadsDataTransformer){
				searchEvent.view="AL";
			} else if(LoadPerVehiclePlantVehicleDataTransformer){
				searchEvent.view="LV";
			} else {
				searchEvent.view="OL";
			}
			seekInTreeViewRunner(evt.searchString,evt.refTextInput);
		}
		public var cursores:Array=[];
		private function seekInTreeViewRunner(labl:String,ref:TextInput):void{
			cursores=[];
			var totalCursors:int=0;
			var dataCursor:IHierarchicalCollectionViewCursor = view.gantt.dataGridBack.dataProvider.createCursor();
			while (dataCursor.current != null)
			{
				totalCursors++;
				var added:Boolean=false;
				if(dataCursor.current["label"].toString().toLowerCase().indexOf(labl.toLowerCase())!=-1 || dataCursor.current["id"].toString().toLowerCase().indexOf(labl.toLowerCase())!=-1){
					//view.gantt.dataGridBack.selectedItem(dataCursor.current);
					added=true;
					cursores.push(dataCursor["index"]);
				}
				if(!added && dataCursor.current["labelField"]!="plant" && dataCursor.current["label"]!=ExtrasConstants.VEHICLE_DISPONIBLE && dataCursor.current["label"]!=ExtrasConstants.VEHICLE_ASSIGNED  && dataCursor.current["label"]!=ExtrasConstants.LOAD_UNASSIGNED){
					//buscar el vehiculo y/o la orden
					for(var res:int=0;res<dataCursor.current._children.length;res++){
						if((dataCursor.current._children.getItemAt(res).payload.equipNumber.toLowerCase().indexOf(labl.toLowerCase())!=-1
							|| dataCursor.current._children.getItemAt(res).payload.equipLabel.toLowerCase().indexOf(labl.toLowerCase())!=-1
							|| dataCursor.current._children.getItemAt(res).payload.orderLoad.toLowerCase().indexOf(labl.toLowerCase())!=-1)){
							cursores.push(dataCursor["index"]);
						}
					}
				}
				dataCursor.moveNext();
			}
			if(labl!=""){
				view.gantt.dataGridBack.scrollToIndex(cursores[0]);
				view.gantt.dataGridBack.selectedIndices=cursores;
				//var scrollIndexPosition:int=(view.gantt.dataGridBack.maxVerticalScrollPosition/totalCursors)*cursores[0];
				//view.gantt.dataGridBack.verticalScrollPosition=int(scrollIndexPosition);
			} else {
				view.gantt.dataGridBack.selectedIndices=[];
			}
			
			ref.setFocus();
			ref.setSelection(ref.text.length,ref.text.length);
		}
		private function activeTestMode(evt:ContextMenuEvent):void{
			AbstractReceiverService.isTestMode=!AbstractReceiverService.isTestMode;
		}
		private function activeDataLogging(evt:ContextMenuEvent):void{
			DispatcherIslandImpl.dataLogging=!DispatcherIslandImpl.dataLogging;
		}
		private function createSession(evt:ContextMenuEvent):void{
			//PopUpManager.createPopUp(view,TestWindowSession,true);
			PopUpManager.centerPopUp(PopUpManager.createPopUp(view,TestWindowSession,true));
		}
		private function searchProduction(evt:ContextMenuEvent):void{
			var ventana:searchProductionOrders=PopUpManager.createPopUp(view,searchProductionOrders,true) as searchProductionOrders;
			ventana.flexContext=currentTransformer.getTree().ctx;   
			PopUpManager.centerPopUp(ventana);
			
		}
		public static var isUsingPush:Boolean=false;
		public static var isDragging:Boolean=true;
		public function cancelAllMovements(evt:MouseEvent):void{
			DispatcherViewMediator.isDragging=false;
			var now:Date=TimeZoneHelper.getServer();
			if (isSameAsBaseDate(now) ){
				currentTransformer.timeChanged(now);
			}
			logger.debug("CANCELA MOVS");
		}
		public function continueAllMovements(evt:MouseEvent):void{
			logger.debug("CONTINUA MOVS");
			DispatcherViewMediator.isDragging=true;
		}
		/**
		 * Este metodo permite saber cuando un task renderer esta siendo cambiado por drag and drop
		 * esto permite que ejecutemos o no la animacion que corre cuando se cambian las fechas de los renders
		 * 
		 **/
		public function itemChangeAsDragAndDrop(evt:GanttSheetEvent):void{
			return;
		}
		
		/**
		 * Metodo para saber cuando empieza a editarse un item por drag&drop
		 *
		 **/
		public function itemChangeAsDragAndDropBegin(evt:GanttSheetEvent):void{
			DispatcherViewMediator.isDragging=true;
		}
		
		/**
		 * metodo que permite limpiar los mensajes del area de notificacion
		 * se manda un evento a WD para decirle que los limpie
		 * */
		public function clearMessages(evt:MouseEvent):void{
			DispatcherIslandHelper.ordersWithError=[];
			view.logMessage.dataProvider=new ArrayCollection();
			GanttServiceReference.dispatchIslandEvent(new ClearMessagesArea());
		}
		/**
		 * Sort de los elementos
		 * **/
		public function taskSort(value:Array):Array{
			//se puede usar el startTime como ratificador
			var resultados:Array=new Array();
			/*for(var q:int=value.length-1;q>-1;q--){
				if(value[q]["data"]["payload"]["workAroundDestroy"]=="DELE"){
					value.splice(q,1);
				}
			}*/
			for(var q:int=0;q<value.length;q++){
				//recorrer para recalcular
				var indice:int=0;
				for(var w:int=0;w<resultados.length;w++){
					var res:int=ObjectUtil.dateCompare(value[q].startTime,resultados[w].startTime);
					if(res==-1){
						indice=w;
					}
				}
				resultados.splice(indice,0,value[q]);
				
			}
			//run the results in case we need to invert due to an unassign
			
			return resultados;
		}
		/**
		 * mock de pushes
		 * /
		 * */
		public function crearNuevoPush(evt:MouseEvent):void{
			/*if(seekAndDestroy(createDummySalesOrder(),createDummySalesOrder().plant,createDummySalesOrder().equipStatus,createDummySalesOrder().orderNumber)==3){
			this.addData(createDummySalesOrder(),GanttTask.STEP_INITIAL);
			}*/
			//move2CurrentTime(false);
			var mheader:Object=new Object();
			mheader["equipStatus"]=ExtrasConstants.VEHICLE_ASSIGNED;
			mheader["plant"]="D084";
			mheader["plantId"]="D084";
			currentTransformer.addHeader(mheader);
			mheader["equipStatus"]=ExtrasConstants.VEHICLE_DISPONIBLE;
			mheader["plant"]="D084";
			mheader["plantId"]="D084";
			currentTransformer.addHeader(mheader);
			mheader["equipStatus"]=ExtrasConstants.LOAD_UNASSIGNED;
			mheader["plant"]="D084";
			mheader["plantId"]="D084";
			currentTransformer.addHeader(mheader);
		}
		public function borrarPush(evt:MouseEvent):void{
			if(seekAndDestroy(createDummySalesOrder("Pedro","15:35:00","D074"),createDummySalesOrder("Pedro","15:35:00","D074").plant,createDummySalesOrder("Pedro","15:35:00","D074").equipStatus,createDummySalesOrder("Pedro","15:35:00","D074").orderNumber)==1){
				this.addData(createDummySalesOrder("Pedro","15:35:00","D074"),GanttTask.STEP_INITIAL);
			}
		}
		
		
		public function fullScreenMenuHandler(event:ContextMenuEvent) : void{  
			FullScreenHelper.toggleFullScreen();
		}
		public function copyReceivedData(event:ContextMenuEvent) : void{  
			
		}
		
		public function cleanLog(e:ContextMenuEvent):void{
			/*System.setClipboard(
			ReflectionHelper.object2XML(logging,"Log")
			);*/
			/*ContextMenuHelper.cleanMenu();
			Logger.inDebug=!Logger.inDebug;
			if(Logger.inDebug){
			ContextMenuHelper.addContextMenuOption("Deactivate Log", cleanLog);
			} else {
			ContextMenuHelper.addContextMenuOption("Activate Log", cleanLog);
			}
			ContextMenuHelper.addContextMenuOption("Copy Log", copyLog);*/
			//ContextMenuHelper.addContextMenuOption("Copy Received Data",copyReceivedData);
			//ContextMenuHelper.addContextMenuOption("Copy Data", copyToClipboard);
			//ContextMenuHelper.addContextMenuOption("Toggle Full Screen View",fullScreenMenuHandler);
			logging.removeAll();
		}
		
		
		
		/**
		 * Este metodo valida que la fecha seleccionada por el usuario sea la misma que la fecha que recibe como parametro
		 * 
		 */
		public function isSameAsBaseDate(now:Date):Boolean {
			var uno:Boolean=(FlexDateHelper.getDateString(now,"YYYY/MM/DD") == FlexDateHelper.getDateString(baseDate,"YYYY/MM/DD"));
			if(DispatcherIslandImpl.pushBaseDate==null){
				return false;
			}
			var dos:Boolean=(new Date()).date==DispatcherIslandImpl.pushBaseDate.date;
			return uno && dos;
		}
		
		public var lastRenderedMinute:String="";
		public var lastRenderedSeconds:String="";
		
		/**
		 * ESte metodo es el que permite hacer acciones cuando cambia el tiempo
		 * y va a mandar a llamar a diferentes metodos segun se requiera hacer actualizaciones de los datos
		 * @reference  processTimeChangedEveryEvent
		 * @reference  processTimeChangedEverySeconds
		 * @reference  processTimeChangedEveryMinute
		 */
		public function timeChanged(e:TimerEvent=null):void{
			
			//var now:Date = new Date();
			var now:Date=TimeZoneHelper.getServer();
			if (isSameAsBaseDate(now)){//&& isDataLoaded){
				processTimeChangedEveryEvent(now);
				now.seconds = int(now.seconds /10) * 10;
				if (FlexDateHelper.parseTimeString(now) != lastRenderedSeconds){
					processTimeChangedEverySeconds(now);
					lastRenderedSeconds = FlexDateHelper.parseTimeString(now);
				}
				now.setSeconds(0);
				if (FlexDateHelper.parseTimeString(now) != lastRenderedMinute){
					processTimeChangedEveryMinute(now);
					lastRenderedMinute = FlexDateHelper.parseTimeString(now);
				}
			}
		}
		public static var visibleTimeChange:Boolean=false;
		public function visibleTimeChanged(e:GanttSheetEvent):void{
			//var now:Date = new Date();
			
			var now:Date=TimeZoneHelper.getServer();
			if (isSameAsBaseDate(now) ){
				if (currentTransformer != null){
					currentTransformer.timeChanged(now);
				}
			}
		}
		
		
		
		/**
		 * Este metodos se ejecuta cada Evento que tiene el Timer
		 * @reference timeChanged
		 */
		public function processTimeChangedEveryEvent(time:Date):void {
			drawTimeIndicators();
		}
		
		/**
		 * Este metodos se ejecuta cada Minuto
		 * @reference timeChanged
		 */
		public function processTimeChangedEveryMinute(time:Date):void {
			
			if(currentTimeFlag){
				move2CurrentTime(false);
				
				/*var delayStartTime:Date = new Date();
				delayStartTime.setSeconds(0);
				
				view.gantt.ganttSheet.visibleTimeRangeStart		= FlexDateHelper.getDateWithHours(delayStartTime,delayStartTime.getHours(),delayStartTime.getMinutes(),0);
				view.gantt.ganttSheet.minVisibleTime			= FlexDateHelper.getDateWithHours(delayStartTime,delayStartTime.getHours(),delayStartTime.getMinutes(),0);
				*/
			}
			if (isSameAsBaseDate(time) ){
				currentTransformer.timeChanged(time);
			}
		}
		
		
		/**
		 * Este metodos se ejecuta cada 10 segundos que tiene el Timer
		 * @reference timeChanged
		 */
		public function processTimeChangedEverySeconds(time:Date):void {
			
		}
		
		
		public var lastRenderedDate:String = "";
		
		
		
		
		/**
		 * Este metodo agenda el siguiente timer que se necesita
		 * @reference timeChanged
		 */
		private function scheduleTimeIndicatorTimer():void
		{
			var loc1:Date = new Date();
			timeIndicatorTimer.delay = ilog.utils.TimeUnit.MINUTE.milliseconds - (loc1.seconds * 1000 + loc1.milliseconds);
			timeIndicatorTimer.start();
			return;
		}
		
		
		/**
		 * Este metodo dibuja las lineas que se necesiten 
		 * 
		 */
		private function drawTimeIndicators():void {
			
			if (timeIndicators && timeController && timeController.configured) {
				var g:Graphics=timeIndicators.graphics;
				g.clear();
				if (isCurrentDateData){
					drawLine(g,0,0x000099);
					drawLine(g,getPlantVisibilityLineMinutes(),0x990000);
				}
			}
		}
		
		/**
		 * esta funcio regresa el valor que tenga el Paramtero PLANT_VIS_AREA_L
		 */
		public function getPlantVisibilityLineMinutes():Number{
			return GanttServiceReference.getParamNumber(ParamtersConstants.PLANT_VIS_AREA_L);
		}
		
		/**
		 * Este metodo regresa las coordenadas de cierta fecha 
		 * 
		 */
		public static function getCoordinate(date:Date):Number{
			return timeController.getCoordinate(date) as Number
		} 
		
		/**
		 * Este metodo dibuja sobre el lienzo uuna linea deacuerdo a los parametros que se obtienen
		 */
		private var lcounter:int=0;
		public function drawLine(g:Graphics,minutes:int,color:Number,thinkness:int=2):void{
			if(currentTransformer!=null && lcounter==50){
				currentTransformer.sortView();
				lcounter=0;
			}
			lcounter++;
			//var timeController:Object = TimeControllerGetter.getTimeController(view.gantt.ganttSheet);
			//var fecha:Date = view.gantt.ganttSheet.calendar.floor(FlexDateHelper.addMinutes(new Date(),minutes), ilog.utils.TimeUnit.MINUTE, 1);
			var fechaZonaHorariaOffSet:Date=TimeZoneHelper.getServer();
			//fechaZonaHorariaOffSet=FlexDateHelper.addSeconds(fechaZonaHorariaOffSet,getTimer()/1000);
			DispatcherViewMediator.endDate=FlexDateHelper.addMinutes(fechaZonaHorariaOffSet,minutes);
			var posicion:Number = timeController.getCoordinate(FlexDateHelper.addMinutes(fechaZonaHorariaOffSet,minutes)) as Number;
			if (posicion >= 0 && posicion < timeIndicators.width) 
			{
				g.lineStyle(thinkness,color , view.gantt.ganttSheet.getStyle("currentTimeIndicatorAlpha"));
				g.moveTo(posicion, 0);
				g.lineTo(posicion, timeIndicators.height);
			}
		}
		
		//////////////////
		//  funciones de estatus de Servicios
		//////////////////
		// Funciones de ayuda para dibujar
		
		[Bindable] [Embed(source="/assets/s_s_ledg.gif")] private var green:Class;             
		[Bindable] [Embed(source="/assets/s_s_ledr.gif")] private var red:Class;
		[Bindable] [Embed(source="/assets/s_s_ledy.gif")] private var yellow:Class;            
		
		/**
		 * Este metodo valida que icono se quiere pintar, pero esto es para los Eventos de los servicios
		 */
		override public function getIcon(status:String):Class {
			if (status == CONNECTED){
				return green;
			}
			else if (status == FAULT){
				return yellow;
			}
			else {
				return red;
			}
		}
		
		/**
		 * Este metodo regresa el modelos de los servicios, 
		 * para que se dibuje los estatus de
		 */
		override public function getModel():IServiceModel{
			return GanttServiceReference.getModel();
		}
		
		
		
		
		/**
		 * Este metodo es el que nos dice que etiqueta se debe pintar 
		 * Segun los datos de la OTR
		 */
		public function getLabel(label:String):String{
			return GanttServiceReference.getLabel(label);
		}
		/**
		 * Este metodo actualiza los servicios
		 * 
		 */
		override protected function serviceReady(e:ServiceEvent):void{
			super.serviceReady(e);
			setLabels(e.service.config.name);
		}
		
		/**
		 * ESte metodo settea las etiquetas deacuerdo al idioma en que se haya entrado, 
		 * 
		 * Este metodo se ejecuta en la carga inicial cuando el servicio esta listo
		 * 
		 */
		private var colorsSet:Boolean=false;
		public function setLabels(service:String):void{
			
			if (service == "DispatchIsland"){
				
				
				view.zoomin.toolTip = getLabel(OTRConstants.ZOOM_IN_TOOLTIP);
				view.zoomout.toolTip = getLabel(OTRConstants.ZOOM_OUT_TOOLTIP);
				view.showAll.toolTip = getLabel(OTRConstants.SHOW_ALL_TOOLTIP);
				view.currentTime.toolTip = getLabel(OTRConstants.CURRENT_TIME_TOOLTIP);
				view.refresh.toolTip = getLabel(OTRConstants.REFRESH_TOOLTIP);
				view.loadsPerOrderLabel.label = getLabel(OTRConstants.LOADS_PER_ORDER_HEADER);
				view.loadsPerVehicleLabel.label = getLabel(OTRConstants.LOADS_PER_VEHICLE_HEADER);
				view.assignedLoadsLabel.label = getLabel(OTRConstants.ASSIGNED_LOADS_HEADER);
				//view.loadWithOutRecipe_cb.label=getLabel(OTRConstants.SHOW_NO_RECIPE);
				view.collect.label=getLabel(OTRConstants.COLLECT_ORDERS);
				view.currentCostLabel.text = getLabel(OTRConstants.CURRENT_COST_LABEL);
				view.currentCostCurr.text = getLabel(OTRConstants.CURRENT_COST_CURRENCY);
				
				view.optimalCostLabel.text = getLabel(OTRConstants.OPTIMAL_COST_LABEL);
				view.optimalCostCurr.text = getLabel(OTRConstants.OPTIMAL_COST_CURRENCY);
				
				view.warning.text = getLabel(OTRConstants.WARNING);
				view.logMessage.text = "";
				
				
				
				GanttServiceReference.setCollectFilterStatus(GanttServiceReference.getParamNumber(ParamtersConstants.COLLECT_LOAD_DIS) != 0);
				view.collect.selected = GanttServiceReference.getCollectFilterStatus();
				
				ValidateParameters.validateOTR();
				ValidateParameters.validateParams();
				ValidateParameters.validateTVARV();
				
			}
			if(!colorsSet){
				var coloresNuevos:ArrayCollection=GanttServiceReference.getColorPalette() as ArrayCollection;
				for(var q:int=0;q<coloresNuevos.length;q++){
					var canvasc:CanvasColor=new CanvasColor();
					view.colorReference.colores.addChild(canvasc);
					canvasc.setStyle("backgroundColor",coloresNuevos.getItemAt(q).FILL_COLOR);
					canvasc.setStyle("borderThickness",2); 
					canvasc.setStyle("borderColor",coloresNuevos.getItemAt(q).BORDER_COLOR);
					canvasc.setStyle("borderStyle","solid");
					//canvasc.texto.text=coloresNuevos.getItemAt(q).DESCRIPTION;
					canvasc.toolTip=coloresNuevos.getItemAt(q).DESCRIPTION+"\n"+coloresNuevos.getItemAt(q).CONCEPT;
					canvasc.addEventListener(MouseEvent.MOUSE_OVER,cambiarColor);
				}
				//view.colorReference.addEventListener(MouseEvent.ROLL_OUT,resetColors);
				colorsSet=true;
			}
		
		}
		
		public function cambiarColor(evt:MouseEvent):void{
			BottomTabNavigator.currentColor=String(evt.currentTarget.getStyle("backgroundColor"));
			var ac:ArrayCollection=currentTransformer.getTasksTree();
			for(var tsk:int=0;tsk<ac.length;tsk++){
				if(ac.getItemAt(tsk).payload.graphicReference!=null){
					var uno:String=String(evt.currentTarget.getStyle("backgroundColor"));
					var dos:String=String(ac.getItemAt(tsk).payload.graphicReference.getStyle("backgroundColor"));
					
					var tres:String=String(evt.currentTarget.getStyle("borderColor"));
					var cuatro:String=(ac.getItemAt(tsk).payload.graphicReference.getStyle("borderColor"));
					
				}
			}
		}
		private function resetColors(evt:MouseEvent):void{
			var ac:ArrayCollection=currentTransformer.getTasksTree();
			for(var tsk:int=0;tsk<ac.length;tsk++){
				if(ac.getItemAt(tsk).payload.graphicReference!=null){
					ac.getItemAt(tsk).payload.graphicReference.alpha=1;
				}
			}
		}
		public function setColorToCanvas(color:ColorSetting, canvas:Canvas):void{
			
			canvas.setStyle("backgroundColor",color.fillColor); 
			//canvas.setStyle("backgroundAlpha",0.7); 
			canvas.setStyle("borderThickness",color.borderThickness); 
			canvas.setStyle("borderColor",color.borderColor);
			canvas.setStyle("borderStyle","solid");
		}
		
		////////////////////////////////////
		/// SECCION DE EVENTOS DEL MOUSE
		////////////////////////////////////
		/**
		 * Evento de volver a conectar al LCDS
		 */
		override public function clickStatus(service:String):void{
			dispatch(new LCDSReconnectEvent());
		}
		
		
		
		
		//public var collectFilterFlag:Boolean = false;
		public function collectFilter(e:MouseEvent):void{
			DispatcherIslandImpl.masterKey=true;
			//collectFilterFlag = view.collect.selected;
			GanttServiceReference.setCollectFilterStatus(view.collect.selected);
			
			
			requestRefresh();
		}
		
		
		/**
		 * Solcitud de refresh hecha por el usuario
		 * Solo el boton puede actualizar los datos usando la variable de llave maestra
		 */
		
		private var masterKey:Boolean=false;
		public function refresh(e:MouseEvent):void{
			DispatcherIslandImpl.masterKey=true;
			requestRefresh();
		}
		/**
		 * Este metodo es un refresh por otra causa que no es explicitamente la solicitud del usuario 
		 */
		public function requestRefresh():void{
			
			GanttServiceReference.refreshData();
		}
		
		/**
		 * Esta funcion es para mostrar en pantalla todas las cargas que estan visibles
		 */
		public function showAll(e:MouseEvent):void{
			view.gantt.ganttSheet.showAll();			
		}
		/**
		 * Esta es la solicitud del usuario de hacer zoom in
		 * Si se tiene apretado el boton de Control hace todo el zoomin que se pueda
		 */
		public function zoomin(e:MouseEvent):void{
			
			var factor:Number  = 0.5;
			if (e.ctrlKey){
				factor=0.001;
			}
			if (isCurrentDateData){
				//view.gantt.ganttSheet.zoom(factor, new Date(), true );
				view.gantt.ganttSheet.zoom(factor, null, true );
			}
			else {
				view.gantt.ganttSheet.zoom(factor, null, true );
			}
			//var now:Date = new Date();
			var now:Date=TimeZoneHelper.getServer();
			if (isSameAsBaseDate(now) ){
				currentTransformer.timeChanged(now);
			}
		} 
		
		/**
		 * Esta es la solicitud del usuario de hacer zoom out
		 * Si se tiene apretado el boton de Control hace todo el zoomou que se pueda
		 */
		public function zoomout(e:MouseEvent):void{
			
			var factor:Number  = 2;
			if (e.ctrlKey){
				factor=1000;
			}
			if (isCurrentDateData){
				view.gantt.ganttSheet.zoom(factor, null, true );
				view.gantt.ganttSheet.addEventListener(EffectEvent.EFFECT_END,endAnimation)
				//view.gantt.ganttSheet.zoom(factor, new Date(), true );
			}
			else {
				view.gantt.ganttSheet.zoom(factor, null, true );
			}
			//var now:Date = new Date();
			var now:Date=TimeZoneHelper.getServer();
			if (isSameAsBaseDate(now) ){
				currentTransformer.timeChanged(now);
			}
		}
		
		private function endAnimation(evt:EffectEvent):void{
			logger.debug("efecto terminado");
		}
		
		private var currentTimeFlag:Boolean=true;
		
		/**
		 * Este evento cacha cuando el usuario ya no quiere que e mueva automaticamente la pantalla
		 */
		public function gotoCurrentTime(e:MouseEvent):void{
			currentTimeFlag = !currentTimeFlag;
			if (currentTimeFlag){
				//view.gantt.ganttSheet.moveTo(
				move2CurrentTime(true);
			}
		}
		
		
		
		
		
		
		
		/**
		 * Esta funcion se manda a llamar cuando se termina
		 */
		private function itemEditEnd(e:GanttSheetEvent):void{
			DispatcherViewMediator.isDragging=false;
			if (e.reason == GanttSheetEventReason.CANCELLED) {
				var task:GanttTask = e.item as GanttTask;
				addData(task.data,GanttTask.STEP_MOVED);
			}
		}
		
		
		
		
		
		
		
		
		
		public function copyToClipboard(e:ContextMenuEvent):void {
			if (currentTransformer != null && currentTransformer.getResourcesTree() != null){
				System.setClipboard(
					"<Data>\n"+
					rawData+
					"<tasksTree>\n"+
					ReflectionHelper.object2XML(currentTransformer.getTasksTree(),"tasksTree","  ")+
					"</tasksTree>\n"+
					"<resourcesTree>\n"+
					ReflectionHelper.object2XML(currentTransformer.getResourcesTree(),"resourcesTree","  ")+
					"</resourcesTree>\n"+
					"</Data>"
				);
			}
		}
		
		public function returnLogEvent(e:ReturnLogEvent):void{
			for(var q:int=0;q<e.returnLog.length;q++){
				if(e.returnLog.getItemAt(q).toString().indexOf("ERROR")!=-1){
					e.returnLog.setItemAt(e.returnLog.getItemAt(q)+" "+getLabel(OTRConstants.DISP_ERROR_DELIVERY_DATE),q);
				}
			}
			view.logMessage.dataProvider = e.returnLog.source.reverse();
			
		}
		
		// Funciones 
		public function islandDataConfirmEventHandler(e:FlashIslandsDataEvent):void{
			addDatas(e.data.loads,GanttTask.STEP_MOVED);
		}
		
		/**
		 * Se cambia el modo operativo en los ervicios y despues se manda a 
		 */
		public function gotPushFromPlantOperativeMode(e:PlantOperativeModeLCDSEvent):void {
			DispatcherViewMediator.isUsingPush=true;
			if(Logger.inDebug){
				Logger.pushPlantOperativeCounter++;
				//view.gotPush.text="PUSH PLANT "+e.plantId;//+"  "+((new Date().minutes)+""+(new Date().seconds))+"/"+Logger.pushPlantOperativeCounter+"/"+Logger.pushProduction+"/"+Logger.pushSalesOrderCounter+"/"+Logger.pushVehicleCounter;
				Logger.incomingPush("Plant operative push",e.plantId,this.view)
			}
			if(GanttServiceReference.getPlantInfo(e.plantId)){
				
				GanttServiceReference.setPlantType(e.plantId,e.zoperativeMode);
				view.gantt.dataGridBack.invalidateDisplayList();
				view.gantt.dataGridBack.invalidateList();
				/*view.gantt.dataGridBack.invalidateProperties();
				view.gantt.dataGridBack.invalidateSize();*/
				//var now:Date = new Date();
				var now:Date=TimeZoneHelper.getServer();
				if (isSameAsBaseDate(now) ){
					currentTransformer.timeChanged(now);
				}
			}
			view.gantt.dataGridBack.invalidateDisplayList();
			view.gantt.dataGridBack.invalidateList();
			
		}
		public static var issales:Boolean=false;
		public function createDummySalesOrder(nom:String="PAULA HERNANDEZ",fecha:String="15:10:00",planta="D072"):OrderLoad{
			var var1:OrderLoad=new OrderLoad();
			var1.cycleTimeMillis=16500;
			var1.cycleTime="04:35:00";
			var1.soldToName="N-tek";
			var1.orderVolume=17;
			//var1.startTime=new Date(2011,8,2,8);
			var1.materialId="000000000010000792";
			var1.valueAdded="";
			var1.shipConditions="01";
			var1.loadFrequency="0";
			var1.mepTotcost=102125.8;
			var1.cpQuantity=0;
			var1.loadingDate=new Date(2011,8,20);
			var1.loadingTime=fecha;
			var1.loadingTimestamp=new Date(2011,8,20);
			var1.unloadingTime="15:50:00"
			var1.vehicleMaxVol=7;
			var1.totCost=89241.5;
			var1.loadNumber="L1";
			var1.itemCategory="ZTC1";
			var1.idHlevelItem="";
			var1.hlevelItem=1000;
			var1.additionalFlag=false;
			var1.contactName1=nom;
			var1.orderReason="";
			var1.contactTel="";
			var1.cpDescription="";
			var1.shipmentStatus="";
			var1.constructionProduct="";
			var1.itemNumber="1001";
			var1.jobSiteId="0065010066";
			var1.paytermLabel="";
			var1.timeFixIndicator="";
			var1.orderStatus="INPC";
			var1.itemCurrency="MXN";
			var1.contactName2="";
			var1.equipNumber="";
			var1.loadUom="M3";
			var1.deliveryGroup=1;
			var1.deliveryTime="03:43:00"
			var1.firstDescription="CCO-1-100-28-1";
			var1.firstDeliveryTime="03:43:00";
			
			var1.orderNumber="0000031150";
			var1.hlevelItem=1000;
			var1.confirmQty=7;
			var1.orderUom="M3";
			var1.jobSiteName="RESIDENCIAL CUITLAHUAC (SQC)";
			var1.postalCode="65670";
			var1.isDelayed=false;
			var1.remainFlag=false;
			var1.orderLoad="0000031150-L1";
			var1.houseNoStreet="CUITLAHUAC 134";
			var1.firstOrderNumber="0000031150";
			var1.city="TLALPAN";
			var1.deliveryAmount="";
			var1.cpUom="";
			var1.loadStatus="NOST";
			var1.equipStatus=ExtrasConstants.LOAD_UNASSIGNED;
			var1.warning="";
			var1.materialDes="CCO-1-100-28-1";
			var1.renegFlag=false;
			var1.cpOrderNumber="";
			var1.completedDelayed=false;
			var1.soldToNumber="0050160376";
			var1.plant=planta;
			var1.loadVolume=7;
			return var1;
		}
		public static var nonVisilbleItems:DictionaryMap=new DictionaryMap();
		public function gotPushFromSalesOrder(e:Object):void{
			DispatcherViewMediator.isUsingPush=true;
			var cargas:ArrayCollection=e.datas;
			if(cargas.length==0){
				return;
			}
			//determine if continue with the time stamp
			if(currentStampLoads[(cargas.getItemAt(0) as OrderLoad).orderNumber]!=null){
				if(currentStampLoads[(cargas.getItemAt(0) as OrderLoad).orderNumber]>(cargas.getItemAt(0) as OrderLoad).numberTimeStamp){
					//doesnt need to continue, is old
					return;
				} else {
					currentStampLoads[(cargas.getItemAt(0) as OrderLoad).orderNumber]=(cargas.getItemAt(0) as OrderLoad).numberTimeStamp;
				}
			}
			//Create the sort field
			/*var dataSortField:SortField = new SortField("_numericDate",false,true,true);
			
			var dataSort:Sort = new Sort();
			dataSort.fields = [dataSortField];
			cargas.sort = dataSort;
			//refresh the collection to sort
			cargas.refresh();*/
			if(Logger.inDebug){
				Logger.pushSalesOrderCounter++;
				//view.gotPush.text="PUSH SALESORDER:";//+((new Date().minutes)+""+(new Date().seconds))+"/"+Logger.pushPlantOperativeCounter+"/"+Logger.pushProduction+"/"+Logger.pushSalesOrderCounter+"/"+Logger.pushVehicleCounter;
				Logger.incomingPush("Sales Order push",(cargas.getItemAt(0) as OrderLoad).orderLoad+"/"+(cargas.getItemAt(0) as OrderLoad).orderNumber,this.view)
			}
			
			DispatcherViewMediator.issales=true;
			//var continuar:Boolean=false;
			//aqui descartar el push de las plantas que no estan permitidas
			
			
			//descartar los pedidos de bombeo que vienen por push en vista de cargas por vehiculo -- ZTX0
			//if(currentTransformer is LoadPerVehiclePlantVehicleDataTransformer){
			//var arc:ArrayCollection=currentTransformer.getTasksTree();
			for(var b:int=cargas.length-1;b>-1;b--){
				if((cargas.getItemAt(b) as OrderLoad).loadStatus=="CMPL" || (cargas.getItemAt(b) as OrderLoad).loadStatus=="DELE"){
					nonVisilbleItems.put((cargas.getItemAt(b) as OrderLoad).orderNumber+":"+(cargas.getItemAt(b) as OrderLoad).orderLoad+":"+(cargas.getItemAt(b) as OrderLoad).itemNumber,(cargas.getItemAt(b) as OrderLoad).orderNumber+":"+(cargas.getItemAt(b) as OrderLoad).orderLoad+":"+(cargas.getItemAt(b) as OrderLoad).itemNumber)
				}
				//(cargas.getItemAt(b) as OrderLoad).comingFromPush=true;
				if(((cargas.getItemAt(b) as OrderLoad).loadNumber==null || (cargas.getItemAt(b) as OrderLoad).loadNumber=="") && (cargas.getItemAt(b) as OrderLoad).itemCategory!="ZTX0"){
					(cargas.getItemAt(b) as OrderLoad).loadNumber="L"+(b+1);
					(cargas.getItemAt(b) as OrderLoad).orderLoad=(cargas.getItemAt(b) as OrderLoad).orderNumber+"-"+(cargas.getItemAt(b) as OrderLoad).loadNumber;
				} else if(((cargas.getItemAt(b) as OrderLoad).loadNumber==null || (cargas.getItemAt(b) as OrderLoad).loadNumber=="") && (cargas.getItemAt(b) as OrderLoad).itemCategory=="ZTX0"){
					(cargas.getItemAt(b) as OrderLoad).loadNumber="B";
					(cargas.getItemAt(b) as OrderLoad).orderLoad=(cargas.getItemAt(b) as OrderLoad).orderNumber+"-"+(cargas.getItemAt(b) as OrderLoad).loadNumber;
				}
				var seekAndDestroyResult:Number=-1;
				//try{
				
					seekAndDestroyResult=seekAndDestroy((cargas.getItemAt(b) as OrderLoad),(cargas.getItemAt(b) as OrderLoad).plant,(cargas.getItemAt(b) as OrderLoad).equipStatus,(cargas.getItemAt(b) as OrderLoad).orderNumber);					
				
					
				/*} catch(e3:Error){
				logger.debug("ERROR FOUND ON SEEKANDDESTROY:"+(cargas.getItemAt(b) as OrderLoad).orderNumber+(cargas.getItemAt(b) as OrderLoad).loadNumber+"\n"+e3.message);
				}*/
				//buscarlo
				//var searchitem:*=currentTransformer.getTree().ctx.get("bababa");
				//Alert.show((cargas.getItemAt(b) as OrderLoad).plant,seekAndDestroyResult.toString());
				var dateone:Number=(cargas.getItemAt(b) as OrderLoad).loadingTimestamp.time;
				var datetwo:Number=getDayStart().time;
				var datefour:Number=getDayEnd().time;
				var alg002:Date=DispatcherIslandImpl.fechaBase;
				var omitChange:Boolean=false;
				var listaPlantas:ArrayCollection=GanttServiceReference.getPlantsPlain();
				var isMyPlantHere:Boolean=false;
				for(var bb:int=0;bb<listaPlantas.length;bb++){
					if((cargas.getItemAt(b) as OrderLoad).plant==listaPlantas.getItemAt(bb).plantId){
						//se encontro la planta, se recomienda seguir
						isMyPlantHere=true;
					}
				}
				if((((cargas.getItemAt(b) as OrderLoad).itemCategory=="ZTX0" || (cargas.getItemAt(b) as OrderLoad).itemCategory=="ZRWN") && currentTransformer is LoadPerVehiclePlantVehicleDataTransformer) 
					|| seekAndDestroyResult==0 || seekAndDestroyResult==2 
					|| (((cargas.getItemAt(b) as OrderLoad).loadStatus=="ASSG" || (cargas.getItemAt(b) as OrderLoad).loadStatus=="TJST" || (cargas.getItemAt(b) as OrderLoad).loadStatus=="LDNG") && currentTransformer is LoadsPerOrderAbstractDataTransformer) 
					|| (dateone<datetwo || dateone>datefour)
					|| (GanttServiceReference.getParamString("DISP_PUMP_DFE")=="0" && ((cargas.getItemAt(b) as OrderLoad).itemCategory=="ZTX0" || (cargas.getItemAt(b) as OrderLoad).itemCategory=="ZRWN") && currentTransformer is LoadsPerOrderAbstractDataTransformer)
					|| ((cargas.getItemAt(b) as OrderLoad).equipStatus==ExtrasConstants.LOAD_UNASSIGNED && (currentTransformer is AssignedLoadsDataTransformer))
					|| ((cargas.getItemAt(b) as OrderLoad).loadStatus=="ASSG" && !(currentTransformer is LoadPerVehiclePlantVehicleDataTransformer))
					|| isRedirect(cargas.getItemAt(b) as OrderLoad)
					|| !isMyPlantHere
					|| (cargas.getItemAt(b) as OrderLoad).loadStatus=="DELE"){
					logger.debug("SALESORDER PUSH ERASE:"+(cargas.getItemAt(b) as OrderLoad).orderNumber+(cargas.getItemAt(b) as OrderLoad).loadNumber);
					logger.debug("REASON:"+seekAndDestroyResult+"  fecha es futura:"+((cargas.getItemAt(b) as OrderLoad).loadingDate.date!=DispatcherIslandImpl.pushBaseDate.date && (cargas.getItemAt(b) as OrderLoad).loadingDate.date!=getDayEnd().date));
					cargas.removeItemAt(b);
					omitChange=true;
					//this.reportPlant((cargas.getItemAt(b) as OrderLoad));
					//this.reportTruck((caras.get
					
				}
				if(!omitChange){
					for(var v:int=0;v<DispatcherViewMediator.vehiculosCorrectos.length;v++){
						//en realidad no hay q borrar nada, solo registrarlo en los encabezados
						if((cargas.getItemAt(b) as OrderLoad).equipNumber==DispatcherViewMediator.vehiculosCorrectos.getItemAt(v).equipNumber && DispatcherViewMediator.vehiculosCorrectos.getItemAt(v).equipStatus!=(cargas.getItemAt(b) as OrderLoad).equipStatus){
							logger.debug("SALESORDER PUSH VEHICLE ASSIGN:"+DispatcherViewMediator.vehiculosCorrectos.getItemAt(v).equipNumber+"   "+DispatcherViewMediator.vehiculosCorrectos.getItemAt(v).equipLabel);
							var clonVehiculo:Object=ReflectionHelper.cloneObject(DispatcherViewMediator.vehiculosCorrectos.getItemAt(v));
							
							seekAndDestroyVehicle(DispatcherViewMediator.vehiculosCorrectos.getItemAt(v),null,(cargas.getItemAt(b) as OrderLoad).equipLabel);
							clonVehiculo.equipStatus=(cargas.getItemAt(b) as OrderLoad).equipStatus;
							clonVehiculo.maintplant=(cargas.getItemAt(b) as OrderLoad).plant;
							clonVehiculo.plant=(cargas.getItemAt(b) as OrderLoad).plant;
							clonVehiculo.plantId=(cargas.getItemAt(b) as OrderLoad).plant;
							currentTransformer.addHeader(clonVehiculo);
						}
					}
				}
				//(cargas.getItemAt(b) as OrderLoad).comingFromPush=true;
				//antes de mandar las nuevas cargas comprobar que los vehiculos esten marcados como asignados
				
				/*else {
				//this.seekAndDestroy(cargas.getItemAt(b),(cargas.getItemAt(0) as OrderLoad).plant,(cargas.getItemAt(0) as OrderLoad).equipStatus,(cargas.getItemAt(0) as OrderLoad).orderNumber);
				}*/
				//}
				
			}
			/*if(cargas.length>0){
			currentTransformer.timeChanged(TimeZoneHelper.getServer());
			}*/
			//if(currentTransformer is LoadsPerOrderAbstractDataTransformer){
			//}
			//if(cargas.length>0){
			logger.debug("SALESORDER CARGAS PASAN:"+cargas.length);
			addDatas(cargas,GanttTask.STEP_MOVED);
			//requestRefresh();
			//currentTransformer.addHeaders(DispatcherViewMediator.vehiculosCorrectos);
			//actualizar los titulos de los elementos
			//if(cargas.length==0){
				currentTransformer.sortView();
			//}
			/*var llaves:ArrayCollection=currentTransformer.getTree().ctx.getAvailableKeys();
			for(var c:int=0;c<llaves.length;c++){
			if((llaves.getItemAt(c).toString().split(":").length==3 && (currentTransformer is LoadPerVehiclePlantVehicleDataTransformer)) || (llaves.getItemAt(c).toString().split(":").length==2 && !(currentTransformer is LoadPerVehiclePlantVehicleDataTransformer))){
			var resourceItem:Object=currentTransformer.getTree().ctx.get(llaves.getItemAt(c).toString());
			//ver cuantos hijos tiene y actualizar su label
			if(resourceItem._children.length>0){
			var itemLabel:String=resourceItem.label;
			
			if(resourceItem.hasOwnProperty("totalChildrenCount")){
			resourceItem.totalChildrenCount=resourceItem._children.length;
			}
			} else {
			//borrarlo del padre
			//currentTransformer.getTree().ctx.remove(llaves.getItemAt(c).toString());
			}
			//si no tiene hijos
			} 
			}*/
			//}
			//darle dos segundos
			//if (isSameAsBaseDate(TimeZoneHelper.getServer()) ){
			currentTransformer.timeChanged(TimeZoneHelper.getServer());
			//}
			abstractUpdateByTimerPush();
			view.gantt.updateExpandedItems();
			//}
		}
		private function updateByTimerPush(evt:TimerEvent):void{
			DispatcherViewMediator.isUsingPush=true;
			//view.gantt.ganttSheet.zoom(.9, null, true );
			//actualizar dataprovider
			//view.gantt.dataGridBack.dataProvider.refresh();
			//view.invalidateDisplayList();
			currentTransformer.addHeaders(DispatcherViewMediator.vehiculosCorrectos);
			/*processTimeChangedEveryMinute(TimeZoneHelper.getServer());*/
			//if (isSameAsBaseDate(TimeZoneHelper.getServer()) ){
			//move2CurrentTime(true);
			currentTransformer.timeChanged(TimeZoneHelper.getServer());
			view.gantt.ganttSheet.dispatchEvent(new MouseEvent(MouseEvent.CLICK));
			//Alert.show("Tiempo");
			
			//}
			//buscar y destruir los vacios
			//seekAndDestroyEmpty();
			//currentTransformer.sortView();
		}
		private var tcTimer:Timer;
		
		private function seekAndDestroyEmpty():void{
			var dataCursor:IHierarchicalCollectionViewCursor = view.gantt.dataGridBack.dataProvider.createCursor();
			while (dataCursor.current != null)
			{
				//if(
				//view.gantt.dataGridBack.dataProvider.removeChild( view.gantt.dataGridBack.dataProvider.getParentItem(dataCursor.current), dataCursor.current );
				dataCursor.moveNext();
			}
		}
		/*private function unregisterVehicle(equipo:Equipment):String{
			var regresa:String="";
			
		}*/
		private function seekAndDestroyVehicle(equipo:Object,planta:String=null,equipLabel:String=null):int{
			var dataCursor:IHierarchicalCollectionViewCursor = view.gantt.dataGridBack.dataProvider.createCursor();
			var regresa:int=0;
			var alg:*=DispatcherViewMediator.vehiculosCorrectos;
			
			
			if(equipo.status=="ASSN" || equipo.status=="AVLB" || equipo.status=="ATPL"){
				/*
				if context key is the same as the new key, then there will not be changes
				but if they are different, then, changes apply
				*/
				var llaves:ArrayCollection=currentTransformer.getTree().ctx.getAvailableKeys();
				var vehicleFound:String="";
				for(var c:int=llaves.length-1;c>-1;c--){
					if(llaves.getItemAt(c).toString().indexOf(equipo.equipNumber)!=-1){
						var childNode:GenericNode=currentTransformer.getTree().ctx.get(llaves.getItemAt(c).toString());
						var childParent:ArrayCollection=childNode.getParent()["_children"];
						//buscar las cargas asignadas porque este vehiculo acaba de liberarse y liberar sus cargas
						for(var qi:int=childNode["_children"].length-1;qi>-1;qi--){
							childNode["_children"].getItemAt(qi)["payload"].workAroundDestroy="DELE";
							childNode["_children"].getItemAt(qi)["data"].workAroundDestroy="DELE";
							//and erase the loads inside
							(childNode["_children"] as ArrayCollection).removeItemAt(qi);
						}
						//equipo.maintplant+":"+equipo.status+":"+equipo.equipNumber
						vehicleFound=equipo.equipNumber;
						//look for the vehicle in the parent
						for(var q:int=childParent.length-1;q>-1;q--){
							if(childParent.getItemAt(q)==childNode){
								childParent.removeItemAt(q);
							}
						}
						currentTransformer.getTree().ctx.remove(llaves.getItemAt(c).toString());
					}
				}
				if(vehicleFound!="" || equipo.equipNumber!=""){
					for(var d:int=DispatcherViewMediator.vehiculosCorrectos.length-1;d>-1;d--){
						if(DispatcherViewMediator.vehiculosCorrectos.getItemAt(d)["equipNumber"]==vehicleFound || DispatcherViewMediator.vehiculosCorrectos.getItemAt(d)["equipNumber"]==equipo.equipNumber){
							DispatcherViewMediator.vehiculosCorrectos.removeItemAt(d);
						}
					}
				}
			}
			
			
			var eraseItem:ArrayCollection=currentTransformer.getResourcesTree();
			/*
			If the tree is probably contracted, the vehicle will not be seeing, for this case
			lets search in the context
			*/
			while (dataCursor.current != null)
			{
				if(dataCursor.current["label"].toString()==ExtrasConstants.VEHICLE_DISPONIBLE){
					
				}
				if(dataCursor.current["label"].toString().indexOf(equipLabel)!=-1){
						currentTransformer.getTree().ctx.remove(dataCursor.current.payload["plant"]+":"+dataCursor.current.payload["equipStatus"]+":"+equipo.equipNumber);
						//comparar contra la planta, si NO esta en la misma planta o su estatus es DIFERENTE
						if((dataCursor.current as LoadsPerVehicleResource).id!=equipo.maintplant+":"+equipo.status+":"+equipo.equipNumber){
							//buscarlo y quitarlo de los vehiculos correctos
							
							
							view.gantt.dataGridBack.dataProvider.removeChild( view.gantt.dataGridBack.dataProvider.getParentItem(dataCursor.current), dataCursor.current );
							//buscar y destruirlo del mapa interno
						}
				}
				dataCursor.moveNext();
			}
			return regresa;
		}
		/**
		 * Regresa 0 cuando el item/carga se encuentra ya creado y solo se actualizaron sus datos-done
		 * regresa 1 cuando el item se encuentra ya creado pero en un nivel diferente, eso es, se borra del nivel anterior
		 * regresa 2 cuando el item se encuentra en una planta que no esta en la vista o que se cancelo /borrado completo
		 * regresa 3 cuando el item no se encontro-done
		 * */
		
		private function seekAndDestroy(item:OrderLoad,planta:String=null,equip:String=null,lablToErase:String=null):Number{
			//buscar el dataprovider
			
			var regresa:Number=-1;
			var isPlantPresent:Boolean=false;
			var listaPlantas:ArrayCollection=GanttServiceReference.getPlantsPlain();
			for(var b:int=0;b<listaPlantas.length;b++){
				if(item.plant==listaPlantas.getItemAt(b).plantId){
					//se encontro la planta, se recomienda seguir
					regresa=3;
					isPlantPresent=true;
				}
			}
			if(regresa==-1){
				//no se encontro en las cargas, asi q hay q borrarlo
				regresa=2;
			}
			var currentListObject:Object;
			var currentListIndex:int=0;
			var dataCursor:IHierarchicalCollectionViewCursor=null;
			try{
				dataCursor = view.gantt.dataGridBack.dataProvider.createCursor();
			} catch(e){
				dataCursor=null;
				logger.debug("ERROR ON DATACURSOR:"+planta+":"+equip+":"+lablToErase);
			}
			if(dataCursor==null){
				return regresa;
			}
			var alg001:Boolean=false;
			while (currentListIndex<currentTransformer.getTree().ctx.getAvailableKeys().length/*dataCursor.current != null*/)
			{
				currentListObject=currentTransformer.getTree().ctx.get(currentTransformer.getTree().ctx.getAvailableKeys().getItemAt(currentListIndex).toString());
				/*if (dataCursor.currentDepth < maxDepth){
				dataProvider.openNode(dataCursor.current);*/
				/*if((dataCursor.current["id"] as String)==lablToErase){
				//borrar ese elemento
				}*/
				
				//if(dataCursor.current["plant"]==planta && lablToErase==dataCursor.current["label"].indexOf(lablToErase)!=-1){
				var fullID:String=String(planta+":"+equip+":"+lablToErase);
				var shortID:String=String(planta+":"+lablToErase);
				
				if(currentTransformer is LoadsPerOrderAbstractDataTransformer){
					fullID=String(planta+":"+equip+":"+lablToErase);
					shortID=String(planta+":"+lablToErase);
				} else if(currentTransformer is LoadPerVehiclePlantVehicleDataTransformer){
					fullID=String(planta+":"+equip+":"+item.equipNumber);
					shortID=String(planta+":"+equip+":"+lablToErase);
				} else if(currentTransformer is AssignedLoadsDataTransformer){
					fullID=String(planta)+":"+item.loadNumber;
					shortID=String(planta)+":"+item.loadNumber;
				}
				var dateone:Number=item.loadingTimestamp.time;
				var datetwo:Number=getDayStart().time;
				var datethree:Number=getDayEnd().time;
				//when a vehicle has more than one charge, we need to remove every single one identifying the view
				if(/*dataCursor.current*/currentListObject is LoadsPerVehicleResource){
					if(/*dataCursor.current.labelField*/currentListObject.labelField=="equipNumber"){
						// buscar en los hijos y ver si ya no esta asignado
						var truckChildren:ArrayCollection=currentListObject._children/*dataCursor.current._children*/;
						for(var cin:int=truckChildren.length-1;cin>-1;cin--){
							if(((truckChildren.getItemAt(cin).payload as OrderLoad).orderNumber==item.orderNumber && (truckChildren.getItemAt(cin).payload as OrderLoad).itemNumber==item.itemNumber) && (item.equipNumber=="" || (item.codStatusProdOrder=="80" && item.itemCategory=="ZTC1" && item.loadStatus=="CMPL"))){
								//borrar
								updateContext(item,currentListObject["equipNumber"]/*dataCursor.current["equipNumber"]*/);
								regresa=1;
							}
						}
					}
					var vehi:ArrayCollection=DispatcherViewMediator.vehiculosCorrectos;
				}
				if((currentListObject["id"]/*dataCursor.current["id"]*/==fullID || currentListObject["id"]/*dataCursor.current["id"]*/==shortID) 
					&& (item.loadStatus!="CNCL" && item.orderStatus!="CNCO" && item.orderStatus!="CMPO") 
					&& (dateone>=datetwo && dateone<=datethree)){
					regresa=0;
					//actualizar en dataCursor.current["payload"]
					
					//si son el mismo pegar valores y actualizar el current transformer y currenttree
					//buscar en sus children
					var childs:ArrayCollection=(currentListObject/*dataCursor.current*/ as GanttResource).getChildren();
					//in order to get updated the sort function
					if(childs!=null){
						for(var q:int=0;q<childs.length;q++){
							//com.cemex.rms.common.gantt
							if(childs.getItemAt(q) is GanttTask){
								var it:GanttTask=childs.getItemAt(q) as GanttTask;
								if((item.itemNumber==(it["payload"] as OrderLoad).itemNumber || item.loadNumber==(it["payload"] as OrderLoad).loadNumber) && item.orderNumber==(it["payload"] as OrderLoad).orderNumber/* && item.numberTimeStamp>(it["payload"] as OrderLoad).numberTimeStamp*/){
									//to do apply erase changes
									//borrar el cambio pues puede ocacionar errores
									
									//currentTransformer.getTree().ctx.remove(dataCursor.current["id"]);
									alg001=true;
									var cumsFromPush:Boolean=false;
									var expandedIndex:int=(it["payload"] as OrderLoad).indexPosition;
									eraseSpecialCaseContext(item);
									if((it["payload"] as OrderLoad).loadingTime!=item.loadingTime){
										cumsFromPush=true;
										
									}
									ReflectionHelper.copyParameters(item,it["payload"]);
									it.data=item;
									(it["payload"] as OrderLoad).indexPosition=expandedIndex;
									if(cumsFromPush){
										item.comingFromPush=true;
										(it["payload"] as OrderLoad).comingFromPush=true;
									}
									(dataCursor.current as GanttResource).minDateNumber=item._numericDate;
								}
							} else if(childs.getItemAt(q) is GanttResource){
								var its:GanttResource=childs.getItemAt(q) as GanttResource;
								if((item.itemNumber==its["payload"].itemNumber || item.loadNumber==its["payload"].loadNumber) /*&& item.numberTimeStamp>(it["payload"] as OrderLoad).numberTimeStamp*/){
									//currentTransformer.getTree().ctx.remove(dataCursor.current["id"]);
									alg001=true;
									eraseSpecialCaseContext(item);
									ReflectionHelper.copyParameters(item,its["payload"]);
									its.data=item;
									if(its["payload"].loadingTime!=item.loadingTime){
										item.comingFromPush=true;
										its["payload"].comingFromPush=true;
										
									}
								}
								(currentListObject/*dataCursor.current*/ as GanttResource).minDateNumber=item._numericDate;
							}
							//if(item.numberTimeStamp>(it["payload"] as OrderLoad).numberTimeStamp){
								ReflectionHelper.copyParameters(item,currentListObject["payload"]/*dataCursor.current["payload"]*/);
							/*} else {
								alg001=true;
								regresa=0;
							}*/
						}
					}
					if(!alg001){
						//no se encontro y comunicamos que se agregue
						regresa=3;
					}
					
				} else if(currentListObject/*dataCursor.current*/["label"].toString().indexOf(lablToErase)!=-1){
					//se encuentra en una planta que no tenemos pero que antes estuvo aqui
					regresa=2;
					if((item.loadStatus=="ASSG" || item.loadStatus=="LDNG" || item.loadStatus=="TJST") && (currentTransformer is AssignedLoadsDataTransformer)){
						return regresa;
					}
					
					if(isPlantPresent){
						regresa=1;
					}
					
					var isItemPresent:int=0;
					//necesitamos ver si en efecto fue un cambio de planta, qizas es una partida viviendo en otra planta
					//primero buscamos si existe esa otra partida en la otra planta, si existe ya llegaremos a ella
					//si no existe fue un cambio de planta y hay que borrarla de donde la encontremos
					if(!(dateone>=datetwo && dateone<=datethree)){
						isItemPresent=updateContext(item,"?",true);
					} else {
						isItemPresent=updateContext(item,"?",false);
					}
					
					if(isItemPresent==0){
						//ya no se encontro el elemento
						//si no se encuentra pero tiene estatus asignado o TJST, significa que lo cambiaron de vehiculo
						if(item.loadStatus=="ASSG" || item.loadStatus=="TJST"){
							//regresa 1 que es presente en planta, pongamoslo
							regresa=1;
							if(alg001){
								regresa=0;
							}
						} else {
							regresa=0;
						}
					} else if(isItemPresent==1) {
						regresa=0;
						/*if(isPlantPresent){
							regresa=1;
						}*/
					} else if(isItemPresent==2){
						regresa=3;
					}
					if(item.orderStatus=="CNCO" || item.orderStatus=="CMPO"){
						regresa=2;
						//buscar las llaves
						
						
						//LA SIGUIENTE LINEA BORRA TODO UN PEDIDO, se reepondran al llegar las cargas del pedido
						try{
							
							var childs:ArrayCollection=(currentListObject/*dataCursor.current*/ as GanttResource).getChildren();
							if(childs!=null){
								for(var q:int=0;q<childs.length;q++){
									//com.cemex.rms.common.gantt 
									var it:GanttTask=childs.getItemAt(q) as GanttTask;
									this.lastOptimalCost=this.lastOptimalCost-(it["payload"] as OrderLoad).mepTotcost;
									this.lastCostFormat=this.lastCostFormat-(it["payload"] as OrderLoad).totCost;
									//currentTransformer.getTree().ctx.remove((it["payload"] as OrderLoad).plant+":"+(it["payload"] as OrderLoad).equipStatus+":"+item.orderNumber);
									//ReflectionHelper.copyParameters(item,dataCursor.current["payload"]);
								}
							}
							//no borramos los de cargas asignadas
							
							if(!(currentTransformer is AssignedLoadsDataTransformer)){
								//antes de destruirlo completamente, hay que verificar que la carga itemNumber
								//-si no esta el itemNumber, no hay por que borrarlo, no afecta
								//-si esta el item number hay que quitarl el itemNumber
								view.gantt.dataGridBack.dataProvider.removeChild( view.gantt.dataGridBack.dataProvider.getParentItem(currentListObject/*dataCursor.current*/), currentListObject/*dataCursor.current*/ );
								logger.debug("SEEKANDDESTROY:"+planta+":"+equip+":"+lablToErase);
							} else {
								currentListObject./*dataCursor.current.*/children.removeItemAt(0);
							}
							//si se borra quitamos su costo
						} catch(e2:Error){
							
							logger.debug("ERROR ON SEEKANDDESTROY:"+e2.message);
						}
					}
					if(!isPlantPresent){
						updateContext(item);
					}
				}
				currentListIndex++;
				//dataCursor.moveNext();
			}
			
			//dataProvider.refresh();
			if (isSameAsBaseDate(TimeZoneHelper.getServer()) ){
				move2CurrentTime(false);
			}
			
			
			if((item.loadStatus=="CMPL" || item.loadStatus=="CNCL" || item.orderStatus=="CNCO" || item.orderStatus=="CMPO" || (item.orderStatus=="" && item.loadStatus=="")) && regresa==3){
				regresa=2;
			}
			//antes de continuar verificar que no este en el contexto
			
			return regresa;
		}
		public function eraseSpecialCaseContext(item:Object):void{
			if(((item as OrderLoad).loadStatus=="ASSG"
				|| (item as OrderLoad).loadStatus=="TJST"
				|| (item as OrderLoad).loadStatus=="LDNG"
				|| (item as OrderLoad).loadStatus=="DELE"
				|| isRedirect(item as OrderLoad))
				&& currentTransformer is LoadsPerOrderAbstractDataTransformer){
				updateContext(item);	
			} else if((item as OrderLoad).loadStatus=="CMPL"
				|| isRedirect(item as OrderLoad)){
				updateContext(item);
			}
		}
		public function updateContext(item:Object,numEquipoDummy:String="?",erase:Boolean=true):int{
			var llaves:ArrayCollection=currentTransformer.getTree().ctx.getAvailableKeys();
			var isItemPresent:int=0;
			for(var n:int=0;n<llaves.length;n++){
				//si encontramos el numero de pedido buscamos a quien debemos borrar
				if(llaves.getItemAt(n).indexOf(item.orderNumber)!=-1 || llaves.getItemAt(n).indexOf(numEquipoDummy)!=-1){
					//a) buscar el elemento a nivel de orden y quitarlo del nivel de orden
					var resourceObjectinit:Object=currentTransformer.getTree().ctx.get(llaves.getItemAt(n).toString());
					for(var ch:int=resourceObjectinit._children.length-1;ch>-1;ch--){
							if((resourceObjectinit._children.getItemAt(ch).payload as OrderLoad).itemNumber.indexOf(item.itemNumber)!=-1 && (resourceObjectinit._children.getItemAt(ch).payload as OrderLoad).orderNumber.indexOf(item.orderNumber)!=-1 /*&& item.numberTimeStamp>(resourceObjectinit._children.getItemAt(ch).payload as OrderLoad).numberTimeStamp*/){
								//quitarlo del nivel de la orden
								if(!erase){
									if((resourceObjectinit._children.getItemAt(ch).payload as OrderLoad).plant!=item.plant || (resourceObjectinit._children.getItemAt(ch).payload as OrderLoad).equipStatus!=item.equipStatus){
										(resourceObjectinit._children.getItemAt(ch).payload as OrderLoad).workAroundDestroy="DELE";
										//fue un cambio de planta y por tal motivo, hay que eliminarlo
										resourceObjectinit._children.removeItemAt(ch);
										isItemPresent=2;
									} else {
										isItemPresent=0;
										
									}
								} else {
									resourceObjectinit._children.removeItemAt(ch);
									isItemPresent=1;
								}
								//regresa=0;
								if(resourceObjectinit.hasOwnProperty("totalChildrenCount")){
									resourceObjectinit.totalChildrenCount=resourceObjectinit._children.length;
								}
							}
						
					}
					if(resourceObjectinit._children.length==0){
						//borrar todo el elemento
						
						//b) buscarlo en el padre y borrarlo del padre
						var idString:String=llaves.getItemAt(n).substr(0,String(llaves.getItemAt(n)).lastIndexOf(":"));
						var resourceObject:Object=currentTransformer.getTree().ctx.get(idString);
						var confirmErase:Boolean=false;
						for(var ch:int=resourceObject._children.length-1;ch>-1;ch--){
							//commented ItemNumber to allow erasing the parent node
							var obj:Object=resourceObject._children.getItemAt(ch);
							if(resourceObject._children.getItemAt(ch).payload.orderNumber==item.orderNumber && !(currentTransformer is AssignedLoadsDataTransformer) /*&& item.numberTimeStamp>resourceObject._children.getItemAt(ch).payload.numberTimeStamp/* && resourceObject._children.getItemAt(ch).payload.itemNumber==item.itemNumber*/){
								resourceObject._children.removeItemAt(ch);
								confirmErase=true;
								if(numEquipoDummy!="?"){
									//search the vehicle and reassign
									for(var e:int=DispatcherViewMediator.vehiculosCorrectos.length-1;e>-1;e--){
										if(DispatcherViewMediator.vehiculosCorrectos.getItemAt(e).equipNumber==numEquipoDummy){
											DispatcherViewMediator.vehiculosCorrectos.getItemAt(e).equipStatus=ExtrasConstants.VEHICLE_DISPONIBLE;
										}
									}
								}
							} else if(obj.payload.hasOwnProperty("orderLoad")){
								if(String(obj.payload["orderLoad"]).indexOf(item.orderLoad)!=-1
									&& (currentTransformer is AssignedLoadsDataTransformer)
									/*&& item.numberTimeStamp>obj.payload["numberTimeStamp"]*/){
									confirmErase=true;
									resourceObject._children.removeItemAt(ch);
								}
							}
							//regresa=0;
						}
						if(confirmErase){
							currentTransformer.getTree().ctx.remove(llaves.getItemAt(n).toString());
						}
					}
				}
			}
			return isItemPresent;
		}
		
		public function gotPushFromVehicle(e:VehicleLCDSEvent):void{
			DispatcherViewMediator.isUsingPush=true;
			if(Logger.inDebug){
				Logger.pushVehicleCounter++;
				//view.gotPush.text="PUSH VEHICLE:"+e.vehicle//((new Date().minutes)+""+(new Date().seconds))+"/"+Logger.pushPlantOperativeCounter+"/"+Logger.pushProduction+"/"+Logger.pushSalesOrderCounter+"/"+Logger.pushVehicleCounter;
				Logger.incomingPush("Vehicle push",e.vehicle.equipment,this.view)
			}
			
			var equipment:Equipment = e.vehicle;
			//ignore if the equiment is dummy
			if(equipment.fleetNum=="RMS-DUMMY"){
				return;
			}
			for(var d:int=DispatcherViewMediator.vehiculosCorrectos.length-1;d>-1;d--){
				if(DispatcherViewMediator.vehiculosCorrectos.getItemAt(d)["equipNumber"]==equipment.equipNumber && DispatcherViewMediator.vehiculosCorrectos.getItemAt(d)["equipStatus"]==equipment.equipStatus && DispatcherViewMediator.vehiculosCorrectos.getItemAt(d)["maintplant"]==equipment.maintplant && equipment.status!="ATPL"){
					// ya existe y vehiculo tmb cambiado previamente
					return;
				}
			}
			var seekanddestroy:int=-1;
			//ignore if the 
			if(equipment.status=="AVLB" || equipment.status=="ASSN" || equipment.status=="ATPL"){
				seekanddestroy=this.seekAndDestroyVehicle(equipment,null,equipment.equipLabel);
				//buscar vehiculo y borrarlo D136:Asignadas:000000000011004264
				//this.seekAndDestroy(null,equipment.maintplant,equipment.equipLabel);
				if (GanttServiceReference.getPlantInfo(equipment.maintplant) != null){
					
					if (currentView == TVARVCPreffixes.LOADS_PER_VEHICLE_VIEW_ID){
						
						/*var plain:Object = GanttServiceReference.updateEquipment(equipment);
						currentTransformer.addHeader(plain);*/
						
						//DispatcherViewMediator.vehiculosCorrectos.addItem(plain);
						//currentTransformer.addHeader(equipment);
						/*view.gantt.dataGridBack.invalidateDisplayList();
						view.gantt.dataGridBack.invalidateList();*/
						//var vehicle:Equipment = GanttServiceReference.removeVehicle(equipment);
						//currentTransformer.deleteHeader(vehicle);
						
					}
				}
			} 
			var plain:Object=new Object();
			plain["plant"] = equipment.maintplant;
			plain["plantId"]=equipment.maintplant;
			ReflectionHelper.copySimpleParameters(equipment,plain);
			//buscar si tenemos la planta
			if(GanttServiceReference.getPlantInfo(equipment.maintplant) ==null){
				return;
			}
			if((equipment.status=="AVLB" || equipment.status=="ASSN") && (seekanddestroy==0 || seekanddestroy==3)){
				/*var plain:Object = GanttServiceReference.updateEquipment(equipment);
				currentTransformer.addHeader(plain);*/
				DispatcherViewMediator.vehiculosCorrectos.addItem(plain);
			}
			currentTransformer.addHeaders(DispatcherViewMediator.vehiculosCorrectos);
			abstractUpdateByTimerPush();
			/**/
		}
		
		private function abstractUpdateByTimerPush():void{
			if(tcTimer==null){
				tcTimer=new Timer(2000,1);
				tcTimer.addEventListener(TimerEvent.TIMER_COMPLETE,updateByTimerPush);
			}
			tcTimer.delay=2000;
			tcTimer.reset();
			tcTimer.stop();
			tcTimer.start();
		}
		
		
		public function gotPushFromProductionStatus(e:ProductionStatusLCDSEvent):void{
			DispatcherViewMediator.isUsingPush=true;
			if(Logger.inDebug){
				Logger.pushProduction++;
				//view.gotPush.text="PUSH PRODUCTION:"+e.productionData;//((new Date().minutes)+""+(new Date().seconds))+"/"+Logger.pushPlantOperativeCounter+"/"+Logger.pushProduction+"/"+Logger.pushSalesOrderCounter+"/"+Logger.pushVehicleCounter;
				Logger.incomingPush("Production push",e.productionData.aufnr,this.view)
			}
			if (currentTransformer is LoadPerVehiclePlantVehicleDataTransformer) {
				
				var productionData:ProductionData = e.productionData;
				productionData.txt04=translateProductionStatus(productionData.stonr);
				var task:GanttTask = currentTransformer.getTask(productionData);
				//identificamos el estatus y aplicamos el cambio de color
				/*
				No          Stat        Text MORADO
				
				40           QUEU   QUEUED
				
				50           INPR      IN PRODUCTION
				
				60           PCNF     PARTIAL CONFIRMATION
				
				80           CNF       FINAL CONFIRMATION
				
				No          Stat        Text Rojo
				
				70           STOP     STOP PROD ORDER
				*/
				
				
				GanttServiceReference.setProductionData(productionData);
				view.gantt.dataGridBack.invalidateDisplayList();
				view.gantt.dataGridBack.invalidateList();
				
				if(task==null){
					return;
				}
				if(productionData.txt04!="200" && productionData.txt04!="100"){
					(task.data as OrderLoad).txtStatusProdOrder=productionData.txt04;
					(task.data as OrderLoad).prodOrder=productionData.aufnr;
					(task.data as OrderLoad).codStatusProdOrder=productionData.stonr;
				}
				switch(Number(productionData.stonr)){
					case 70:
						(task.data as OrderLoad).pborderColor="0XFD675C";
						(task.data as OrderLoad).pfillColor="0XFD675C";
						break;
					case 30:
					case 40:
					case 50:
					case 60:
						(task.data as OrderLoad).pfillColor="0XEDDAF0";
						(task.data as OrderLoad).pborderColor="0XEDDAF0";
						(task.data as OrderLoad).loadStatus="LDNG";
						break;
					case 80:
						(task.data as OrderLoad).pfillColor="0XAAEEA3";
						(task.data as OrderLoad).pborderColor="0XF5F67E";
						break;
					case 20:
						(task.data as OrderLoad).pfillColor="0XC4F9C4";
						(task.data as OrderLoad).pborderColor="0XC4F9C4";
						break;
				}
				if (isSameAsBaseDate(TimeZoneHelper.getServer()) ){
					currentTransformer.timeChanged(TimeZoneHelper.getServer());
				}
			}
			
		}
		
		
		public function translateProductionStatus(STONR:String):String{
			if(STONR==null){
				return "";
			} else if(STONR==""){
				return "";
			}
			
			var translation:String="";
			if(getLabel(OTRConstants.WARNING)=="Warning"){
				//traducir
				switch(STONR){
					case "10":
						translation="CRTD";
						break;
					case "20":
						translation="REL";
						break;
					case "30":
						translation="SENT";
						break;
					case "40":
						translation="QUEU";
						break;
					case "50":
						translation="INPR";
						break;
					case "60":
						translation="PCNF";
						break;
					case "70":
						translation="STOP";
						break;
					case "80":
						translation="CNF";
						break;
				}
			} else {
				switch(STONR){
					case "10":
						translation="ABIE";
						break;
					case "20":
						translation="LIB.";
						break;
					case "30":
						translation="ENVI";
						break;
					case "40":
						translation="ENCO";
						break;
					case "50":
						translation="ENPR";
						break;
					case "60":
						translation="NOTP";
						break;
					case "70":
						translation="PARO";
						break;
					case "80":
						translation="NOTI";
						break;
				}
			}
			return translation;
		}
		public function islandDataEventFake(ev:FlashIslandsDataEvent):void{
			GenericTaskRendererMediator.isWaitingFakePush=false;
			var obj:Object=new Object();
			obj.datas=ev.data.loads
			gotPushFromSalesOrder(obj);
		}
		/**
		 * Esta funcion se manda allamar cuando los datos que llegan del Flash Island tienene que sobre escribir todos los datos
		 * 
		 * las siguientes variables controlan las pantallas para que no se regresen, no se muevan y permitan visualizar
		 */
		
		public function islandDataEventHandler(e:FlashIslandsDataEvent):void{
			//do translations
			if(DispatcherIslandImpl.currentSelector!=AbstractReceiverService.SQL_SELECTOR){
				AbstractReceiverService.SQL_SELECTOR=DispatcherIslandImpl.currentSelector;
				dispatch(new LCDSReconnectEvent());
			}
			view.cleanMsg.label=getLabel(OTRConstants.DISP_CLEAN_LOG);
			var availableKeys:ArrayCollection=LoadPerOrderPlantOrderResourceRenderer.expandableItems.getAvailableKeys();
			for(var len:int=availableKeys.length-1;len>-1;len--){
				LoadPerOrderPlantOrderResourceRenderer.expandableItems.remove(availableKeys.getItemAt(len).toString());
			}
			logger.info("islandDataEventHandler() init");
			/*if(!DispatcherIslandImpl.masterKey){
			return;
			}*/
			DispatcherIslandImpl.masterKey=false;
			setBaseDate(e.data.baseDate);
			//changeViews(currentView);
			// Set the CurrentTransformer
			updateTransformers();
			
			// SEt the time
			drawTimeIndicators();
			//move2CurrentTime(true);
			
			// prepare the DataFiltering and reporting
			setFilters();
			var cargas:ArrayCollection=e.data.loads;
			
			/*if(this.currentTransformer is LoadPerVehiclePlantVehicleDataTransformer){
			//descartar todas los pedidos de bombas de la vista de loadsPerVehicle
			for(var g:int=cargas.length-1;g>-1;g--){
			if((cargas.getItemAt(g) as OrderLoad).itemCategory=="ZTX0"){
			//borrarlo por ser de bombeo
			cargas.removeItemAt(g);
			}
			}
			}*/
			initRootNodes(cargas);
			cargas.source.sortOn("_numericDate", Array.NUMERIC);
			//cargas.source.reverse();
			// STart Adding data to the transformer
			addDatas(cargas,GanttTask.STEP_INITIAL);
			assignDataProviders();
			rawData = e.data.rawData;
			/*view.validateNow();
			view.validateDisplayList();
			view.validateProperties();
			DispatcherViewMediator.isUsingPush=true;
			currentTransformer.timeChanged(TimeZoneHelper.getServer());
			view.gantt.validateSize();*/
			// Start moving the time
			//refreshTime.start();
			//move2CurrentTime(true);
			//view.gantt.ganttSheet.showAll();
			abstractUpdateByTimerPush();
			logger.info("islandDataEventHandler() end");
			currentTransformer.sortView();
			if (currentView == TVARVCPreffixes.LOADS_PER_ORDER_VIEW_ID){
				view.gantt.ganttSheet.zoomFactor=((64*DispatcherIslandImpl.timing_lpo)/60)*1000;
			}
			else if (currentView == TVARVCPreffixes.LOADS_PER_VEHICLE_VIEW_ID){
				view.gantt.ganttSheet.zoomFactor=((64*DispatcherIslandImpl.timing_lpv)/60)*1000;
			}
			else if (currentView == TVARVCPreffixes.ASSIGNED_LOADS_VIEW_ID){
				view.gantt.ganttSheet.zoomFactor=((64*DispatcherIslandImpl.timing_alo)/60)*1000;
			}
			
		}
		
		
		
		/**
		 * Esta funcion actualiza la fecha actual configurada por el usuario pero el el 
		 * WebDynpro de tal forma que se pueda 
		 * 
		 */
		public function setBaseDate(baseDate:Date):void{
			this.baseDate = baseDate;
			logger.info("setBaseDate()   init    "+(new Date()));
			//var now:Date = new Date();
			var now:Date=TimeZoneHelper.getServer();
			var nowDateString:String = FlexDateHelper.getDateString(now,"YYYY/MM/DD");
			var baseDateString:String = FlexDateHelper.getDateString(baseDate,"YYYY/MM/DD");
			
			if (nowDateString == baseDateString) {
				
				isCurrentDateData = true;
				move2CurrentTime(true);
			}
			else {
				isCurrentDateData = false;
				view.gantt.ganttSheet.minVisibleTime			= getDayStart();
				view.gantt.ganttSheet.maxVisibleTime		 	= getDayEnd();
				view.gantt.ganttSheet.showAll();
			}
			logger.info("setBaseDate()   init    "+(new Date()));
		}
		
		
		
		
		
		
		// Funciones que cambian las vistas
		
		private var isCurrentDateData:Boolean = false;
		
		
		/**
		 * Esta funcion sirve para seleccionar el Transformer de datos que se quiere utilizar con respecto  
		 * a los botones que se hayan utilizado
		 */
		private var initialBatcherView:Boolean=false;
		public function updateTransformers():void{
			if(!initialBatcherView && DispatcherIslandImpl.isBatcher){
				currentView = TVARVCPreffixes.LOADS_PER_VEHICLE_VIEW_ID;
				initialBatcherView=true;
				view.viewStack.selectedIndex=1;
				
			}
			if (currentView == TVARVCPreffixes.LOADS_PER_ORDER_VIEW_ID){
				if (isPlain){
					currentTransformer = loadsPerHourPlain;
				}
				else {
					currentTransformer = loadsPerHourGroupedPlants;
				}
				view.gantt.ganttSheet.zoomFactor=((64*DispatcherIslandImpl.timing_lpo)/60)*1000;
			}
			else if (currentView == TVARVCPreffixes.LOADS_PER_VEHICLE_VIEW_ID){
				currentTransformer = loadsPerVehicle;
				view.gantt.ganttSheet.zoomFactor=((64*DispatcherIslandImpl.timing_lpv)/60)*1000;
			}
			else if (currentView == TVARVCPreffixes.ASSIGNED_LOADS_VIEW_ID){
				view.gantt.ganttSheet.zoomFactor=((64*DispatcherIslandImpl.timing_alo)/60)*1000;
				currentTransformer = assignedLoads;
			}
			else {
				
			}
		}
		
		/**
		 * Esta funcion calcula el limite inferior de fecha que estara visible en la sabana de desapach
		 * 
		 */
		public function getDayStart():Date {
			var returnDate:Date=null;
			var initDay:Date = FlexDateHelper.getDateWithHours(baseDate,0,0,0);
			var param:Number = GanttServiceReference.getParamNumber(ParamtersConstants.MAX_DISP_DELIVER);
			//return DateTimeFunc.dateAdd("n",-param,initDay);
			/*
			Esta linea verifica que los menus no esten bloqueados debido a la fecha
			actual
			*/
			if(!DispatcherIslandImpl.areMenusAvailable){
				returnDate=initDay;
			} else{
				returnDate=DateTimeFunc.dateAdd("n",-param,initDay);
			}
			return returnDate;
		}
		
		/**
		 * Esta funcion calcula el limite superior de fecha que estara visible en la sabana de desapach
		 * 
		 */
		public static var endDate:Date;
		public function getDayEnd():Date {
			var returnDate:Date=null;
			var endDay:Date = FlexDateHelper.getDateWithHours(baseDate,23,59,59);
			var param:Number = GanttServiceReference.getParamNumber(ParamtersConstants.MAX_DISP_FUTURE);
			/*
			Esta linea verifica que los menus no esten bloqueados debido a la fecha
			actual
			*/
			if(!DispatcherIslandImpl.areMenusAvailable){
				returnDate=endDay;
			} else{
				returnDate=DateTimeFunc.dateAdd("n",param,endDay);
			}
			
			return returnDate;
			
		}
		
		
		/**
		 * Esta funcion calcula el Settea los limites visibles segun la vista en donde este
		 * 
		 */
		public function move2CurrentTime(initial:Boolean=false):void {
			/*if(currentTransformer!=null){
				currentTransformer.sortView();
			}
*/			if (isCurrentDateData) {
				
				
				var delayStartTime:Date = TimeZoneHelper.getServer();
				//var delayStartTime:Date=new Date();
				//delayStartTime.setTime(delayStartTime.getTime()+getTimer());
				delayStartTime.setSeconds(0);
				
				
				// En base a la vista es el tiempo que se debe ver 
				if (currentView == TVARVCPreffixes.LOADS_PER_ORDER_VIEW_ID){
					
					// En base a la vista es el tiempo que se debe ver
					
					view.gantt.ganttSheet.minVisibleTime			= FlexDateHelper.getDateWithHours(delayStartTime,delayStartTime.getHours(),delayStartTime.getMinutes(),0);
					
					
					if (initial){
						view.gantt.ganttSheet.visibleTimeRangeStart		= FlexDateHelper.getDateWithHours(delayStartTime,delayStartTime.getHours(),delayStartTime.getMinutes(),0);
						view.gantt.ganttSheet.visibleTimeRangeEnd  	= FlexDateHelper.addMinutes(FlexDateHelper.getDateWithHours(delayStartTime,delayStartTime.getHours(),delayStartTime.getMinutes(),0),0);
						view.gantt.ganttSheet.maxVisibleTime		 	= getDayEnd();
						view.gantt.ganttSheet.zoom(((64*DispatcherIslandImpl.timing_lpo)/60),null,true);
						
					}
					
				}
				else if (currentView == TVARVCPreffixes.LOADS_PER_VEHICLE_VIEW_ID 
					|| currentView == TVARVCPreffixes.ASSIGNED_LOADS_VIEW_ID){
					if (initial){
						
						view.gantt.ganttSheet.minVisibleTime			= getDayStart();
						view.gantt.ganttSheet.visibleTimeRangeStart		= FlexDateHelper.getDateWithHours(delayStartTime,delayStartTime.getHours(),delayStartTime.getMinutes(),0);
						
						view.gantt.ganttSheet.maxVisibleTime		 	= getDayEnd()
						if(currentView == TVARVCPreffixes.LOADS_PER_VEHICLE_VIEW_ID){
							view.gantt.ganttSheet.zoom(((64*DispatcherIslandImpl.timing_lpv)/60),null,true);
							view.gantt.ganttSheet.visibleTimeRangeEnd  	= FlexDateHelper.addMinutes(FlexDateHelper.getDateWithHours(delayStartTime,delayStartTime.getHours(),delayStartTime.getMinutes(),0),0);
							
						} else {
							
							view.gantt.ganttSheet.visibleTimeRangeEnd  	= FlexDateHelper.addMinutes(FlexDateHelper.getDateWithHours(delayStartTime,delayStartTime.getHours(),delayStartTime.getMinutes(),0),0);
							view.gantt.ganttSheet.zoom(((64*DispatcherIslandImpl.timing_alo)/60),null,true);
						}
					}
				}
				
			}
			
		}
		
		
		
		
		private var isPlain:Boolean=false;
		
		//public var currentGrouping:int = 0;
		
		// Esta es la seccion de las vistas
		
		
		
		// Funciones para modificar datos en todos los modelos al mismo tiempo
		/**
		 * Estas variables es la vista actual del gantt
		 */
		public var currentTransformer:IDataTransformer;
		
		/**
		 * este es el transformador que hace la vista de aroles Order-> Plant 
		 */
		//public var orderPlantTransformer:IDataTransformer = new OrderPlantDataTransformer();
		
		
		/**
		 * este es el transformador que hace la vista de aroles Plant->Order 
		 */
		//public var plantOrderTransformer:IDataTransformer = new PlantOrderDataTransformer();
		
		/**
		 * este es el transformador que hace la vista de aroles Order
		 */
		//public var orderTransformer:IDataTransformer = new OrderDataTransformer();
		
		
		
		public var loadsPerHourPlain:IDataTransformer = new LoadPerOrderOrderDataTransformer();
		public var loadsPerHourGroupedPlants:IDataTransformer = new LoadPerOrderPlantOrderDataTransformer();
		
		public var loadsPerVehicle:IDataTransformer = new LoadPerVehiclePlantVehicleDataTransformer();
		
		public var assignedLoads:IDataTransformer = new AssignedLoadsDataTransformer();
		
		
		
		public var reorderOnAddData:Boolean = false;
		
		public function addData(data:Object,step:String):void{
			//this.seekAndDestroy(null);
			if (currentTransformer != null){
				//var now:Date = new Date();
				var now:Date=TimeZoneHelper.getServer();
				if (!isSameAsBaseDate(now)){
					now = null;
				}
				currentTransformer.addData(data,step,now);
				//scores.addData(data);
				if (reorderOnAddData){
					doRefreshOrder();
				}
			}
		}
		
		
		public function addDatas(datas:ArrayCollection,step:String):void{
			//updatingDatas  = true;
			
			if (currentTransformer != null){
				//now es relativo a la fecha del servidor
				//var now:Date = new Date();
				var now:Date=TimeZoneHelper.getServer();
				if (!isSameAsBaseDate(now)){
					now = null;
				}
				currentTransformer.addDatas(datas,step,now);
				
				if (reorderOnAddData){
					doRefreshOrder();
				}
				
			}
			//scores.addDatas(datas);
			
		}
		
		/* Variable que contiene los vehiculos correctos para usarlos en otros sitios como menus emergentes*/
		public static var vehiculosCorrectos:ArrayCollection;
		public static var reportesPlantas:Array;
		public static var currentStampLoads:Array=[];
		public static var reportesTrucks:Array;
		public static var visibleLoadItemsPlain:Array=new Array();
		public static var visibleVehicleItemsPlain:Array=new Array();
		public function initRootNodes(loads:ArrayCollection):void{
			visibleLoadItemsPlain=[];
			visibleVehicleItemsPlain=[];
			logger.info("initRootNodes()   init    "+(new Date()));
			DispatcherViewMediator.reportesPlantas=[];
			DispatcherViewMediator.reportesTrucks=[];
			var plants:ArrayCollection = GanttServiceReference.getPlantsPlain();
			for(var v:int=0;v<plants.length;v++){
				DispatcherViewMediator.createPlantReport(plants.getItemAt(v).plantId);
			}
			logger.info("initRootNodes()   creo reporte    "+(new Date()));
			//recorrer las cargas
			
			reports=null;
			currentTransformer.initHeaders(plants,getReports());
			
			var equipments:ArrayCollection = GanttServiceReference.getEquipmentsPlain();
			var equiposCorrectos:Array=[];
			/**
			 * Este fix permite visualizar los vehiculos correctamente
			 * y evitar que aparezcan como "undefined"
			 *
			 * */
			var buenos:Array=[];
			var buenos2:Array=[];
			
			for(var e:int=0;e<equipments.length;e++){
				equipments.getItemAt(e).equipStatus=ExtrasConstants.VEHICLE_DISPONIBLE;
				DispatcherViewMediator.createTruckReport(equipments.getItemAt(e).equipNumber);
				for(var w:int=loads.length-1;w>-1;w--){
					if(loads.getItemAt(w).equipNumber!=""){
						if(String(equipments.getItemAt(e).equipNumber).indexOf(loads.getItemAt(w).equipNumber)!=-1 || Number(equipments.getItemAt(e).equipNumber)==Number(loads.getItemAt(w).equipLabel)){
							buenos2.push(loads.getItemAt(w).equipNumber);
							equipments.getItemAt(e).maintplant=loads.getItemAt(w).plant;
							equipments.getItemAt(e).plant=loads.getItemAt(w).plant;
							equipments.getItemAt(e).plantId=loads.getItemAt(w).plant;
							break;
						}
					}
				}
			}
			logger.info("initRootNodes()   creo vehiculos    "+(new Date()));
			currentStampLoads=[];
			var visibleLoadItems:ArrayCollection=GanttServiceReference.getTVARVCSelection(TVARVCConstants.LOADS_PER_ORDER_VIEW_ID_L);
			//var visibleLoadItemsPlain:Array=new Array();
			for(var vli:int=0;vli<visibleLoadItems.length;vli++){
				visibleLoadItemsPlain.push(visibleLoadItems.getItemAt(vli)["low"]);
			}
			var visibleVehicleItems:ArrayCollection=GanttServiceReference.getTVARVCSelection(TVARVCConstants.LOADS_PER_VEHICLE_VIEW_ID_L);
			//var visibleVehicleItemsPlain:Array=new Array();
			for(var vli:int=0;vli<visibleVehicleItems.length;vli++){
				visibleVehicleItemsPlain.push(visibleVehicleItems.getItemAt(vli)["low"]);
			}
			for(var w:int=loads.length-1;w>-1;w--){
				if(currentStampLoads[(loads.getItemAt(w) as OrderLoad).orderNumber]==null){
					currentStampLoads[(loads.getItemAt(w) as OrderLoad).orderNumber]=(loads.getItemAt(w) as OrderLoad).numberTimeStamp;
				}
				(loads.getItemAt(w) as OrderLoad).txtStatusProdOrder=translateProductionStatus(loads.getItemAt(w).codStatusProdOrder);
				if((loads.getItemAt(w) as OrderLoad).txtStatusProdOrder=="100" || (loads.getItemAt(w) as OrderLoad).txtStatusProdOrder=="200"){
					(loads.getItemAt(w) as OrderLoad).txtStatusProdOrder="";
				}
				if((visibleLoadItemsPlain.indexOf((loads.getItemAt(w) as OrderLoad).loadStatus)==-1 && currentTransformer is LoadsPerOrderAbstractDataTransformer)
					|| (visibleVehicleItemsPlain.indexOf((loads.getItemAt(w) as OrderLoad).loadStatus)==-1 && currentTransformer is LoadPerVehiclePlantVehicleDataTransformer)
					|| (((loads.getItemAt(w) as OrderLoad).itemCategory=="ZTX0" || (loads.getItemAt(w) as OrderLoad).itemCategory=="ZRWN") && currentTransformer is LoadPerVehiclePlantVehicleDataTransformer)
					|| (GanttServiceReference.getParamString("DISP_PUMP_DFE")=="0" && ((loads.getItemAt(w) as OrderLoad).itemCategory=="ZTX0" || (loads.getItemAt(w) as OrderLoad).itemCategory=="ZRWN") && currentTransformer is LoadsPerOrderAbstractDataTransformer)
					|| isRedirect(loads.getItemAt(w) as OrderLoad)
					//recipes
					|| ((loads.getItemAt(w) as OrderLoad).bomValid!="1" && showNoRecipesOnly)){
					loads.removeItemAt(w);
				}
			}
			logger.info("initRootNodes()   descarto bombas    "+(new Date()));
			//hacer otro barrido para buscar los vehiculos asignados
			var pasados:Array=[];
			for(var s:int=0;s<buenos2.length;s++){
				for(var e:int=0;e<equipments.length;e++){
					if(equipments.getItemAt(e).equipNumber==buenos2[s]){
						equipments.getItemAt(e).equipStatus=ExtrasConstants.VEHICLE_ASSIGNED;						
						
					} 
				}
			}
			logger.info("initRootNodes()   acomodo arreglo    "+(new Date()));
			//y otro barrido para quitar los vehiculos que sean W y q tengan estatus diferente de asignado
			for(var e:int=equipments.length-1;e>-1;e--){
				if((equipments.getItemAt(e).equicatgry=="W" && equipments.getItemAt(e).equipStatus==ExtrasConstants.VEHICLE_DISPONIBLE) || (equipments.getItemAt(e).status!="AVLB" && equipments.getItemAt(e).equipStatus==ExtrasConstants.VEHICLE_DISPONIBLE) || (equipments.getItemAt(e).equicatgry=="Y" && equipments.getItemAt(e).equipStatus==ExtrasConstants.VEHICLE_DISPONIBLE)){
					equipments.removeItemAt(e);
				} 
			}
			DispatcherViewMediator.vehiculosCorrectos=equipments;
			var dataSortField:SortField = new SortField("turnTimeStampNumber",false,false,true);
			
			var dataSort:Sort = new Sort();
			dataSort.fields = [dataSortField];
			DispatcherViewMediator.vehiculosCorrectos.sort = dataSort;
			DispatcherViewMediator.vehiculosCorrectos.refresh();
			currentTransformer.addHeaders(equipments);
			logger.info("initRootNodes()   end    "+(new Date()));
		}
		public function isRedirect(carga:OrderLoad):Boolean{
			return ((carga.loadStatus=="TJST" || carga.loadStatus=="ASSG") && carga.constructionProduct.indexOf("P")!=-1 && carga.visible!="");
		}
		public static function createTruckReport(truckLabel:String):void{
			if(DispatcherViewMediator.reportesTrucks[truckLabel]==null && truckLabel!=""){
				DispatcherViewMediator.reportesTrucks[truckLabel]=new Object();
				DispatcherViewMediator.reportesTrucks[truckLabel].DeliveredVolume=0;
				DispatcherViewMediator.reportesTrucks[truckLabel].PendingConfirmedVolume=0;
				DispatcherViewMediator.reportesTrucks[truckLabel].PendingToBeConfirmedVolume=0;
			}
		}
		public static function createPlantReport(plantLabel:String):void{
			if(DispatcherViewMediator.reportesPlantas[plantLabel]==null){
				DispatcherViewMediator.reportesPlantas[plantLabel]=new Object();
				DispatcherViewMediator.reportesPlantas[plantLabel].ScheduledVolume=0;
				DispatcherViewMediator.reportesPlantas[plantLabel].DeliveredVolume=0;
				DispatcherViewMediator.reportesPlantas[plantLabel].ConfirmedVolume=0;
				DispatcherViewMediator.reportesPlantas[plantLabel].ToBeConfirmedVolume=0;
			}
		}
		public static function reportTruck(carga:Object):void{
			DispatcherViewMediator.createTruckReport(carga.equipNumber);
			switch(carga.loadStatus){
				case "OJOB":
				case "UNLD":
				case "TJST":
					DispatcherViewMediator.reportesTrucks[carga.equipNumber].DeliveredVolume+=carga.loadVolume;	
					break;
				case "NOST":
				case "ASSG":
				case "HOLI":
				case "LDNG":
					DispatcherViewMediator.reportesTrucks[carga.equipNumber].PendingConfirmedVolume+=carga.loadVolume;
					break;
				case "TBCL":
					DispatcherViewMediator.reportesTrucks[carga.equipNumber].PendingToBeConfirmedVolume+=carga.loadVolume;
					break;
			}
		}
		public static function reportPlant(carga:Object):void{
			DispatcherViewMediator.createPlantReport(carga.plant);
			switch(carga.loadStatus){
				case "OJOB":
				case "UNLD":
				case "TJST":
					DispatcherViewMediator.reportesPlantas[carga.plant].DeliveredVolume+=carga.loadVolume;
					DispatcherViewMediator.reportesPlantas[carga.plant].ScheduledVolume+=carga.loadVolume;
					break;
				case "NOST":
				case "ASSG":
				case "HOLI":
				case "LDNG":
					DispatcherViewMediator.reportesPlantas[carga.plant].ConfirmedVolume+=carga.loadVolume;
					DispatcherViewMediator.reportesPlantas[carga.plant].ScheduledVolume+=carga.loadVolume;
					break;
				case "TBCL":
					DispatcherViewMediator.reportesPlantas[carga.plant].ToBeConfirmedVolume+=carga.loadVolume;
					DispatcherViewMediator.reportesPlantas[carga.plant].ScheduledVolume+=carga.loadVolume;
					break;
			}
		}
		private var reports:DictionaryMap;
		public function getReports():DictionaryMap{
			if (reports == null){
				reports = new DictionaryMap();
				reports.put("",getRootReport());
				reports.put("plant",getPlantReport());
				reports.put("equipNumber",getEquipmentReport());
				
			}
			
			return reports;
		}
		
		public function getRootReport():ViewReportObject{
			var report:ViewReportObject = new ViewReportObject();
			var reportConfig:ArrayCollection =  new ArrayCollection();
			
			
			reportConfig.addItem(getReportConfig("optimalCost",	"mepTotcost"   , ReportConfig.ACCUMULATE));
			reportConfig.addItem(getReportConfig("currentCost",	"totCost", ReportConfig.ACCUMULATE));
			report.singleton = true;
			
			BindingUtils.bindProperty(this,"currentCostFormatter",report,"currentCost");
			BindingUtils.bindProperty(this,"optimalCostFormatter",report,"optimalCost");
			report.setReportsConfig(reportConfig);
			return report;
		}
		/**
		 * this function will reformat the currency in digits
		 **/
		public static var mtcost:Number
		public var lastCostFormat:Number=0;//totCost
		public function set currentCostFormatter(quantity:Number):void{
			//TOTAL COST
			//quitar decimales
			
			var num:CurrencyFormatter=new CurrencyFormatter();
			num.precision=2;
			num.currencySymbol="";
			//here we will define the user number format from backend
			if(DispatcherIslandImpl.userFormat.toLowerCase()=="x"){
				//nothing to do here
			} else if(DispatcherIslandImpl.userFormat.toLowerCase()=="y") {
				num.decimalSeparatorFrom=",";
				num.decimalSeparatorTo=",";
				num.thousandsSeparatorFrom=" ";
				num.thousandsSeparatorTo=" ";
			} else {
				num.decimalSeparatorFrom=",";
				num.decimalSeparatorTo=",";
				num.thousandsSeparatorFrom=".";
				num.thousandsSeparatorTo=".";
			}
			//aqui traemos el parametro q va a servir para mostrar
			if(DispatcherIslandImpl.getRMSCostDisplay()==1){
				view.currentCost.text=num.format(quantity);
			} else if(DispatcherIslandImpl.getRMSCostDisplay()==2){
				view.currentCost.text="";
			} else if(DispatcherIslandImpl.getRMSCostDisplay()==3){
				view.currentCost.text=num.format(quantity-DispatcherViewMediator.mtcost);
			}
			lastCostFormat=quantity;
		}
		public var lastOptimalCost:Number=0;//mepTotcost
		public function set optimalCostFormatter(quantity:Number):void{
			lastOptimalCost=quantity;
			//MEP COST
			DispatcherViewMediator.mtcost=quantity;
			var num:CurrencyFormatter=new CurrencyFormatter();
			num.precision=2;
			num.currencySymbol=""
			
			if(DispatcherIslandImpl.getRMSCostDisplay()==1){
				view.optimalCost.text=num.format(quantity);
			} else if(DispatcherIslandImpl.getRMSCostDisplay()==2){
				view.optimalCost.text="";
			} else if(DispatcherIslandImpl.getRMSCostDisplay()==3){
				view.optimalCost.text="0";
			}
		}
		
		public function getPlantReport():PlantReport{
			var report:PlantReport = new PlantReport();
			var reportConfig:ArrayCollection =  new ArrayCollection();
			
			reportConfig.addItem(getReportConfig("assignedLoads",	"loadVolume"   , ReportConfig.ACCUMULATE,		getAssignedLoadsFilter()));
			reportConfig.addItem(getReportConfig("CanceledVolume",	"loadVolume"   , ReportConfig.ACCUMULATE,		getCanceledVolumeFilter()));
			reportConfig.addItem(getReportConfig("ConfirmedVolume",	"loadVolume"   , ReportConfig.ACCUMULATE,	getConfirmedVolumeFilter()));
			reportConfig.addItem(getReportConfig("DeliveredVolume",	"loadVolume"   , ReportConfig.ACCUMULATE,	getDeliveredVolumeFilter()));
			reportConfig.addItem(getReportConfig("ScheduledVolume",	"loadVolume"   , ReportConfig.ACCUMULATE,	getScheduledVolumeFilter()));
			reportConfig.addItem(getReportConfig("ToBeConfirmedVolume",	"loadVolume"   , ReportConfig.ACCUMULATE,getToBeConfirmedVolumeFilter()));
			reportConfig.addItem(getReportConfig("UOM",				"loadUom"   , ReportConfig.KEEP_LAST_VALUE));
			report.singleton = false;
			report.setReportsConfig(reportConfig);
			var valor:*=report;
			return report;
		}
		
		public function getAssignedLoadsFilter():ArrayCollection{
			return new ArrayCollection([
				FilterHelper.getFilterCondition("loadStatus",GanttServiceReference.getTVARVCSelection(TVARVCConstants.PLANT_TOOLTIP_LOAD_FILTER_ASSIGNED),FilterCondition.EQUALS)
			]);
		}
		public function getCanceledVolumeFilter():ArrayCollection{
			return new ArrayCollection([
				FilterHelper.getFilterCondition("loadStatus",GanttServiceReference.getTVARVCSelection(TVARVCConstants.PLANT_TOOLTIP_LOAD_FILTER_CANCELED),FilterCondition.EQUALS)
			]);
		}
		public function getConfirmedVolumeFilter():ArrayCollection{
			return new ArrayCollection([
				FilterHelper.getFilterCondition("loadStatus",GanttServiceReference.getTVARVCSelection(TVARVCConstants.PLANT_TOOLTIP_LOAD_FILTER_CONFIRMED),FilterCondition.EQUALS)
			]);
		}
		public function getDeliveredVolumeFilter():ArrayCollection{
			return new ArrayCollection([
				FilterHelper.getFilterCondition("loadStatus",GanttServiceReference.getTVARVCSelection(TVARVCConstants.PLANT_TOOLTIP_LOAD_FILTER_DELIVERIED),FilterCondition.EQUALS)
			]);
		}
		public function getScheduledVolumeFilter():ArrayCollection{
			return new ArrayCollection([
				//FilterHelper.getFilterCondition("loadStatus",GanttServiceReference.getTVARVCSelection(TVARVCConstants.PLANT_TOOLTIP_LOAD_FILTER_),FilterCondition.EQUALS)
			]);
		}
		public function getToBeConfirmedVolumeFilter():ArrayCollection{
			return new ArrayCollection([
				FilterHelper.getFilterCondition("loadStatus",GanttServiceReference.getTVARVCSelection(TVARVCConstants.PLANT_TOOLTIP_LOAD_FILTER_TO_BE_CONFIRMED),FilterCondition.EQUALS)
			]);
		}
		
		public function getEquipmentReport():VehicleReport{
			var report:VehicleReport = new VehicleReport();
			var reportConfig:ArrayCollection =  new ArrayCollection();
			var o:OrderLoad;
			
			var delivered:ArrayCollection = getEquipmentDeliveredFilter();
			var confirmed:ArrayCollection = getEquipmentConfirmedFilter();
			var tobeconfirmed:ArrayCollection = getEquipmentToBeConfirmedFilter();
			
			reportConfig.addItem(getReportConfig("DeliveredLoadsCount"			,	"loadVolume"   , ReportConfig.COUNT			,delivered));
			reportConfig.addItem(getReportConfig("DeliveredVolume"				,	"loadVolume"   , ReportConfig.ACCUMULATE	,delivered));
			reportConfig.addItem(getReportConfig("PendingConfirmedLoadsCount"	,	"loadVolume"   , ReportConfig.COUNT			,confirmed));
			reportConfig.addItem(getReportConfig("PendingConfirmedVolume"		,	"loadVolume"   , ReportConfig.ACCUMULATE	,confirmed));
			reportConfig.addItem(getReportConfig("PendingToBeConfirmedLoadsCount",	"loadVolume"   , ReportConfig.COUNT			,tobeconfirmed));
			reportConfig.addItem(getReportConfig("PendingToBeConfirmedVolume"	,	"loadVolume"   , ReportConfig.ACCUMULATE	,tobeconfirmed));
			reportConfig.addItem(getReportConfig("UOM"							,	"loadUom"   , ReportConfig.KEEP_LAST_VALUE));
			report.singleton = false;
			report.setReportsConfig(reportConfig);
			return report;
		}
		
		public function getEquipmentDeliveredFilter():ArrayCollection{
			return new ArrayCollection([
				FilterHelper.getFilterCondition("loadStatus",GanttServiceReference.getTVARVCSelection(TVARVCConstants.VEHICLE_TOOLTIP_LOAD_FILTER_DELIVERIED),FilterCondition.EQUALS)
			]);
		}
		public function getEquipmentConfirmedFilter():ArrayCollection{
			return new ArrayCollection([
				FilterHelper.getFilterCondition("loadStatus",GanttServiceReference.getTVARVCSelection(TVARVCConstants.VEHICLE_TOOLTIP_LOAD_FILTER_CONFIRMED),FilterCondition.EQUALS)
			]);
		}
		public function getEquipmentToBeConfirmedFilter():ArrayCollection{
			return new ArrayCollection([
				FilterHelper.getFilterCondition("loadStatus",GanttServiceReference.getTVARVCSelection(TVARVCConstants.VEHICLE_TOOLTIP_LOAD_FILTER_TO_BE_CONFIRMED),FilterCondition.EQUALS)
			]);
		}
		
		public function getReportConfig(reportField:String,dataField:String,type:String,filters:ArrayCollection=null):ReportConfig{
			var config:ReportConfig =  new ReportConfig();
			
			//config.resourceField = resourceField;
			config.reportField = reportField;
			config.name = reportField;
			config.dataField = dataField;
			config.type = type;
			config.filters = filters;
			return config;
		}
		
		public function setFilters():void{
			logger.info("setFilters()   init    "+(new Date()));
			var filters:ArrayCollection = new ArrayCollection();
			//if (currentView != "FXD_ASL"){
			
			filters.addItem(FilterHelper.getFilterCondition("loadingTimestamp",new ArrayCollection([null]),FilterCondition.NOT_EQUALS));
			//DELICATE CHANGE COLORS
			//filters.addItem(new ArrayCollection([FlexDateHelper.getDateString(baseDate,"YYYYMMDD")]),FilterCondition.EQUALS);
			filters.addItem(FilterHelper.getFilterConditionTVARVC("orderStatus",GanttServiceReference.getTVARVCSelection(currentView+TVARVCPreffixes.ORDER_SUFFIX),FilterCondition.EQUALS));
			filters.addItem(FilterHelper.getFilterConditionTVARVC("loadStatus",GanttServiceReference.getTVARVCSelection(currentView+TVARVCPreffixes.LOAD_SUFFIX),FilterCondition.EQUALS));
			//filters.addItem(FilterHelper.getFilterCondition("loadingDate",new ArrayCollection([FlexDateHelper.getDateString(baseDate,"YYYYMMDD")]),FilterCondition.EQUALS));
			
			// Si no esta la bandera, quiere decir que se tiene que filtrar
			if(!GanttServiceReference.getCollectFilterStatus()){
				
				filters.addItem(FilterHelper.getFilterConditionTVARVC("shipConditions",GanttServiceReference.getTVARVCSelection(TVARVCConstants.COLLECT_ORDER),FilterCondition.EQUALS));
			}
			
			var displPumpService:Boolean = GanttServiceReference.getParamNumber(ParamtersConstants.DISP_PUMP_DFE) != 0 ;
			if(!displPumpService){
				new OrderLoad().itemCategory;
				filters.addItem(FilterHelper.getFilterConditionTVARVC("itemCategory",GanttServiceReference.getTVARVCSelection(TVARVCConstants.PUMP_SERVICE),FilterCondition.NOT_EQUALS));
			}
			
			var lower:Date =  getDayStart();
			var higher:Date = getDayEnd();
			filters.addItem(FilterHelper.getFilterCondition("loadingTimestamp",new ArrayCollection([lower]),FilterCondition.GREATER_THAN));
			filters.addItem(FilterHelper.getFilterCondition("loadingTimestamp",new ArrayCollection([higher]),FilterCondition.LOWER_THAN));
			currentTransformer.setFilters(filters);
			logger.info("setFilters()   init    "+(new Date()));
		}
		
		public function ordernar(e:MouseEvent):void {
			//view.gantt.resourceDataProvider.refresh();
			refreshTime.start();
		}
		
		/**
		 *  Esta funcion camia los parametros de la vista, de tal forma que 
		 * 
		 */
		public function changeViewHandler(e:Event):void{
			DispatcherIslandImpl.masterKey=true;
			var viewStack:ViewStack = e.target as ViewStack;
			var viewName:String = viewStack.selectedChild["name"];
			
			currentView = viewName;
			//changeViews(viewName);
			requestRefresh();	
			isPlain = false;
		}
		
		
		public function togglePlainView(e:TogglePlaintViewEvent):void{
			isPlain = !isPlain;
			//changeViews(currentView);
			requestRefresh();
		}
		/**
		 * Esta funcion sirve para actualizar los transformers y DataPRoviders 
		 * al mismo tiempo, uno despues de otro
		 
		 private function updateTransformerDataProvider():void{
		 
		 }
		 */
		
		
		public static var currentView:String = TVARVCPreffixes.LOADS_PER_ORDER_VIEW_ID;
		//public var currentView:String = LoadPerVehiclePlantVehicleDataTransformer.LOADS_PER_VEHICLE_VIEW_ID;
		
		
		/**
		 * Esta funcion sirve para asignarle los datos al Gantt
		 *  Utilizando el Tranformer que se haya seleccionado
		 * 
		 * 
		 */
		public static var gH:ResourceHierarchicalData;
		private function assignDataProviders():void{
			logger.info("assignDataProviders()   init    "+(new Date()));
			var dataSources:Object =  new Object();
			
			//updatingDatas = true;
			//currentTransformer.regenerate();
			//updatingDatas = false;
			view.gantt.resourceDataProvider = new ArrayCollection();
			view.gantt.taskDataProvider = new ArrayCollection();
			/*if (isPlain){
			dataSources.tasks = currentTransformer.getTasksTree();
			dataSources.resources = currentTransformer.getResourcesTree();
			}
			else {
			*/
			
			// se crea para un arbol
			var rstree:ArrayCollection=currentTransformer.getResourcesTree();
			var hd:ResourceHierarchicalData = new ResourceHierarchicalData(currentTransformer.getResourcesTree());
			gH=hd;
			hd.childrenField = "children";
			var sort:Sort=new Sort();
			sort.fields=[new SortField("children")];
			logger.info("setFilters()   init    "+(new Date()));
			
			dataSources.resources  = hd;
			dataSources.tasks  = currentTransformer.getTasksTree();
			//}
			currentTransformer.setColumns(view.gantt.dataGrid,view.gantt.ganttSheet);
			//currentTransformer.setColumnsDataGridCustomization(view.gantt.dataGrid);
			// Este metodo esta colisionando cuando se hace un sort
			//view.gantt.dataGrid.addEventListener(MouseEvent.CLICK,datoClicked);
			
			//BindingUtils.bindProperty(view.gantt,"resourceDataProvider",dataSources,"resources");
			//BindingUtils.bindProperty(view.gantt,"taskDataProvider"    ,dataSources,"tasks");
			
			view.gantt.resourceDataProvider = dataSources.resources;
			view.gantt.taskDataProvider = dataSources.tasks;
			
			/*var sorter:Sort =  currentTransformer.getSorter();
			view.gantt.resourceDataProvider.sort = sorter;*/
			
			doRefreshOrder();
			//view.gantt.resourceDataProvider.refresh();
			//sorter.compareFunction = currentTransformer.getSorterFunction();
			//view.gantt.resourceDataProvider.sort = sorter;
			logger.info("assignDataProviders()   init    "+(new Date()));
		}
		
		private var refreshTime:Timer = new Timer(100,1);
		public function doRefresh(e:TimerEvent):void{
			//doRefreshOrder();
		}
		
		//public var isDataLoaded:Boolean = false;
		public function doRefreshOrder():void{
			//if (isDataLoaded){
			//dataGridBack
			view.gantt.resourceDataProvider.refresh();
			try {
				
			}
			catch (e:*){
				logger.error(e);
				throw e;
			}
			view.gantt.dataGridBack.invalidateDisplayList();
			view.gantt.dataGridBack.invalidateList();
			//}
		}
		
		
		
		
		
		
	}
}

