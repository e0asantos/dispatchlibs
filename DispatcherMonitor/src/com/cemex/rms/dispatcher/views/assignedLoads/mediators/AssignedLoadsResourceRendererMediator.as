package com.cemex.rms.dispatcher.views.assignedLoads.mediators
{
	import com.cemex.rms.dispatcher.helpers.TooltipHelper;
	import com.cemex.rms.dispatcher.views.assignedLoads.AssignedLoadsResourceRenderer;
	import com.cemex.rms.dispatcher.views.assignedLoads.tree.AssignedLoadsResource;
	import com.cemex.rms.dispatcher.views.mediators.GenericResourceRendererMediator;
	import com.cemex.rms.dispatcher.views.reports.PlantReport;
	import com.cemex.rms.dispatcher.views.reports.VehicleReport;
	
	import mx.controls.Image;
	import mx.core.UIComponent;
	

	
	public class AssignedLoadsResourceRendererMediator extends GenericResourceRendererMediator
	{
		public function AssignedLoadsResourceRendererMediator()
		{
			super();
		}
		
		[Inject]
		public var view:AssignedLoadsResourceRenderer;
		
		public override function getView():UIComponent{
			return view;
		}
		public override function processClickAction():void{
			//Alert.show("Actions:view.toolTip"+view.toolTip + "\n"+view["data"]);
		}
		
		public override function processClickDato():void{
			//Alert.show("DATOS:view.toolTip"+view.toolTip + "\n"+view["data"]);
		}
		
		
		override public function onRegister():void {
			super.onRegister();
			view.toolTip = TooltipHelper.getOrderTooltip(getGanttResource());

			var info:Image = getView()["info"] as Image;

			info.source = tootipNormal;
		}
		[Bindable] [Embed(source="/assets/Request.gif")] private var tootipNormal:Class;
		[Bindable] [Embed(source="/assets/Request_2.gif")] private var tooltipOpcion:Class;
		
		
		
		public override function showTooltip(field:String):String{
			var resource:AssignedLoadsResource = getGanttResource() as AssignedLoadsResource;
			if (field == "plant"){
				
				if(resource != null){
					var plant:PlantReport = resource.getReportsObject() as PlantReport;
					if (plant != null){
						
						return TooltipHelper.getPlantsTooltip(getGanttResource(),plant);
					}
				}
				return "Cannot process Tooltip for Plant["+resource.label+"]";
			}
			else if (field == "equipLabel"){
				if(resource != null){
					var vehicle:VehicleReport = resource.getReportsObject() as VehicleReport;
					if (vehicle != null){
						return TooltipHelper.getVehicleTooltip(getGanttResource(),vehicle);
					}
				}
				return "Cannot process Tooltip for Plant["+resource.label+"]";
			}
			else if (field == "orderNumber" || field=="orderLoad"){
				return  TooltipHelper.getOrderTooltip(getGanttResource());	
			}
			else{ 
				return super.showTooltip(field);
			}
			
		}
	}
	
}