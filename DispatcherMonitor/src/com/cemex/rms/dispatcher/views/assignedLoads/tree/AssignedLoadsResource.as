package com.cemex.rms.dispatcher.views.assignedLoads.tree
{
	import com.cemex.rms.common.gantt.GanttResource;

	public dynamic class AssignedLoadsResource extends GanttResource
	{
		public function AssignedLoadsResource() {
			super();
			
		}
		public override function get label():String {
			var etiqueta:String = super.label;
			if(super.labelField=="plant" && super.data.hasOwnProperty("name")){
				etiqueta=etiqueta+" "+super.data["name"];
			}

			return etiqueta;
		}
	}
}