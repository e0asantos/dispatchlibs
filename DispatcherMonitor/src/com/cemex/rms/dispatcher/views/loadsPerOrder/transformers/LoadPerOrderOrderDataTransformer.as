package com.cemex.rms.dispatcher.views.loadsPerOrder.transformers
{
	import com.cemex.rms.common.transformer.FieldColumn;
	import com.cemex.rms.dispatcher.views.loadsPerOrder.LoadPerOrderOrderResourceRenderer;
	import com.cemex.rms.dispatcher.views.loadsPerOrder.LoadPerOrderPlantOrderResourceRenderer;
	import com.cemex.rms.dispatcher.views.loadsPerOrder.LoadPerOrderTaskRenderer;
	import com.cemex.rms.dispatcher.views.loadsPerOrder.headers.LoadsPerOrderOrderHeader;
	import com.cemex.rms.dispatcher.views.loadsPerOrder.tree.LoadsPerOrderResource;
	import com.cemex.rms.dispatcher.views.reports.ReportConfig;
	
	import ilog.gantt.GanttDataGrid;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.controls.advancedDataGridClasses.AdvancedDataGridColumn;
	import mx.core.ClassFactory;
	import mx.core.IFactory;

	public class LoadPerOrderOrderDataTransformer extends LoadsPerOrderAbstractDataTransformer
	{
		public function LoadPerOrderOrderDataTransformer()
		{
			super();
			
			setFields(new ArrayCollection(["orderNumber"])
				);//,LoadPerOrderTooltipHelper.getResourceData()
				
			setFieldColumns(new ArrayCollection([
				new FieldColumn("orderNumber",null,"Order",155,LoadPerOrderOrderResourceRenderer,LoadsPerOrderOrderHeader)
			]));
			
			
		/*	setReports(new ArrayCollection([
				new ReportInfo()
			
			]));*/
		}
		 //taskItemRenderer="com.cemex.rms.dispatcher.views.renderers.GanttTaskRenderer"
		public override function getTaskRenderer():IFactory{
			return new ClassFactory(LoadPerOrderTaskRenderer);
		}
		
		
		
		
		
		
	}
}