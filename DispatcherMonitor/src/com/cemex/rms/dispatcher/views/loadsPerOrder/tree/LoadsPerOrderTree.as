package com.cemex.rms.dispatcher.views.loadsPerOrder.tree
{
	import com.cemex.rms.common.gantt.GanttTree;
	import com.cemex.rms.common.logging.ILogger;
	import com.cemex.rms.common.logging.LoggerFactory;
	import com.cemex.rms.common.tree.NodeResource;
	import com.cemex.rms.common.tree.NodeTask;
	import com.cemex.rms.dispatcher.helpers.StatusHelper;
	import com.cemex.rms.dispatcher.helpers.TimeZoneHelper;
	
	import mx.collections.ArrayCollection;

	public class LoadsPerOrderTree extends GanttTree
	{
		public function LoadsPerOrderTree(fields:ArrayCollection)
		{
			super(fields);
		}
		
		
		override public function shouldRemoveResourceIfEmpty(resource:NodeResource):Boolean{
			logger.debug("shouldRemoveResourceIfEmpty("+resource+")");
			if (resource != null){ 
				logger.debug("shouldRemoveResourceIfEmpty("+resource.getLabelField() +","+ resource.childrenCount+")");
				
				return resource.getLabelField() == "orderNumber" && resource.childrenCount == 0;
			}
			return false;
		}
		
		
		public override function timeChangedTask(time:Date,nodeTask:NodeTask):Boolean{
			
			
			//var nowTime:Number = time.getTime();
			var nowTime:Number=TimeZoneHelper.getServer().getTime();
			var task:LoadsPerOrderTask = nodeTask as LoadsPerOrderTask; 
			
			var startingTime:Number = task.startTime.getTime();
			var endTime:Number = task.endTime.getTime();
			var loadingTime:Number = task.getRawStartTime().getTime();		
			
			var timeTemp:Number = (nowTime - loadingTime) / (60*1000);
			task.initExtraValues();	
			
			if (task.payload.isDelayed) {
				task.delayTimeLabel = "D"+ Math.ceil(timeTemp) ;
				var delayed:Date = TimeZoneHelper.getServer();
				delayed.setSeconds(0);
				task.startTime = delayed;
				if (task.getTaskItem() != null){
					task.getTaskItem().startTime = delayed;
					task.getTaskItem().endTime = task.endTime;
				}
			}
			else {
				if (endTime < nowTime){
					return false;
				}
				task.delayTimeLabel = "";
			}
			
			getTasksTree().itemUpdated(task);
			
			return true;
		}
		
		
		private var logger:ILogger = LoggerFactory.getLogger("LoadsPerOrderTree");
		
		
		
		protected override function hasSameValues(target:Object,temp:Object):Boolean {
			var targetTask:LoadsPerOrderTask = target as LoadsPerOrderTask;
			var tempTask:LoadsPerOrderTask = temp as LoadsPerOrderTask;
			return targetTask != null && tempTask != null &&
				(""+targetTask.payload.itemNumber) == (""+tempTask.payload.itemNumber) &&
				(""+targetTask.payload.orderNumber) == (""+tempTask.payload.orderNumber);
		}
		protected override function newNode():NodeResource{
			return new LoadsPerOrderResource();
		}
		protected override function newTask():NodeTask {
			return new LoadsPerOrderTask();
		}
		
		
		
		override public function deleteOldNode(oldNode:NodeResource,node:NodeResource,field:String):Boolean{
			/*
			switch(field) {
				case "orderNumber":
					return oldNode.data[field] == node.data[field]
					
					&& oldNode.id != node.id;
			}
			*/
			return false;
		}
		
	}
}