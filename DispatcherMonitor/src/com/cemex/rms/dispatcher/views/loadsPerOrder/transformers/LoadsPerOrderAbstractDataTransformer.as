package com.cemex.rms.dispatcher.views.loadsPerOrder.transformers
{
	import com.cemex.rms.common.date.FlexDateHelper;
	import com.cemex.rms.common.gantt.GanttResource;
	import com.cemex.rms.common.transformer.GenericDataTransformer;
	import com.cemex.rms.dispatcher.constants.TVARVCConstants;
	import com.cemex.rms.dispatcher.constants.TVARVCPreffixes;
	import com.cemex.rms.dispatcher.helpers.GanttServiceReference;
	import com.cemex.rms.dispatcher.views.loadsPerOrder.tree.LoadsPerOrderResource;
	import com.cemex.rms.dispatcher.views.loadsPerOrder.tree.LoadsPerOrderTree;
	import com.cemex.rms.dispatcher.vo.OrderLoad;
	
	import ilog.gantt.GanttDataGrid;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Image;
	import mx.controls.advancedDataGridClasses.AdvancedDataGridColumn;
	
	public class LoadsPerOrderAbstractDataTransformer extends GenericDataTransformer
	{
		public function LoadsPerOrderAbstractDataTransformer()
		{
			super();
		}
		
		public override function setColumnsDataGridCustomization(dataGrid:GanttDataGrid):void{
			tree.currentDataGrid=dataGrid;
			dataGrid.iconFunction = iconFunction;
			dataGrid.styleFunction = null;
		}
		[Embed(source="/assets/s_s_wspo_orange.png")]            
		[Bindable]             
		private var order_bom_orange:Class;
		
		[Embed(source="/assets/s_s_wspo_gray.png")]            
		[Bindable]             
		private var order_bom_gray:Class;
		
		[Embed(source="/assets/s_s_wspo_red.png")]            
		[Bindable]             
		private var order_bom_red:Class;
		
		[Embed(source="/assets/s_s_wspo_3.png")]            
		[Bindable]             
		private var order:Class;
		
		[Embed(source="/assets/s_s_wspo_rbom.png")]            
		[Bindable]             
		private var order_rbom:Class;
		
		[Embed(source="/assets/s_s_wspo.gif")]
		[Bindable]
		private var normal_order:Class;
		
		
		[Embed(source="/assets/s_b_plnt_distr.gif")]            
		[Bindable]             
		private var plant_distribuida:Class;
		
		[Embed(source="/assets/s_b_plnt_centr.gif")]            
		[Bindable]             
		private var plant_centralizada:Class;
		
		[Embed(source="/assets/s_b_plnt_auton.gif")]            
		[Bindable]             
		private var plant_autonoma:Class;
		
		[Embed(source="/assets/s_s_wspo_green.png")]            
		[Bindable]             
		private var order_green:Class;
		
		private function iconFunction(resource:GanttResource):Class{
			/*(order_bom as Image).toolTip="Hello World";
			(order as Image).toolTip="Hello World";*/
			if (resource.getLabelField() == "orderNumber"){
				/*if(String(resource.data.bomValid).toLowerCase()=="x"){
					return order_bom;
				} else {
					return order;	
				}
*/					
				if(resource.data.bomValid=="1"){
					//rojo
					return order_bom_red;
				} else if(resource.data.bomValid=="2"){
					//color naranja
					return order_bom_orange;
				} else if(resource.data.bomValid=="3"){
					//color gris
					return order_bom_gray;
				}
				if(resource.data.flagRecipe!=""){
					//return order_rbom;
				}
				return order_green;
				
			}
			else if (resource.getLabelField() == "plant"){
				var plantId:String = (""+resource.data["plantId"]);
				var plantType:String = GanttServiceReference.getPlantType(plantId);
				//Alert.show(plantId +"-" + plantType);
				if (plantType == GanttServiceReference.getTVARVCParam(TVARVCConstants.PLOPM_D).low){
					return plant_distribuida;	
				}
				else if (plantType == GanttServiceReference.getTVARVCParam(TVARVCConstants.PLOPM_C).low){
					return plant_centralizada;
				}
				else if (plantType == GanttServiceReference.getTVARVCParam(TVARVCConstants.PLOPM_A).low){
					return plant_autonoma;
				}
			}
			return null;
		}
		
		
		public override function getViewID():String{
			return TVARVCPreffixes.LOADS_PER_ORDER_VIEW_ID;
		}
		
		public override function setFields(fields:ArrayCollection):void{
			this.fields =  fields;
			tree = new LoadsPerOrderTree(fields);
		}
		
		
		public override function setTreePlusMinus(dataGrid:GanttDataGrid):void{
			
			dataGrid.setStyle("disclosureOpenIcon", minus);
			dataGrid.setStyle("disclosureClosedIcon", plus);
			
		}
		
		[Embed(source="/assets/plus.gif")]
		public var plus:Class;
		
		[Embed(source="/assets/minus.gif")]
		public var minus:Class; 
		
		
		
	}
}