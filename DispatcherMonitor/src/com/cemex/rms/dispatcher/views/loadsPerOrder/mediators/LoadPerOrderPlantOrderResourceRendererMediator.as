package com.cemex.rms.dispatcher.views.loadsPerOrder.mediators
{
	import com.cemex.rms.dispatcher.helpers.TooltipHelper;
	import com.cemex.rms.dispatcher.services.IGanttServiceFactory;
	import com.cemex.rms.dispatcher.views.loadsPerOrder.LoadPerOrderPlantOrderResourceRenderer;
	import com.cemex.rms.dispatcher.views.loadsPerOrder.tree.LoadsPerOrderResource;
	import com.cemex.rms.dispatcher.views.mediators.GenericResourceRendererMediator;
	import com.cemex.rms.dispatcher.views.reports.PlantReport;
	
	import flash.events.MouseEvent;
	
	import mx.controls.Alert;
	import mx.controls.Image;
	import mx.core.UIComponent;
	
	
	
	public class LoadPerOrderPlantOrderResourceRendererMediator extends GenericResourceRendererMediator
	{
		public function LoadPerOrderPlantOrderResourceRendererMediator()
		{
			super();
		}
		
		[Inject]
		public var view:LoadPerOrderPlantOrderResourceRenderer;
		
		[Inject]
		public var services:IGanttServiceFactory;
		
		
		
		public override function getView():UIComponent{
			return view;
		}
		
		
		override public function onRegister():void {
			super.onRegister();
			view.addEventListener(MouseEvent.CLICK,clickDetection);
			//view.toolTip = TooltipHelper.getOrderTooltip(getGanttResource());
			
			var info:Image = getView()["info"] as Image;
			//if (getLabeField() == "equipLabel" && getGanttResource()["isHeader"]){
			info.source = tootipNormal;
		}
		public function clickDetection(evt:MouseEvent):void{
			var inEvent:*=evt;
		}
		[Bindable] [Embed(source="/assets/Request.gif")] private var tootipNormal:Class;
		[Bindable] [Embed(source="/assets/Request_2.gif")] private var tooltipOpcion:Class;
		
		
		public override function showTooltip(field:String):String{
			
			if (field == "plant"){
				var resource:LoadsPerOrderResource = getGanttResource() as LoadsPerOrderResource;
				if(resource != null){
					var plant:PlantReport = resource.getReportsObject() as PlantReport;
					if (plant != null){
						var algo:*=getGanttResource();
						var planta:*=plant;
						return TooltipHelper.getPlantsTooltip(getGanttResource(),plant);
					}
				}
				return "Cannot process Tooltip for Plant["+resource.label+"]";
			}
			else if (field == "orderNumber"){
				return  TooltipHelper.getOrderTooltip(getGanttResource());	
			}
			else{ 
				return super.showTooltip(field);
			}
			
		}
	}
}