package com.cemex.rms.dispatcher.views.loadsPerVehicle.mediators
{
	import com.cemex.rms.common.helpers.PopupHelper;
	import com.cemex.rms.dispatcher.constants.ExtrasConstants;
	import com.cemex.rms.dispatcher.constants.OTRConstants;
	import com.cemex.rms.dispatcher.constants.SecurityConstants;
	import com.cemex.rms.dispatcher.constants.TVARVCPreffixes;
	import com.cemex.rms.dispatcher.helpers.GanttServiceReference;
	import com.cemex.rms.dispatcher.helpers.TooltipHelper;
	import com.cemex.rms.dispatcher.services.IGanttServiceFactory;
	import com.cemex.rms.dispatcher.services.flashislands.impl.DispatcherIslandImpl;
	import com.cemex.rms.dispatcher.services.flashislands.requests.vehicle.ReleaseVehicle;
	import com.cemex.rms.dispatcher.services.flashislands.requests.vehicle.SupportPlant;
	import com.cemex.rms.dispatcher.services.flashislands.requests.vehicle.VehicleInPlant;
	import com.cemex.rms.dispatcher.views.loadsPerVehicle.LoadsPerVehicleResourceRenderer;
	import com.cemex.rms.dispatcher.views.loadsPerVehicle.tree.LoadsPerVehicleResource;
	import com.cemex.rms.dispatcher.views.mediators.GenericResourceRendererMediator;
	import com.cemex.rms.dispatcher.views.reports.PlantReport;
	import com.cemex.rms.dispatcher.views.reports.VehicleReport;
	
	import flash.events.MouseEvent;
	
	import mx.binding.utils.BindingUtils;
	import mx.controls.Image;
	import mx.controls.Menu;
	import mx.core.UIComponent;
	import mx.events.MenuEvent;
	
	public class LoadPerVehicleResourceRendererMediator extends GenericResourceRendererMediator
	{
		public function LoadPerVehicleResourceRendererMediator()
		{
			super();
		}
		
		[Inject]
		[Bindable]
		public var view:LoadsPerVehicleResourceRenderer;
		
		[Inject] public var service:IGanttServiceFactory;
		
		public override function getView():UIComponent{
			return view;
		}
		public override function processClickAction():void{
			//Alert.show("Actions:view.toolTip"+view.toolTip + "\n"+view["data"]);
		}
		
		
		public function popup(event:MouseEvent):void {
			/*
			Esta linea verifica que los menus no esten bloqueados debido a la fecha
			actual
			*/
			if(!DispatcherIslandImpl.areMenusAvailable){
				return;
			}
			if (getLabeField() == "equipNumber" && getGanttResource()["isHeader"]){
				
				
				var actions:Menu = PopupHelper.popupFromXML(event,"@label",getResourceActions());
				actions.addEventListener(MenuEvent.ITEM_CLICK,menuClick);
			}
		}
		public function menuClick(event:MenuEvent):void {

			var data:String = event.item.@data;
			if ( data!= null && data != ""  ){
				
				var plant:String = getGanttResource()["data"]["plant"];
				var vehicle:String = getGanttResource()["data"]["equipNumber"];
				var orderNumber:String = getGanttResource()["data"]["orderNumber"];
				var operativeMode:String = GanttServiceReference.getPlantType(plant);
				if (data == SecurityConstants.MENU_SUPPORT_PLANT){
					var support:SupportPlant = new SupportPlant();
					support.EQUNR = vehicle;
					support.WERKS = plant;
					if (GanttServiceReference.isEnabledField(getViewName(),SecurityConstants.MENU_SUPPORT_PLANT,operativeMode)){
						GanttServiceReference.dispatchIslandEvent(support);
					}
				}
				else if (data == SecurityConstants.MENU_RELEASE_VEHICLE){
					
					var release:ReleaseVehicle = new ReleaseVehicle();
					release.EQUNR = vehicle;
					release.WERKS = plant;
					/*if (GanttServiceReference.isEnabledField(getViewName(),SecurityConstants.MENU_RELEASE_VEHICLE,operativeMode)){
						GanttServiceReference.dispatchIslandEvent(release);
					}*/
				}
				else if (data == SecurityConstants.MENU_VEHICLE_IN_PLANT){
					
					var vehicleInPlant:VehicleInPlant = new VehicleInPlant();
					vehicleInPlant.EQUNR = vehicle;
					vehicleInPlant.WERKS = plant;
					//vehicleInPlant.VBELN=orderNumber;
					
					if (GanttServiceReference.isEnabledField(getViewName(),SecurityConstants.MENU_VEHICLE_IN_PLANT,operativeMode)){
						GanttServiceReference.dispatchIslandEvent(vehicleInPlant);
					}
				}
			}
		}
		
		
		
		public function getViewName():String{
			return TVARVCPreffixes.LOADS_PER_VEHICLE_VIEW_ID;
		}

		
		
		public function getLabel(label:String):String{
			return GanttServiceReference.getLabel(label);
		}
		public function getResourceActions():XML {
			var result:XML = PopupHelper.getXML("root");
			//result.appendChild(PopupHelper.getMenuItemSepataror());
			var menu:XML;
			if(this.view.data.equipStatus==ExtrasConstants.VEHICLE_DISPONIBLE){
				menu = PopupHelper.getMenuItem(getLabel(OTRConstants.SUPPORT_PLANT),SecurityConstants.MENU_SUPPORT_PLANT,"extra","EVENTO/Funcion/REF",4);
				result.appendChild(menu);
			} else {
				menu = PopupHelper.getMenuItem(getLabel(OTRConstants.VEHICLE_IN_PLANT),SecurityConstants.MENU_VEHICLE_IN_PLANT,"extra","EVENTO/Funcion/REF",4);
				result.appendChild(menu);
				
			}
			/*menu = PopupHelper.getMenuItem(getLabel(OTRConstants.RELEASE_VEHICLE),SecurityConstants.MENU_RELEASE_VEHICLE,"extra","EVENTO/Funcion/REF",4);
			result.appendChild(menu);*/
			//add2(result,getMenuItemOverLap(overlaps,null,"Show Data",OrderOperationRequestEvent.SHOW_DATA_REQUEST,null,PopupHelper.MENU_ACTION_FUNCTION));
			
			
			//add2(result,getMenuItemOverLap(overlaps,null,"Show Data",OrderOperationRequestEvent.SHOW_DATA_REQUEST,null,PopupHelper.MENU_ACTION_FUNCTION));
			return result;
		}
		override public function onRegister():void {
			super.onRegister();
			//view.toolTip = TooltipHelper.getOrderTooltip(getGanttResource());
			
			BindingUtils.bindSetter(dataSetter,view,"data");
			getView().addEventListener(MouseEvent.CLICK, popup);
			
			updateIcon();
		}
		
		public function dataSetter(data:*):void{
			
			updateIcon();
		}
		
		public function updateIcon():void{
			if (getView() != null){
				var info:Image = getView()["info"] as Image;
				
				if (getLabeField() == "equipNumber"  ){
					
					info.source = tooltipOpcion;
				}
				else if (getLabeField() == "equipStatus"){
					info.source = null;
				}
				else {
					info.source = tootipNormal;
				}
			}
		}
		
		[Bindable] [Embed(source="/assets/Request.gif")] private var tootipNormal:Class;
		[Bindable] [Embed(source="/assets/s_wdvtry.gif")] private var tooltipOpcion:Class;

		
		
		public override function showTooltip(field:String):String{
			var resource:LoadsPerVehicleResource = getGanttResource() as LoadsPerVehicleResource;
			if (field == "plant"){
				
				if(resource != null){
					var plant:PlantReport = resource.getReportsObject() as PlantReport;
					if (plant != null){
						return TooltipHelper.getPlantsTooltip(getGanttResource(),plant);
					}
				}
				return "Cannot process Tooltip for Plant["+resource.label+"]";
			}
			else if (field == "equipNumber"){
				if(resource != null){
					var vehicle:VehicleReport = resource.getReportsObject() as VehicleReport;
					if (vehicle != null){
						return TooltipHelper.getVehicleTooltip(getGanttResource(),vehicle);
					}
				}
				return "Cannot process Tooltip for Equipment["+resource.label+"]";
			}
			else if (field == "orderNumber"){
				return  TooltipHelper.getOrderTooltip(getGanttResource());	
			}/*
			else if(field == "equipStatus"){
				return "";
			}*/
			else{ 
				return super.showTooltip(field);
			}
			
		}
	}
}