package com.cemex.rms.dispatcher.views.loadsPerVehicle.headers
{
	
	import com.cemex.rms.dispatcher.constants.OTRConstants;
	import com.cemex.rms.dispatcher.events.SearchBtnClickViewEvent;
	import com.cemex.rms.dispatcher.helpers.GanttServiceReference;
	
	import flash.events.FocusEvent;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	
	import mx.controls.Alert;
	import mx.utils.StringUtil;
	
	import org.robotlegs.mvcs.Mediator;
	
	
	public class LoadsPerVehicleHeaderMediator extends Mediator
	{
		public function LoadsPerVehicleHeaderMediator()
		{
			super();
		}
		
		[Inject]
		public var view:LoadsPerVehicleHeader;
		
		public var searchMyLabel:String="";
		public function getLabel(label:String):String{
			return GanttServiceReference.getLabel(label);
		}
		
		override public function onRegister():void {
			
			//view.addEventListener(MouseEvent.CLICK,mouseeventHandler);
			
			view.loadsPerVehicle.text = "Search";//getLabel(OTRConstants.LOADS_PER_VEHICLE_HEADER);
			view.searchText.text="";
			view.loadsPerVehicle.toolTip = getLabel(OTRConstants.LOADS_PER_VEHICLE_HEADER_TOOLTIP);
			view.searchText.addEventListener(KeyboardEvent.KEY_UP,searchBtnClick);
			//view.searchText.addEventListener(FocusEvent.FOCUS_OUT,elementFocusChange);
			eventMap.mapListener(eventDispatcher,KeyboardEvent.KEY_DOWN,setTextGlobalSearch);
			view.searchText.text=searchMyLabel;
		}
		
		public function setTextGlobalSearch(evt:KeyboardEvent):void{
			if(evt.charCode==0){
				return;
			}
			if(evt.charCode==8){
				view.searchText.text=view.searchText.text.substr(0,-1);
				searchMyLabel=searchMyLabel.substr(0,-1);
			} else {
				view.searchText.text+=String.fromCharCode(evt.charCode);
				searchMyLabel=searchMyLabel+String.fromCharCode(evt.charCode).toString();
			}
			
			view.searchText.setSelection(view.searchText.text.length,view.searchText.text.length);
			searchBtnClick();
		}
		public function elementFocusChange(ebt:FocusEvent):void{
			view.searchText.setFocus();
			view.searchText.setSelection(view.searchText.text.length,view.searchText.text.length);
		}
		public function searchBtnClick(evt:KeyboardEvent=null):void{
			var busqueda:SearchBtnClickViewEvent=new SearchBtnClickViewEvent();
			busqueda.searchString=searchMyLabel;
			busqueda.refTextInput=view.searchText;
			eventDispatcher.dispatchEvent(busqueda);
		}
		
		public function  mouseeventHandler(e:MouseEvent):void{
			if (e.ctrlKey){
				e.stopPropagation();
			}
		}
	}
}