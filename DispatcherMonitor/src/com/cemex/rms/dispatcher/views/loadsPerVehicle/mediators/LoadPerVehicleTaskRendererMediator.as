package com.cemex.rms.dispatcher.views.loadsPerVehicle.mediators
{
	import com.cemex.rms.common.gantt.GanttTask;
	import com.cemex.rms.common.logging.ILogger;
	import com.cemex.rms.common.logging.LoggerFactory;
	import com.cemex.rms.dispatcher.constants.TVARVCPreffixes;
	import com.cemex.rms.dispatcher.helpers.StatusHelper;
	import com.cemex.rms.dispatcher.services.IGanttServiceFactory;
	import com.cemex.rms.dispatcher.services.flashislands.helper.WDRequestHelper;
	import com.cemex.rms.dispatcher.views.loadsPerVehicle.LoadsPerVehicleTaskRenderer;
	import com.cemex.rms.dispatcher.views.loadsPerVehicle.tree.LoadsPerVehicleTask;
	import com.cemex.rms.dispatcher.views.mediators.GenericTaskRendererMediator;
	import com.cemex.rms.dispatcher.vo.OrderLoad;
	
	import mx.core.UIComponent;
	
	public class LoadPerVehicleTaskRendererMediator extends GenericTaskRendererMediator
	{
		public function LoadPerVehicleTaskRendererMediator()
		{
			super();
			
		}
		
		[Inject]
		public var view: LoadsPerVehicleTaskRenderer;
		
		[Inject]
		public var services: IGanttServiceFactory;
		
		protected override function getView():UIComponent{
			return view;
		}
		override public function onRegister():void {
			super.onRegister();
			
			var task:LoadsPerVehicleTask = getGanttTask() as LoadsPerVehicleTask;
			if (task != null){
				task.setView(view);
			} 
		}
		
		
		public override function getViewName():String{
			return TVARVCPreffixes.LOADS_PER_VEHICLE_VIEW_ID;
		}

		
		private var logger:ILogger = LoggerFactory.getLogger("LoadPerVehicle");
		
		public override function showChecked(action:String, task:GanttTask):Boolean{
			if(action == WDRequestHelper.ORDER_ON_HOLD_REQUEST){
				return StatusHelper.isOrderOnHold(task.data as OrderLoad);
			}
			return false;
		}
		
		public override function getExtraInfoIfOverlap(task:GanttTask):String {
			var payload:OrderLoad = task.data as OrderLoad;
			return payload.orderNumber+":"+payload.itemNumber+":"+payload.loadNumber + "("+payload.loadingTime+")";
		}		
		
		
		/*
		public override function getTaskActions():XML {
			
			var result:XML = PopupHelper.getXML("root");
			result.appendChild(PopupHelper.getMenuItem(getExtraInfoIfOverlap(getGanttTask()),null,null,null,0,false));
			
			
			var overlaps:ArrayCollection = getOverlap();
			
			addMenu(result,overlaps);
			
			return result;
		}
		*/
	
		/*
		public override function processTimeChangedEveryMinute():void{
			checkDelayedTime();
		}
		
		public function isOrderOnHold():Boolean {
		return OrderStatusHelper.isOrderOnHold(getGanttTask().payload);
		}
		
		
		public function getTVARV(name:String):TVARVC{
		return services.getFlashIslandService().getTVARVCValue(name);
		}
		
		*/
		
		
		/*
		public override function getTaskActions():XML {
			var result:XML = PopupHelper.getXML("root");
			
			var overlaps:ArrayCollection = getOverlap();
			add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.ORDER_ON_HOLD), OrderOperationRequestEvent.ORDER_ON_HOLD_REQUEST,"check"));
			add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.CHANGE_DELIVERY_TIME), OrderOperationRequestEvent.CHANGE_DELIVERY_TIME_REQUEST));
			add2(result,PopupHelper.getMenuItemSepataror());
			add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.CHANGE_PLANT), OrderOperationRequestEvent.CHANGE_PLANT_REQUEST));
			add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.ASSIGN_VEHICLE), OrderOperationRequestEvent.ASSIGN_VEHICLE_REQUEST));
			
			add2(result,PopupHelper.getMenuItemSepataror());
			add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.CHANGE_VOLUME), OrderOperationRequestEvent.CHANGE_VOLUME_REQUEST));
			//add2(result,getMenuItemIfOverlap(getLabel(OTRConstants.REUSE_CONCRETE), OrderOperationRequestEvent.REUSE_CONCRETE_REQUEST));
			//add2(result,getMenuItemIfOverlap(getLabel(OTRConstants.REDIRECT_LOAD), OrderOperationRequestEvent.REDIRECT_LOAD_REQUEST));
			add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.ASSIGN_TO_PLANT), OrderOperationRequestEvent.ASSIGN_PLANT_REQUEST));
			add2(result,PopupHelper.getMenuItemSepataror());
			add2(result,getMenuItemOverLap(overlaps,null,"Show Data",OrderOperationRequestEvent.SHOW_DATA_REQUEST,null,PopupHelper.MENU_ACTION_FUNCTION));

			return result;
		}
	
	
		*/
		
		
		
		
		/*
		public function showMenu(action:String, task:GanttTask,extra:String):Boolean{
			
			if(services.getFlashIslandService().isBatcher()){
				return showBatcherMenu(action,task,extra);
			}
			else if (services.getFlashIslandService().isDispatcher()){
				return showDispatcherMenu(action,task,extra);
			}
			
			return false;
		}
		
		*/
		
		

	
		/*
		public function filterPlants(plants:ArrayCollection , overlaps:ArrayCollection) :DictionaryMap {
		var result:DictionaryMap =  new DictionaryMap();
		for(var i:int  = 0 ; i < overlaps.length ; i ++){
		var task:GanttTask = overlaps.getItemAt(i) as GanttTask;
		for (var j:int = 0 ; j < plants.length ; j ++){
		var plant:Plant = plants.getItemAt(j) as Plant;
		var payload:OrderLoad = task.data as OrderLoad;
		if (plant.plantId == payload.plant && result.get(plant.plantId) == null){
		result.put(plant.plantId , plant);
		}
		}
		}
		return result;
		}
		*/
		/*
		public function addMenu(result:XML,overlaps:ArrayCollection) :void {
			
			add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.ASSIGN_TO_PLANT), WDRequestHelper.ASSIGN_PLANT_REQUEST,SecurityConstants.ASSIGN_PLANT_REQUEST));
			
			
			//add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.CHANGE_PLANT), WDRequestHelper.CHANGE_PLANT_REQUEST_MENU));
			var changePlant:XML = PopupHelper.getMenuItem(getLabel(OTRConstants.CHANGE_PLANT),null,null,null,-1,true,null,false);
			
			add2(changePlant,getMenuItemOverLap(overlaps,DispatcherConstants.ORDER_SCOPE,getLabel(OTRConstants.ORDER_LABEL), WDRequestHelper.CHANGE_PLANT_REQUEST_MENU,SecurityConstants.CHANGE_PLANT_REQUEST_MENU));
			add2(changePlant,getMenuItemOverLap(overlaps,DispatcherConstants.LOAD_SCOPE,getLabel(OTRConstants.LOAD_LABEL), WDRequestHelper.CHANGE_PLANT_REQUEST_MENU,SecurityConstants.CHANGE_PLANT_REQUEST_MENU));
			add2(result,changePlant);
			add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.CHANGE_VOLUME), WDRequestHelper.CHANGE_VOLUME_REQUEST,SecurityConstants.CHANGE_VOLUME_REQUEST));
			//add2(result,getMenuItemOverLap(overlaps,null,getLabel("REUSE"), WDRequestHelper.REUSE_CONCRETE_REQUEST));
			add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.ORDER_ON_HOLD), WDRequestHelper.ORDER_ON_HOLD_REQUEST,SecurityConstants.ORDER_ON_HOLD_REQUEST,"check"));
			add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.CHANGE_DELIVERY_TIME), WDRequestHelper.CHANGE_DELIVERY_TIME_REQUEST,SecurityConstants.CHANGE_DELIVERY_TIME_REQUEST));
			
			
			add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.ASSIGN_VEHICLE), WDRequestHelper.ASSIGN_VEHICLE_REQUEST,SecurityConstants.ASSIGN_VEHICLE_REQUEST));
			addAssignVehicles(result,overlaps);
			
			add2(result,PopupHelper.getMenuItemSepataror());
			var plants:DictionaryMap = filterPlants(services.getFlashIslandService().getPlants(), overlaps);
			var keys:ArrayCollection = plants.getAvailableKeys();
			for (var i:int = 0 ; i < keys.length ; i ++ ) {
				var key:String = keys.getItemAt(i) as String;
				var plant:Plant = plants.get(key);
				var wc:ArrayCollection = plant.workcenter;
				for (var j:int  = 0; j < wc.length; j++ ){
					var workcenter:Workcenter = wc.getItemAt(j) as Workcenter;
					add2(result,getMenuItemOverLap(overlaps,workcenter.arbpl,getLabel(OTRConstants.BATCH) + ":" + workcenter.arbpl, WDRequestHelper.BATCH_REQUEST,SecurityConstants.BATCH_REQUEST));
				}
			}
			add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.MANUAL_BATCH), WDRequestHelper.MANUAL_BATCH_REQUEST,SecurityConstants.MANUAL_BATCH_REQUEST));
			
			add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.RE_USE), WDRequestHelper.RE_USE_REQUEST,SecurityConstants.RE_USE_REQUEST));
			add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.CONTINUE_PRODUCTION), WDRequestHelper.CONTINUE_PRODUCTION_REQUEST,SecurityConstants.CONTINUE_PRODUCTION_REQUEST));
			add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.RESTART_LOAD), WDRequestHelper.RESTART_LOAD_REQUEST,SecurityConstants.RESTART_LOAD_REQUEST));
			add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.QUALITY_ADJUSTMENT), WDRequestHelper.QUALITY_ADJUSTMENT_REQUEST,SecurityConstants.QUALITY_ADJUSTMENT_REQUEST));
			
			
			add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.RE_PRINT), WDRequestHelper.RE_PRINT_REQUEST,SecurityConstants.RE_PRINT_REQUEST));
			
			add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.DISPATCH), WDRequestHelper.DISPATCH_REQUEST,SecurityConstants.DISPATCH_REQUEST));
			
			// Este siempre se despliega
			add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.DISPLAY_PO), WDRequestHelper.DISPLAY_PO_REQUEST,SecurityConstants.DISPLAY_PO_REQUEST));
			
			add2(result,getMenuItemOverLap(overlaps,null,"Show Data",WDRequestHelper.SHOW_DATA_REQUEST,SecurityConstants.SHOW_DATA_REQUEST,null,PopupHelper.MENU_ACTION_FUNCTION));
			
		}
		
		public function addAssignVehicles(result:XML,overlaps:ArrayCollection):void{
			var plants:DictionaryMap = filterPlants(services.getFlashIslandService().getPlants(), overlaps);
			var keys:ArrayCollection = plants.getAvailableKeys();
			
			for (var i:int = 0 ; i < keys.length ; i ++ ) {
				var key:String = keys.getItemAt(i) as String;
				var plant:Plant = plants.get(key);
				
				var eq:ArrayCollection = plant.equipments;
				var sort:Sort =  new Sort();
				sort.compareFunction = compareEquipments;
				eq.sort = sort; 
				eq.refresh();
				// sort
				var availableVehicles:ArrayCollection = new ArrayCollection();
				
				availableVehicles.addItem(FilterHelper.getFilterConditionTVARVC("status",new ArrayCollection([GanttServiceReference.getTVARVCParam(TVARVCConstants.VEHICLES_STATUS_AVAILABLE)]),FilterCondition.EQUALS));
				
				if(!GanttServiceReference.getCollectFilterStatus()){
					availableVehicles.addItem(FilterHelper.getFilterCondition("equicatgry",new ArrayCollection(["V"]),FilterCondition.EQUALS));
				}
				var maxVehicles:Number = GanttServiceReference.getParamNumber(ParamtersConstants.NUM_VEHI_DISP);
				var count:int = 0 ;
				for (var k:int  = 0; k < eq.length && count < maxVehicles; k++ ){
					var equipment:Equipment = eq.getItemAt(k) as Equipment;
					
					if (FilterHelper.matchFilters(equipment,availableVehicles)){
						add2(result,getMenuItemOverLap(overlaps,WDRequestHelper.getAssignOneVehicleExtraID(plant,equipment) ,getLabel(OTRConstants.ASSIGN_VEHICLE)+":"+plant.plantId + ":" + equipment.equipment, WDRequestHelper.ASSIGN_ONE_VEHICLE_REQUEST,SecurityConstants.ASSIGN_ONE_VEHICLE_REQUEST));
						count++;
					}					
				}
			}
			
		}
		*/
		/*
		public function addDispatcherMenu(result:XML,overlaps:ArrayCollection) :void {
			
			add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.ASSIGN_TO_PLANT), WDRequestHelper.ASSIGN_PLANT_REQUEST));
			
			
			//add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.CHANGE_PLANT), WDRequestHelper.CHANGE_PLANT_REQUEST_MENU));
			var changePlant:XML = PopupHelper.getMenuItem(getLabel(OTRConstants.CHANGE_PLANT),null,null,null,-1,true,null,false);
			
			add2(changePlant,getMenuItemOverLap(overlaps,DispatcherConstants.ORDER_SCOPE,getLabel(OTRConstants.ORDER_LABEL), WDRequestHelper.CHANGE_PLANT_REQUEST_MENU));
			add2(changePlant,getMenuItemOverLap(overlaps,DispatcherConstants.LOAD_SCOPE,getLabel(OTRConstants.LOAD_LABEL), WDRequestHelper.CHANGE_PLANT_REQUEST_MENU));
			add2(result,changePlant);
			add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.CHANGE_VOLUME), WDRequestHelper.CHANGE_VOLUME_REQUEST));
			//add2(result,getMenuItemOverLap(overlaps,null,getLabel("REUSE"), WDRequestHelper.REUSE_CONCRETE_REQUEST));
			add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.ORDER_ON_HOLD), WDRequestHelper.ORDER_ON_HOLD_REQUEST,"check"));
			add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.CHANGE_DELIVERY_TIME), WDRequestHelper.CHANGE_DELIVERY_TIME_REQUEST));
			
			
			add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.ASSIGN_VEHICLE), WDRequestHelper.ASSIGN_VEHICLE_REQUEST));
			
			
			addAssignVehicles(result,overlaps);
		}
		public function addAssignVehicles(result:XML,overlaps:ArrayCollection):void{
			var plants:DictionaryMap = filterPlants(services.getFlashIslandService().getPlants(), overlaps);
			var keys:ArrayCollection = plants.getAvailableKeys();
			
			for (var i:int = 0 ; i < keys.length ; i ++ ) {
				var key:String = keys.getItemAt(i) as String;
				var plant:Plant = plants.get(key);
				
				var eq:ArrayCollection = plant.equipments;
				var sort:Sort =  new Sort();
				sort.compareFunction = compareEquipments;
				eq.sort = sort; 
				eq.refresh();
				// sort
				var availableVehicles:ArrayCollection = new ArrayCollection();
				
				availableVehicles.addItem(FilterHelper.getFilterConditionTVARVC("status",new ArrayCollection([GanttServiceReference.getTVARVCParam(TVARVCConstants.VEHICLES_STATUS_AVAILABLE)]),FilterCondition.EQUALS));
				
				if(!GanttServiceReference.getCollectFilterStatus()){
					availableVehicles.addItem(FilterHelper.getFilterCondition("equicatgry",new ArrayCollection(["V"]),FilterCondition.EQUALS));
				}
				var maxVehicles:Number = GanttServiceReference.getParamNumber(ParamtersConstants.NUM_VEHI_DISP);
				var count:int = 0 ;
				for (var k:int  = 0; k < eq.length && count < maxVehicles; k++ ){
					var equipment:Equipment = eq.getItemAt(k) as Equipment;
					
					if (FilterHelper.matchFilters(equipment,availableVehicles)){
						add2(result,getMenuItemOverLap(overlaps,WDRequestHelper.getAssignOneVehicleExtraID(plant,equipment) ,getLabel(OTRConstants.ASSIGN_VEHICLE)+":"+plant.plantId + ":" + equipment.equipment, WDRequestHelper.ASSIGN_ONE_VEHICLE_REQUEST));
						count++;
					}					
				}
			}
			
		}
		public function addBatcherMenu(result:XML,overlaps:ArrayCollection) :void {
			
			addDispatcherMenu(result,overlaps);
			
			
			add2(result,PopupHelper.getMenuItemSepataror());
			var plants:DictionaryMap = filterPlants(services.getFlashIslandService().getPlants(), overlaps);
			var keys:ArrayCollection = plants.getAvailableKeys();
			for (var i:int = 0 ; i < keys.length ; i ++ ) {
				var key:String = keys.getItemAt(i) as String;
				var plant:Plant = plants.get(key);
				var wc:ArrayCollection = plant.workcenter;
				for (var j:int  = 0; j < wc.length; j++ ){
					var workcenter:Workcenter = wc.getItemAt(j) as Workcenter;
					add2(result,getMenuItemOverLap(overlaps,workcenter.arbpl,getLabel(OTRConstants.BATCH) + ":" + workcenter.arbpl, WDRequestHelper.BATCH_REQUEST));
				}
			}
			add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.MANUAL_BATCH), WDRequestHelper.MANUAL_BATCH_REQUEST));
			
			add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.RE_USE), WDRequestHelper.RE_USE_REQUEST));
			add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.CONTINUE_PRODUCTION), WDRequestHelper.CONTINUE_PRODUCTION_REQUEST));
			add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.RESTART_LOAD), WDRequestHelper.RESTART_LOAD_REQUEST));
			add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.QUALITY_ADJUSTMENT), WDRequestHelper.QUALITY_ADJUSTMENT_REQUEST));
			
			
			add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.RE_PRINT), WDRequestHelper.RE_PRINT_REQUEST));
			
			add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.DISPATCH), WDRequestHelper.DISPATCH_REQUEST));
			
			// Este siempre se despliega
			add2(result,getMenuItemOverLap(overlaps,null,getLabel(OTRConstants.DISPLAY_PO), WDRequestHelper.DISPLAY_PO_REQUEST));
		}
		
		*/
		
		
		
		
		
		
		/*
		public function showDispatcherMenu(action:String, task:GanttTask, extra:String):Boolean{
			var payload:OrderLoad = task.data as OrderLoad;
			var plant:String = payload.plant;
			var operationType:String = services.getFlashIslandService().getPlantType(plant);
			
			var extraSplit:Array;
			switch (action){
				
				case WDRequestHelper.ASSIGN_VEHICLE_REQUEST:
					
					return checkAssignVehicle(operationType);
				case WDRequestHelper.ASSIGN_ONE_VEHICLE_REQUEST:
					extraSplit = extra.split(":");
					//if ((payload.shipConditions == GanttServiceReference.getTVARVCParam(TVARVCConstants.COLLECT_ORDER).low && extraSplit[5] == "V")
					//	|| (payload.shipConditions != GanttServiceReference.getTVARVCParam(TVARVCConstants.COLLECT_ORDER).low && extraSplit[5] == "W") ){
					if ((payload.shipConditions == GanttServiceReference.getTVARVCParam(TVARVCConstants.SHIPCOND).low && extraSplit[5] == "W")
						|| (payload.shipConditions != GanttServiceReference.getTVARVCParam(TVARVCConstants.SHIPCOND).low && extraSplit[5] == "V") ){
						//se deben mostra
						if (extra.indexOf(plant) == 0){
							logger.debug("VEHICLE_ASSIGN_ONE_VEHICLE_REQUEST ("+checkAssignVehicle(operationType)+":"+plant+":"+operationType+") extra:("+extra + "):payload.shipConditions:(" + payload.shipConditions+")"+ ":SHIPCOND("+GanttServiceReference.getTVARVCParam(TVARVCConstants.SHIPCOND).low+")");
							return checkAssignVehicle(operationType);
						}
						else {
							logger.debug("VEHICLE_ASSIGN_ONE_VEHICLE_REQUEST (false:"+plant+") extra:("+extra + "):payload.shipConditions:(" + payload.shipConditions+")"+ ":SHIPCOND("+GanttServiceReference.getTVARVCParam(TVARVCConstants.SHIPCOND).low+")");
							return false;
						}
					}
					else {
						
						logger.debug("VEHICLE_ASSIGN_ONE_VEHICLE_REQUEST (false) extra:("+extra + "):payload.shipConditions:(" + payload.shipConditions+")"+ ":SHIPCOND("+GanttServiceReference.getTVARVCParam(TVARVCConstants.SHIPCOND).low+")");
						return false;
					}
					
				
				case WDRequestHelper.ASSIGN_PLANT_REQUEST:					
			}
			//Alert.show("Show:" +action);
			return true;
		}
		public function checkAssignVehicle(operationType:String):Boolean {
		
			
			if (operationType == GanttServiceReference.getTVARVCParam(TVARVCConstants.PLOPM_C).low){
				// CENTRALIZADO				
				return true;	
			} 
			else if (operationType == GanttServiceReference.getTVARVCParam(TVARVCConstants.PLOPM_D).low
				|| operationType == GanttServiceReference.getTVARVCParam(TVARVCConstants.PLOPM_C).low){
				// DISTRIBUIDO// AUTONOMOUS
				return false;
			}
			else {
				return false;
			}
		
		}
		public function showBatcherMenu(action:String, task:GanttTask,extra:String):Boolean{
			var payload:OrderLoad = task.data as OrderLoad;
			var plant:String = payload.plant;
			var operationType:String = services.getFlashIslandService().getPlantType(plant);
			
			
			var extraSplit:Array;
			var prodData:ProductionData = services.getFlashIslandService().getProductionData(payload);
			if (WDRequestHelper.SHOW_DATA_REQUEST == action ){
				return true;
			}
			if (prodData == null){
				
				switch(action){
					
					case WDRequestHelper.ASSIGN_VEHICLE_REQUEST:
						return checkAssignVehicle(operationType);
					case WDRequestHelper.ASSIGN_ONE_VEHICLE_REQUEST:
						extraSplit = extra.split(":");
						//if ((payload.shipConditions == GanttServiceReference.getTVARVCParam(TVARVCConstants.COLLECT_ORDER).low && extraSplit[5] == "V")
						//	|| (payload.shipConditions != GanttServiceReference.getTVARVCParam(TVARVCConstants.COLLECT_ORDER).low && extraSplit[5] == "W") ){
						if ((payload.shipConditions == GanttServiceReference.getTVARVCParam(TVARVCConstants.SHIPCOND).low && extraSplit[5] == "W")
							|| (payload.shipConditions != GanttServiceReference.getTVARVCParam(TVARVCConstants.SHIPCOND).low && extraSplit[5] == "V") ){
							//se deben mostra
							if (extra.indexOf(plant) == 0){
								logger.debug("VEHICLE_ASSIGN_ONE_VEHICLE_REQUEST ("+checkAssignVehicle(operationType)+":"+plant+":"+operationType+") extra:("+extra + "):payload.shipConditions:(" + payload.shipConditions+")"+ ":SHIPCOND("+GanttServiceReference.getTVARVCParam(TVARVCConstants.SHIPCOND).low+")");
								return checkAssignVehicle(operationType);
							}
							else {
								logger.debug("VEHICLE_ASSIGN_ONE_VEHICLE_REQUEST (false:"+plant+") extra:("+extra + "):payload.shipConditions:(" + payload.shipConditions+")"+ ":SHIPCOND("+GanttServiceReference.getTVARVCParam(TVARVCConstants.SHIPCOND).low+")");
								return false;
							}
						}
						else {
							
							logger.debug("VEHICLE_ASSIGN_ONE_VEHICLE_REQUEST (false) extra:("+extra + "):payload.shipConditions:(" + payload.shipConditions+")"+ ":SHIPCOND("+GanttServiceReference.getTVARVCParam(TVARVCConstants.SHIPCOND).low+")");
							return false;
						}
					case WDRequestHelper.ASSIGN_PLANT_REQUEST:
					case WDRequestHelper.CHANGE_PLANT_REQUEST_MENU:
					case WDRequestHelper.CHANGE_VOLUME_REQUEST:
					case WDRequestHelper.ORDER_ON_HOLD_REQUEST:
					case WDRequestHelper.CHANGE_DELIVERY_TIME_REQUEST:
					case WDRequestHelper.DISPATCH_REQUEST:
						
						return true;
				}
			}
			else {
				if (operationType == GanttServiceReference.getTVARVCParam(TVARVCConstants.PLOPM_D).low 
					|| GanttServiceReference.getTVARVCParam(TVARVCConstants.PLOPM_A).low){
					// DISTRIBUIDO// AUTONOMOUS
					
					if (prodData.stonr == "10"){
						switch(action){
							case WDRequestHelper.ASSIGN_VEHICLE_REQUEST:
								
								return checkAssignVehicle(operationType);
							case WDRequestHelper.ASSIGN_ONE_VEHICLE_REQUEST:
								extraSplit = extra.split(":");
								//if ((payload.shipConditions == GanttServiceReference.getTVARVCParam(TVARVCConstants.COLLECT_ORDER).low && extraSplit[5] == "V")
								//	|| (payload.shipConditions != GanttServiceReference.getTVARVCParam(TVARVCConstants.COLLECT_ORDER).low && extraSplit[5] == "W") ){
								if ((payload.shipConditions == GanttServiceReference.getTVARVCParam(TVARVCConstants.SHIPCOND).low && extraSplit[5] == "W")
									|| (payload.shipConditions != GanttServiceReference.getTVARVCParam(TVARVCConstants.SHIPCOND).low && extraSplit[5] == "V") ){
									//se deben mostra
									if (extra.indexOf(plant) == 0){
										logger.debug("VEHICLE_ASSIGN_ONE_VEHICLE_REQUEST ("+checkAssignVehicle(operationType)+":"+plant+":"+operationType+") extra:("+extra + "):payload.shipConditions:(" + payload.shipConditions+")"+ ":SHIPCOND("+GanttServiceReference.getTVARVCParam(TVARVCConstants.SHIPCOND).low+")");
										return checkAssignVehicle(operationType);
									}
									else {
										logger.debug("VEHICLE_ASSIGN_ONE_VEHICLE_REQUEST (false:"+plant+") extra:("+extra + "):payload.shipConditions:(" + payload.shipConditions+")"+ ":SHIPCOND("+GanttServiceReference.getTVARVCParam(TVARVCConstants.SHIPCOND).low+")");
										return false;
									}
								}
								else {
									
									logger.debug("VEHICLE_ASSIGN_ONE_VEHICLE_REQUEST (false) extra:("+extra + "):payload.shipConditions:(" + payload.shipConditions+")"+ ":SHIPCOND("+GanttServiceReference.getTVARVCParam(TVARVCConstants.SHIPCOND).low+")");
									return false;
								}
							case WDRequestHelper.ASSIGN_PLANT_REQUEST:
							case WDRequestHelper.CHANGE_PLANT_REQUEST_MENU:
							case WDRequestHelper.CHANGE_VOLUME_REQUEST:
							case WDRequestHelper.ORDER_ON_HOLD_REQUEST:
							case WDRequestHelper.CHANGE_DELIVERY_TIME_REQUEST:
							case WDRequestHelper.DISPLAY_PO_REQUEST:
								return true;
						}
					}
					else if (prodData.stonr == "20"){
						switch(action){
							case WDRequestHelper.ASSIGN_VEHICLE_REQUEST:
								
								return checkAssignVehicle(operationType);
							case WDRequestHelper.ASSIGN_ONE_VEHICLE_REQUEST:
								extraSplit = extra.split(":");
								//if ((payload.shipConditions == GanttServiceReference.getTVARVCParam(TVARVCConstants.COLLECT_ORDER).low && extraSplit[5] == "V")
								//	|| (payload.shipConditions != GanttServiceReference.getTVARVCParam(TVARVCConstants.COLLECT_ORDER).low && extraSplit[5] == "W") ){
								if ((payload.shipConditions == GanttServiceReference.getTVARVCParam(TVARVCConstants.SHIPCOND).low && extraSplit[5] == "W")
									|| (payload.shipConditions != GanttServiceReference.getTVARVCParam(TVARVCConstants.SHIPCOND).low && extraSplit[5] == "V") ){
									//se deben mostra
									if (extra.indexOf(plant) == 0){
										logger.debug("VEHICLE_ASSIGN_ONE_VEHICLE_REQUEST ("+checkAssignVehicle(operationType)+":"+plant+":"+operationType+") extra:("+extra + "):payload.shipConditions:(" + payload.shipConditions+")"+ ":SHIPCOND("+GanttServiceReference.getTVARVCParam(TVARVCConstants.SHIPCOND).low+")");
										return checkAssignVehicle(operationType);
									}
									else {
										logger.debug("VEHICLE_ASSIGN_ONE_VEHICLE_REQUEST (false:"+plant+") extra:("+extra + "):payload.shipConditions:(" + payload.shipConditions+")"+ ":SHIPCOND("+GanttServiceReference.getTVARVCParam(TVARVCConstants.SHIPCOND).low+")");
										return false;
									}
								}
								else {
									
									logger.debug("VEHICLE_ASSIGN_ONE_VEHICLE_REQUEST (false) extra:("+extra + "):payload.shipConditions:(" + payload.shipConditions+")"+ ":SHIPCOND("+GanttServiceReference.getTVARVCParam(TVARVCConstants.SHIPCOND).low+")");
									return false;
								}
							case WDRequestHelper.BATCH_REQUEST:
							case WDRequestHelper.MANUAL_BATCH_REQUEST:
							case WDRequestHelper.DISPLAY_PO_REQUEST:
								return true;
						}
					}
					else if (prodData.stonr == "30"){
						switch(action){
							case WDRequestHelper.DISPLAY_PO_REQUEST:
								return true;
						}
					}
					else if (prodData.stonr == "40" || prodData.stonr == "50" || prodData.stonr == "60"){
						switch(action){
							case WDRequestHelper.RE_PRINT_REQUEST:
							case WDRequestHelper.DISPLAY_PO_REQUEST:
								return true;
						}
					}	
					else if (prodData.stonr == "70"){
						switch(action){
							case WDRequestHelper.RE_USE_REQUEST:
							case WDRequestHelper.CONTINUE_PRODUCTION_REQUEST:
							case WDRequestHelper.RESTART_LOAD_REQUEST:
							case WDRequestHelper.RE_PRINT_REQUEST:
							case WDRequestHelper.DISPLAY_PO_REQUEST:
								return true;
						}
					}	
					else if (prodData.stonr == "80"){
						switch(action){
							case WDRequestHelper.QUALITY_ADJUSTMENT_REQUEST:
							case WDRequestHelper.RE_PRINT_REQUEST:
							case WDRequestHelper.DISPLAY_PO_REQUEST:
								return true;
						}
					}
				}
				else if (operationType == GanttServiceReference.getTVARVCParam(TVARVCConstants.PLOPM_C).low){
					// CENTRALIZADO				
					if (prodData.stonr == "10"){
						switch(action){
							case WDRequestHelper.DISPLAY_PO_REQUEST:
								return true;
						}
					}
					else if (prodData.stonr == "20"){
						switch(action){
							case WDRequestHelper.BATCH_REQUEST:
							case WDRequestHelper.MANUAL_BATCH_REQUEST:
							case WDRequestHelper.DISPLAY_PO_REQUEST:
								return true;
						}
					}
					else if (prodData.stonr == "30"){
						switch(action){
							case WDRequestHelper.DISPLAY_PO_REQUEST:
								return true;
						}
					}
					else if (prodData.stonr == "40" || prodData.stonr == "50" || prodData.stonr == "60"){
						switch(action){
							case WDRequestHelper.RE_PRINT_REQUEST:
							case WDRequestHelper.DISPLAY_PO_REQUEST:
								return true;
						}
					}	
					else if (prodData.stonr == "70"){
						switch(action){
							case WDRequestHelper.RE_USE_REQUEST:
							case WDRequestHelper.CONTINUE_PRODUCTION_REQUEST:
							case WDRequestHelper.RESTART_LOAD_REQUEST:
							case WDRequestHelper.RE_PRINT_REQUEST:
							case WDRequestHelper.DISPLAY_PO_REQUEST:
								return true;
						}
					}	
					else if (prodData.stonr == "80"){
						switch(action){
							case WDRequestHelper.QUALITY_ADJUSTMENT_REQUEST:
							case WDRequestHelper.RE_PRINT_REQUEST:
							case WDRequestHelper.DISPLAY_PO_REQUEST:
								return true;
						}
					}
				} 
				
				
				
			}
			
			
			//Alert.show("Show:" +action);
			return false;
		}
		*/
		
		
	}
}