package com.cemex.rms.dispatcher.views.reports
{
	public class PlantReport extends ReportObject
	{
		public function PlantReport()
		{
		}
		
		[Bindable]
		public var assignedLoads:Number = 0;
		[Bindable]
		public var ScheduledVolume:Number = 0 ;
		[Bindable]
		public var DeliveredVolume:Number = 0 ;
		[Bindable]
		public var ConfirmedVolume:Number = 0 ;
		[Bindable]
		public var ToBeConfirmedVolume:Number = 0 ;
		[Bindable]
		public var CanceledVolume:Number = 0 ;
		[Bindable]
		public var UOM:String ="" ;

	}
}