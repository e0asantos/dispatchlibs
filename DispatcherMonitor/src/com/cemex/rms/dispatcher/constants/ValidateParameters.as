package com.cemex.rms.dispatcher.constants
{
	import com.cemex.rms.common.reflection.ReflectionHelper;
	
	import flash.net.FileReference;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import com.cemex.rms.dispatcher.helpers.GanttServiceReference;

	public class ValidateParameters
	{
		public function ValidateParameters()
		{
		}
		
		public static function validateTVARV():void{
			var tvarv:ArrayCollection = ReflectionHelper.getConstNames(TVARVCConstants);
			var temp:String = "";
			for (var i:int = 0 ; i < tvarv.length ; i++){
				var uno:* = GanttServiceReference.getTVARVCParam(TVARVCConstants[""+tvarv.getItemAt(i)]);
				var dos:* = GanttServiceReference.getTVARVCSelection(TVARVCConstants[""+tvarv.getItemAt(i)]);
				if (uno == null && dos == null){
					//Alert.show("params:" + tvarv.getItemAt(i));
					temp += "tvarv:" + tvarv.getItemAt(i) +":::>"+TVARVCConstants[""+tvarv.getItemAt(i)]+ "\n";
				}
			}
			if (temp != ""){
				//Alert.show("Valores que faltan\n" +temp);
			}
		}
		public static function validateParams():void{
			var params:ArrayCollection = ReflectionHelper.getConstNames(ParamtersConstants);
			var temp:String = "";
			for (var i:int = 0 ; i < params.length ; i++){
				
				var param:String = GanttServiceReference.getParamString(ParamtersConstants[""+params.getItemAt(i)]);
				if (param == null){
					
					temp += "param:" +params.getItemAt(i)+":::\t>"+ ParamtersConstants[""+params.getItemAt(i)] + "\n";
				}
			}
			if (temp != ""){
				//Alert.show("Valores que faltan\n" +temp);
			}
		}
		
		public static function validateOTR():void{
			var otr:ArrayCollection = ReflectionHelper.getConstNames(OTRConstants);
			
			var temp:String = "";
			for (var i:int = 0; i < otr.length ; i++){	
				
				var label:String = GanttServiceReference.getLabel(OTRConstants[""+otr.getItemAt(i)]);
				//Alert.show(label + "::" + OTRConstants[""+otr.getItemAt(i)]);
				if (label == null || (""+OTRConstants[""+otr.getItemAt(i)]) == (""+label)){
					
					temp += "otr:" +otr.getItemAt(i)+":::\t>"+ OTRConstants[""+otr.getItemAt(i)] + "\n";
				}
			}
			if (temp != ""){
				//Alert.show("Valores que faltan\n" +temp);
			}		
		} 
	}
}