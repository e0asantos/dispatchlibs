package com.cemex.rms.dispatcher.constants
{
	public class ExtrasConstants
	{
		import com.cemex.rms.dispatcher.helpers.GanttServiceReference;
		
		
		
		public function ExtrasConstants()
		{
		}
		public static const VEHICLE_ASSIGNED:String = GanttServiceReference.getLabel(OTRConstants.ASSIGN_LOAD);//"Asignadas";
		public static const VEHICLE_DISPONIBLE:String = GanttServiceReference.getLabel(OTRConstants.AVAILABLE_LOAD);//"Disponible";
		public static const LOAD_UNASSIGNED:String = GanttServiceReference.getLabel(OTRConstants.NOT_ASSIGN_LOAD);//"No asignados";
		
		public static const CONFIRMED_VOLUME:String="ConfirmedVolume";
		public static const SCHEDULED_VOLUME:String="ScheduledVolume";
		public static const DELIVERED_VOLUME:String="DeliveredVolume";
		public static const TO_BE_CONFIRMED_VOLUME:String="ToBeConfirmedVolume";
		public static const CANCEL_VOLUME:String="CanceledVolume";
	}
}