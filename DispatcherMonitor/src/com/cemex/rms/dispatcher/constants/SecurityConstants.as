package com.cemex.rms.dispatcher.constants
{
	public class SecurityConstants
	{
		public function SecurityConstants()
		{
		} 
		
		//<DM_BATC>_<FXD_LPO>_ASSIGN_TO_PLANT_*01*
		public static const LOAD_TOOLTIP:String 				="TIP_LOAD";
		public static const BOM_REQUEST:String 				="BTN_BOM_REQUEST";
		public static const LOAD_TOOLTIP_PRODUCTION:String 		="TIP_LOAD_PRODUCTION";
		
		public static const DRAG_UNASSIGN_VEHICLE:String		="DRG_UNASSIGN_VEHICLE";
		public static const DRAG_ASSIGN_VEHICLE:String			="DRG_ASSIGN_VEHICLE";
		public static const DRAG_CHANGE_DELIVERY_TIME:String	="DRG_CHANGE_DELIVERY_TIME";
		public static const DRAG_CHANGE_PLANT:String	="DRG_CHANGE_PLANT";
		
		
		public static const MENU_ASSIGN_VEHICLE:String		="BTN_ASSIGN_VEHICLE";
		public static const MENU_ASSIGN_ONE_VEHICLE:String	="BTN_ASSIGN_ONE_VEHICLE";
		public static const MENU_ASSIGN_PLANT:String			="BTN_ASSIGN_PLANT";
		public static const MENU_CHANGE_PLANT:String	="BTN_CHANGE_PLANT";
		public static const MENU_CHANGE_VOLUME:String		="BTN_CHANGE_VOLUME";
		public static const MENU_ORDER_ON_HOLD:String		="BTN_ORDER_ON_HOLD";
		public static const MENU_CHANGE_DELIVERY_TIME:String	="BTN_CHANGE_DELIVERY_TIME";
		public static const MENU_DISPLAY_PO:String			="BTN_DISPLAY_PO";
		public static const MENU_DISPATCH:String				="BTN_DISPATCH";
		public static const MENU_BATCH:String				="BTN_BATCH";
		public static const MENU_MANUAL_BATCH:String			="BTN_MANUAL_BATCH";
		public static const MENU_RE_PRINT:String				="BTN_RE_PRINT";
		public static const MENU_RE_USE:String				="BTN_RE_USE";
		public static const MENU_CONTINUE_PRODUCTION:String	="BTN_CONTINUE_PRODUCTION";
		public static const MENU_RESTART_LOAD:String			="BTN_RESTART_LOAD";
		public static const MENU_QUALITY_ADJUSTMENT:String	="BTN_QUALITY_ADJUSTMENT";
		public static const MENU_SHOW_DATA:String			="BTN_SHOW_DATA";
		public static const MENU_BATCH_PER_WORK_CENTER:String="MENU_BATCH_PER_WORK_CENTER";
		public static const MENU_ANTICIPATED_LOADS:String="BTN_ANTICIPATED_LOADS";
		public static const MENU_UNASSIGN:String="BTN_UNASSIGN";
		public static const MENU_INTERCHANGE_VEHICLE:String="BTN_INTERCHANGE_VEHICLE";
		
		
		//public static const REUSE_CONCRETE_REQUEST:String	="REUSE_CONCRETE_REQUEST";
		public static const MENU_CHANGE_FREQUENCY:String	="BTN_CHANGE_FREQUENCY";
		
		// ASL
		public static const MENU_START_BATCHING:String	="BTN_START_BATCHING";
		public static const MENU_RENEGOTIATE_LOAD:String	="BTN_RENEGOTIATE_LOAD";
		public static const MENU_UNASSIGN_PLANT:String	="BTN_UNASSIGN_PLANT";
		
		
		//Vehicle
		public static const MENU_SUPPORT_PLANT:String	="VEHICLE_SUPPORT_PLANT";
		public static const MENU_RELEASE_VEHICLE:String	="VEHICLE_RELEASE_VEHICLE";
		public static const MENU_VEHICLE_IN_PLANT:String="VEHICLE_VEHICLE_IN_PLANT";
		
		
	}
}