package com.cemex.rms.dispatcher.services.lcds.events
{
	import com.cemex.rms.dispatcher.services.flashislands.vo.ProductionData;
	
	import flash.events.Event;
	
	public class ProductionStatusLCDSEvent extends Event
	{
		public static const LCDS_DATA_RECEIVED:String = "productionStatusLCDSDataReceived";
		
		public var productionData:ProductionData;
		
		public function ProductionStatusLCDSEvent(productionData:ProductionData, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(LCDS_DATA_RECEIVED, bubbles, cancelable);
			this.productionData = productionData;
		}
	}
}