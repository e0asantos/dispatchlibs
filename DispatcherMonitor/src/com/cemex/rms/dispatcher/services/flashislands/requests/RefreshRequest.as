package com.cemex.rms.dispatcher.services.flashislands.requests
{
	import com.cemex.rms.common.flashislands.SimpleFlashIslandEventObject;
	import com.cemex.rms.dispatcher.services.flashislands.impl.DispatcherIslandHelper;

	public class RefreshRequest extends SimpleFlashIslandEventObject
	{
		public function RefreshRequest()
		{
		}
		public override function getEventName():String
		{
			return "populateDS";
		}
	}
}