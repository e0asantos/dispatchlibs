package com.cemex.rms.dispatcher.services.flashislands.requests.dispatch
{
	import com.cemex.rms.common.flashislands.IFlashIslandEventObject;

	public class ClearMessagesArea implements IFlashIslandEventObject
	{
		public function ClearMessagesArea()
		{
		}	
		public function getEventName():String{
			return "clearMessagesArea";
		}
	}
}