package com.cemex.rms.dispatcher.services.flashislands.events
{
	import com.cemex.rms.dispatcher.services.flashislands.vo.FlashIslandsData;
	
	import flash.events.Event;
	
	public class FlashIslandsDataEvent extends Event
	{
		public static const FLASHISLAND_DATA_READY:String = "flashIslandDataReady";
		
		public static const FLASHISLAND_DATA_CONFIRM:String = "flashIslandDataConfirm";
		
		public static const FLASHISLAND_DATA_FAKE_PUSH:String = "flashIslandDataFakePush";
		
		public var data:FlashIslandsData;
		
		public function FlashIslandsDataEvent(type:String,data:FlashIslandsData, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			this.data = data;
		}
	}
}