package com.cemex.rms.dispatcher.services.flashislands.requests.dispatch
{
	import com.cemex.rms.common.flashislands.IFlashIslandEventObject;
	import com.cemex.rms.dispatcher.services.flashislands.impl.DispatcherIslandHelper;
	import com.cemex.rms.dispatcher.vo.OrderLoad;

	public class ChangeFrequency implements IFlashIslandEventObject
	{
		public function ChangeFrequency()
		{
		}
		
		public var VBELN:String;
		public var POSNR:String;
		
		public function getEventName():String{
			return "changeFrecuency";
		}
	}
}