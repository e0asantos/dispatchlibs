package com.cemex.rms.dispatcher.controller.lcds
{
	import com.cemex.rms.dispatcher.events.lcds.LCDSReconnectEvent;
	import com.cemex.rms.dispatcher.services.IGanttServiceFactory;
	
	import org.robotlegs.mvcs.Command;
	
	public class LCDSReconnectCommand extends Command
	{
		public function LCDSReconnectCommand()
		{
			super();
		}
		
		[Inject] public var service:IGanttServiceFactory;
		
		[Inject] public var event:LCDSReconnectEvent;
		
		public override function  execute():void {
			
			service.getProductionStatusPIReceiver().saveRemove();
			service.getProductionStatusPIReceiver().reconnect();
			service.getSalesOrderPIReceiver().reconnect();
			service.getVehiclePIReceiver().reconnect();
			service.getPlantPIReceiver().reconnect();
		}
	}
}