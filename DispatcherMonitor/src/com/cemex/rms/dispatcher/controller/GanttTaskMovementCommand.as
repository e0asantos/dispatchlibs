package com.cemex.rms.dispatcher.controller
{
	import com.cemex.rms.common.date.FlexDateHelper;
	import com.cemex.rms.common.logging.ILogger;
	import com.cemex.rms.common.logging.LoggerFactory;
	import com.cemex.rms.dispatcher.constants.SecurityConstants;
	import com.cemex.rms.dispatcher.events.DispatcherConstants;
	import com.cemex.rms.dispatcher.events.GanttTaskMovementEvent;
	import com.cemex.rms.dispatcher.helpers.GanttServiceReference;
	import com.cemex.rms.dispatcher.services.flashislands.helper.WDRequestHelper;
	import com.cemex.rms.dispatcher.services.flashislands.vo.ProductionData;
	import com.cemex.rms.dispatcher.views.mediators.DispatcherViewMediator;
	import com.cemex.rms.dispatcher.vo.OrderLoad;
	
	import org.robotlegs.mvcs.Command;

	public class GanttTaskMovementCommand extends Command
	{
		public function GanttTaskMovementCommand() {
			
			super();
		}
		private var logger:ILogger = LoggerFactory.getLogger("GanttTaskMovementCommand");
		
		[Inject] public var event:GanttTaskMovementEvent;
		
		/**
		 * Este metodo lanza los eventos respectivos utilizando los datos necesarios respecto al cambio que se hizo
		 * durante el drag and drop
		 * 
		 */
		
		
		public override function  execute():void {
			var from:OrderLoad = event.origin;
			var to:OrderLoad = event.destiny;
			
			var request:OrderLoad = from.clone();
			var prodData:ProductionData = GanttServiceReference.getProductionData(request);
			var toPlantType:String = GanttServiceReference.getPlantType(request.plant);
			if(from.orderStatus=="BLCK" || from.loadStatus=="BLCK"){
				return;
			}
			//logger.info("from("+ReflectionHelper.object2XML(from)+")\n"+
			//	"to("+ReflectionHelper.object2XML(to)+")\n"+
			//	"request("+ReflectionHelper.object2XML(request)+")\n");
			if (from.plant != to.plant) {
				logger.info("plant");
				request.plant = to.plant;
				
				if (GanttServiceReference.isEnabledField(DispatcherViewMediator.currentView,SecurityConstants.DRAG_CHANGE_PLANT,toPlantType)){	
					GanttServiceReference.dispatchIslandEvent(
						WDRequestHelper.getChangePlantRequestDrag(request,prodData,toPlantType,
							DispatcherConstants.LOAD_SCOPE						
						)	
					);
				}
			}
			else if (from.equipNumber != to.equipNumber && FlexDateHelper.getTimestampString(from.startTime) == FlexDateHelper.getTimestampString(to.startTime)) {
				logger.info("equipNumber");
				
				if(to.equipNumber == ""){
					request.equipNumber = to.equipNumber;
					//request.equipLabel = to.equipLabel;
					if (GanttServiceReference.isEnabledField(DispatcherViewMediator.currentView,SecurityConstants.DRAG_UNASSIGN_VEHICLE,toPlantType)){	
						GanttServiceReference.dispatchIslandEvent(
							WDRequestHelper.getUnassignVehicleRequest(request,prodData,toPlantType,null)
						);
					}
				}
				else {
					
					request.equipNumber = to.equipNumber;
					//request.equipLabel = to.equipLabel;
					if (GanttServiceReference.isEnabledField(DispatcherViewMediator.currentView,SecurityConstants.DRAG_ASSIGN_VEHICLE,toPlantType)){
						if(toPlantType!="03" && toPlantType!="02"){
							GanttServiceReference.dispatchIslandEvent(
								WDRequestHelper.getAssignVehicleRequest(request,prodData,toPlantType,null,"X")
							);
						}
					}
				}
			}
			else if (FlexDateHelper.getTimestampString(from.startTime) != FlexDateHelper.getTimestampString(to.startTime)){
				
				logger.info("time");
				request.startTime = FlexDateHelper.copyDate(to.startTime);
				//request.endTime = FlexDateHelper.addMinutes(request.startTime,3);
				
				if (GanttServiceReference.isEnabledField(DispatcherViewMediator.currentView,SecurityConstants.DRAG_CHANGE_DELIVERY_TIME,toPlantType)){
					GanttServiceReference.dispatchIslandEvent(
						WDRequestHelper.getChangeDeliveryTimeRequest(request,prodData,toPlantType,null)
					);				
				}
			}
			else {
				
				// No se tiene otra funcionalidad con el Drag & Drop
			}
			/*
			
			service.getFlashIslandService().orderOnHold(
				
				GanttTaskRendererHelper.getOrderOnHoldRequest(
					event.task.payload
				)
			);*/
			
		}
	}
}