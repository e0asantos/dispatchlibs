package com.cemex.rms.common.transformer
{
	import com.cemex.rms.common.gantt.GanttTask;
	import com.cemex.rms.common.tree.NodeTask;
	import com.cemex.rms.common.utils.DictionaryMap;
	import com.cemex.rms.common.gantt.GanttTree;
	import ilog.gantt.GanttDataGrid;
	import ilog.gantt.GanttSheet;
	
	import mx.collections.ArrayCollection;
	import mx.collections.Sort;

	public interface IDataTransformer {
		
		function getTask(targetRef:Object):GanttTask;
		
		function addData(data:Object,step:String,date:Date):void;
		//function addDataNodeInLine(node:NodeTask):void;
		
		function addDatas(datas:ArrayCollection,step:String,date:Date):void;
		
		//function initRoots(datas:ArrayCollection):void;
		function initHeaders(datas:ArrayCollection,reports:DictionaryMap):void;
		function addHeaders(datas:ArrayCollection):void;
		function addHeader(data:Object):void;
		
		//function regenerate():void;
		
		function timeChanged(time:Date):void;
		
		function getViewID():String;
		
		//function updateAll():void;
		//function checkTimedValues():void;
		//function refresh():void;
		
		function getResourcesTree():ArrayCollection;
		function getTasksTree():ArrayCollection;
		function setFilters(filters:ArrayCollection):void;
		
		function getTree():GanttTree;
		
		function setColumns(dataGrid:GanttDataGrid,ganttSheet:GanttSheet):void;
		function getSorter():Sort;
		function sortView():void;
		function getSorterFunction(field:String):Function;
		
/* 		function getStartTime(payload:Object):Date;
		function getEndTime(startTime:Date,payload:Object):Date;
		
		function getColor(payload:Object):Number;
		function getBorder(payload:Object):Number;
		function getThickness(payload:Object):Number;
		
	 */
		
	}
}