package com.cemex.rms.common.transformer
{
	import com.cemex.rms.common.gantt.GanttResource;
	import com.cemex.rms.common.gantt.GanttTask;
	import com.cemex.rms.common.gantt.GanttTree;
	import com.cemex.rms.common.logging.ILogger;
	import com.cemex.rms.common.logging.LoggerFactory;
	import com.cemex.rms.common.utils.DictionaryMap;
	import com.cemex.rms.dispatcher.constants.ExtrasConstants;
	import com.cemex.rms.dispatcher.services.flashislands.impl.DispatcherIslandImpl;
	import com.cemex.rms.dispatcher.services.flashislands.vo.Equipment;
	import com.cemex.rms.dispatcher.views.assignedLoads.transformers.AssignedLoadsDataTransformer;
	import com.cemex.rms.dispatcher.views.loadsPerOrder.transformers.LoadsPerOrderAbstractDataTransformer;
	import com.cemex.rms.dispatcher.views.loadsPerOrder.tree.LoadsPerOrderResource;
	import com.cemex.rms.dispatcher.views.loadsPerVehicle.transformers.LoadPerVehiclePlantVehicleDataTransformer;
	import com.cemex.rms.dispatcher.views.loadsPerVehicle.tree.LoadsPerVehicleResource;
	import com.cemex.rms.dispatcher.views.mediators.DispatcherViewMediator;
	
	import mx.collections.ArrayCollection;
	import mx.collections.Sort;
	import mx.collections.SortField;
	import mx.controls.AdvancedDataGrid;
	import mx.controls.Alert;
	import mx.controls.advancedDataGridClasses.AdvancedDataGridColumn;
	import mx.core.IFactory;
	import mx.events.AdvancedDataGridEvent;
	import mx.events.DataGridEvent;
	import mx.utils.ObjectUtil;
	
	import ilog.gantt.GanttDataGrid;
	import ilog.gantt.GanttSheet;

	public class GenericDataTransformer  implements IDataTransformer
	{
		protected var tree:GanttTree;
		
		protected var fields:ArrayCollection;
		protected var fieldColumns:ArrayCollection;
		
		
		public function GenericDataTransformer() {
			super();	
		}
		public function getViewID():String{
			throw new Error("The viewID should be Overriden");
		}
		
		public var lastTimeChanged:Number = 0;
		/**
		 * 
		 * 
		 */
		private var logger:ILogger = LoggerFactory.getLogger("GenericTransformer");
		public function timeChanged(time:Date):void {
			var timechange:Number = time.getTime();
			//if ( (timechange - lastTimeChanged) > 5000){
				//Estoy tratando de hacer que no refresque tan rapido
				
				if(DispatcherViewMediator.isDragging || (timechange - lastTimeChanged) > 5000 || DispatcherViewMediator.isUsingPush){
					logger.debug("timeChanged:Start");
					tree.timeChanged(time);
					logger.debug("timeChanged:END");
					lastTimeChanged = timechange;
					DispatcherViewMediator.isUsingPush=false;
				}
			//}
		}
		
		public function getTree():GanttTree{
			return tree;
		}
		
		protected function getColumn(field:FieldColumn):AdvancedDataGridColumn{
			
			var column:AdvancedDataGridColumn =  new AdvancedDataGridColumn();
			column.width = field.width;
			column.dataField = field.name;
			
			column.headerText = field.text;
			//column.sortCompareFunction = getSorterFunction;	
			
			if (field.renderer != null){
				column.itemRenderer = field.renderer;
			}
			if (field.headerRenderer != null){
				column.headerRenderer = field.headerRenderer;
				column.sortable = false;
			}
			return column;
		}
		
		
		public function getSortFunction(obj1:Object,obj2:Object):int {
		//	//return dob_sort;
			return 0;
		}
		
		/*public function dob_sort(itemA:Object, itemB:Object):int {
			//var dateA:Date = itemA.dob ? new Date(itemA.dob) : null;
			//var dateB:Date = itemB.dob ? new Date(itemB.dob) : null;
			//return ObjectUtil.dateCompare(dateA, dateB);
			return ObjectUtil.stringCompare(itemA["label"],itemB["label"]);             
		} 
		*/
		
		
		
		public function setColumns(dataGrid:GanttDataGrid,ganttSheet:GanttSheet):void{
			var arr:Array =  new Array();
			for (var i:int=0 ; i < fieldColumns.length ; i ++  ){
				arr.push(getColumn(fieldColumns.getItemAt(i) as FieldColumn));
			}
			dataGrid.columns = arr;
			ganttSheet.taskItemRenderer =  getTaskRenderer();
			setColumnsDataGridCustomization(dataGrid);
			setTreePlusMinus(dataGrid);
		}
		public  function setTreePlusMinus(dataGrid:GanttDataGrid):void{
			
		
		}
		
		
		
		
		public  function setColumnsDataGridCustomization(dataGrid:GanttDataGrid):void{
			
			dataGrid.styleFunction=null; 
			dataGrid.iconFunction=null;
		}
	
		public function getTaskRenderer():IFactory{
			throw new Error("The method getTaskRendere should be implemented");
		}
		public function setFieldColumns(fieldColumns:ArrayCollection):void{
			this.fieldColumns =  fieldColumns;
			var ad:AdvancedDataGrid;
			
		}
		
		public function setFields(fields:ArrayCollection):void{
			this.fields =  fields;
			tree = new GanttTree(fields);
		}
		public function setFilters(filters:ArrayCollection):void{
			tree.setFilters(filters);
		}
		
		public function getSorter():Sort {
			
			var sort:Sort = new Sort();
			
				
				
					var temp:FieldColumn = fieldColumns.getItemAt(0) as FieldColumn;
					var sorter:SortField = new SortField(temp.name, false,false);
					//var sorter:SortField = new SortField(null, false,false);
					sorter.compareFunction = getSorterFunction(temp.name);
					
					//sorter = new SortField(temp.name, false,false);
					//sorter.compareFunction = sortByMinDate;
					//sortFields.push(sorter);
				
				sort.fields = [sorter];
				
				
				//var fooSort:Sort = new Sort();
				//var sortField:SortField = new SortField(null, true);
				//fooSort.compareFunction = compareFooAssets;
				//fooAssetsADG.dataProvider.sort = fooSort;
				//fooSort.fields = [sortField];
				//fooAssetsADG.dataProvider.refresh();

				
			
			//sort.compareFunction = getSorterFunction(temp.name);
			
			return sort;
		}
		
		public function getSorterFunction(field:String):Function{
			return sortByMinDate;
		}
		
		public function sortByMinDate(a:Object=null, b:Object=null, c:Object=null):int {
			//Alert.show("compare(a:"+a+",b:"+b+")");
			var x:GanttResource = a as GanttResource;
			var y:GanttResource = b as GanttResource;
			if (x != null && y != null){
				//Alert.show("stringCompare(a:"+x.label+":"+FlexDateHelper.getTimestampString(x.getMinStartDate())+" | b:"+y.label+":"+FlexDateHelper.getTimestampString(y.getMinStartDate())+") | c:" + ObjectUtil.dateCompare(x.getMinStartDate(),y.getMinStartDate()));
				var dateCompareResult:int=ObjectUtil.dateCompare(x.getMinStartDate(),y.getMinStartDate());
				if(dateCompareResult==0){
					//regresar la que sea más pequeña
					return ObjectUtil.numericCompare(Number(x.payload["orderNumber"]),Number(y.payload["orderNumber"]));
				}
				return dateCompareResult;
				
			}
			else {
				Alert.show("compare(a:"+a+",b:"+b+")");
				return ObjectUtil.compare(a,b);
			}
		}
		
		
		
		
		//ESte metodo no se va a usar
		private function advancedDataGridDefaultSort(dataGrid:GanttDataGrid, dgColumn:int):void{
			dataGrid.dispatchEvent(new AdvancedDataGridEvent(AdvancedDataGridEvent.SORT, false,true,dgColumn));
		}
		// Este metodo no sale
		private function dataGridDefaultSort(dgName:Object, dgColumn:int):void{ 
			dgName.dispatchEvent(new DataGridEvent(DataGridEvent.HEADER_RELEASE,   false,true,dgColumn,null,0,null,null,0));
		}
		
		
		public function addData(data:Object,step:String,date:Date):void {
			tree.addData(data,step,date,false);
			
		}
				
		public function getTask(targetRef:Object):GanttTask {
			return tree.getTask(targetRef);
		} 
		
		/*
		public function initRoots(datas:ArrayCollection):void{
			tree.reset();
			tree.initRoots(fields,fields.getItemAt(0) as String,datas);
		}

		*/
		/* 
		public function addDataNodeInLine(node:NodeTask):void {
			tree.addDataNodeInLine(node);
		} */
		
		
		public function addDatas(datas:ArrayCollection,step:String,date:Date):void{
			if(DispatcherViewMediator.issales){
				DispatcherViewMediator.issales=false;
			}
			for (var i:int = 0 ; i < datas.length ; i ++) {
				var temp:Object =  datas[i];
				addData(temp,step,date);
			}
			sortView();
		}
		
		/**
		 * this method performs the sort of the tasks inside
		 */
		public function sortView():void{
			var conte:DictionaryMap=tree.ctx;
			if(conte==null){
				return;
			}
			var llaves:ArrayCollection=tree.ctx.getAvailableKeys();
			if(this is LoadsPerOrderAbstractDataTransformer){
				for(var v:int=0;v<llaves.length;v++){
					if(llaves.getItemAt(v).toString().split(":").length==1 && llaves.getItemAt(v).toString()!=""){
						var resourceInit:LoadsPerOrderResource=tree.ctx.get(llaves.getItemAt(v).toString());
						//resourceInit.getMinStartDate()
						//get children and sort them by date
						var innerChildren:ArrayCollection=resourceInit._children;
						if(innerChildren.length>0){
							if(!(innerChildren[0] is LoadsPerOrderResource)){
								continue;
							}
						}
						//Create the sort field
						var dataSortField:SortField = new SortField("minDateNumber",false,false,true);
						
						var dataSort:Sort = new Sort();
						dataSort.fields = [dataSortField];
						innerChildren.sort = dataSort;
						//refresh the collection to sort
						innerChildren.refresh();
						//name of the field of the object on which you wish to sort the Collection
						
						/*for(var r:int=0;r<innerChildren.length;r++){
						//if((innerChildren.getItemAt(r) as LoadsPerOrderResource).getMinStartDate()>
						
						}*/
					}
				}
			} else if(this is LoadPerVehiclePlantVehicleDataTransformer){
				for(var v:int=0;v<llaves.length;v++){
					if(llaves.getItemAt(v).toString().split(":").length==2 && llaves.getItemAt(v).toString()!="" && llaves.getItemAt(v).toString().indexOf(ExtrasConstants.VEHICLE_DISPONIBLE)==-1){
						var resourceInit2:LoadsPerVehicleResource=tree.ctx.get(llaves.getItemAt(v).toString());
						//resourceInit.getMinStartDate()
						//get children and sort them by date
						var innerChildren2:ArrayCollection=resourceInit2._children;
						//Create the sort field
						var dataSortField:SortField = new SortField("minDateNumber",false,false,true);
						
						var dataSort:Sort = new Sort();
						dataSort.fields = [dataSortField];
						innerChildren2.sort = dataSort;
						//refresh the collection to sort
						innerChildren2.refresh();
						//name of the field of the object on which you wish to sort the Collection
						
						/*for(var r:int=0;r<innerChildren.length;r++){
						//if((innerChildren.getItemAt(r) as LoadsPerOrderResource).getMinStartDate()>
						
						}*/
					}
				}
			} else if(this is AssignedLoadsDataTransformer){
				
			}
		}
		
		
		public function initHeaders(datas:ArrayCollection,reports:DictionaryMap):void{
			tree.reset(reports);
			addHeaders(datas);
			//agregar los encabezados para todas las plantas
			if(this is LoadPerVehiclePlantVehicleDataTransformer){
				for(var q:int=0;q<datas.length;q++){
					var planta:String=datas.getItemAt(q)["plant"];
					var mheader:Object=new Object();
					mheader["equipStatus"]=ExtrasConstants.VEHICLE_ASSIGNED;
					mheader["plant"]=planta;
					mheader["plantId"]=planta;
					addHeader(mheader);
					mheader["equipStatus"]=ExtrasConstants.VEHICLE_DISPONIBLE;
					mheader["plant"]=planta;
					mheader["plantId"]=planta;
					addHeader(mheader);
					mheader["equipStatus"]=ExtrasConstants.LOAD_UNASSIGNED;
					mheader["plant"]=planta;
					mheader["plantId"]=planta;
					addHeader(mheader);
				}
			}
		}
		
		public function addHeaders(datas:ArrayCollection):void{
			for (var i:int = 0 ; i < datas.length ; i ++) {
				var temp:Object =  datas[i];
				addHeader(temp);
			}
		}
		public function addHeader(data:Object):void {
			tree.addData(data,null,null,true);
		}
		/*
		public function regenerate():void {
			tree.regenerate();
		}*/
		
		
		public function getResourcesTree():ArrayCollection{
			
			var result:ArrayCollection =  this.tree.getResourcesTree();
			return result;
		}
		public function getTasksTree():ArrayCollection{
			return this.tree.getTasksTree();
		}
		
		public static function compareEquipments(a:Object,b:Object,fields:Array = null):int
		{
			//fnDtfParceFunct is a Date parse function avail in DownloadCode.
			var eq1:Equipment = a as Equipment;
			var eq2:Equipment= b as Equipment;
			if (eq1 == null || eq2 == null){
				if (eq1 == null && eq2 == null){
					return 0;
				}
				else {
					if (eq1 == null){
						return ObjectUtil.compare(null, eq2.turnTimestamp);
					}
					else {
						return ObjectUtil.compare(eq1.turnTimestamp, null);
					}
				}
			}
			
			var comp:int = ObjectUtil.dateCompare(eq1.turnTimestamp, eq2.turnTimestamp);
			
			if(comp == 0){
				comp = ObjectUtil.compare(eq1.equipment,eq2.equipment);
			}
			return comp;
		}
		
	
	}
}