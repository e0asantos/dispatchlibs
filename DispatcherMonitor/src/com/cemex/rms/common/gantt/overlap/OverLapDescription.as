package com.cemex.rms.common.gantt.overlap
{
	public class OverLapDescription
	{
		public function OverLapDescription()
		{
		}
		
		public static const OVERLAP_TYPE_LEFT:String="OverLap-LEFT";
		public static const OVERLAP_TYPE_RIGHT:String="OverLap-RIGHT";
		public static const OVERLAP_TYPE_OFF:String="OverLap-OFF";
		public static const OVERLAP_TYPE_ALL:String="OverLap-ALL";
		public static const OVERLAP_TYPE_CENTER:String="OverLap-CENTER";
		public static const OVERLAP_TYPE_MULTI:String="OverLap-MULTI";
		
		public var overlapType:String=OVERLAP_TYPE_OFF;
		
		
		public var points:Array =  new Array();
		
	}
}