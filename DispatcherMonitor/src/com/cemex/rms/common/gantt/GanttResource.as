package com.cemex.rms.common.gantt
{
	import com.cemex.rms.common.tree.NodeResource;

	public dynamic class GanttResource extends NodeResource
	{
		public function GanttResource() {
			super();
		}
		
		
		public function get payload():Object{
			return data ;
		}
		public function set payload(_payload:Object):void{
			data = _payload;
		}
		
		
		public function getLabelFromField(field:String):String {
			if (field != null ){
				if (this[field] != null && (""+this[field]) != ""){
					if(field=="orderNumber"){
						return this[field] + "("+totalChildrenCount+")";
					} else {
						return this[field];
					}
				}
				else {
					return " ";
				}
				
			}
			return null;
		}
		
		
		public function get label():String {
			if (getLabelField() != null ){
				
				var result:String = getLabelFromField(getLabelField());
				if (result != null){
					return result;
				}
			}
			return id;
		}
		public function get labelVisible():String {
			if (hasVisibleTasks){
				return "";
			}
			else {
				return this.label;	
			}
		}
		
		
		
	}
}