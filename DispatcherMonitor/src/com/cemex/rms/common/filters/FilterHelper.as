package com.cemex.rms.common.filters
{
	
	import com.adobe.fiber.runtime.lib.DateTimeFunc;
	import com.adobe.fiber.runtime.lib.StringFunc;
	import com.cemex.rms.common.flashislands.vo.TVARVC;
	import com.cemex.rms.common.reflection.ReflectionHelper;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.utils.ObjectUtil;
	/**
	 * Esta clase helper nos ayuda a validar si se cumple con los filtrados, 
	 * nos ayuda a armar los objetos de condiciones 
	 * 
	 */
	public class FilterHelper
	{
		public function FilterHelper()
		{
		}
		
		/**
		 * Este metodo nos ayuda a armar la condicion de flitrado y nos regresa el objeto armado
		 */
		public static function getFilterCondition(field:String,values:ArrayCollection,action:String):FilterCondition{
			
			var filter:FilterCondition = new FilterCondition();
			filter.field = field;
			filter.values = values;
			filter.action = action;
			return filter;
		}
		
		
		/**
		 * recibe un arreglo de valores pero que los objetos de valores son del tipo TVARVC
		 * y se arma la condicion de filtrado.
		 * 
		 */
		public static function getFilterConditionTVARVC(field:String,values:ArrayCollection,action:String):FilterCondition{
			
			return getFilterCondition(field,getValuesArrayFromTVARVCField(values),action);
		}
		
		/**
		 * Esta funcion lo que hace es convertir un arreglo de valores, de tipo TVARVC
		 * en un arreglo de strings para meterlos en una condicion
		 * 
		 */
		protected static function getValuesArrayFromTVARVCField(tvarc:ArrayCollection,field:String="low"):ArrayCollection {
			
			var result:ArrayCollection =  new ArrayCollection();
			for (var i:int = 0 ; i < tvarc.length ; i ++ ) {
				var temp:TVARVC = tvarc.getItemAt(i) as TVARVC;
				result.addItem(temp[field]);
			}
			return result;
		}
		
		/**
		 * Esta funcion lo que hace es revisar si un objeto cumple con los filtros que se paran como parametro, 
		 * el ultimo parametro es un arreglo en el cual se registran los resultados
		 */
		public static function matchFilters(target:Object,filters:ArrayCollection,info:ArrayCollection=null):Boolean {
			
			//Alert.show("shouldRenderTask:"+ReflectionHelper.object2XML(target,"target")+"\n"+ ReflectionHelper.object2XML(filters,"filters"));
			
			if (filters != null && target != null){
				for (var i:int = 0 ;  i < filters.length ; i ++){
					var filter:FilterCondition = filters.getItemAt(i) as FilterCondition;
					var valida:Boolean = false;
					if (target.hasOwnProperty(filter.field)) {
						for (var j:int = 0 ; j < filter.values.length ; j++) {
							
							var filterValue:Object = filter.values.getItemAt(j);
							var targetValue:Object = target[filter.field];
							
							
							
							var compareResult:int = -999;
							
							var temp:String = "x";
							/*if ((""+filterValue)== (""+targetValue)){
							compareResult == 0 ;
							}
							else*/ 
							if (filterValue is Date && targetValue is Date){
								var filterDate:Date = filterValue as Date;
								var targetDate:Date = targetValue as Date;
								compareResult = DateTimeFunc.dateCompare(filterDate,targetDate);
								temp="y";
							}
							else if (filterValue is String && targetValue is String) {
								compareResult = StringFunc.compare(filterValue as String,targetValue as String);
								temp="z";
							}
							else {						
								compareResult = ObjectUtil.compare(filterValue,targetValue);
								temp="a";
							}
							
							//Alert.show(temp+"--"+"act:"+filter.action + "..compareResult"+compareResult +"(\n"+  targetValue+ ",\n" +filterValue+")");
							if ( 
								compareResult != -999  && 
								((filter.action == FilterCondition.EQUALS && compareResult == 0)
									|| (filter.action == FilterCondition.NOT_EQUALS && compareResult != 0)
									|| (filter.action == FilterCondition.GREATER_THAN && compareResult < 0) 
									|| (filter.action == FilterCondition.LOWER_THAN && compareResult > 0)
								) 
							){
								
								valida = true;
								break;
							}
							//else {
							//	info.addItem("compareResult:"+compareResult + "->" + filter.action);
							//}
						}
					}
					if (!valida){
						if (info != null){
							info.addItem("The field["+filter.field+"] in the target with value["+target[filter.field]+"] didn't match the Filters"
								+ReflectionHelper.object2XML(filter,"filter")
							);
						}
						return false;
					}
				}
			}
			return true;
		}
		
	}
}