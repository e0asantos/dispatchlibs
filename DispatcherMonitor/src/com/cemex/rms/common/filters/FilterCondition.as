package com.cemex.rms.common.filters
{
	import mx.collections.ArrayCollection;

	/**
	 * Esta clase son las condiciones de Filtrado que se confguran para validar si 
	 * se cumple con alguna condicion
	 * 
	 */
	public class FilterCondition
	{
		public function FilterCondition()
		{
			this.values =  new ArrayCollection();
		}
		public static const EQUALS:String = "EQ";
		public static const GREATER_THAN:String = "GT";
		public static const LOWER_THAN:String = "LT";
		public static const NOT_EQUALS:String = "NE";
		
		
		public var values:ArrayCollection;
		public var field:String;
		public var action:String ;
		
	}
}