package com.cemex.rms.common.tree
{
	import ilog.gantt.TaskItem;
	
	import mx.collections.ArrayCollection;
	import mx.core.UIComponent;
	
	//import flash.events.EventDispatcher;
	/**
	 * Esta clase tiene los datos minimos para que se haga el render de una tarea 
	 * trae la posicion un dato , y un arreglo de los containers, esta clase va de la mano de AbstractTree
	 * Cualquier implementacion de AbstractTree debe decir que implementacion de NodeTask va a utilizar 
	 * 
	 * Esta es como una clase abstracta
	 */
	public class NodeTask  extends GenericNode
	{
		public function NodeTask()
		{
			super();
		}
		
		
		private var _resourceId:String;
		private var _step:String;

		private var _container:ArrayCollection;
		private var _allTasks:ArrayCollection;
		
		
		public function setContainer(_container:ArrayCollection):void{
			this._container = _container;
		}
		public function getContainer():ArrayCollection{
			return this._container;
		}
		
		
		public function setAllTasks(_allTasks:ArrayCollection):void{
			this._allTasks = _allTasks;
		}
		public function getAllTasks():ArrayCollection{
			return this._allTasks;
		}
		
		public function get step():String{
			return _step;
		}
		public function set step(_step:String):void{
			this._step = _step;
		}
		
		
		public function canRenderField(field:String):Boolean{
			return false;
		}
		
		
		
		
		private var _idFields:ArrayCollection;
		public function setIdFields(idFields:ArrayCollection):void{
			this._idFields = _idFields;
		}
		
		public function get resourceId():String{
			return _resourceId;
		}
		public function set resourceId(_resourceId:String):void{
			this._resourceId = _resourceId;
		}
		public function get resourcePosition():int{
			if (_container == null){
				return -1;
			}
			return _container.getItemIndex(this) + 1;
		}
		public function set resourcePosition(x:int):void{
		}
		public function set resourceTaskCount(x:int):void{
		}
		
		
		public function get resourceTaskCount():int{
			if (_container == null){
				return -1;
			}
			return _container.length;
		}
		
		public function get startTime():Date{
			if (_data == null){
				return null;
			}
			return _data.startTime;
		}
		public function set startTime(_startTime:Date):void{
			if (_data != null){
				_data.startTime = _startTime;
			}
		}
		public function get endTime():Date{
			if (_data == null){
				return null;
			}
			return _data.endTime;
		}
		public function set endTime(_endTime:Date):void{
			if (_data != null){
				_data.endTime = _endTime;
			}
		}
		
		
		
		public function getRawStartTime():Date{
			return _data.startTime;
		}
		public function getRawEndTime():Date{
			return _data.endTime;
		}
		
		
	}
}