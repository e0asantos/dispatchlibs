package test.time
{
	import com.adobe.fiber.runtime.lib.DateTimeFunc;

	public class TimelineConverter
	{
		public function TimelineConverter()
		{	
		}
		
		
		public function setStartDate(_start:Date):void{
			this.start = _start;
			if (timeIncrement != null){
			//	this.roundedStartDate = floorDate(_start,timeIncrement);
			}
		}
		
		public function setTimeIncrement(_timeIncrement:TimeIncrement):void{
			this.timeIncrement = _timeIncrement;
			if (start != null){
			//	this.roundedStartDate = floorDate(start,timeIncrement);
			}
		}
		
	
		
		//public var roundedStartDate:Date;
		//private var width:Number;
		private var start:Date;
		public var timeIncrement:TimeIncrement;
		
		public function getNextDate(date:Date):Date {
			return DateTimeFunc.dateAdd(timeIncrement.incremetUnits,timeIncrement.incremetDelta,date);
		}
		public function getPastDate(date:Date):Date {
			return DateTimeFunc.dateAdd(timeIncrement.incremetUnits,-timeIncrement.incremetDelta,date);
		}
		
		
		public static function floorDate(date:Date,timeIncrement:TimeIncrement ):Date {
			
			var increment:Number = timeIncrement.incremetDelta;
			var incrementUnits:String = timeIncrement.incremetUnits;
			
			var result:Date = FlexDateHelper.copyDate(date);
			if (TimeConstants.REFERENCE_MINUTE == incrementUnits){
				if (increment == 1){
					result.setSeconds(0);
				}
				else {
					result.setMinutes(floorTo(result.getMinutes(),increment), 0);
				}
			}
			else if (TimeConstants.REFERENCE_HOUR == incrementUnits){
				if (increment == 1){
					result.setMinutes(0, 0);
				}
				else {
					result.setHours(floorTo(result.getHours(), increment),0, 0);
				}
			}
			else if (TimeConstants.REFERENCE_DAY == incrementUnits){
				if (increment == 1){
					result.setHours(0, 0, 0);
				}
				else {
					//result.setHours(floorTo(result.getHours(), increment),0, 0);
				}
			}
			return result;
		}
		
		
		public static function floorTo(value:Number, to:Number):Number{
			return Math.floor(value / to) * to;
		}
		
		public function getPositionFromDate(date:Date):Number {
			var interval:Number = date.getTime() - start.getTime();
			var proportion :Number = interval / timeIncrement.deltaTime;
			
			return timeIncrement.deltaWidth * proportion ;
		} 	
		public function getDateFromPosition(pos:Number):Date {
			var proportion:Number = (pos * timeIncrement.incremetDelta) / timeIncrement.deltaWidth;
			var date:Date = DateTimeFunc.dateAdd(timeIncrement.incremetUnits,proportion,start);
			return date;
		}
		
		/*
		public function isVisible(date:Date):Boolean{
			var pos:Number = getPositionFromDate(date);
			return pos >= 0 && pos <= width;
		}
		*/
	}
}