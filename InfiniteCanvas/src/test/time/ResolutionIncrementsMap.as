package test.time
{
	import com.adobe.fiber.runtime.lib.DateTimeFunc;
	
	import flash.utils.Dictionary;
	
	import mx.controls.Alert;

	public class ResolutionIncrementsMap
	{
		public function ResolutionIncrementsMap()
		{
			map =  new Dictionary();
			map[TimeConstants.RESOLUTION_MINUTES_5] = getMinutes5Increment();
			map[TimeConstants.RESOLUTION_MINUTES_15] = getMinutes15Increment();
			map[TimeConstants.RESOLUTION_MINUTES_30] = getMinutes30Increment();
			map[TimeConstants.RESOLUTION_HOURS] = getHourIncrement();
			map[TimeConstants.RESOLUTION_HOURS_4] = getHour4Increment();
			map[TimeConstants.RESOLUTION_HOURS_12] = getHour12Increment();
			map[TimeConstants.RESOLUTION_DAY] = getDayIncrement();		
			
		}
		
		public function getIncrement(id:Number):TimeBarIncrement{
			
			var inc:TimeBarIncrement = map[id] as TimeBarIncrement;
			
			if (inc == null){
				inc = map[TimeConstants.RESOLUTION_DAY] as TimeBarIncrement;
			}
			return inc;
		}
		
		
		private var map:Dictionary;
		
		private function getIncrementElement(minWidth:Number ,formatter:String ,incrementDelta:Number, incrementUnits:String,times:Number):TimeIncrement {
			
			var result:TimeIncrement = new TimeIncrement();
			result.deltaWidth = minWidth;
			result.incremetDelta = incrementDelta;
			result.incremetUnits = incrementUnits;
			result.times = times;
			result.formatter = formatter;
			
				
			var now:Date = new Date();
			var nextStep:Date = DateTimeFunc.dateAdd(incrementUnits,incrementDelta,now);
			result.deltaTime = nextStep.getTime() - now.getTime();
			
			return result;
		}
		
		
		public function floorTo(value:Number, to:Number):Number{
			return Math.floor(value / to) * to;
		}
		
		
		
		
		public function getMinutes5Increment():TimeBarIncrement{
			
			var increment:TimeBarIncrement = new TimeBarIncrement();
			increment.lower  = getIncrementElement(50	,"NN:SS", 1	,TimeConstants.REFERENCE_MINUTE,1);
			increment.higher = getIncrementElement(250	,"YYYY/MM/DD JJ:NN", 5	,TimeConstants.REFERENCE_MINUTE,5);
			increment.name ="getMinutes5Increment";
			//increment.lowerPast.setSeconds (0);
			//increment.higherPast.setMinutes(floorTo(increment.start.getMinutes(), 5), 0);
			return increment;
		}
		
		
		public function getMinutes15Increment():TimeBarIncrement{
			var increment:TimeBarIncrement = new TimeBarIncrement();
			increment.lower  = getIncrementElement(30	 ,"NN'", 1		,TimeConstants.REFERENCE_MINUTE,1);
			increment.higher = getIncrementElement(30*15,"YYYY/MM/DD JJ:NN", 15		,TimeConstants.REFERENCE_MINUTE ,15);
			increment.name ="getMinutes15Increment";
			//increment.lowerPast.setSeconds (0);
			//increment.higherPast.setMinutes(floorTo(increment.start.getMinutes(), 15), 0);
			return increment;
		}
		
		public function getMinutes30Increment():TimeBarIncrement{
			var increment:TimeBarIncrement = new TimeBarIncrement();
			increment.lower  = getIncrementElement(50	,"NN:SS", 5			,TimeConstants.REFERENCE_MINUTE,1);
			increment.higher = getIncrementElement(300	,"YYYY/MM/DD JJ", 30		,TimeConstants.REFERENCE_MINUTE ,6);
			increment.name ="getMinutes30Increment";
			//increment.lowerPast.setMinutes (floorTo(increment.start.getMinutes(),  5), 0);
			//increment.higherPast.setMinutes(floorTo(increment.start.getMinutes(), 30), 0);
			return increment;
		}
		
		public function getHourIncrement():TimeBarIncrement{
			var increment:TimeBarIncrement = new TimeBarIncrement();
			increment.lower  = getIncrementElement(50	,"NN:SS", 15		,TimeConstants.REFERENCE_MINUTE,1);
			increment.higher = getIncrementElement(200	,"YYYY/MM/DD JJ",  1		,TimeConstants.REFERENCE_HOUR ,4);
			increment.name ="getHourIncrement";
			//increment.lowerPast.setMinutes (floorTo(increment.start.getMinutes(), 15), 0);
			//increment.higherPast.setMinutes(0, 0);
			return increment;
		}
		
		
		
		
		public function getHour4Increment():TimeBarIncrement{
			var increment:TimeBarIncrement = new TimeBarIncrement();
			increment.lower  = getIncrementElement(30	,"NN'", 15		,TimeConstants.REFERENCE_MINUTE,1);
			increment.higher = getIncrementElement(120	,"YYYY/MM/DD JJ",  1		,TimeConstants.REFERENCE_HOUR ,4);
			increment.name ="getHour4Increment";
			//increment.lowerPast.setMinutes (floorTo(increment.start.getMinutes(), 15), 0);
			//increment.higherPast.setHours(floorTo(increment.start.getHours(), 4),0, 0);
			return increment;
		}
		public function getHour12Increment():TimeBarIncrement{
			var increment:TimeBarIncrement = new TimeBarIncrement();
			increment.lower  = getIncrementElement(30		,"JJ",  1		,TimeConstants.REFERENCE_HOUR,1);
			increment.higher = getIncrementElement(30*12	,"YYYY/MM/DD JJ",  12		,TimeConstants.REFERENCE_HOUR ,12);
			increment.name ="getHour12Increment";
			//increment.lowerPast.setMinutes(0 , 0);
			//increment.higherPast.setHours(floorTo(increment.start.getHours(), 12), 0, 0);
			return increment;
		}
		public function getDayIncrement():TimeBarIncrement{
			var increment:TimeBarIncrement = new TimeBarIncrement();
			increment.lower  = getIncrementElement(25		,"JJ",  1		,TimeConstants.REFERENCE_HOUR,1);
			increment.higher = getIncrementElement(25*24	,"YYYY/MM/DD",  1		,TimeConstants.REFERENCE_DAY ,24);
			increment.name ="getDayIncrement";
			//increment.lowerPast.setMinutes(0 , 0);
			//increment.higherPast.setHours(0, 0, 0);
			return increment;
		}
		
		
	
		
	}
}