package test.time
{
	public class HeaderChild
	{
		public function HeaderChild()
		{
		}
		
		[Bindable]
		public var text:String;
		
		[Bindable]
		public var width:Number;
		
		[Bindable]
		public var x:Number;
		
		[Bindable]
		public var date:Date;
	}
}