package test.time
{
	import com.adobe.fiber.runtime.lib.DateTimeFunc;
	
	import flash.events.MouseEvent;
	
	import mx.binding.utils.BindingUtils;
	import mx.collections.ArrayCollection;
	import mx.containers.Canvas;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.Label;
	import mx.core.ScrollPolicy;
	import mx.core.UIComponent;
	import mx.events.FlexEvent;
	import mx.events.ResizeEvent;
	
	public class Timeline extends Canvas
	{
		
		private var lowerHeaderPool:ArrayCollection;
		private var higherHeaderPool:ArrayCollection;
		public function Timeline()
		{
			super();
			
			horizontalScrollPolicy = ScrollPolicy.OFF;
			verticalScrollPolicy = ScrollPolicy.OFF;
			clipContent = true;
			
			setStyle("borderColor",0xff00ff);
			setStyle("borderThickness",2);
			setStyle("borderStyle","inset");
			
			initHeaderPool();
		}
		
		
		public function initHeaderPool():void{
			
			var now:Date =  new Date();
			converterLower = new TimelineConverter();
			converterHigher = new TimelineConverter();
			
			setZoomLevel(1);
			//temp = converter.getDateFromPosition(currentWidth);
			//temp = TimelineConverter.floorDate(temp,converter.timeIncrement);
		
			//this.roundedStartDate = floorDate(_start,timeIncrement);
			
		}
	
		
		override protected function createChildren():void
		{
			super.createChildren();
			
			lowerHeaderPool =  new ArrayCollection();
			higherHeaderPool =  new ArrayCollection();
			
			var now:Date =  new Date();
			converterLower.setStartDate(now);
			converterHigher.setStartDate(now);
			
			
			var lowDate:Date = TimelineConverter.floorDate(now, converterLower.timeIncrement);
			var highDate:Date = TimelineConverter.floorDate(now, converterHigher.timeIncrement);
			
			lowerHeaderPool.addItem(createHeaderChild(lowDate,converterLower));
			higherHeaderPool.addItem(createHeaderChild(highDate,converterHigher));
		}
		
		public function setDeltaScrolling(delta:Number, commit:Boolean=false):void {
			
			
			moveIfNotVisible(lowerHeaderPool,delta,converterLower,commit);
			moveIfNotVisible(higherHeaderPool,delta,converterHigher,commit);	
			
			
			
		}
		
		/*
		public function getPositionFromDate(date:Date):Number {
			converterLower.
			var interval:Number = date.getTime() - start.getTime();
			var proportion :Number = interval / timeIncrement.deltaTime;
			
			return timeIncrement.deltaWidth * proportion ;
		} 	
		public function getDateFromPosition(pos:Number):Date {
			var proportion:Number = (pos * timeIncrement.incremetDelta) / timeIncrement.deltaWidth;
			var date:Date = DateTimeFunc.dateAdd(timeIncrement.incremetUnits,proportion,start);
			return date;
		}
		
		*/
		
		public function moveIfNotVisible(headerPool:ArrayCollection,delta:Number, converter:TimelineConverter, commit:Boolean=false):void{
			
			
			
			
			
			var addLeft:ArrayCollection =  new ArrayCollection();
			var addRight:ArrayCollection =  new ArrayCollection();
			
			
			
			if (headerPool == null || headerPool.length  == 0 ){
				return;
			}
			
			
			var firstButon:HeaderButton = headerPool.getItemAt(0) as HeaderButton;
			
			if (headerPool.length < 2){
				firstButon.x += delta;		
				return;
			}
			var lastButon:HeaderButton = headerPool.getItemAt(headerPool.length - 1) as HeaderButton;
			
			var tempPos:Number ;
		
			
					
			if (delta < 0){
				//scroll a la izquierda 
				//Entonces se necesita llenar el lado derecho				
				tempPos = firstButon.x + delta + firstButon.width;
				if (tempPos < 0 ){
					// Ya no es visible
					headerPool.removeItemAt(0);
					
					firstButon.date = converter.getNextDate(lastButon.date);
					firstButon.text = FlexDateHelper.getDateString(firstButon.date,converter.timeIncrement.formatter);
					firstButon.x = lastButon.x + lastButon.width;
							
					headerPool.addItem(firstButon);
				}
			}
			else if (delta > 0){
				//scroll a la derecha 
				//entonces se necesita llenar el lado izquierdo
				
				tempPos = lastButon.x + delta;
				
				if (currentWidth < tempPos){
					headerPool.removeItemAt(headerPool.length - 1);
					var firstDate:Date = converter.getPastDate(firstButon.date);
					updateHeaderChild(firstDate,converter,lastButon);
					
					lastButon.date = converter.getPastDate(firstButon.date);
					lastButon.text = FlexDateHelper.getDateString(lastButon.date,converter.timeIncrement.formatter);
					lastButon.x = firstButon.x - firstButon.width;
					headerPool.addItemAt(lastButon,0);
					
				}
			}
			
			// Se actualizan todos los Botones
			for (var i :int = 0 ; i < headerPool.length  ; i ++){
				var boton:HeaderButton = headerPool.getItemAt(i ) as HeaderButton;
				boton.x += delta;
			}
		}
		
		
		
		
		public var currentWidth:Number = -1;
		
		public function setVisibleWidth(w:Number):void{
			if( w > 0&& w != currentWidth){
				
				
				if (currentWidth < w){
				
					updateHeaderWidth(w,converterHigher,higherHeaderPool,0);
					updateHeaderWidth(w,converterLower,lowerHeaderPool,22);
				}
				currentWidth = w;
			}
		}
		
		/**
		 * ESta funcion condirea que estan en orden todas las fechas
		 */
		private function getLastDate(headerPool:ArrayCollection):Date{
			if (headerPool == null || headerPool.length == 0){
				return null;
			}
			else {
				var header:HeaderButton = headerPool.getItemAt(headerPool.length - 1 ) as HeaderButton;
				return header.date;
			}
		}
		private function updateHeaderWidth(w:Number, converter:TimelineConverter,headerPool:ArrayCollection, y:Number):void{
			
			var temp:Date = converter.getNextDate(getLastDate(headerPool));		
			
			var isLast:Boolean = false;
			while(!isLast) {
				
				var header:HeaderButton = createHeaderChild(temp,converter);
				header.height = 22;
				header.y = y;
				header.addEventListener(MouseEvent.CLICK,headerClick,false,0,true);
				headerPool.addItem(header);
				addChild(header);
				
				isLast = w < header.x;
				temp = converter.getNextDate(temp);
			}
			
		}
		
		override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void
		{
			super.updateDisplayList(unscaledWidth, unscaledHeight);
			setVisibleWidth(unscaledWidth);
		}
		public var initial:Boolean = false;
		
		
		
		
		public function createHeaderChild(temp:Date,converter:TimelineConverter):HeaderButton {
			
			var header:HeaderButton =  new HeaderButton();
			header.date = temp;
			header.text = FlexDateHelper.getDateString(temp,converter.timeIncrement.formatter);
			header.width = converter.timeIncrement.deltaWidth;
			header.x = converter.getPositionFromDate(header.date); 
			return header;
		}
		
		public function updateHeaderChild(temp:Date,converter:TimelineConverter, header:HeaderButton):void {
			
			
			header.date = temp;
			header.text = FlexDateHelper.getDateString(temp,converter.timeIncrement.formatter);
			header.width = converter.timeIncrement.deltaWidth;
		}
		
		
		
		
		public function headerClick(e:MouseEvent):void{
			
		}
		
		
		
		public static const LOWER_HEADER_POOL:Number = 20;
		public static const HIGHER_HEADER_POOL:Number = 20;
		
		
		
		// Son las funcciones que 
		
		
		private var _visibleStartDate:Date;
		private var _visibleEndDate:Date;
		private var _availableStartDate:Date;
		private var _availableEndDate:Date;
		
		
		public function get visibleStartDate():Date{
			return _visibleStartDate;
		}
		public function get visibleEndDate():Date{
			return _visibleEndDate;
		}
		public function get availableStartDate():Date{
			return _availableStartDate;
		}
		public function get availableEndDate():Date{
			return _availableEndDate;
		}
		
		
		
		
		
		public function set availableStartDate(_availableStartDate:Date):void{
			this._availableStartDate=_availableStartDate;
			if (_availableStartDate.getTime() > _visibleStartDate.getTime()){
				visibleStartDate = FlexDateHelper.copyDate(_availableStartDate);
			}
		}
		public function set availableEndDate(_availableEndDate:Date):void{
			
			this._availableEndDate=_availableEndDate;
			if (_availableEndDate.getTime() < _visibleEndDate.getTime()){
				visibleEndDate = FlexDateHelper.copyDate(_availableEndDate);
			}
		}
		
		
		
		
		public function set visibleStartDate(_visibleStartDate:Date):void{
			this._visibleStartDate=_visibleStartDate;
		}
		public function set visibleEndDate(_visibleEndDate:Date):void{
			this._visibleEndDate=_visibleEndDate;
		}
		
		
		
		
		public function setZoomLevel(zoomLevel:Number):void {
			var resolution:Number = mapZoomResultion(zoomLevel);
			var timeBar:TimeBarIncrement = resolutionMap.getIncrement(resolution);
			converterLower.setTimeIncrement(timeBar.lower);
			converterHigher.setTimeIncrement(timeBar.higher);
			
			updateHeaderZoom(currentWidth,converterHigher,higherHeaderPool,0);
			updateHeaderZoom(currentWidth,converterLower,lowerHeaderPool,22);
		}
		
		
		private function updateHeaderZoom(w:Number, converter:TimelineConverter,headerPool:ArrayCollection, y:Number):void{
			
			
			if (w < 1){
				return;
			}
			
			var temp:Date = getLastDate(headerPool);
			var isLast:Boolean = false;
			
			var i:int = 0 ;
			while(!isLast) {
				
				var header:HeaderButton;
				if (i >= headerPool.length){
					header = createHeaderChild(temp,converter);
					header.height = 22;
					header.y = y;
					header.addEventListener(MouseEvent.CLICK,headerClick,false,0,true);
					headerPool.addItem(header);
					addChild(header);
				}
				else {
					header = headerPool.getItemAt(i) as HeaderButton;
					updateHeaderChild(temp,converter,header);
					header.width = converter.timeIncrement.deltaWidth;
					header.x = converter.getPositionFromDate(header.date); 
				}
				isLast = w < header.x;
				temp = converter.getNextDate(temp);
				i++;
			}
			invalidateSize();
			invalidateProperties();
			invalidateDisplayList();
		}
		
		
		private var converterLower:TimelineConverter;
		private var converterHigher:TimelineConverter;
		
		
		private var zoomLevel:int = 0;
		private var resolutionMap:ResolutionIncrementsMap =  new ResolutionIncrementsMap();
		
		//private var currentTimeBarIncrement:TimeBarIncrement;
		
		public function mapZoomResultion(zoom:Number):Number {
			
			
			switch (zoom){
				case 1:
					return TimeConstants.RESOLUTION_MINUTES_5;
				case 2:
					return TimeConstants.RESOLUTION_MINUTES_15;
				case 3:
					return TimeConstants.RESOLUTION_MINUTES_30;
				case 4:
					return TimeConstants.RESOLUTION_HOURS;
				case 5:
					return TimeConstants.RESOLUTION_HOURS_4;
				case 6:
					return TimeConstants.RESOLUTION_HOURS_12;
			}
			return TimeConstants.RESOLUTION_DAY;
		}
		
		private function getResolutionLevel(start:Date,end:Date):Number {
			
			var interval:Number = end.getTime() - start.getTime();
			
			if (interval < TimeConstants.RESOLUTION_MINUTES_5){
				return TimeConstants.RESOLUTION_MINUTES_5;
			}
			else if (interval < TimeConstants.RESOLUTION_MINUTES_15){
				return TimeConstants.RESOLUTION_MINUTES_15;
			}
			else if (interval < TimeConstants.RESOLUTION_MINUTES_30){
				return TimeConstants.RESOLUTION_MINUTES_30;
			}
			else if (interval < TimeConstants.RESOLUTION_HOURS){
				return TimeConstants.RESOLUTION_HOURS;	
			}
			else if (interval < TimeConstants.RESOLUTION_HOURS_4){
				return TimeConstants.RESOLUTION_HOURS_4;
			}
			else if (interval < TimeConstants.RESOLUTION_HOURS_12){
				return TimeConstants.RESOLUTION_HOURS_12;
			}
			else {
				return TimeConstants.RESOLUTION_DAY;
			}
		}
		
		
		public function setZoomDelta(zoomDelta:int):void {
			
			if (zoomDelta > 0 ) {
				if ((zoomDelta + zoomLevel) > TimeConstants.MAX_ZOOM_LEVEL){
					zoomLevel = TimeConstants.MAX_ZOOM_LEVEL;
				}
				else {
					zoomLevel = zoomDelta + zoomLevel;
				}
			}
			else {
				if ((zoomDelta + zoomLevel) < TimeConstants.MIN_ZOOM_LEVEL){
					zoomLevel = TimeConstants.MIN_ZOOM_LEVEL;
				}
				else {
					zoomLevel = zoomDelta + zoomLevel;
				}
			}
		}
		
		
		
		public function getPercentDate(percent:Number):Date {
			var temp:Number = getAvailableInterval() * percent;
			
			var time:Number = temp + _availableStartDate.getTime() ;
			var date:Date = new Date();
			date.setTime(time);
			return date;
		}
		
		public function getVisiblePercent():Number {
			var visible:Number = _visibleEndDate.getTime() - _visibleStartDate.getTime();
			var availableInterval:Number = getAvailableInterval();
			return visible / availableInterval;
		}
		
		public function getAvailableInterval():Number {
			var rangeInterval:Number = _availableEndDate.getTime() - _availableStartDate.getTime();
			return rangeInterval;
		}
		
		
	}
}