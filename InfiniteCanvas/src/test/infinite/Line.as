package test.infinite
{
	public class Line
	{
		public function Line()
		{
		}
		
		public static const LINE_PUNTEADA:String = "LINE_PUNTEADA";
		public static const LINE_SOLID:String = "LINE_SOLID";
		public static const LINE_OTRA:String = "LINE_OTRA";
		
		public var name:String;
		
		public var x:Number;
		public var type:String;
		public var color:Number;
	}
}