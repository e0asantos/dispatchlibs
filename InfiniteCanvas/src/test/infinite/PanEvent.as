package test.infinite
{
	import flash.events.Event;
	
	public class PanEvent extends Event
	{
		
		public static const SCROLL:String = "scrollEvent";
		public static const SCROLL_RELEASE:String = "scrollReleaseEvent";
		public var deltaX:Number;
		public var deltaY:Number;
		public function PanEvent(type:String,deltaX:Number,deltaY:Number, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			this.deltaX = deltaX;
			this.deltaY = deltaY;
		}
	}
}